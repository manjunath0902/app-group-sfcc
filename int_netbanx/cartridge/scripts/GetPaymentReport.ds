/**
* This script retrieves a list of payments made to the API using a GET. You can include an offset and the number of records to return (up to 100).
* Use next and prev links, relative to the offset, to obtain the next/previous records (as applicable). 
*
* @input Num : Number Number of records to return (Max = 100)
* @input Start : Number Record number at which to start
* @output Response : Object Text of response with it's type (json, html or undefined)
*
*/
importPackage( dw.system );
importScript( 'int_netbanx:/lib/libNetbanx.ds' );

function execute( args : PipelineDictionary ) : Number
{
	// Get and configure request parameters
	var num : Number = args.Num;
	var start : Number = args.Start;
	
	// Do not change these values (operation, endpoint), unless you are sure that these changes are necessary
	var operation : String = 'GET';
	var endpoint : String = 'payments';
	
	var reportParameters : Object = {};
	if (!empty(num) && (num > 0) && (num <= 100)) {
		reportParameters.num = num;
	}
	if (!empty(start) && (start > 0)) {
		reportParameters.start = start;
	}
	
	var getPaymentReportResponse : Object = makeNetbanxRequest(operation, endpoint, '', '', reportParameters);

	if (getPaymentReportResponse.contentType != 'error') {
		args.Response = getPaymentReportResponse;
	} else {
		// Error handling
		Logger.error("[" + getPaymentReportResponse.scriptFileName + "] Error while processing transaction GetPaymentReport: " + getPaymentReportResponse.errorText);
		args.Response = getPaymentReportResponse;
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}

