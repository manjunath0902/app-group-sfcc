/**
* This script retrieves a list of orders made to the API using a GET. You can include an offset and the number of records to return (up to 100).
* Use next and prev links, relative to the offset, to obtain the next/previous records (as applicable). 
*
* @input Num : Number Number of records to return (Max = 100)
* @input Start : Number Record number at which to start
*
* @output Response : Object Text of response with it's type (json, html or undefined)
*
*/
importPackage( dw.system );
importScript( 'int_netbanx:/lib/libNetbanx.ds' );

function execute( args : PipelineDictionary ) : Number
{
	// Get and configure request parameters
	var num : Number = args.Num;
	var start : Number = args.Start;
	
	// Do not change these values (operation, endpoint), unless you are sure that these changes are necessary
	var operation : String = 'GET';
	var endpoint : String = 'orders';
	
	
	var reportParameters : Object = {};
	if (!empty(num) && (num > 0) && (num <= 100)) {
		reportParameters.num = num;
	}
	if (!empty(start) && (start > 0)) {
		reportParameters.start = start;
	}
	
	var getOrderReportResponse : Object = makeNetbanxRequest(operation, endpoint, '', '', reportParameters);
	
	if (getOrderReportResponse.contentType != 'error') {
		args.Response = getOrderReportResponse;
	} else {
		// Error handling
		Logger.error("[" + getOrderReportResponse.scriptFileName + "] Error while processing transaction GetOrderReport: " + getOrderReportResponse.errorText);
		args.Response = getOrderReportResponse;
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}

