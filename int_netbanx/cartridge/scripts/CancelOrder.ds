/**
* Cancels an order and returns a response object.
*
* @input OrderId : String Id of order we want to cancel
*
* @output Response : Object Text of response with it's type (json, html or undefined)
*
*/
importPackage( dw.system );
importScript( 'int_netbanx:/lib/libNetbanx.ds' );

function execute( args : PipelineDictionary ) : Number
{
	// Get and configure request parameters
	var orderId : String = args.OrderId;

	// Do not change these values, unless you are sure that these changes are necessary
	var operation : String = 'DELETE';
	var endpoint : String = 'orders';


	var cancelOrderResponse : Object = makeNetbanxRequest(operation, endpoint, orderId);

	if (cancelOrderResponse.contentType != 'error') {
		args.Response = cancelOrderResponse;
	} else {
		// Error handling
		Logger.error("[" + cancelOrderResponse.scriptFileName + "] Error while processing transaction CancelOrder: " + cancelOrderResponse.errorText);
		args.Response = cancelOrderResponse;
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}
