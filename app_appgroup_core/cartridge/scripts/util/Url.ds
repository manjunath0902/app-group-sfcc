'use strict';

var URLAction = require('dw/web/URLAction');
var URLParameter = require('dw/web/URLParameter');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');

/**
 * @description Generate URL for the current request with locale
 * @param {PipelineDictionary} pdict the pipeline dictionary object of current request
 * @param {string} locale the locale
 */
exports.getCurrent = function getCurrent(pdict, locale, incomingSiteId) {
	var currentAction = !empty(pdict.CurrentSession.clickStream.last) && pdict.CurrentSession.clickStream.last != null ? pdict.CurrentSession.clickStream.last.pipelineName : '';
	var currentSiteID = Site.getCurrent().getID();
	if (!locale) {
		locale = 'default';
	}
	var urlAction = new URLAction(currentAction, incomingSiteId, locale);
	var args = [urlAction];
	var parameterMap = pdict.CurrentHttpParameterMap;

	// iterate over current request's parameters, put them into the URL
	for (var p in parameterMap) {
		if (parameterMap.hasOwnProperty(p)) {
			// ignore the lang parameter
			if (p === 'lang') {
				continue;
			}
			args.push(new URLParameter(p, parameterMap[p]));
		}
	}
	
	var currentLocale = pdict.CurrentRequest.locale;
	
	var params = '';
	
	if (!empty(pdict.CurrentRequest.httpQueryString)) {
		params = "?" + pdict.CurrentRequest.httpQueryString;
	}
	
	var updatedUrl = pdict.CurrentRequest.httpProtocol + '://' + pdict.CurrentRequest.httpHost + URLUtils.url.apply(null, args);
	
	//var updatedUrl = pdict.CurrentRequest.httpProtocol + '://' + pdict.CurrentRequest.httpHost + pdict.CurrentRequest.httpPath + params;
		
	//updatedUrl = updatedUrl.replace('Sites-'+currentSiteID+'-Site', 'Sites-'+incomingSiteId+'-Site').replace(currentLocale, locale);
		
	
	/*return pdict.CurrentRequest.httpProtocol + '://' +
		pdict.CurrentRequest.httpHost +
		URLUtils.url.apply(null, args);*/
	return updatedUrl;
		
};

/**
 * @description Generate URL for the current request with locale
 * @param {CurrentRequest} Current request
 * @param {CurrentSession} Current session
 * @param {string} locale the locale
 */
exports.getRedirectURL = function getCurrent(request, session, locale, siteId) {
	var currentAction = session.clickStream.last != null ? session.clickStream.last.pipelineName : '';
	if (!locale) {
		locale = 'default';
	}
	var urlAction = new URLAction(currentAction, siteId, locale);
	var args = [urlAction];
	var parameterMap = request.httpParameterMap;

	// iterate over current request's parameters, put them into the URL
	for (var p in parameterMap) {
		if (parameterMap.hasOwnProperty(p)) {
			// ignore the lang parameter
			if (p === 'lang') {
				continue;
			}
			args.push(new URLParameter(p, parameterMap[p]));
		}
	}

	return request.httpProtocol + '://' +
		request.httpHost +
		URLUtils.url.apply(null, args);
};


exports.getCurrentForSS = function getCurrent(pdict, locale) {
	var currentAction = !empty(pdict.CurrentSession.clickStream.last) && pdict.CurrentSession.clickStream.last != null ? pdict.CurrentSession.clickStream.last.pipelineName : '';
	var siteId = Site.getCurrent().getID();
	if (!locale) {
		locale = 'default';
	}
	var urlAction = new URLAction(currentAction, siteId, locale);
	var args = [urlAction];
	var parameterMap = pdict.CurrentHttpParameterMap;

	// iterate over current request's parameters, put them into the URL
	for (var p in parameterMap) {
		if (parameterMap.hasOwnProperty(p)) {
			// ignore the lang parameter
			if (p === 'lang' || p == 'source' || p == 'format') {
				continue;
			}
			args.push(new URLParameter(p, parameterMap[p]));
		}
	}

	return pdict.CurrentRequest.httpProtocol + '://' +
		pdict.CurrentRequest.httpHost +
		URLUtils.url.apply(null, args);
};

