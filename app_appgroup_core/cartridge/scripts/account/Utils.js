/**
 * This script provides utility functions shared across other account
 * related scripts. Reused script components for account management
 * should be contained here, while this script is imported into the
 * requiring script.
 */

var Logger = require('dw/system/Logger');
var ArrayList = require('dw/util/ArrayList');

/**
 * Determines a unique address ID for an address to be save the given
 * address book. The function first checks the city as the candidate ID
 * or appends a counter to the city (if already used as address ID) and
 * checks the existence of the resulting ID candidate. If the resulting
 * ID is unique this ID is returned, if not the counter is incremented and
 * checked again.
 *
 * @param {String} city
 * @param {dw.customer.AddressBook} addressBook
 * @returns {String}
 */
function determineUniqueAddressID (city, addressBook) {
    var counter = 0;
    var existingAddress = null;

    // check, if attribute "city" is set and has a value
    if (!city) {
        Logger.debug("Cannot determine unique address ID from non existing or not set attribute \"city\".");
        return;
    }

    // initialize the candidate ID
    var candidateID = city;

    while (existingAddress == null) {
        existingAddress = addressBook.getAddress(candidateID);

        if (existingAddress) {
            // this ID is already taken, increment the counter
            // and try the next one
            counter++;
            candidateID = city + "-" + counter;
            existingAddress = null;
        } else {
            return candidateID;
        }
    }
}

/**
 * Returns a possible equivalent address to the given order address from the
 * address book or null, if non equivalent address was found.
 *
 * @param {dw.customer.AddressBook} addressBook
 * @param {dw.order.OrderAddress} orderAddress
 * @returns {dw.customer.CustomerAddress}
 */
function getEquivalentAddress(addressBook, orderAddress) {
    var address;
    var addresses = addressBook.addresses;
    var iter = addresses.iterator();

    while (iter.hasNext()) {
        address = iter.next();
        if (address.isEquivalentAddress(orderAddress)) {
            return address;
        }
    }
}

/**
 * Returns a year JSON object with help of configuration given in custom object
 */
function yearObj(customObjName){
	var yearList = new ArrayList();
	var yearCustomObj : CustomObject = dw.object.CustomObjectMgr.getCustomObject("CustomDropDowns", customObjName);
	if (!empty(yearCustomObj) && !empty(yearCustomObj.getCustom()["DropDownContent"]) && yearCustomObj.getCustom()["DropDownContent"] != null) {
		var fromYear = JSON.parse(yearCustomObj.getCustom()["DropDownContent"]).startingYear;
		var toYear = fromYear + JSON.parse(yearCustomObj.getCustom()["DropDownContent"]).yearCount;
		for(var year=fromYear;year<=toYear;year++){
			var yearObj = {};
			yearObj.optionid = year.toFixed(0);
			yearObj.value		= year.toFixed(0);
			yearObj.label		= year.toFixed(0);
			yearList.add(yearObj);
		}
		var yearObj = {};
		yearObj.value		= "";
		yearObj.label		= "Year";
		yearList.add(yearObj);
			
		return yearList;
	}
	return;
}

/**
 * Returns full month name if month number is passed as a parameter.
 *
 * @param month in number
 * @returns FullMonth
 */

function getFullMonth(monthNumber) {
     var monthArray = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
     return monthArray[monthNumber];
}

module.exports = {
    determineUniqueAddressID: determineUniqueAddressID,
    getEquivalentAddress: getEquivalentAddress,
    yearObj: yearObj,
    getFullMonth: getFullMonth
};
