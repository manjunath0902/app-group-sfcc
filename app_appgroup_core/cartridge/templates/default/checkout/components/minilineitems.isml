<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
    Renders mini lineitems for order summary and mini cart.

    Parameters:
    p_showreverse     	: boolean to render the line items in reverse order
    p_lineitemctnr     	: the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_productlineitem	: in case of mini cart this is the product lineitem just got added/changed so it should be expanded and at the top of the list
    p_giftcertlineitem	: in case of mini cart this is the gift certificate line item just got added/changed so it should be expanded and at the top of the list

</iscomment>
<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('imageTransformationService') == 'DIS'}">
	<isscript>
		var ProductImage = require('app_disbestpractice/cartridge/scripts/product/ProductImageSO');
	</isscript>
</isif>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>

<isif condition="${LineItemCtnr != null}">
	<isscript>
		var pliList : dw.util.Collection = new dw.util.ArrayList(LineItemCtnr.productLineItems);
		if( pdict.p_showreverse )
		{
			// order of items is reverse in case of mini cart display
			pliList.reverse();

			// remove the bonus item from the mini cart display
			var tempList : dw.util.ArrayList = new dw.util.ArrayList();
			
			// add the recently touched/added product line item at the top of the list
			if( pdict.p_productlineitem )
			{
				tempList.add( pdict.p_productlineitem );
			}
			
			for( var ind in pliList )
			{
				var li = pliList[ind];
				
				// skip recently touched/added item, its already added before the loop
				if( empty(pdict.p_productlineitem) || (pdict.p_productlineitem && li.position != pdict.p_productlineitem.position))
				{
					tempList.add( li );
				}
			}
			pliList = tempList;
		}
	</isscript>

	<iscomment>the item count is incremented after each display of a line item</iscomment>
	<isset name="itemCount" value="${1}" scope="page"/>

	<iscomment>render a newly added gift certificate line item at the top</iscomment>
	<isif condition="${pdict.p_giftcertlineitem != null}">
		
		<div class="mini-cart-product">
			<div class="mini-cart-image">				
				<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${pdict.p_giftcertlineitem.lineItemText}"/>" />
			</div>
			<div class="mini-cart-product-details-cont">
				<div class="mini-cart-name">
					<span><isprint value="${pdict.p_giftcertlineitem.lineItemText}"/></span>
				</div>
	
				<div class="mini-cart-pricing">
					${Resource.msg('global.qty','locale',null)}: 1
					<isprint value="${pdict.p_giftcertlineitem.price}"/>
				</div>
					
			</div>			
		</div>
		
		<isset name="itemCount" value="${itemCount+1}" scope="page"/>
		
	</isif>

	<iscomment>product line items (only rendered if we haven't add a gift certificate to the basket)</iscomment>
		<isloop items="${pliList}" var="productLineItem" status="loopstate">
			
			<div class="mini-cart-product <isif condition="${loopstate.last}">last</isif>">
				<iscomment> Commenting since it is out of scope
					<span class="mini-cart-toggle fa"></span>
				</iscomment>
				<div class="mini-cart-image">					
					<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('imageTransformationService') == 'DIS'}">
					    <isset name="imageObjDesktop" value="${new ProductImage('minicartDesktop',productLineItem.product,0)}" scope="page"/>
					    <img src="${imageObjDesktop.getURL()}" alt="${imageObjDesktop.getAlt()}" title="${imageObjDesktop.getTitle()}" class=""/>
					    <iscomment>
							Commintting as APP-G is using one image to load based on break-point 
						    <isset name="imageObjIPad" value="${new ProductImage('minicartIPad',productLineItem.product,0)}" scope="page"/>
						    <isset name="imageObjIPhone" value="${new ProductImage('minicartIPhone',productLineItem.product,0)}" scope="page"/>
						    <img src="${imageObjIPad.getURL()}" alt="${imageObjIPad.getAlt()}" title="${imageObjIPad.getTitle()}" class="ipad-only"/>
						    <img src="${imageObjIPhone.getURL()}" alt="${imageObjIPhone.getAlt()}" title="${imageObjIPhone.getTitle()}" class="iphone-only"/>
					    </iscomment>
					<iselse/>
						<isif condition="${productLineItem.product != null && productLineItem.product.getImage('small',0) != null}">
							<img src="${productLineItem.product.getImage('small',0).getURL()}" alt="${productLineItem.product.getImage('small',0).alt}" title="${productLineItem.product.getImage('small',0).title}"/>
						<iselse/>
							<img src="${URLUtils.staticURL('/images/noimagesmall.png')}" alt="${productLineItem.productName}"  title="${productLineItem.productName}"/>
						</isif>
					</isif>					
				</div>
				<div class="mini-cart-product-details-cont">
					<div class="mini-cart-name">
                        <isset name="itemUrl" value="${empty(productLineItem.categoryID) ? URLUtils.http('Product-Show','pid', productLineItem.productID) : URLUtils.http('Product-Show','pid', productLineItem.productID, 'cgid', productLineItem.categoryID)}" scope="page"/>
                           <iscomment>JIRA PREV-655: User is able to click on bonus product name. Added "productLineItem.isBonusProductLineItem()"</iscomment>
                           <isif condition="${!productLineItem.getProduct().isVariant()}">
                                  <isset name="productName" value="${productLineItem.getProduct().getID()}" scope="page" /> 
                           <iselse/>
                                  <isset name="productName" value="${productLineItem.getProduct().getMasterProduct().getID()}" scope="page" /> 
                           </isif>
                           <isif condition="${productLineItem.isBonusProductLineItem()}">
                                  <div><isprint value="${productName}" /></div>
                                  <div class="mini-desc-name"><isprint value="${productLineItem.productName.substring(productName.length + 1)}" /></div>
                           <iselse/>                        
                               <a href="${itemUrl}" title="${Resource.msgf('product.label','product',null, productLineItem.productName)}"><div><isprint value="${productName}" /></div><div class="mini-desc-name"><isprint value="${productLineItem.productName.substring(productName.length + 1)}" /></div></a>
                           </isif>
                    </div>
					
					<div class="mini-cart-attributes">
						<isdisplayvariationvalues product="${productLineItem.product}"/>
					</div>
		
					<isdisplayproductavailability p_productli="${productLineItem}" p_displayinstock="${false}" p_displaypreorder="${true}" p_displaybackorder="${true}"/>
		
					<div class="mini-cart-pricing">
				
				    <span class="label">${Resource.msg('global.qty','locale',null)}:</span>
				    <span class="value"><isprint value="${productLineItem.quantity}"/></span>

				    <isif condition="${productLineItem.bonusProductLineItem}">
					    <isset name="bonusProductPrice" value="${productLineItem.getAdjustedPrice()}" scope="page"/>
						<isinclude template="checkout/components/displaybonusproductprice" />
						<div class="pricing-section">
							<span class="mini-cart-price-label"> <isprint value="${Resource.msg('global.pricecolon','locale',null)}" /> </span>
						    <span class="minicart-item-bonus mini-cart-price"><isprint value="${bonusProductPriceValue}"/></span>
					    </div>
				    <iselse/>
				    	<isset name="productTotal" value="${productLineItem.adjustedPrice}" scope="page"/>
				    	<isif condition="${productLineItem.optionProductLineItems.size() > 0}">
							<isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
								<isset name="productTotal" value="${productTotal.add(optionLI.adjustedPrice)}" scope="page"/>
							</isloop>
						</isif>
						<div class="pricing-section">
							<span class="mini-cart-price-label"> <isprint value="${Resource.msg('global.pricecolon','locale',null)}" /> </span>
						    <span class="mini-cart-price"><isprint value="${productTotal}"/></span>
					    </div>
				    </isif>
				    <iscomment>Final Sale</iscomment>
				    <isif condition="${!(productLineItem.bonusProductLineItem)}">
						<isif condition="${(!empty(productLineItem.product) && productLineItem.product != null) ? (productLineItem.product.custom.hasOwnProperty('markDownPrice') && productLineItem.product.custom.markDownPrice != null) : false}">
						<isset name="markDownPrice" value="${productLineItem.product.custom.markDownPrice}" scope="page" />
							<isif condition= "${markDownPrice != null && (markDownPrice == 'M1' || markDownPrice == 'M2' || markDownPrice == 'M3' || markDownPrice == 'T1' || markDownPrice == 'T2' || markDownPrice == 'T3') }">
								<div class="final-sale">
									<isprint value="${Resource.msg('product.finalsale','product',null)}"/>
								</div>
							</isif>
						</isif>
					</isif>
					
					</div>
				</div>
			</div>
			
			<isset name="itemCount" value="${itemCount+1}" scope="page"/>
			
		</isloop>	

	<iscomment>gift certificate line items (only rendered if we haven't add a product to the basket)</iscomment>
		<isloop items="${LineItemCtnr.giftCertificateLineItems}" var="gcLI" status="loopstate">
			<iscomment>
				Omit showing the gift certificate that was just added (in case we render the minicart).
				This gift certificate has already been rendered at the top before the product line items.
			</iscomment>
			<isif condition="${!(pdict.p_giftcertlineitem != null && pdict.p_giftcertlineitem.UUID.equals(gcLI.UUID))}">
				
				<div class="mini-cart-product <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">
					
					<div class="mini-cart-image">
						<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${gcLI.lineItemText}"/>" />
					</div>
		
					<div class="mini-cart-name">
						<isprint value="${gcLI.lineItemText}"/>
					</div><!-- END: name -->
		
					<div class="mini-cart-pricing">
						<span class="label">${Resource.msg('global.qty','locale',null)}: 1</span>
						<span class="value"><isprint value="${gcLI.price}"/></span>
					</div>
					
				</div>
				<isset name="itemCount" value="${itemCount+1}" scope="page"/>
			</isif>
		</isloop>	


</isif>