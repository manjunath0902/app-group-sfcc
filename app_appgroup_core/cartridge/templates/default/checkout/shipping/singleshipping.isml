<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType') == 'ONE_PAGE'}">
	<isset name="DecoratorTemplate" value="checkout/pt_checkout" scope="page"/>
	<isif condition="${pdict.CurrentHttpParameterMap.format.stringValue == 'ajax' || pdict.CurrentHttpParameterMap.login.value == 'true'}">
		<isset name="DecoratorTemplate" value="util/pt_empty" scope="page"/>
	</isif>
<iselse/>
	<isset name="DecoratorTemplate" value="checkout/pt_checkout" scope="page"/>
</isif>
<isdecorate template="${DecoratorTemplate}">

<isinclude template="util/modules"/>

<isset name="checkoutpagename" value="shipping" scope="page" />

<isif condition="${pdict.CurrentCustomer.isRegistered()}">
	<div class="checkoutheaderlink">
		<a class="shippingtocart" href="${URLUtils.http('Cart-Show')}"><isprint value="${Resource.msg('checkoutretuntobag','checkout',null)}" encoding="off" /></a>
	</div>
	<div class="secure-checkout">
		<isprint value="${Resource.msg('billing.securecheckout','checkout',null)}" encoding="off" />
		<div class="checkoutcountrytooltip">
			<isslot id="checkoutcountrytip" context="global" description="Replacement of country tooltip with slot" />
		</div>
	</div>
<iselse>
	<div class="checkoutheaderlink guest-user">
		<a class="shippingtocart" href="${URLUtils.http('Cart-Show')}"><isprint value="${Resource.msg('checkoutretuntobag','checkout',null)}" encoding="off" /></a>
	</div>
	<div class="guest-checkout">
		<isprint value="${Resource.msg('billing.guestcheckout','checkout',null)}" encoding="off" />
		<p class="guest-para"><isprint value="${Resource.msg('billing.guestcheckout.label','checkout',null)}" /></p>
		<div class="checkoutcountrytooltip">
			<isslot id="checkoutcountrytip" context="global" description="Replacement of country tooltip with slot" />
		</div>
	</div>
</isif>
<div class="paypalerroe">
	<span class="paypal_shipping_error hide">${Resource.msg('shipping.paypalerror','checkout',null)}</span>
</div>
<iscomment>
	This template visualizes the first step of the single shipping checkout
	scenario. It renders a form for the shipping address and shipping method
	selection. Both are stored at a single shipment only.
</iscomment>

<iscomment>Report this checkout step (we need to report two steps)</iscomment>

<isreportcheckout checkoutstep="${2}" checkoutname="${'ShippingAddress'}"/>
<isreportcheckout checkoutstep="${3}" checkoutname="${'ShippingMethod'}"/>

<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType') == 'ONE_PAGE'}">
	<ischeckouttabs  position="header" checkoutpage="shipping" />
	<div class="spc-shipping">
</isif>

<isscript>
	importScript("app_appgroup_core:/cart/CartUtils.ds");
	var productListAddresses = CartUtils.getAddressList(pdict.Basket, pdict.CurrentCustomer, true);
</isscript>

	<iscomment>checkout progress indicator</iscomment>

	<ischeckoutprogressindicator step="1" rendershipping="${pdict.Basket.productLineItems.size() == 0 ? 'false' : 'true'}"/>

	<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.singleshipping.shippingAddress.htmlName}" class="checkout-shipping address form-horizontal">
	
		<fieldset>
		<isif condition="${pdict.HomeDeliveries}">
				<iscomment>shipping address area</iscomment>

					<legend>
						<span class="shipping-heading-info">${Resource.msg('singleshipping.enteraddress','checkout',null)}</span>
					</legend>

					<iscomment>Entry point for Multi-Shipping (disabled on purpose)</iscomment>
					<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableMultiShipping')}">
						 <isscript>
                            var plicount = 0;

                            for each (var shipment in pdict.Basket.shipments) {
                            	for each (var product in shipment.productLineItems) {
                            		//PREVAIL - JIRA PREV-656 : Not able to proceed with multishipping when the user have bonus products - Added Below condition to exclude normal Bonus products.
                            	    if(product.bonusProductLineItem && empty(product.bonusDiscountLineItem)){
	                                	continue;
	                                }
	                                plicount += product.quantity;
                            	}
                            }
                        </isscript>
						<isif condition="${plicount > 1 }">
							<div class="ship-to-multiple">
								${Resource.msg('singleshipping.multiple','checkout',null)}
								<button class="shiptomultiplebutton button-fancy-medium cancel" type="submit" name="${pdict.CurrentForms.singleshipping.shipToMultiple.htmlName}" value="${Resource.msg('global.yes','locale',null)}">
									${Resource.msg('global.yes','locale',null)}
								</button>
							</div>
						</isif>
					</isif>

					<iscomment>display select box with stored addresses if customer is authenticated and there are saved addresses</iscomment>
					<isif condition="${pdict.CurrentCustomer.authenticated && pdict.CurrentCustomer.profile.addressBook.addresses.size() > 0}">
						<div class="select-address form-row">
							<label for="${pdict.CurrentForms.singleshipping.addressList.htmlName}">
								${Resource.msg('global.selectaddressmessage','locale',null)}
							</label>
							<div class="field-wrapper">
								<iscomment>JIRA PREV-33:Shipping page: Not displaying the selected Address details, in the "Select an Address" drop down. Added p_form attribute</iscomment>
								<isaddressselectlist p_listId="${pdict.CurrentForms.singleshipping.addressList.htmlName}" p_listaddresses="${productListAddresses}" p_form="shipping"/>
							</div>
						</div>
					</isif>

					<isscript>
						var currentCountry = require('app_appgroup_core/cartridge/scripts/util/Countries').getCurrent(pdict);
					</isscript>
					<isdynamicform formobject="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields}" formdata="${currentCountry.dynamicForms.addressDetails}"/>

					<iscomment>Add address to Address Book</iscomment>
					<isif condition="${pdict.CurrentCustomer.authenticated}">
						<isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addToAddressBook}" rowclass="addtoaddress-row" type="checkbox"/>
					</isif>

					<iscomment>Use address for Billing Address</iscomment>
					<isscript>
							var useasbillatt = {
											checked: "checked"
										};
					</isscript>
					<isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress}" rowclass="useasbilling-row" type="checkbox" attributes="${useasbillatt}" />

					<iscomment>Is this a gift, SD-04.07.02.03 : Commenting this since Gift message is out of scope.
						<isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.isGift}" type="radio"/>
						
						<isscript>
							var attributes = {
								rows: 4,
								cols: 10,
								'data-character-limit': 250
							};
						</isscript>
						<isinputfield rowclass="gift-message-text" formfield="${pdict.CurrentForms.singleshipping.shippingAddress.giftMessage}" type="textarea" attributes="${attributes}"/>
					</iscomment>
				</fieldset>

				<div id="shipping-method-list" class="shipping-method-list-container">
					<isinclude url="${URLUtils.https('COShipping-UpdateShippingMethodList')}"/>
				</div>
		</isif>
		<fieldset>

			<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')}">
				<isset name="instoreShipmentsExists" value="${false}" scope="page"/>
				<isinclude template="checkout/shipping/storepickup/instoremessages"/>
			<iselse/>
				<isset name="instoreShipmentsExists" value="${false}" scope="page"/>
			</isif>


			<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp') && instoreShipmentsExists}">
				<div class="form-row form-row-button instore-continue-button continue-button">
			<iselse/>
				<div class="form-row form-row-button continue-button">
			</isif>

				<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType') == 'ONE_PAGE'}">
					<button class="button-fancy-large saveShipping spc-shipping-btn" type="submit" name="${pdict.CurrentForms.singleshipping.shippingAddress.save.htmlName}" value="${Resource.msg('global.continue','locale',null)}"><span>${Resource.msg('shipping.continuebilling','checkout',null)}</span></button>
				<iselse/>
					<button class="button-fancy-large" type="submit" name="${pdict.CurrentForms.singleshipping.shippingAddress.save.htmlName}" value="${Resource.msg('global.continuebilling','locale',null)}"><span>${Resource.msg('global.continuebilling','locale',null)}</span></button>
				</isif>
			</div>

			<iscomment>Entry point for Multi-Shipping (disabled on purpose)</iscomment>
			<isif condition="${pdict.Basket.productLineItems.size() > 1 && false}">
				<div class="ship-to-multiple">
					${Resource.msg('singleshipping.multiple','checkout',null)} <a href="${URLUtils.https('COShippingMultiple-Start')}">${Resource.msg('global.yes','locale',null)}</a>
				</div>
			</isif>

			<input type="hidden" name="${dw.web.CSRFProtection.getTokenName()}" value="${dw.web.CSRFProtection.generateToken()}"/>

		</fieldset>

	</form>
    
<isscript>
	importScript("app_appgroup_core:util/ViewHelpers.ds");
	var addressForm = pdict.CurrentForms.singleshipping.shippingAddress.addressFields;
	var countries = ViewHelpers.getCountriesAndRegions(addressForm);
	var json = JSON.stringify(countries);
</isscript>
<script>window.Countries = <isprint value="${json}" encoding="off"/>;</script>

<isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType') == 'ONE_PAGE'}">
	<ischeckouttabs  position="footer" checkoutpage="shipping"/>
	</div>
</isif>

<iscomment>
     DO NOT REMOVE: PREVAIL - Added below script block in Prevail to maintain fixed page titles.
     Document page title cannot be used as there is a possibility to get that changed.
</iscomment>
<isscript>var pageContextExt = { title: 'shipping information'};</isscript>

</isdecorate>
<script>
var $country =  $('select[name$="_country"]');
if ($country.parent().find('ul li').length <= 1 ) {
	$country.closest('.field-wrapper').find('.custom-select').addClass('country-name');
	$(".country-name .country").attr("readonly", "readonly");
} else {
	$country.closest('.field-wrapper').find('.custom-select').removeClass('country-name');
}
</script>