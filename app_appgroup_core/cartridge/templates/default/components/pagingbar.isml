<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isif condition="${!pdict.pagingmodel.empty}">

<isset name="sitePrefs" value="${dw.system.Site.getCurrent().getPreferences()}" scope="page" />
<isset name="slotPositions" value="${((sitePrefs.getCustom()['PLPSlots'] != null) && (JSON.parse(sitePrefs.getCustom()['PLPSlots']).SlotPositions != null))? JSON.parse(sitePrefs.getCustom()['PLPSlots']).SlotPositions : ''}" scope="page" />
<isset name="slotPositionsArray" value="${slotPositions.split('|')}" scope="page" />

<isscript>
	var current = pdict.pagingmodel.start,
		totalCount = pdict.pagingmodel.count,
		pageSize = pdict.pagingmodel.pageSize,
		pageURL = pdict.pageurl,
		currentPage = pdict.pagingmodel.currentPage,
		maxPage = pdict.pagingmodel.maxPage,
		showingStart = current + 1,
		showingEnd = current + pageSize;

	if (showingEnd > totalCount) {
		showingEnd = totalCount;
	}

	lr = 2; // number of explicit page links to the left and right
	if (maxPage <= 2*lr) {
		rangeBegin = 0;
		rangeEnd = maxPage;
	} else {
		rangeBegin = Math.max(Math.min(currentPage - lr, maxPage - 2 * lr), 0);
		rangeEnd = Math.min( rangeBegin + 2 * lr, maxPage );
	}
</isscript>
<isset name="numberOfPages" value="${pdict.pagingmodel.pageCount}" scope="page" />
<isif condition="${slotPositions.length > 0}"> 
	<isset name="assetCount" value="${0}" scope="page" />
	<isloop items="${slotPositionsArray}" var="position" status="loopstate">
		<isset name="assetID" value="${pdict.ProductSearchResult.category.ID + '-slot-tile-' + (loopstate.index+1).toFixed()}" scope="page" />
		<isif condition="${dw.content.ContentMgr.getContent(assetID) != null}">
			<isset name="assetCount" value="${assetCount + 1}" scope="page" />
		</isif>
	</isloop>
	<isif condition="${assetCount > 0}">
		<isset name="numberOfPages" value="${(pdict.pagingmodel.count+(assetCount))/12}" scope="page" />
		<isif condition="${Math.ceil(numberOfPages) > (maxPage+1)}">
			<isset name="maxPage" value="${numberOfPages}" scope="page" />
			<isset name="rangeEnd" value="${Math.min( rangeBegin + 2 * lr, maxPage )}" scope="page" />
		</isif>
	</isif>
</isif>

<div class="pagination">
	<div class="results-hits">
		<iscomment>Commenting as this is out of scope for AppGroup <isif condition="${pdict.ProductSearchResult && pdict.ProductSearchResult.count > 0}"> <span class="toggle-grid"><i class="fa fa-th fa-lg" data-option="column"></i><i class="fa fa-th-list fa-lg" data-option="wide"></i></span></isif> </iscomment>
		${Resource.msg('global.paginginformation.showing', 'locale', null)}
		<isif condition="${!empty(pdict.OrderPagingModel) || !dw.system.Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll')}">
			<isprint value="${StringUtils.formatInteger(showingStart)}"/> - <isprint value="${StringUtils.formatInteger(showingEnd)}"/> ${Resource.msg('paginginformation.of', 'search', null)}
		</isif>
		<isprint value="${pdict.pagingmodel.count}"/> <span class="hide-in-tab">${Resource.msg('search.results', 'search', null)}</span>
	</div>
	<iscomment>Avoid empty paging div or empty div with empty ul</iscomment>
	<isif condition="${(!empty(pdict.OrderPagingModel) || !dw.system.Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll')) && Math.ceil(numberOfPages) > 1}">
		<ul>
		<isif condition="${(currentPage > 0) && (maxPage > 4)}">
			<iscomment>
				<li class="first-last">
					<a class="page-first" title="${Resource.msg('global.paginginformation.first.label', 'locale', null)}" href="${decodeURI(pdict.pagingmodel.appendPaging( pageURL, 0))}"><i class="fa fa-angle-double-left"></i><span class="visually-hidden">${Resource.msg('global.paginginformation.first', 'locale', null)}</span></a>
				</li>
			</iscomment>
			<li class="first-last">
				<isset name="assetCount" value="${0}" scope="page" />
				<isloop items="${slotPositionsArray}" var="position" status="loopstate">
					<isset name="assetID" value="${pdict.ProductSearchResult.category.ID + '-slot-tile-' + (loopstate.index+1).toFixed()}" scope="page" />
					<isif condition="${position <= ((currentPage-1)*pageSize) && (dw.content.ContentMgr.getContent(assetID) != null)}">
						<isset name="assetCount" value="${assetCount + 1}" scope="page" />
					</isif>
				</isloop>
				<a class="page-previous" title="${Resource.msg('global.paginginformation.previous.label', 'locale', null)}" href="${decodeURI(pdict.pagingmodel.appendPaging( pageURL, ((currentPage-1)*pageSize) - assetCount))}"><i class="icons-sprite"></i><span class="visually-hidden">${Resource.msg('global.paginginformation.previous.label', 'locale', null)}</span></a>
			</li>
		</isif>

		<isif condition="${maxPage >= 1}">
			<isloop status="i" begin="${rangeBegin}" end="${rangeEnd}">
				<isif condition="${i.index != currentPage}">
					<li>
						<iscomment>APPG-610 : SK & MK | Content slots on PLP</iscomment>
						<isset name="counter" value="${(i.index-1) * pdict.pagingmodel.pageSize}" scope="page" />
						<isset name="productCount" value="${pdict.pagingmodel.pageSize}" scope="page" />
						<isset name="previowsPageProductCount" value="${(i.index) * pdict.pagingmodel.pageSize}" scope="page" />
						
						<isloop items="${slotPositionsArray}" var="position" status="loopstate"> 
						<isset name="assetID" value="${pdict.ProductSearchResult.category.ID + '-slot-tile-' + (loopstate.index+1).toFixed()}" scope="page" />
						<isif condition="${position != '' && (dw.content.ContentMgr.getContent(assetID) != null)}"> 
							<isif condition="${(position > ((i.index-1) * productCount)) && (position <= ((i.index) * productCount))}"> 
								<isset name="productCount" value="${productCount-1}" scope="page" />
							</isif>	
							<isif condition="${counter > 0}"> 
								<isif condition="${position <= ((i.index-1) * previowsPageProductCount)}">
									<isset name="previowsPageProductCount" value="${previowsPageProductCount-1}" scope="page" />
								</isif>
							</isif>
						</isif>
						</isloop>
						<isset name="previowsPageProductCount" value="${counter == 0 ? productCount : previowsPageProductCount}" scope="page" />
					
						<iscomment>This long line is needed to avoid extra whitespaces in the link text</iscomment>
						<a class="page-<isprint value="${i.index + 1}" style="0"/>" title="${Resource.msgf('global.paginginformation.goto.label', 'locale', null, i.index + 1)}" href="${decodeURI(pdict.pagingmodel.appendPaging( pageURL, previowsPageProductCount))}"><isprint value="${i.index + 1}" style="0"/></a>
					</li>
				<iselse/>
					<li class="current-page" title="${Resource.msgf('global.paginginformation.current.label', 'locale', null, i.index + 1)}">
						<isprint value="${i.index + 1}" style="0"/>
					</li>
				</isif>
			</isloop>

			<isif condition="${(current < totalCount - pageSize) && (maxPage > 4)}">
				<li class="first-last page-last">
					<isset name="assetCount" value="${0}" scope="page" />
					<isloop items="${slotPositionsArray}" var="position" status="loopstate">
						<isset name="assetID" value="${pdict.ProductSearchResult.category.ID + '-slot-tile-' + (loopstate.index+1).toFixed()}" scope="page" />
						<isif condition="${position <= ((currentPage+1)*pageSize) && (dw.content.ContentMgr.getContent(assetID) != null)}">
							<isset name="assetCount" value="${assetCount + 1}" scope="page" />
						</isif>
					</isloop>
					<a class="page-next" title="${Resource.msg('global.paginginformation.next.label', 'locale', null)}" href="${decodeURI(pdict.pagingmodel.appendPaging( pageURL, ((currentPage+1) * pageSize) - assetCount))}"><i class="icons-sprite"></i><span class="visually-hidden">${Resource.msg('global.paginginformation.next', 'locale', null)}</span></a>
				</li>
				<iscomment>
					<li class="first-last">
						<a class="page-last" title="${Resource.msg('global.paginginformation.last.label', 'locale', null)}" href="${decodeURI(pdict.pagingmodel.appendPaging( pageURL, current + (maxPage - currentPage) * pageSize))}"><i class="fa fa-angle-double-right"></i><span class="visually-hidden">${Resource.msg('global.paginginformation.last', 'locale', null)}</span></a>
					</li>
				</iscomment>
			</isif>
		</isif>
		</ul>
	</isif>
	</div>
</isif>
