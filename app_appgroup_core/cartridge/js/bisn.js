'use strict';

var $cache = {};
var ajax = require('./ajax'),
	dialog = require('./dialog'),
    util = require('./util');

function initializeCache() {
    $cache.main = $('#main');
    $cache.bisnButton = $('.backInStockNotification .bisnButton');
    $cache.bisnpdpForm = $('.pdpForm, .item-list');
}

function initializeEvents() {
	// Need to display Get Notified only when atleast one variant is OOS.
	if ($('div.product-variations ul.swatches.size li.unavailableVariant').length > 0) {
		if ($('ul.swatches.color').find('.selected').length > 0) {
			$('div.bisn-notify-me').show();
		}else {
			$('div.bisn-notify-me').hide();
		}
	} else {
		$('div.bisn-notify-me').hide();
	}
    
    $cache.bisnpdpForm.on('click', 'div.bisn-notify-me span.bisn-link, .wishlist-notify-me', function (e) {
      e.preventDefault();
      var currObj = $(this);
      var product;
      var selectedColor;
      if (currObj.closest('.pt_wish-list').length >0 ){
    	  product = currObj.closest('form').find('input[name="bisnProduct"]').val();
    	  selectedColor = currObj.closest('.table-content').find('input.selectedColorSwatch').val();
      }else {
    	  product = $('input[name=bisnProduct]').val();
    	  selectedColor = $(currObj).closest('#product-content').find('input.selectedColorSwatch').val();
      }
      if (typeof(selectedColor) == 'undefined' || selectedColor == 'null' || selectedColor == '') {
    	  return;
      }
      $(document).find("#bisnpopup_Container").remove();
      // The below ajax will hit controller not the pipeline
      $.ajax({
          url :util.appendParamsToUrl(Urls.getBisnPopUp, {
          	 pid : product,
          	selectedcolor: selectedColor
          }),
          type: "POST",
                 success : function(data){
                	 			   $("#bisnpopup_Container").remove();
                                   var dlg = dialog.create({target:$("#bisnpopup_Container"), options:{
                                                async: false,
                                                height:'auto',
                                                width: 324,
                                                dialogClass : "Bisn_popup"
                                            }
                                   });
                                $("#bisnpopup_Container").append(data);
                                dlg.dialog("open");
                                var this_ul = $('.ui-dialog.Bisn_popup'),
                                	windowHeight = $(window).height(),
                                	currObjTop = currObj.offset().top,
                                	dialogHeight = this_ul.height(),
                                	totalOffset = windowHeight - currObjTop;
                                if ($('.pt_product-details').length > 0) {
                                	dlg.closest('.ui-dialog').css('top',$('.bisn-notify-me').offset().top + 30);
                                    dlg.closest('.ui-dialog').css('left',$('.bisn-notify-me').offset().left);
                                }
                                if (dialogHeight > totalOffset) {
                                	$("html,body").animate({
                                    	scrollTop: dialogHeight
                                    },1000);
                                }
                                $('body').addClass('unscroll');
                                setTimeout(function(){ 
                                	if ($('.pt_product-details').length > 0) {
                                		dlg.closest('.ui-dialog').css('top',$('.bisn-notify-me').offset().top + 30);
                                    	dlg.closest('.ui-dialog').css('left',$('.bisn-notify-me').offset().left);
                                	}
                                }, 1000);
                                initializeCache();
                                initializeBISNPopUpEventListener();
                 }
            });
      });
}

function initializeBISNPopUpEventListener() {
	$('div.backInStockNotification ul.swatches li.selectable').on('click', function(e){
	   var currObj = $(this);
       if(currObj.hasClass('selected')){
    	  currObj.closest('div.bisnProductInfoRow').find('li').removeClass('selected');
	      if($('div.bisnEmailFormContainer button.bisnButton').prop("disabled")){
	    	//do nothing
	       }else{
	    	 $('div.bisnEmailFormContainer button.bisnButton').attr('disabled', 'disabled');
	       }
	    }else{
	    	currObj.closest('div.bisnProductInfoRow').find('li').removeClass('selected');
	    	currObj.addClass('selected');
	        $('div.bisnEmailFormContainer button.bisnButton').removeAttr('disabled')
	    }
		var pid = currObj.find('#varProductId').val();
		currObj.closest('div.backInStockNotification').find('#bisnpid').val(pid);
	});
	
	$('.backInStockNotification').keypress(function(e) {
	    var keycode = (e.keyCode ? e.keyCode : e.which);
	    if (keycode == '13') {
	    	var email = $('.backInStockNotification .bisnemail').val();
	    	if (validateEmail(email) && !$cache.bisnButton.prop("disabled")){
	    		$cache.bisnButton.trigger('click');
	    	}
	        return false;
	    }
	});
	
	$cache.bisnButton.on('click', function (e) {
        e.preventDefault();
        var $this = $(this).closest('form');
        var invalidEmail = Resources.EMAILNOTIFICATIONFAIL;
        var pid = $this.find('#bisnpid').val();
        var email = $this.find('.bisnemail').val();
        if ((!validateEmail(email) || email === null)) {
        	!$('#bisnemailnstatus').is(':visible') ? $('<div id="bisnemailnstatus"></div>').insertBefore('button.bisnButton') : '';
            $this.find('#bisnemailnstatus').text('');
            $this.find('#bisnemailnstatus').addClass('error');
            $this.find('#bisnemailnstatus').text(invalidEmail);
        } else {
            $this.find('#bisnemailnstatus').removeClass('error').text('');
            var url = util.appendParamsToUrl(Urls.emailNotification, {
                bisnpid: pid,
                bisnemail: email,
                format: 'ajax'
            });
            
            // The below ajax will hit pipeline not controller
            $.ajax({
                url: url,
                success: function (data) {
                	if (typeof(data) == 'string') {
                		//$('div.backInStockNotification').replaceWith( $("<div/>").addClass("bisn-emailsubmit-msg").text(Resources.EMAILNOTIFICATIONSUCCESS));
                    	$('div.backInStockNotification').replaceWith(data);
                    	$('button.bisn-continueshop').on('click', function(){
                    		$('button.ui-dialog-titlebar-close').trigger('click');
                    	});
                	} else if (typeof(data) == "object") {
                		!data.error ? $this.find('#bisnemailnstatus').text(data.errorCode).css('color', 'RED') : '';
                	}else {
                		$this.find('#bisnemailnstatus').text(Resources.EMAILNOTIFICATIONFAIL).css('color', 'RED');
                	}
                    if (isGTMEnabled) {
                        require('./gtm').bisnPDP();
                    }
                },
                failure: function () {
                	$this.find('#bisnemailnstatus').text(Resources.EMAILNOTIFICATIONFAIL).css('color', 'RED');
                }
            });
        }

    });
	// Closing BISN popup event, as its behavior is different from site's general popup
	$('button.ui-dialog-titlebar-close').on('click', function (){
		$('body').removeClass('unscroll');
	});
}

function validateEmail(email) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return reg.test(email);
}

exports.init = function() {
    initializeCache();
    initializeEvents();
}
