'use strict';
var ajax = require('./ajax'),
	util = require('./util'),
	dialog = require('./dialog');

var init = function () {
	var $email_form = $('.footer-subscription-form');
	
	function subMenuPos() {
		if ($(window).width() > 767) {
			var $width = 0;
			$(".level-1 > li").each(function() {
				var $left = $(this).position().left;
				var $right = $("#wrapper").width() - $('#navigation').outerWidth();
				$width += $(this).outerWidth();
				$(this).find(".level-2").css({"padding-left": $left, "padding-right": $right});
			});
			
			$('.level-1').addClass('loaded');
		}else{
			$('.level-1').removeClass('loaded');
		}
	}
	
	$(window).on('load resize', function() {
		subMenuPos();
	}).on('scroll', function(){
		if($('#mini-cart .mini-cart-products').is(':visible')){
			var $h = $('#mini-cart .mini-cart-products').height();
			var $pos = $('#mini-cart .mini-cart-products').scrollTop();
    		$('#mini-cart .mini-cart-products').height('auto').height($h);
    		$('#mini-cart .mini-cart-products').scrollTop($pos);
    		if($('#mini-cart .mini-cart-product').length > 4){
    			util.customscrollbar($('#mini-cart .mini-cart-products'));
    		}
		}
	});
	
	$('.level-1 > li').hover(function(){
		subMenuPos();
	});
	
	$(".level-1 > li > a").on('click', function(e){
		$(".level-1 > li > a").not(this).removeClass('clicked');
		if(!$(this).hasClass('clicked') && util.isMobile() && $(window).width() > 767){
			e.preventDefault()
			$(this).addClass('clicked');
		}
	});
	
	$(".cookie-info").on("click", function(){
		$(this).closest(".cookie-notifier-content").toggleClass('active').find(".cookie-data").toggle();
	});
	
	$(".close-btn").on("click", function() {
		$(this).parent().hide();
	});
	
	$email_form.on('keyup', '#subscription-email', function (e) {
		var keyCode = e.keyCode || window.event.keyCode;
		if(keyCode != 13){
			$email_form.find('span.error').hide();
			$email_form.find('#subscription-email').removeClass('error');
		}
	}).on('focus', '#subscription-email', function(){
        $(".foot-tooltip").show();
        $email_form.find('span.error').hide();
		$email_form.find('#subscription-email').removeClass('error');
	}).on('blur', '#subscription-email', function(){
    	$(".foot-tooltip").hide();
	}).on('click', '#home-email', function(e){
		var $val = $email_form.find('#subscription-email').val(),
			$regex = /^[\w\.\&\+\-\/\_]+@[\w\-]+\.[\w]{2,6}$/;
		
		if($val == ""){
			e.preventDefault();
			$email_form.find('span.error').html(Resources.VALIDATE_REQUIRED).show();
			$email_form.find('#subscription-email').addClass('error');
		}else if(!$regex.test($val)){
			e.preventDefault();
			$email_form.find('span.error').html(Resources.VALIDATE_EMAIL).show();
			$email_form.find('#subscription-email').addClass('error');
		}else{
			$email_form.find('span.error').hide();
			$email_form.find('#subscription-email').removeClass('error');
		}
	});
	
	$(".header-search input").on("focus", function() {
		var userInfoFlyout = $("li.user-info");
		if ($(userInfoFlyout).hasClass("active")) {
			$(userInfoFlyout).removeClass("active");
		}
	});
	
	$(document).on('change keyup mouseup', 'input[name$="_postal"]', function () {
		var $form = $(this).closest('form');
		var $country = $form.length > 0 ? $form.find('.country') : 0;
		if ($country.length > 0 && $country.val() == 'CA'){
			$(this).val($(this).val().toUpperCase());
		}
    });
	$(document).on('change keyup mouseup', 'input[name$="_postalCode"]', function () {
		var $country = $('.current-country');
		if ($country.length > 0 && $country.attr('current-country') == 'CA'){
			$(this).val($(this).val().toUpperCase()); 
		}
    });
	
	function opennewletterpopup(){
		if (!$('.format-newsletter').is(":visible") && $('.subscription-slot-content').length > 0) {
			dialog.open({
				html: $('.subscription-slot-content'),
				options: {
	                dialogClass: 'format-newsletter'
	            }
			});
		}
	}
	
	// New popup rule as per ticket APPG-824
	var PgCount = window.SitePreferences.NEWSLETTER_POPUP_PAGECOUNT;
	var newPgCount;
	var siteVisitCount;
	var pagecountcookieage = window.SitePreferences.PAGECOUNT_COOKIE_AGE;
	var DiffOption = window.SitePreferences.NEWSLETTER_POPUP_PAGEDIFF;
	var currentSessionID = $('input[name="welcome-session-id"]').val();
	var getnewsletterpopupcookie = util.getCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount'); // To keep the track of page count
	var getnewsletterSiteVisitCount = util.getCookie(window.SitePreferences.SITE_ID +'SessionCountCookie'); // To keep the track of number of site(session) visit
	
	siteVisitCount = parseInt(getnewsletterSiteVisitCount);
	
	// check for page count from cookie, if cookie is not available then create it.
	if (getnewsletterpopupcookie == ''){
		util.createCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount', 1, parseInt(pagecountcookieage));
		var getCookiePageCount = util.getCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount');
		newPgCount = parseInt(getCookiePageCount);
	}else{
		util.removeCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount');
		newPgCount = parseInt(getnewsletterpopupcookie) + 1;
		util.createCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount', newPgCount, parseInt(pagecountcookieage));
	}
	
	// Making Preference control to open popup 
	if (window.SitePreferences.NEWSLETTER_POPUP_NEWRULE){
		if ((PgCount != 0 && PgCount == newPgCount) && ((siteVisitCount == 0) || ((DiffOption != 0) && (siteVisitCount % DiffOption == 0)))){
			opennewletterpopup();
		}
	}else{
		// Old logic for newsletter popup
		var ExpireTime = window.SitePreferences.COOKIE_EXPIRE_TIME;
		var AgingCookieDuration = window.SitePreferences.AGING_COOKIE_DURATION;
		var cookieName = window.SitePreferences.SITE_ID +'promoActive';
		
		if($('.subscription-slot-content .subscription-content').length > 0 && document.cookie.indexOf(cookieName) < 0){
			if(document.cookie.indexOf("tempCookie") < 0){
				var dateObj = new Date();
		        document.cookie = "tempCookie"+"="+dateObj+"; path=/";
			}
			if(document.cookie.indexOf("tempCookie") > 0){
				var currentDate = new Date();
				var getCookieValues = function(cookie) {
					var cookieArray = cookie.split('=');
					if(cookieArray[0].trim() == "tempCookie")
					return cookieArray[1].trim();
				};
				var getCookieNames = function(cookie) {
					var cookieArray = cookie.split('=');
					if(cookieArray[0].trim() == "tempCookie")
					return cookieArray[0].trim();
				};
				var cookies = document.cookie.split(';');
				var cookieValue = cookies.map(getCookieValues)[cookies.map(getCookieNames).indexOf("tempCookie")];
				var cookieCreatedDate = new Date(cookieValue);
				var cookieExpireDate = new Date(cookieValue);
				cookieExpireDate.setSeconds(cookieCreatedDate.getSeconds() + 40);
				
				if(currentDate > cookieExpireDate){
					dialog.open({
						html: $('.subscription-slot-content'),
						options: {
			                dialogClass: 'format-newsletter'
			            }
					});	    	
			    }else{
			    	var secDiff = Math.abs((cookieExpireDate.getTime()-currentDate.getTime())/1000);
			    	setTimeout(function(){ 
						dialog.open({
							html: $('.subscription-slot-content'),
							options: {
				                dialogClass: 'format-newsletter'
				            }
						}); }, secDiff*1000);
			    }
			}
		}
	}
	$(document).on('click', '.format-newsletter button.ui-dialog-titlebar-close' , function (e) {
	     var popUpClosedate = new Date();
	     popUpClosedate.setTime(popUpClosedate.getTime() + AgingCookieDuration*1440*60000);
	     if (document.cookie.indexOf(cookieName) < 0) {
	         document.cookie = cookieName+"="+1+"; expires="+popUpClosedate.toUTCString()+"; path=/";
	         document.cookie = "tempCookie" + '=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	     }
	});
	$(document).on('click', '.format-newsletter .email-submission' , function (e) {
        var popUpSubmissionDate = new Date();
        popUpSubmissionDate.setFullYear(popUpSubmissionDate.getFullYear()+10);
        if (document.cookie.indexOf(cookieName) < 0) {
            document.cookie = cookieName+"="+1+"; expires="+popUpSubmissionDate.toUTCString()+"; path=/";
            document.cookie = "tempCookie" + '=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
	});
	
	//Open Header-Cookie Based on settings in pref. APPG-824
    var pageCount = window.SitePreferences.COOKIE_HEADERMAT_PAGECOUNT;
    var headerDiferOption = window.SitePreferences.COOKIE_HEADERMAT_DIFEROPTION;
    if (window.SitePreferences.NEWSLETTER_POPUP_NEWRULE){
		// Based on page count and on session count (Site visit)
	    if ((pageCount != 0 && pageCount == parseInt(newPgCount)) && ((siteVisitCount == 0) || ((headerDiferOption != 0) && (siteVisitCount % headerDiferOption == 0)))){
	    	$('.cookie-notifier-content').removeClass("hide");
	    }
    }else {
		var notifierCookieName = window.SitePreferences.SITE_ID +'cookieNotifier';
		if($('.cookie-usage-popup').length > 0 && document.cookie.indexOf(notifierCookieName) < 0){
			$('.cookie-notifier-content').removeClass("hide");
		}else if($('.cookie-usage-popup').length > 0 && document.cookie.indexOf(notifierCookieName) > 0){
			var popUpSubmissionDate = new Date();
	        popUpSubmissionDate.setFullYear(popUpSubmissionDate.getFullYear()+10);
	        document.cookie = notifierCookieName+"="+1+"; expires="+popUpSubmissionDate.toUTCString()+"; path=/";
			$('.cookie-notifier-content').remove();
		}
	}
	$(document).on('click', '.cookie-notifier-content .cookie-info-close' , function (e) {
		var popUpSubmissionDate = new Date();
        popUpSubmissionDate.setFullYear(popUpSubmissionDate.getFullYear()+10);
        if (document.cookie.indexOf(notifierCookieName) < 0 || window.SitePreferences.COOKIE_HEADERMAT_NEWRULE) {
            document.cookie = notifierCookieName+"="+1+"; expires="+popUpSubmissionDate.toUTCString()+"; path=/";
            $('.cookie-notifier-content').remove();
        }
	});
};
exports.init = init;
