'use strict';
var util = require('./util');
/**
 * @description fpProductContextEvents for Set product context
 **/
var fpProductContextEvents = function(){
	var productId = $('input[id="masterID"]').val();
	var color = $('.site-mk').length > 0 ? $('.product-variations .selected-value:first').text() : $('.product-variations ul.color li.selected-value').text();
	if(color == ''){
		color = undefined;
	}
	var size = ($('.product-variations ul.size li.selected a').text().trim() != '') ? $('.product-variations ul.size li.selected a').text().trim() : undefined;
	var sizesArray = new Array();
    var availableSizesArray = new Array();
    $.each( $('ul.size li.selectable'), function( index ){
    	sizesArray.push($('ul.size li a').eq(index).text().trim());
    	if(!$('ul.size li').eq(index).hasClass('unavailableVariant')){
    		availableSizesArray.push($('ul.size li a').eq(index).text().trim());
    	}
    });
    
    var standardPrice = $('input[id="standardPrice"]').val();
    	standardPrice = (standardPrice != undefined && standardPrice != '') ? parseFloat(standardPrice) : undefined;
    var salePrice = $('input[id="salePrice"]').val();
    	salePrice = (salePrice != undefined && salePrice != '') ? parseFloat(salePrice) : undefined;
    if(standardPrice == undefined && salePrice != undefined){
    	standardPrice = salePrice;
    	salePrice = undefined;
    }
    
    return {
    	productId: productId,
    	standardPrice: standardPrice,
    	salePrice: salePrice,
    	color: color,
    	sizesArray: sizesArray,
    	availableSizesArray: availableSizesArray,
    	size : size
    };
};

var fpAddToCartEvent = function(){
	 var variantId = $('input[id="pid"]').val();
     var productId = $('input[id="masterID"]').val();
     var quantity = parseInt('1');
     
     return{
    	 variantId : variantId,
    	 productId : productId,
    	 quantity : quantity
     };
}

var fpGetDeviceLayout = function(){
	var isMobileLayout = util.isMobile();
    var layout;
    if(isMobileLayout != null && isMobileLayout != undefined && isMobileLayout == false){
    	layout = 'desktop';
    }else{
    	if($(window).width() >= 768 && $(window).width() <= 1023 ){
    		layout = 'tablet';
    	}else{
    		layout = 'mobile';
    	}
    }
    return layout;
}

exports.fpProductContextEvents = fpProductContextEvents;
exports.fpAddToCartEvent = fpAddToCartEvent;
exports.fpGetDeviceLayout = fpGetDeviceLayout;