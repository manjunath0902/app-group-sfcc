'use strict';

var ajax = require('./ajax'),
    util = require('./util');
/**
 * @function
 * @description Load details to a given gift certificate
 * @param {String} id The ID of the gift certificate
 * @param {Function} callback A function to called
 */
//PREVAIL-Added pin to handle Custom GC.
exports.checkBalance = function (id, pin, callback) {
    // load gift certificate details
    var url = util.appendParamToURL(Urls.giftCardCheckBalance, 'giftCertificateID', id);
    url = util.appendParamToURL(url, 'gcPin', pin); //PREVAIL-Added pin to handle Custom GC.
    ajax.getJson({
        url: url,
        callback: callback
    });
};
