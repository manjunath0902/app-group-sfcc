'use strict';

var util = require('./util');

function welcomeMatfeature() {
	
	if (window.SitePreferences.WELCOMEMAT_NEWRULE){
		var WelcomeMatPgCount = window.SitePreferences.WELCOMEMAT_PAGECOUNT;
		var WelcomeMatNewPgCount;
		var WelcomeSiteVisitCount;
		var WelcomeDiffOption = window.SitePreferences.WELCOMEMAT_PAGEDIFF;
		
		// Below cookies are created in global.js
		var getwelcomematpagecount = util.getCookie(window.SitePreferences.SITE_ID +'newsletterpopupPageCount'); // To keep the track of page count
		var getWelcomematSiteVisitCount = util.getCookie(window.SitePreferences.SITE_ID +'SessionCountCookie'); // To keep the track of number of site visit
		
		// Check no. of site(session) visit and open welcome mat based on that
		WelcomeSiteVisitCount = parseInt(getWelcomematSiteVisitCount);

		if ((WelcomeMatPgCount != 0 && WelcomeMatPgCount == parseInt(getwelcomematpagecount)) && ((WelcomeSiteVisitCount == 0) || ((WelcomeDiffOption != 0) && (WelcomeSiteVisitCount % WelcomeDiffOption == 0)))){
			welcomeMsg();
		}
	}else{
		var brandID = $('input[name="welcome-site-id"]').val();
		var sessionID = $('input[name="welcome-session-id"]').val();
		var cookieValue= util.getCookie('welcomemat-'+brandID+'-sessionid');
		if(cookieValue == ''){
			util.createCookie('welcomemat-'+brandID+'-sessionid', sessionID, 1);
			welcomeMsg();
		}else if(sessionID != cookieValue){
			util.removeCookie('welcomemat-'+brandID+'-sessionid');
			util.createCookie('welcomemat-'+brandID+'-sessionid', sessionID, 1);
			welcomeMsg();
		}
	}
		
}

function welcomeMsg() {
	var $currObj = $('.welcomemat');
	var $close = $currObj.find('.welcome-close');
	
	/* The hoverEvent variable is to handle the hover event on the welcome popup */
	var hoverEvent = false;
	
	/* When a user lands on any page the slider will slide up */
	setTimeout(function(){ 
		$currObj.slideDown();
    }, 1000);
	
	$(window).load(function() {
		$close.click(function(e) {
			e.preventDefault();
			$(this).closest('.welcomemat').slideUp();
		});
		
		$currObj.hover(function() {
			hoverEvent = true;
			return;
		},function() {
			hoverEvent = false;
			setTimeout(function(){ 
				if(!hoverEvent) {
					$close.trigger('click');
				}
		    }, 4000);
		});
		
		/* If the user does not interact with it within 10 seconds it should slide down */
		setTimeout(function() { 
			if(!hoverEvent) {
				$currObj.slideUp();
			}
	    }, 12000);
	});
	
}

exports.init = function() {
	/* If the user click on locales in welcome popup */
	$('.locale-flags').on('click', function() {
		$('.welcome-close').trigger('click');
	});
	
	welcomeMatfeature();
    //welcomeMsg();
}
