'use strict';

var imagesLoaded = require('imagesloaded'),
    rating = require('./rating'), //PREVAIL-Added to handle ratings display in-case of ajax calls.
    quickview = require('./quickview'),
	util = require('./util');

// Quick view is not needed in APPGROUP as of now, so we are not calling this function
function initQuickViewButtons() {
    $('.tiles-container .product-image').on('mouseenter', function () {
        var $qvButton = $('#quickviewbutton');
        if ($qvButton.length === 0) {
            $qvButton = $('<a id="quickviewbutton" class="quickview"><span class="quickview-text">' + Resources.QUICK_VIEW + '</span><span class="quickview-bg"></span></a>');
        }
        var $link = $(this).find('.thumb-link');
        $qvButton.attr({
            'href': $link.attr('href'),
            'title': $link.attr('title')
        }).appendTo(this);
        $qvButton.on('click', function (e) {
            e.preventDefault();
            quickview.show({
                url: $(this).attr('href').split('#')[0], //PREV JIRA PREV-255 :PLP: On Click Quick view navigating to a wrong page when user first changes the swatches. Taking only href.
                source: 'quickview'
            });

            if (isGoogleAnalyticsEnabled){
                require('./ga').quickview($(this)[0].pathname);
            }
        });
    });
}

function gridViewToggle() {
    $('.toggle-grid').on('click', function () {
        $('.search-result-content').toggleClass('wide-tiles');
        $(this).toggleClass('wide');
    });
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
	// Quick view is not needed in APPGROUP as of now
    // initQuickViewButtons();
    gridViewToggle();
    rating.init(); //PREVAIL-Added to handle ratings display in-case of ajax calls.
    $('.swatch-list').on('mouseleave', function () {
        // Restore current thumb image
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $thumb.data('current');

        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });
    $('.swatch-list .swatch').on('click', function (e) { 
        e.preventDefault();
        if ($(this).hasClass('selected')) {
            return;
        }

        var $tile = $(this).closest('.product-tile');
        $(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
        $(this).addClass('selected');
        $tile.find('.thumb-link').attr('href', $(this).attr('href'));
        $tile.find('name-link').attr('href', $(this).attr('href'));

        var data = $(this).children('img').filter(':first').data('thumb');
        
        var hoverData=$(this).children('img').data('hover');
        $tile.find('.thumb-link').data('hoverimage',hoverData);
        $tile.find('.thumb-link').data('actualimage',data.src);
        
        var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
        var currentAttrs = {
            src: data.src,
            alt: data.alt,
            title: data.title
        };
        $thumb.attr(currentAttrs);
        $thumb.data('current', currentAttrs);

        /*Start JIRA PREV-466 : Product images are not updating in the compare section when the color swatches are changed in PLP.*/
        var pid = $(this).closest('.product-tile').attr('data-itemid');
        $('.compare-items-panel .compare-item').each(function () {
            var compareid = $(this).attr('data-itemid');
            if (pid === compareid) {
                var $compare = $(this).find('.compare-item-image').eq(0);
                $compare.attr(currentAttrs);
                $compare.data('current', currentAttrs);
            }
        });
        /*End JIRA PREV-466*/
    }).on('mouseenter', function () {
        // get current thumb details
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $(this).children('img').filter(':first').data('thumb'),
            current = $thumb.data('current');

        // If this is the first time, then record the current img
        if (!current) {
            $thumb.data('current', {
                src: $thumb[0].src,
                alt: $thumb[0].alt,
                title: $thumb[0].title
            });
        }

        // Set the tile image to the values provided on the swatch data attributes
        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });
    
    

        
	if($('.product-video').length > 0){
		$(this).closest('.product-tile').find('.product-image .b-lazy').addClass('b-loaded');
	    $('.product-tile .product-video').hover(function (e){
	    	e.preventDefault();
	           $(this).closest('.product-tile').find('.product-video').addClass('product-video-hidden');
	           $(this).closest('.product-tile').find('.product-image .b-lazy').addClass('b-loaded');
	           $(this).closest('.product-tile').find('.product-image').removeClass('product-img-hidden');
	    });
	}
		
		$('.product-tile .product-image').hover(function (e){
	    	if($(this).closest('.product-tile').find('.product-video').length > 0) {
	    		//do nothing
	    		e.preventDefault();
	    	} else {
	    		var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
            	if(!isiPad)
            	{
            		$(this).find('img').attr('src',$(this).find('.thumb-link').data('hoverimage'));
            	}
	    	}
	    },function(){
	    	if($(this).closest('.product-tile').find('.product-video').length > 0) {
	    		$(this).closest('.product-tile').find('.product-video').removeClass('product-video-hidden');
		        $(this).closest('.product-tile').find('.product-image').addClass('product-img-hidden');
	    	} else {
	    		var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
            	if(!isiPad)
            	{
            		$(this).find('img').attr('src',$(this).find('.thumb-link').data('actualimage'));
            	}
	    	}
	    });
		
	$('.thumb-link, .name-link').on('click', function(){
		var $nameLink = $(this);
        var $tile = $nameLink.closest('.product-tile');
        var pid = $tile.data('itemid');
        var windowScroll = $(window).scrollTop();

        // Next, set all of the product / grid-based cookies for the PLP display
        // For the viewLink, make sure we get the LAST view-more url used
        if(pid == undefined || pid == ''){return;}
        util.createCookie('productId', pid);
        util.createCookie('offset-value', windowScroll);
	});
}

exports.init = function() {
    var $tiles = $('.tiles-container .product-tile');
    if ($tiles.length === 0) {
        return;
    }
    imagesLoaded('.tiles-container').on('done', function () {
    	$tiles.each(function (idx) {
                $(this).data('idx', idx);
            });
    });
    initializeEvents();
};
