'use strict';

var _ = require('lodash');

var util = {
    /**
     * @function
     * @description appends the parameter with the given name and value to the given url and returns the changed url
     * @param {String} url the url to which the parameter will be added
     * @param {String} name the name of the parameter
     * @param {String} value the value of the parameter
     */
    appendParamToURL: function(url, name, value) {
        // quit if the param already exists
        if (url.indexOf(name + '=') !== -1) {
            return url;
        }
        var separator = url.indexOf('?') !== -1 ? '&' : '?';
        return url + separator + name + '=' + encodeURIComponent(value);
    },
    /**
     * @function
     * @description appends the parameters to the given url and returns the changed url
     * @param {String} url the url to which the parameters will be added
     * @param {Object} params
     */
    appendParamsToUrl: function(url, params) {
        var _url = url;
        _.each(params, function(value, name) {
            _url = this.appendParamToURL(_url, name, value);
        }.bind(this));
        return _url;
    },

    /**
     * @function
     * @description remove the parameter and its value from the given url and returns the changed url
     * @param {String} url the url from which the parameter will be removed
     * @param {String} name the name of parameter that will be removed from url
     */
    removeParamFromURL: function(url, name) {
        if (url.indexOf('?') === -1 || url.indexOf(name + '=') === -1) {
            return url;
        }
        var hash;
        var params;
        var domain = url.split('?')[0];
        var paramUrl = url.split('?')[1];
        var newParams = [];
        // if there is a hash at the end, store the hash
        if (paramUrl.indexOf('#') > -1) {
            hash = paramUrl.split('#')[1] || '';
            paramUrl = paramUrl.split('#')[0];
        }
        params = paramUrl.split('&');
        for (var i = 0; i < params.length; i++) {
            // put back param to newParams array if it is not the one to be removed
            if (params[i].split('=')[0] !== name) {
                newParams.push(params[i]);
            }
        }
        return domain + '?' + newParams.join('&') + (hash ? '#' + hash : '');
    },

    /**
     * @function
     * @description extract the query string from URL
     * @param {String} url the url to extra query string from
     **/
    getQueryString: function(url) {
        var qs;
        if (!_.isString(url)) {
            return;
        }
        var a = document.createElement('a');
        a.href = url;
        if (a.search) {
            qs = a.search.substr(1); // remove the leading ?
        }
        return qs;
    },
    /**
     * @function
     * @description
     * @param {String}
     * @param {String}
     */
    elementInViewport: function(el, offsetToTop) {
        var top = el.offsetTop,
            left = el.offsetLeft,
            width = el.offsetWidth,
            height = el.offsetHeight;

        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        if (typeof(offsetToTop) !== 'undefined') {
            top -= offsetToTop;
        }

        if (window.pageXOffset !== null) {
            return (
                top < (window.pageYOffset + window.innerHeight) &&
                left < (window.pageXOffset + window.innerWidth) &&
                (top + height) > window.pageYOffset &&
                (left + width) > window.pageXOffset
            );
        }

        if (document.compatMode === 'CSS1Compat') {
            return (
                top < (window.document.documentElement.scrollTop + window.document.documentElement.clientHeight) &&
                left < (window.document.documentElement.scrollLeft + window.document.documentElement.clientWidth) &&
                (top + height) > window.document.documentElement.scrollTop &&
                (left + width) > window.document.documentElement.scrollLeft
            );
        }
    },

    /**
     * @function
     * @description Appends the parameter 'format=ajax' to a given path
     * @param {String} path the relative path
     */
    ajaxUrl: function(path) {
        return this.appendParamToURL(path, 'format', 'ajax');
    },

    /**
     * @function
     * @description
     * @param {String} url
     */
    toAbsoluteUrl: function(url) {
        if (url.indexOf('http') !== 0 && url.charAt(0) !== '/') {
            url = '/' + url;
        }
        return url;
    },
    /**
     * @function
     * @description Loads css dynamically from given urls
     * @param {Array} urls Array of urls from which css will be dynamically loaded.
     */
    loadDynamicCss: function(urls) {
        var i, len = urls.length;
        for (i = 0; i < len; i++) {
            this.loadedCssFiles.push(this.loadCssFile(urls[i]));
        }
    },

    /**
     * @function
     * @description Loads css file dynamically from given url
     * @param {String} url The url from which css file will be dynamically loaded.
     */
    loadCssFile: function(url) {
        return $('<link/>').appendTo($('head')).attr({
            type: 'text/css',
            rel: 'stylesheet'
        }).attr('href', url); // for i.e. <9, href must be added after link has been appended to head
    },
    // array to keep track of the dynamically loaded CSS files
    loadedCssFiles: [],

    /**
     * @function
     * @description Removes all css files which were dynamically loaded
     */
    clearDynamicCss: function() {
        var i = this.loadedCssFiles.length;
        while (0 > i--) {
            $(this.loadedCssFiles[i]).remove();
        }
        this.loadedCssFiles = [];
    },
    /**
     * @function
     * @description Extracts all parameters from a given query string into an object
     * @param {String} qs The query string from which the parameters will be extracted
     */
    getQueryStringParams: function(qs) {
        if (!qs || qs.length === 0) {
            return {};
        }
        var params = {},
            unescapedQS = decodeURIComponent(qs);
        // Use the String::replace method to iterate over each
        // name-value pair in the string.
        unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
            function($0, $1, $2, $3) {
                params[$1] = $3;
            }
        );
        return params;
    },

    fillAddressFields: function(address, $form) {
    	var currentCountry = $form.find('[name$="country"]').val();
        for (var field in address) {
            if (field === 'ID' || field === 'UUID' || field === 'key') {
                continue;
            }
            // if the key in address object ends with 'Code', remove that suffix
            // keys that ends with 'Code' are postalCode, stateCode and countryCode
            $form.find('[name$="' + field.replace('Code', '') + '"]').val(address[field]);
            // update the state fields
            if (field === 'countryCode') {
            	$form.find('[name$="country"]').trigger('change');
                $form.find('[name$="country"]').val(address.countryCode);
                $form.find('[name$="country"]').blur();
            }
            if (field === 'stateCode') {
            	//$form.find('[name$="country"]').trigger('change');
                if (currentCountry != address.countryCode) {
            		$form.find('[name$="state"]').val($form.find('[name$="state"] option:eq(0)').val());
            		$form.find('[name$="state"]').blur();
            		continue;
                }
                $form.find('[name$="state"]').val(address.stateCode);
                $form.find('[name$="state"]').blur();
            }
            if (field === 'postalCode') {
            	$form.find('[name$="postal"]').trigger('change');
            }
        }
    },
    
    lazyLoad: function(){
    	 var bLazy = new Blazy({
        	 
    	/*success: function(){
    	    updateCounter();
    	  }*/
    	});
    },
    /**
     * @function
     * @description Updates the number of the remaining character
     * based on the character limit in a text area
     */
    limitCharacters: function() {
        $('form').find('textarea[data-character-limit]').each(function() {
            var characterLimit = $(this).data('character-limit');
            var charCountHtml = String.format(Resources.CHAR_LIMIT_MSG,
                '<span class="char-remain-count">' + characterLimit + '</span>',
                '<span class="char-allowed-count">' + characterLimit + '</span>');
            var charCountContainer = $(this).next('div.char-count');
            if (charCountContainer.length === 0) {
                charCountContainer = $('<div class="char-count"/>').insertAfter($(this));
            }
            charCountContainer.html(charCountHtml);
            // trigger the keydown event so that any existing character data is calculated
            $(this).change();
        });
    },
    selectbox: function(){
    	this.updateselect();
    	
    	$(document).on('click','.selected-option', function(){
    		var $h = 0;
    		var $this = $(this);
    		if($this.siblings('select').is(':disabled')){return false;}

    		if(!util.isMobile()){
    			$('.custom-select.current_item select').trigger('blur');
    			window.$currentkeycode = "";
    			$('.custom-select').not($this.closest('.custom-select')).removeClass('current_item');
    			$this.siblings('.selection-list').css('top', $this.outerHeight());
    			$this.closest('.custom-select').toggleClass('current_item');
        		
        		if(!$this.closest('.custom-select').hasClass('current_item')){
        			$this.siblings('select').trigger('blur');
        		}
        		
        		if($this.siblings('.selection-list').find('li').length > 10){
    	    		$.each($this.siblings('.selection-list').find('li:visible').slice(0,10), function(){
    	    			$h += $(this).outerHeight();
    	    			$this.siblings('.selection-list').height($h - 1);	
    	    		});
    	    		
    	    		var h = 0; 
    	    		$.each($this.closest('.custom-select').find('.selection-list li:visible').splice(0, $this.closest('.custom-select').find('.selection-list li.selected:visible').index() - 1),function(){ h+=$(this).outerHeight();})
    	    		$this.siblings('.selection-list').scrollTop(h);
    	    		
        		}else{
        			$this.siblings('.selection-list').height("auto");	
        		}
    		}
    	}).on('click','.selection-list li', function(){
    		if(!util.isMobile()){
	    		var $item = $(this).closest('.custom-select');
	    			$item.find('li').removeClass('hover selected');
	    			$(this).addClass('selected');
		    		$item.find('select option').eq($(this).index()).prop('selected', true);
		    		$item.find('.selected-option').text($(this).text());
		    		
		    		$item.removeClass('current_item');
		    		$item.find('select').trigger('change');
		    		$item.find('select').trigger('blur');
    		}		
    	}).on('change input','select', function(){
    		util.updateselect();
    	});
    	
    	$(document).on('click', function (e) {
             if (!util.isMobile() && !$('.custom-select.current_item').is(e.target) && !$('.custom-select.current_item').find('*').is(e.target)) {
            	util.closeselectbox();
             }
        });
    	
    	$('body').off('keydown').on('keydown', function (e) {
			var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0,
					$active_select = $('.custom-select.current_item');
				
				if($active_select.length > 0 && !util.isMobile()) {
					e.preventDefault();
					var $list = $active_select.find('li'),
							$active;

					if(!$active_select.find('li.selected').length) {
						$active_select
							.find('li')
							.eq($active_select.find('option:selected').index())
							.addClass('selected');
					}

					if(key === 8) { // Delete key
						window.$currentkeycode = window.$currentkeycode == undefined ? '' : window.$currentkeycode.slice(0, -1);
					} else {
						window.$currentkeycode += window.$currentkeycode == undefined ? '' : $.trim(String.fromCharCode(key).toLowerCase());
					}
					$active = $active_select.find("li[label^='" + window.$currentkeycode + "']");

					if($active.length == 0){
						window.$currentkeycode = window.$currentkeycode.substr(window.$currentkeycode.length - 1, 1);
						$active = $active_select.find("li[label^='" + window.$currentkeycode + "']");
					}
					
					if(key == 40){
						if($active_select.find('li.selected').length > 0 && $active_select.find('li.hover').length == 0){
							$active_select.find('li.selected').addClass('hover');
						} 
						$active = $active_select.find('li.hover');
						$active_select.find('li').removeClass('hover');
						$active.next().addClass('hover');
						
					}else if(key == 38){
						if($active_select.find('li.selected').length > 0 && $active_select.find('li.hover').length == 0){
							$active_select.find('li.selected').addClass('hover');
						} 
						$active = $active_select.find('li.hover');
						if(!$active.prev().hasClass('hide')){
							$active_select.find('li').removeClass('hover');
							$active.prev().addClass('hover');
						}
					}
					else if(key === 13){
						if($active.length == 0 && $active_select.find('li.hover').length > 0){
							$active_select.find('li.hover').trigger('click');
						}else{
							$active.eq(0).trigger('click');
						}
					}else if(key === 9 || key === 27){
						util.closeselectbox();
					}else if($active.length != 0){
						$list.removeClass('hover');
						$active.eq(0).addClass('hover');
					}
					if(key === 8) { // Delete key
						return false;
					}
					
					var h = 0; 
					if($active_select.find('.selection-list li.hover').length == 0){
						$.each($active_select.find('.selection-list li:visible').splice(0, $active_select.find('.selection-list li.selected:visible').index() - 1),function(){ h+=$(this).outerHeight();})
					}else{
						$.each($active_select.find('.selection-list li:visible').splice(0, $active_select.find('.selection-list li.hover:visible').index() - 1),function(){ h+=$(this).outerHeight();})
					}
					
					$active_select.find('.selection-list').scrollTop(h);
				}
		}).off('click').on('click', function (e) {
			if(!util.isMobile()){
				$('select').removeClass('select-focus');
				$('.ui-dialog:visible .dialogInput').remove();
			}
		});
    },
    closeselectbox: function(){
    	 $('.custom-select.current_item select').trigger('blur');
    	 $('.custom-select').removeClass('current_item');
    	 $('.custom-select li').removeClass('hover');
    },
    updateselect: function(){
    	$('.custom-select').each(function(){
    		var $this = $(this);
              if ($this.find('.selected-option').length == 0) {
                  $this.append('<div class="selected-option"></div>');
              }
              if ($this.find('.selection-list').length == 0) {
                  $this.append('<ul class="selection-list"></ul>'); 
                  var $list = '';
                  $this.find('option').each(function() {
                      $list += '<li label="' + $(this).text().toLocaleLowerCase() + '">' + $(this).text() + '<span class="select-checkbox"> </span>'+'</li>'
                  });
                  $this.find('.selection-list').append($list);
              } else {
                  $this.find('.selection-list li').each(function() {
                      $this.attr('label', $.trim($(this).text().toLocaleLowerCase()));
                  });
              }
              
    		$this.find('.selected-option').text($(this).find('select option:selected').text());
    		$this.find('.selection-list li').removeClass('selected').removeClass('hover').eq($(this).find('select option:selected').index()).addClass('selected');
    		
    		if($(this).find('select option:selected').val() != ""){
    			$this.find('.selected-option').addClass('selected');
    		}else{
    			$this.find('.selected-option').removeClass('selected');
    		}
    		
    		if($this.find('.selection-list li .bold').length > 0){
    			$this.find('.selected-option').html($this.find('.selection-list li').eq($this.find('select option:selected').index()).html());
    		}
    		
    		$this.find('li').removeClass('selected');
    		$this.find('li').eq($(this).find('select option:selected').index()).addClass('selected');
    	});
    },
    /**
     * @function
     * @description Binds the onclick-event to a delete button on a given container,
     * which opens a confirmation box with a given message
     * @param {String} container The name of element to which the function will be bind
     * @param {String} message The message the will be shown upon a click
     */
    setDeleteConfirmation: function (container, message) {
        $(container).on('click', '.delete', function() {
            return window.confirm(message);
        });
    },
    /**
     * @function
     * @description Scrolls a browser window to a given x point
     * @param {String} The x coordinate
     */
    scrollBrowser: function (xLocation) {
        $('html, body').animate({
            scrollTop: xLocation
        }, 500);
    },

    isMobile: function () {
        var mobileAgentHash = ['mobile', 'tablet', 'phone', 'ipad', 'ipod', 'android', 'blackberry', 'windows ce', 'opera mini', 'palm'];
        var idx = 0;
        var isMobile = false;
        var userAgent = (navigator.userAgent).toLowerCase();

        while (mobileAgentHash[idx] && !isMobile) {
            isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);
            if(mobileAgentHash[idx] == "tablet" && (userAgent.indexOf('trident') != -1 || userAgent.indexOf('rv:11') != -1)){
                isMobile = false;
            }
            idx++;
        }
        return isMobile;
    },
    customscrollbar: function ($ele) {
    	var $scrollbar = $ele == null ? $('.custom-scrollbar') : $ele,
    		$id = "";
    	$scrollbar.each(function (i) {
    		if ($(this).attr('id') == undefined || $(this).attr('id') == null || $(this).attr('id').indexOf('scrollbar-') != -1) {
    			$(this).attr('id', 'scrollbar-' + i);
    		}
    		$id = $(this).attr('id');
    		if ($(this).hasClass('ps-container')) {
    			Ps.destroy(document.getElementById($id));
    		}
    		Ps.initialize(document.getElementById($id), {
    			wheelSpeed: 1,
    			wheelPropagation: true,
    			minScrollbarLength: 20
    		});
    	});
    },
    numberType: function(){
    	$(document).on('click', '.input-number span.up', function(){
    		var $val = $(this).siblings('input[type="number"]').val();
    			$val = $val == "" ? 1 : parseInt($val);
    			$val += 1;
    			$(this).siblings('input[type="number"]').val($val);
    			if($(this).closest('.item-quantity').length>0)
    			{
    				$('#update-cart').trigger('click');
    			}
    	}).on('click', '.input-number span.down', function(){
    		var $val = $(this).siblings('input[type="number"]').val();
				$val = $val == "" ? 1 : parseInt($val);
				$val -= 1;
				if($val == 0){$val = 1;}
				$(this).siblings('input[type="number"]').val($val);
				if($(this).closest('.item-quantity').length>0)
    			{
    				$('#update-cart').trigger('click');
    			}
    	});
    },
    
     numberUpdate: function(){
    	 $(document).on('blur','.input-number .input-text',function(){
    		 var $val = $(this).siblings('input[type="number"]').val();
    		 $val = $val == "" ? 1 : parseInt($val);
    		 $(this).siblings('input[type="number"]').val($val);
    		 if($(this).closest('.item-quantity').length>0)
 			{
 				$('#update-cart').trigger('click');
 			}
    		 
    	 });
     },
    getCookie: function(name) {
        var value = '; ' + document.cookie,
            parts = value.split('; ' + name + '=');
        if (parts.length == 2) {
            return parts.pop().split(';').shift();
        }
        return '';
    },
    createCookie: function(name,value,days){
        var expires = '',
            $days = !days ? 1/24 : parseInt(days);
        if ($days) {
            var date = new Date();
            date.setTime(date.getTime()+($days*24*60*60*1000));
            expires = '; expires='+date.toUTCString();
            document.cookie = name+'='+value+expires+'; path=/';
        }

    },
    removeCookie: function(name) {
        var expires = '';
        this.createCookie(name,-1,-1);
    },
    validateaddressformfield: function(fieldValue, Regex) {
    	var RegExObj = new RegExp(Regex, "gi");
    	if(RegExObj.test(fieldValue)) {
    		return fieldValue.replace(RegExObj,'');
    	}else {
    		return;
    	}
    }
};

module.exports = util;
