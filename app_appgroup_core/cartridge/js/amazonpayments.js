'use strict';

var ajax = require('./ajax'),
    util = require('./util');

var initAddressWidget = function() {
    var amazonReferenceID = '';
    new OffAmazonPayments.Widgets.AddressBook({
        sellerId: SitePreferences.AP_SELLER_ID,
        onOrderReferenceCreate: function(orderReference) {
            amazonReferenceID = orderReference.getAmazonOrderReferenceId();
        },
        onAddressSelect: function() {
            // Replace the following code with the action that you want to perform
            // after the address is selected.
            // The amazonOrderReferenceId can be used to retrieve
            // the address details by calling the GetOrderReferenceDetails
            // operation. If rendering the AddressBook and Wallet widgets on the
            // same page, you should wait for this event before you render the
            // Wallet widget for the first time.
            // The Wallet widget will re-render itself on all subsequent
            // onAddressSelect events, without any action from you. It is not
            // recommended that you explicitly refresh it.
            var getOrderReferenceUrl = util.appendParamToURL(Urls.getOrderReferenceDetails, 'orderReference', amazonReferenceID);
            ajax.getJson({
                url: getOrderReferenceUrl,
                callback: function(data) {
                    if (data.success == false) {
                        $('#errorMessage').html(data.errorMessage).show();
                        $('[name$="shippingAddress_save"]').attr('disabled', 'disabled');
                    } else {
                        $('#errorMessage').hide();
                        util.fillAddressFields(data, $('.checkout-shipping.address'));
                        $('.checkout-shipping.address').find('[name$="postal"]').trigger('change');
                        $('[name$="shippingAddress_save"]').removeAttr('disabled');
                    }
                }
            });

        },
        design: {
            designMode: 'responsive'
        },
        onError: function(error) {
            //error handling code
            $('#errorMessage').html(error.getErrorMessage()).show();
        }
    }).bind('addressBookWidgetDiv');
};

var initWalletWidget = function() {
    new OffAmazonPayments.Widgets.Wallet({
        sellerId: SitePreferences.AP_SELLER_ID,
        onPaymentSelect: function() {
            // Replace this code with the action that you want to perform
            // after the payment method is selected.
            ajax.getJson({
                url: Urls.setOrderReferenceDetails,
                callback: function(data) {
                    if (data.success == false) {
                        $('#errorMessage').html(data.errorMessage).show();
                        $('[name$="billing_save"]').attr('disabled', 'disabled');
                    } else {
                        $('#errorMessage').hide();
                        $('[name$="billing_save"]').removeAttr('disabled');
                    }
                }
            });
        },
        design: {
            designMode: 'responsive'
        },
        onError: function(error) {
            //error handling code
            $('#errorMessage').html(error.getErrorMessage()).show();
        }
    }).bind('walletWidgetDiv');
};

/*Module Exports*/
exports.initAddressWidget = initAddressWidget;
exports.initWalletWidget = initWalletWidget;
