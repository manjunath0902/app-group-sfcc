'use strict';

var ajax = require('./ajax'),
    progress = require('./progress'),
    checkout = require('./pages/checkout'),
    util = require('./util'),
    tooltip = require('./tooltip'),
    validator = require('./validator'),
    login = require('./login'),
    dialog = require('./dialog');

var $cache = {},
    isSPCLogin = false,
    isSPCShipping = false,
    isSPCMultiShipping = false,
    isSPCBilling = false,
    addressFieldRegex = SitePreferences.ADDRESSFIELD_REGEX;

function initializeCache() {
    $cache = {
        main: $('#main'),
        primary: $('#primary'),
        secondary: $('#secondary')
    };
}

function initializeDom() {
    isSPCLogin = $('.spc-login').length > 0;
    isSPCShipping = $('.spc-shipping').length > 0;
    isSPCMultiShipping = $('.spc-multi-shipping').length > 0;
    isSPCBilling = $('.spc-billing').length > 0;
}

function initializeEvents() {

    if (isSPCLogin) {
        loadSPCLogin();
        login.init();
    } else if (isSPCShipping) {
        loadSPCShipping();
    } else if (isSPCMultiShipping) {
        loadSPCMultiShipping();
    } else if (isSPCBilling) {
        loadSPCBilling();
    }

    var $form = $('.address');
    handleErrors($form);
    
    if (isGoogleAnalyticsEnabled) {
        require('./ga').spc();
    }

    $('.pt_checkout').ajaxError(function () {
        window.location.href = Urls.cartShow;
    });

    $('a.tooltip').click(function (e) {
        e.preventDefault();
    });

    $('.privacy-policy').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });
    updateBillingPromo();
    
    //APPG-775 login logo navigation
    if($('.checkout-header-section .main-container-link').length > 0 && $('.user-logged-in').length == 0){
    	$('.checkout-header-section .main-container-link').on('click',function(e){
    		 e.preventDefault();
    		 window.location = $(this).find('.checkout-userheader.user-account').attr('href');
    	});
    }
    
    $cache.primary.on('click', '.checkout-tab-head[data-href], .billingtoshipping[data-href], .summarytobilling[data-href]', function (e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('href'),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                validator.init();
                if ($cache.primary.find('.spc-login').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    checkout.init();
                }
                progress.hide();
                updateAnchor();
                updateSummary();
            }
        });
    });

    $cache.primary.on('click', '.checkout-tab-head.open[data-refreshurl]', function (e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).data('refreshurl'),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                validator.init();
                if ($cache.primary.find('.spc-login').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    checkout.init();
                }
                progress.hide();
                updateSummary();
            }
        });
    });

    $cache.secondary.on('click', '.order-component-block a, .order-totals-table .order-shipping a', function (e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                updateAnchor();
                updateSummary();
            }
        });
    });

    $cache.primary.on('submit', 'form.address', function (e) {
        if ($(this).find('button[class$=-btn]').length > 0) {
            e.preventDefault();
            $(this).find('button[class$=-btn]').click();
        }
    });
    
}

function handleErrors($form) {
	if ($form.length > 0) {
		//APPG-503
		if ($form.find('select[name$="_addressList"]').length > 0 && $form.find('select[name$="_addressList"]').find('option:selected').data('address').length > 0) {
			$form.find('[name$="state"]').blur();
	    	$form.find('[name$="postal"]').trigger('change');
		} else if ($form.find('[name$="postal"]').val() != '') {
			$form.find('[name$="state"]').blur();
			$form.find('[name$="postal"]').trigger('change');
		}
		
	}
}

function loadSPCLogin() {
    updateSummary();
    $cache.primary.find('.spc-login').trigger('ready');
    $cache.primary.on('click', '.spc-login-btn:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                validator.init();
                if ($cache.primary.find('.spc-login').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    checkout.init();
                    updateCustomerInfo();
                }
                progress.hide();
                updateAnchor();
                var $form = $cache.primary.find('.address');
                handleErrors($form);
            }
        });
    });

    $cache.primary.on('click', '.spc-guest-btn:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                validator.init();
                if ($cache.primary.find('.spc-login').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    checkout.init();
                }
                progress.hide();
                updateAnchor();
                var $form = $cache.primary.find('.address');
                handleErrors($form);
            }
        });
    });
}

function loadSPCShipping() {
    $cache.primary.on('click', '.spc-shipping-btn:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                updateSummary();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                if ($cache.primary.find('.spc-shipping').length === 0) {
                    updateAnchor();
                } else {
                    updateAnchor(true);
                }
                var $form = $cache.primary.find('.address');
                handleErrors($form);
                $( document ).ajaxComplete(function( event, request, settings ) {
                	$('tr.order-total').find('td.totallabel').text(Resources.O_TOTAL);
                });
            }
        });
    });

    $cache.primary.on('click', '.shiptomultiplebutton:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find('input[name="csrf_token"]').attr('name'), form.find('input[name="csrf_token"]').attr('value'));
        progress.show($cache.primary);
        ajax.load({
            url: url,
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                updateSummary();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                updateAnchor();
            }
        });
    });
    if (SitePreferences.ENABLE_UK_CHECKOUT === true) {
	    $cache.primary.on('change', 'select[name="dwfrm_singleshipping_shippingAddress_addressFields_country"]', function (e) {
	        e.preventDefault();
	        var url = Urls.updateStateDropDown;
	        url = util.appendParamToURL(url, 'selectedCountry', $(this).val());
	        progress.show($cache.primary);
	        ajax.load({
	            url: url,
	            type: 'GET',
	            callback: function (data) {
	                //$cache.primary.html(data);
	            	var result = $(data).find("div.custom-select");
	            	$cache.primary.find("select[name$='states_state']").closest('.field-wrapper').html(result);
	                updateSummary();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                validator.init();
	                checkout.init();
	                if ($cache.primary.find('.spc-shipping').length === 0) {
	                    updateAnchor();
	                } else {
	                	progress.hide();
	                    updateAnchor(true);
	                    if($('.checkout-shipping .select-address .address-select').length > 0 && $('.checkout-shipping .select-address .address-select').find(':selected').length > 0){
	                    	var selectedStateCode = JSON.parse($('.checkout-shipping .select-address .address-select').find(':selected').attr('data-address')).stateCode;
	                    	var selectedCountryCode = JSON.parse($('.checkout-shipping .select-address .address-select').find(':selected').attr('data-address')).countryCode;
	                    	if(selectedCountryCode == $('.checkout-shipping select[name$="_country"]').val()){
	                    		$('.checkout-shipping select[name$="_state"]').val(selectedStateCode).trigger('change');
	                    		$('.checkout-shipping select[name$="_state"]').blur();
	                    	}
	                    }
	                }
	            }
	        });
	    });
    }
    
    // Validate Address field to avoid sending '\n \r' APPG-1011
    $('form.checkout-shipping').on('blur', 'input', function (e) {
    	var shippingfieldval = $(this).val();
    	if (shippingfieldval != '' && addressFieldRegex != ''){
    		var filteredShipValue = util.validateaddressformfield(shippingfieldval, addressFieldRegex);
    		if (filteredShipValue != undefined) {
    			$(this).val(filteredShipValue);
    		}
    	}
    });
}

function loadSPCMultiShipping() {
    $cache.primary.on('click', '.spc-multi-shipping-btn:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        $('<input type="hidden" name="' + $(this).attr('name') + '" value="true" />').appendTo(form);
        form.validate();
        if (!form.valid()) {
            return false;
        }
        progress.show($cache.primary);
        ajax.load({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            callback: function(data) {
                $cache.primary.html(data);
                handleBasketError();
                updateSummary();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                updateAnchor();
            }
        });
    });

    $cache.primary.on('click', '.shiptosinglebutton:not(:disabled)', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');

        var url = util.appendParamToURL(form.attr('action'), $(this).attr('name'), true);
        url = util.appendParamToURL(url, form.find('input[name="csrf_token"]').attr('name'), form.find('input[name="csrf_token"]').attr('value'));
        progress.show($cache.primary);
        ajax.load({
            url: url,
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                updateSummary();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                updateAnchor();
            }
        });
    });

    $cache.primary.on('click', '.item-shipping-address a', function (e) {
        e.preventDefault();
        progress.show($cache.primary);
        ajax.load({
            url: $(this).attr('href'),
            type: 'POST',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
                updateAnchor();
            }
        });
    });

}

function loadSPCBilling() {

    $cache.primary.on('click', '.spc-billing-btn:not(:disabled)', function (e) {
        if ($('input[name$=_selectedPaymentMethodID]:checked').val() === 'PayPal') {
            $(this).removeClass('spc-billing-btn');
            //$(this).closest('form').find('#PaymentMethod_CREDIT_CARD').removeAttr('aria-required')
            return true;
        }
        
        e.preventDefault();
        
        // As, News letter subscription is OOS so commenting out this code
//        if (isGTMEnabled) {
//            require('./gtm').newsLetterSignupCheckout();
//        }
        
        var form = $(this).closest('form');
        if ($('input.selectedPaymentMethod').val() != "PayPal") {
            form.validate();
            if (!form.valid()) {
                return false;
            }
        }
        progress.show($cache.primary);
        ajax.load({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            callback: function (data) {
                //$cache.primary.html(data);
                handleBasketError();
                //updateSummary();
                initSPC();
                if ($cache.primary.find('.spc-summary').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    validator.init();
                }
                checkout.init();
                if ($cache.primary.find('.spc-billing').length === 0) {
                    updateAnchor();
                } else {
                    updateAnchor(true);
                }
                progress.hide();
                $('button.placeorderatbilling').trigger('click');
            }
        });
    });
    
    $cache.primary.on('change', 'input[id$="billingAddress_sameasshippingaddress"]', function (e) {
    	var shippingsameasbilling;
    	if ($("#dwfrm_billing_billingAddress_sameasshippingaddress").prop('checked')){
    		shippingsameasbilling = true;
    	} else {
    		shippingsameasbilling = false;
    	}
    	
    	progress.show($cache.primary);
    	ajax.load({
            url: Urls.BillingSameAsShipping,
            data: {
                shippingsameasbilling: shippingsameasbilling
            },
            type: 'POST',
            callback: function (data) {
                $('.billing-address').html(data);
                handleBasketError();
                //updateSummary();
                initSPC();
                if ($cache.primary.find('.spc-summary').length === 0) {
                    tooltip.init();
                    util.limitCharacters();
                    validator.init();
                }
                checkout.init();
                if ($cache.primary.find('.spc-billing').length === 0) {
                    updateAnchor();
                } else {
                    updateAnchor(true);
                }
                updateBillingPromo();
                progress.hide();
            }
        });
    });

    $cache.primary.on('click', '.redemption.giftcert a.remove', function (e) {
        e.preventDefault();
        progress.show($cache.primary);
        var url = $(this).attr('href');
        ajax.load({
            url: url,
            data: {},
            type: 'GET',
            callback: function (data) {
                $cache.primary.html(data);
                handleBasketError();
                updateSummary();
                initSPC();
                tooltip.init();
                util.limitCharacters();
                validator.init();
                checkout.init();
                progress.hide();
            }
        });
    });
    
    // Validate Address field to avoid sending '\n \r' APPG-1011
    $('form.checkout-billing').on('blur', 'input', function (e) {
    	var billingfieldval = $(this).val();
    	if (billingfieldval != '' && addressFieldRegex != ''){
    		var filteredBillValue = util.validateaddressformfield(billingfieldval, addressFieldRegex);
    		if (filteredBillValue != undefined) {
    			$(this).val(filteredBillValue);
    		}
    	}
    });
    
    $cache.primary.on('change', 'select[name="dwfrm_billing_billingAddress_addressFields_country"]', function (e) {
        e.preventDefault();

        if($("#dwfrm_billing_billingAddress_sameasshippingaddress").prop('checked')){
            $('input[id$="billingAddress_sameasshippingaddress"]').attr("checked", false);
        }
        var url = Urls.updateBillingStateDropDown;
        var updatedCountry = $(this).val();
        var isDiffCountry = true;
        
        // Check if tempBillCntryCookie cookie is available and if so is it the value same as the current country code
        if ((document.cookie.length > 0) && (document.cookie.indexOf("tempBillCntryCookie") > 0)) {
            var cookieArray = document.cookie.split(';');
            for (var i=0; i<cookieArray.length; i++) {
                var currCookie = cookieArray[i];
                if (currCookie == updatedCountry) {
                    isDiffCountry = false;
                    document.cookie = "tempBillCntryCookie" + '=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    break;
                }
            }
        }
        
        if (isDiffCountry) {
	        url = util.appendParamToURL(url, 'selectedCountry', updatedCountry);
	        progress.show($cache.primary);
	        ajax.load({
	            url: url,
	            type: 'GET',
	            callback: function (data) {
	                $cache.primary.find("div.billing-address-formfields").html(data);

	                // Store the updated country in a cookie and update the country in the select field again.
	                document.cookie="tempBillCntryCookie"+"="+updatedCountry+"; path=/";
	                $('.checkout-billing select[name$="_country"]').val(updatedCountry);
	                updateSummary();
	                initSPC();
	                tooltip.init();
	                util.limitCharacters();
	                if($('input[id="clearBillingFields"]').val() != 'true'){
	                	validator.init();
	                }
	                checkout.init();
	                if ($cache.primary.find('.spc-shipping').length === 0 && $cache.primary.find('.spc-billing').length === 0) {
	                    updateAnchor();
	                } else {
	                    progress.hide();
	                    updateAnchor(true);
	                    if($('.checkout-billing .select-address .address-select').length > 0 && $('.checkout-billing .select-address .address-select').find(':selected').length > 0){
	                    	var jsonData = JSON.parse($('.checkout-billing .select-address .address-select').find(':selected').attr('data-address'));
	                    	var selectedStateCode = jsonData.stateCode;
	                        var selectedCountryCode = jsonData.countryCode;
	                        var countryCode = $('.checkout-billing select[name$="_country"]').val();
		                    if(selectedCountryCode == countryCode){
		                    	if(countryCode == 'US' || countryCode == 'CA'){
			                        $('.checkout-billing select[name$="_state"]').val(jsonData.stateCode).trigger('change');
				                    $('.checkout-billing select[name$="_state"]').blur();
		                    	}else{
			                        $('.checkout-billing input[name$="_state"]').val(jsonData.stateCode).trigger('change');
				                    $('.checkout-billing input[name$="_state"]').blur();
		                    	}
		                    }
		                    if($('input[id="clearBillingFields"]').val() == 'true'){
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_firstName"]').val(jsonData.firstName);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_lastName"]').val(jsonData.lastName);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_address1"]').val(jsonData.address1);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_address2"]').val(jsonData.address2);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_city"]').val(jsonData.city);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_postal"]').val(jsonData.postalCode);
		                    	$('.checkout-billing input[name$="billingAddress_addressFields_phone"]').val(jsonData.phone);
		                    }
		                    $('input[id="clearBillingFields"]').val(false);
	                    }
	                }
	            }
	        });
        }
    });
}

function updateBillingPromo() {
	$('.couponcode-main .coupon-head').on('click', function() {
    	$(this).toggleClass('active');
    	$(this).closest('.couponcode-main').find('.billing-coupon-code').toggleClass('active');
    });
}

function updateSummary() {
    var url = Urls.summaryRefreshURL;
    var summary = $('#secondary.summary');
    summary.load(url, function () {
        summary.find('.checkout-mini-cart .minishipment .header a').hide();
        summary.find('.order-totals-table .order-shipping .label a').hide();
        if ($cache.primary.find('.spc-summary').length > 0) {
        	summary.addClass('order-review');
        	$cache.primary.addClass('order-primary');
        } else {
        	summary.removeClass('order-review');
        	$cache.primary.removeClass('order-primary');
        }
        
        var $h = 0;
    	if($('.checkout-mini-cart .mini-cart-product').length > 4){
    		$.each($('.checkout-mini-cart .mini-cart-product').splice(0,4), function(){
    			$h += $(this).outerHeight();
    		});
    		$('.checkout-mini-cart').height($h);
    		util.customscrollbar();
    	}else{
    		$('.checkout-mini-cart').height('auto');
    	}
    	
    	if($(window).width() > 767 && util.isMobile()){
    		$('#primary, #secondary').removeAttr('style').syncHeight();
    	}else{
    		$('#primary, #secondary').height('auto');
    	}
    	
    });
}

function updateAnchor(noDefault) {
    var erroredElements = $cache.primary.find('.form-row.error:visible').filter(function (i, ele) {
        return $.trim($(ele).html()).length > 0;
    });

    if (erroredElements.length > 0) {
        jQuery('html, body').animate({
            scrollTop: erroredElements.first().position().top
        }, 500);

    } else if ($('.billing-error').length > 0) {
        jQuery('html, body').animate({
            scrollTop: $('.billing-error').position().top
        }, 500);
    } else if (!noDefault) {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 500);
    }
}

function handleBasketError() {
    if ($cache.primary.find('.pt_cart').length > 0) {
        window.location.href = Urls.cartShow;
    }
}

function updateCustomerInfo() {
    ajax.load({
        url: Urls.customerInfo,
        type: 'GET',
        async: false,
        callback: function (data) {
            $('#navigation').find('.menu-utility-user').find('.user-info').remove();
            $(data).appendTo('#navigation .menu-utility-user');
            $('.user-account').on('click', function (e) {
            	e.preventDefault();
            	if($(this).closest(".user-logged-in").length > 0){
        			$(this).closest('.user-info').toggleClass('active');
        		}else if($(this).closest(".user-logged-in").length == 0 && window.pageContext.type == 'checkout'){
        			$(this).parent('.user-info').toggleClass('active');
        		}else{
        			$(this).parent('.user-info').toggleClass('active');
        		}
            });
        }
    });
}

function initSPC() {
    initializeCache();
    initializeDom();
    initializeEvents();
    util.updateselect();
}

exports.init = function () {
    initializeCache();
    initializeDom();
    initializeEvents();
};
