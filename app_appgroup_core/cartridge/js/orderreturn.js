'use strict';

var dialog = require('./dialog'),
    page = require('./page'),
    progress = require('./progress'),
    validator = require('./validator'),
    util = require('./util');

var returnOrder = {
    /**
     * @private
     * @function
     * @description init events for the orderReturn Page
     */
    init: function () {
    	// Handling checkbox event for order return.
    	$('input[id$="_returnproducts_selectproduct"]').one("click", function(){
    		var $this = $(this);
    		$this.closest('div.line-item').find('div.qtyoforderreturn .field-wrapper').append("<span class='up icons-sprite'></span>");
    		$this.closest('div.line-item').find('div.qtyoforderreturn .field-wrapper').append("<span class='down icons-sprite'></span>");
    	});
    	$('input[id$="_returnproducts_selectproduct"]').click(function() {
    		var $this = $(this);
    		
    		// Check if we have required CD details for order return.
			var pdtChainDriveData = $this.closest('div.line-item').find('div.chainDriveDataForReturn');
			var PName = $this.closest('div.line-item').find('div.name a').text();
			var webDeliveryNumber, cDDeliveryNumber;
			if(pdtChainDriveData.length > 0) {
				webDeliveryNumber = $this.closest('div.line-item').find('div.chainDriveDataForReturn .webDeliveryNumberForReturn').val();
				cDDeliveryNumber = $this.closest('div.line-item').find('div.chainDriveDataForReturn .cdDeliveryNumberForReturn').val();
				if(cDDeliveryNumber == "null"){
					$this.prop('checked', false); 
					window.alert("CDDeliveryNumber for the product "+PName+" is not available, so we can not process this product for return.");
					return;
				}else if(webDeliveryNumber == "null"){
					$this.prop('checked', false);
					window.alert("WebDeliveryNumber for the product "+PName+" is not available, so we can not process this product for return.");
					return;
				}
			}else{
				$this.prop('checked', false);
				window.alert("Delivery information for the product "+PName+" is not available, so we can not process this product for return.");
				return;
			}
    		
    		if($(window).width() > 767) {
    			$this.closest('div.line-item').find('div.reasonfororderreturn').toggle();
    		}
    		if($(window).width() < 768) {
        		$this.closest('div.line-item').find('div.reasonfororderreturns.mobile').toggle();
    		}
    		$this.closest('div.line-item').find('div.pdt-qty').toggle();
    		$this.closest('div.line-item').find('div.return-pdt-qty').toggle();
    		
    		chkForReturnSubmittBtn($this);
    		
    		if(!$this.prop("checked")){
    			$this.closest('div.line-item').find('span.error').hide();
    		}
    	});
    	
    	// Handling select option event
    	$('div.line-items select[id$="_returnproducts_requestreason"]').change(function(){
    		var checkerror = $(this).closest('.line-item-details');
    		var firstReturnReason = $(this).closest("form").find('input.firstReturnReasonValue').val();
    		($(window).width() > 767) ?  checkerror = checkerror.find('span.returnreasonerror') : checkerror.find('span.returnreasonerrormobile'); 
    		($(this).find('option:selected').text() == firstReturnReason) ? checkerror.show() : checkerror.hide();
    		chkForReturnSubmittBtn($(this));
    	});
    	
    	// Handling input quantity event
    	$('div.line-items input[id$="_returnproducts_prodqtytoreturn"]').on('keyup change', function (){
    		var $this = $(this);
    		var err = $this.closest('.line-item-quantity').find('span.pdtqtyerror');
    		var orderedquantity = $this.closest('.line-item-quantity').find('.pdt-qty').text();
    		($this.val() < 1) ? $this.val(1) : $this.val() >= parseInt(orderedquantity) ? ($this.val(parseInt(orderedquantity)), err.hide()) : err.hide();
    		chkForReturnSubmittBtn($this);
    	});
    	
    	// Handling up and down buttons
    	$(document).on('click', '.field-wrapper span.up', function(){
    		var $this = $(this);
    		var $val = $this.siblings('input[type="number"]').val();
    			$val = $val == "" ? 1 : parseInt($val);
    			$val += 1;
    			$this.siblings('input[type="number"]').val($val);
    			var orderedquantity = $this.closest('.line-item-quantity').find('.pdt-qty').text();
    			var err = $this.closest('.line-item-quantity').find('span.pdtqtyerror');
    			($val < 1) ? ($this.siblings('input[type="number"]').val(1), err.hide()) : $val >= parseInt(orderedquantity) ? ($this.siblings('input[type="number"]').val(parseInt(orderedquantity)), err.hide()) : err.hide();
    			chkForReturnSubmittBtn($this);
    	}).on('click', '.field-wrapper span.down', function(){
    		var $val = $(this).siblings('input[type="number"]').val();
    			$val = $val == "" ? 1 : parseInt($val);
    			$val -= 1;
    			if($val <= 0){$val = 1;}
    			$(this).siblings('input[type="number"]').val($val);
    			chkForReturnSubmittBtn($(this));
    	});
    	
    	
    	$('button[name$="_returnproducts_confirm"]').click(function(e) {
    		e.preventDefault();
    		var $this = $(this);
    		var url = $this.closest("form").attr('action');
    		var firstReturnReason = $this.closest("form").find('input.firstReturnReasonValue').val();
    		var CDOrderId = $('div.order-number-information').find('span.value').text();
    		url = util.appendParamToURL(url, 'CDOrderId', CDOrderId);
    		var allOrderDiv = $this.closest("form").find('div.line-item');
    		var count = allOrderDiv.length;
    		var returnOrderObject = {};
    		var RMADetail = [];
    		
    		for(var i=0; i<count; i++){
    			var pdtselectedtoreturn = allOrderDiv.eq(i).find('input[id$="_returnproducts_selectproduct"]').prop("checked");
    			var errinfield = allOrderDiv.eq(i).find('span.error').is(":visible"); // check if there is error in any of the fields of an order
    			if (pdtselectedtoreturn){
    				var prodObject = {};
    				prodObject.CDOrderId = CDOrderId;
    				prodObject.SkuId = allOrderDiv.eq(i).find('div.sku .value').text();
    				prodObject.ProductName = allOrderDiv.eq(i).find('div.name a').text();
    				
    				// Collects CD data for creating request.
					var pdtChainDriveData = allOrderDiv.eq(i).find('div.chainDriveDataForReturn');
    				if(pdtChainDriveData.length > 0) {
    					prodObject.WebDeliveryNumber = allOrderDiv.eq(i).find('div.chainDriveDataForReturn .webDeliveryNumberForReturn').val();
    					prodObject.CDDeliveryNumber = allOrderDiv.eq(i).find('div.chainDriveDataForReturn .cdDeliveryNumberForReturn').val();
    					prodObject.cdcustId = allOrderDiv.eq(i).find('div.chainDriveDataForReturn .cdcustId').val();
    					prodObject.cdorderId = allOrderDiv.eq(i).find('div.chainDriveDataForReturn .cdorderId').val();
    				}
    				
    				// Validation for return reason selection.
    				if($(window).width() > 767) {
    					prodObject.ReturnReason = allOrderDiv.eq(i).find('.reasonfororderreturn option:selected').text();
    				}else{
    					prodObject.ReturnReason = allOrderDiv.eq(i).find('.reasonfororderreturns option:selected').text();
    				}
    				if(prodObject.ReturnReason == firstReturnReason){
    					($(window).width() > 767) ? allOrderDiv.eq(i).find('span.returnreasonerror').show() : allOrderDiv.eq(i).find('span.returnreasonerrormobile').show();
    					errinfield = true;
    				}
    				prodObject.Qty = parseInt(allOrderDiv.eq(i).find('input[id$="_returnproducts_prodqtytoreturn"]').val());
					if(prodObject.Qty == 0){
						allOrderDiv.eq(i).find('span.pdtqtyerror').show();
						errinfield = true;
					}
    				
    				// If error is present in any of the field then don't create object to make service call to Chain drive
    				if (errinfield) {
    					continue;
    				}else{
    					RMADetail.push(prodObject);
    				}
    			}
    		}
    		returnOrderObject.RMADetail = RMADetail;
    		
    		var anyErrInForm = $this.closest("form").find('div.line-items').find('span.error').is(":visible");
    		if($.isEmptyObject(RMADetail) || errinfield || anyErrInForm){
    			// If no product is selected to return then avoiding service call.
    			return;
    		}else{
    			// make the service call
    			progress.show($('#primary'));
                $.ajax({
                    type: 'POST',
                    cache: false,
                    contentType: 'application/json',
                    url: url,
                    data: JSON.stringify(returnOrderObject)
                })
                .done(function (data) {
                    // success
                	$this.closest('div.order-return-details').empty().html(data);
                	$('div.order-return-details').find('input.errorinCD').val() == "true" ? location.reload() : '';
                	progress.hide();
                })
                .fail(function (xhr, textStatus) {
                	var test = $this;
                	progress.hide();
                    // failed
                    if (textStatus === 'parsererror') {
                        window.alert(Resources.BAD_RESPONSE);
                    } else {
                        window.alert(Resources.SERVER_CONNECTION_ERROR);
                    }
                });
    		}
    		
    	});
    }
}

//Handling order return submit button
function chkForReturnSubmittBtn($this) {
		var form = $this.closest("form");
		var $subOrderreturnbtn = form.find('button[name$="_returnproducts_confirm"]');
		var allPdtDiv = form.find('div.line-item');
		var pleaseSelectReason = form.find('input.firstReturnReasonValue').val();
		
		var i = 0;
		while (i < allPdtDiv.length) {
			var prodReturnReason;
			var prodQty;
			if(allPdtDiv.eq(i).find('input:checked').length >= 1){
				if($(window).width() > 767) {
					prodReturnReason = allPdtDiv.eq(i).find('.reasonfororderreturn option:selected').text();
				}else{
					prodReturnReason = allPdtDiv.eq(i).find('.reasonfororderreturns option:selected').text();
				}
				if (prodReturnReason != pleaseSelectReason){
					prodQty = parseInt(allPdtDiv.eq(i).find('input[id$="_returnproducts_prodqtytoreturn"]').val());
					if(prodQty > 0){
						$subOrderreturnbtn.is(":disabled") ? $subOrderreturnbtn.removeAttr('disabled') : '';
						break;
					}
				}else{
					$subOrderreturnbtn.attr("disabled", "disabled");
				}
			}else{
				$subOrderreturnbtn.attr("disabled", "disabled");
			}
		    i++;
		}
	}

module.exports = returnOrder;