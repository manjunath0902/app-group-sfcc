'use strict';

var ajax = require('../../ajax'),
    image = require('./image'),
    progress = require('../../progress'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    fitpredictor = require('../../fitpredictor'),
    pdpSticky = require('./pdpSticky'),
    imagesLoaded = require('imagesloaded');


/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {
    var $pdpForm = $('.pdpForm');
    var qty = $pdpForm.find('input[name="Quantity"]').first().val();
    var params = {
        Quantity: isNaN(qty) ? '1' : qty,
        format: 'ajax',
        productlistid: $pdpForm.find('input[name="productlistid"]').first().val()
    };

    progress.show($('#pdpMain'));

    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $('#product-content'),
        callback: function () {
            if (SitePreferences.STORE_PICKUP) {
                productStoreInventory.init();
            }
            if($('.availability-dynamic-message').data('siteid')=="MK") {
            	$('.availability-dynamic-message').addClass('active');
            }
            image.replaceImages();
            image.replaceVideo();
            imagesLoaded('.product-primary-image').on('always', function () {
            	pdpSticky();
            });
            $('#Quantity').trigger('change'); //JIRA PREV-235:PD page: 'Not Available' message is not displaying when more than Available Qty entered in Qty field.
            tooltip.init();
            $('#product-content').trigger('ready'); // PREVAIL- Added for Gigya integration
            wishlistFeature();
            if ($(window).width() < 767) {
                imagesLoaded('.primary-image-list').on('always', function () {
                	var mainHeight = $('.primary-image-list li:first-child').height();
                	$('.product-primary-image').height(mainHeight); 
                });
            }
        }
    });
};

var wishlistFeature = function(){
	$('.loggedin-heart-icon').on('click', function() {
		var url = $(this).data('url');
		$.ajax({
            url: url,
            success: function (response) {
            	$('.loggedin-heart-icon').addClass('wishlist-added');
            	$('#wishlist-response').dialog({
            		dialogClass : 'wishlist-dialog',
            		width: 340,
            		open: function() {
            			$('.wishlist-dialog').css({top:264,left:307});
            			$('#wishlist-response').css('min-height', 'auto');
            		}
            	});
            }
		 });
	});
}

module.exports = function() {
    var $pdpMain = $('#pdpMain');
    var $quickViewPDP = $('#QuickViewDialog #pdpMain');
    wishlistFeature();
    // hover on swatch - should update main image with swatch image
    $quickViewPDP.on('mouseenter mouseleave', '.swatchanchor', function () {
        var largeImg = $(this).data('lgimg'),
            $imgZoom = $pdpMain.find('.main-image'),
            $mainImage = $pdpMain.find('.primary-image');

        if (!largeImg) {
            return;
        }
        // store the old data from main image for mouseleave handler
        $(this).data('lgimg', {
            hires: $imgZoom.attr('href'),
            url: $mainImage.attr('src'),
            alt: $mainImage.attr('alt'),
            title: $mainImage.attr('title')
        });
        // set the main image
        image.setMainImage(largeImg);
    });

    // click on swatch - should replace product content with new variant
    $pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable')) {
        	if($('.availability-dynamic-message').data('siteid')=="MK") {
        	   $('.availability-dynamic-message').removeClass('active');
        	}
            return;
        }
        updateContent(this.href);
    });

    // change drop down variation attribute - should replace product content with new variant
    $pdpMain.on('change', '.variation-select', function () {
        if ($(this).val().length === 0) {
            return;
        }
        updateContent($(this).val());
    });
    
    // BV read all review event
    $('.bv-custom-readallreview').on('click', function () {
        $('html, body').animate({
            scrollTop: $("div.bv-main-container").offset().top
        }, 200);
    });
    
    //Events for Fit Predictor
    var isFPEnabled = $('input[id="isFPEnabled"]').val();
    if(isFPEnabled && typeof ssp != 'undefined'){
        ssp('set', 'site', {
            currency: $('input[id="siteCurrency"]').val(),
            language: $('input[id="siteLanguage"]').val(),
            layout: fitpredictor.fpGetDeviceLayout(),
            market:  $('input[id="siteMarket"]').val()
        });
        
        if($('input[id="isloggedInCustomer"]').val() != undefined && $('input[id="isloggedInCustomer"]').val() == 'true'){
            ssp("set", "user", {
          	  userId: $('input[id="customerNumber"]').val(),
          	  email: $('input[id="customerEmail"]').val()
            });
        }
        
        var breadcrumb = '';
        $.each( $('.breadcrumb-element'), function( index ){
        	breadcrumb += $('.breadcrumb-element').eq(index).text() + ' > ';
        });
        breadcrumb = breadcrumb.substring(0, breadcrumb.length-3);
        ssp('set', 'page', {
            breadcrumb: breadcrumb,
            type: 'pdp'
        });
        
        var eventDataObject = fitpredictor.fpProductContextEvents();
        if(eventDataObject.salePrice != undefined && eventDataObject.size != undefined){
            ssp('set', 'product', {
                productId: eventDataObject.productId,
                price: eventDataObject.standardPrice,
            	salePrice: eventDataObject.salePrice,
                color: eventDataObject.color,
                sizes: eventDataObject.sizesArray,
                availableSizes: eventDataObject.availableSizesArray,
                size: eventDataObject.size
            });
        }else if (eventDataObject.salePrice != undefined){
            ssp('set', 'product', {
                productId: eventDataObject.productId,
                price: eventDataObject.standardPrice,
				salePrice: eventDataObject.salePrice,
                color: eventDataObject.color,
                sizes: eventDataObject.sizesArray,
                availableSizes: eventDataObject.availableSizesArray
            });
        }else if (eventDataObject.size != undefined){
			ssp('set', 'product', {
                productId: eventDataObject.productId,
                price: eventDataObject.standardPrice,
				color: eventDataObject.color,
                sizes: eventDataObject.sizesArray,
                availableSizes: eventDataObject.availableSizesArray,
				size: eventDataObject.size
            });
		}else{
			ssp('set', 'product', {
                productId: eventDataObject.productId,
                price: eventDataObject.standardPrice,
				color: eventDataObject.color,
                sizes: eventDataObject.sizesArray,
                availableSizes: eventDataObject.availableSizesArray
            });
		}
        
        ssp('start', 'fitpredictor');
        
    	ssp('subscribe', 'prediction', function(event) {
    		if($('.size li.selected').length == 0){
    			 if (event.service === 'fitpredictor' && event.size) {
     				var sizeSwatch = $('.product-variations .size [data-size="' + event.size +'"]:not(.unavailableVariant)');
     				
     				if (sizeSwatch && sizeSwatch.length) {
     					sizeSwatch.find('.swatchanchor').click();
     				}
                 }
    		}
        });
    }
};
