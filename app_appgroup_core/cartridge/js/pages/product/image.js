'use strict';
var dialog = require('../../dialog');
var util = require('../../util');
var qs = require('qs');
var url = require('url');
var _ = require('lodash');
var imagesLoaded = require('imagesloaded');

var zoomMediaQuery = matchMedia('(min-width: 960px)');

/**
 * @description Enables the zoom viewer on the product detail page
 * @param zmq {Media Query List}
 */
function loadZoom (zmq) {
    var $imgZoom = $('#pdpMain .main-image'),
        hiresUrl;
    if (!zmq) {
        zmq = zoomMediaQuery;
    }
    
    $imgZoom.trigger('zoom.destroy');
    
    if ($imgZoom.length === 0 || dialog.isActive() || util.isMobile() || !zoomMediaQuery.matches) {
        return;
    }
    hiresUrl = $imgZoom.attr('href');
  
    if (hiresUrl && hiresUrl !== 'null' && hiresUrl.indexOf('noimagelarge') === -1 && zoomMediaQuery.matches) {
        $imgZoom.zoom({
            url: hiresUrl
        });
    }
}

zoomMediaQuery.addListener(loadZoom);
zoomMediaQuery.addListener(thumbSlick);


function thumbSlick(){
	//slick slider
	var $slick = true;
    if($('#QuickViewDialog').is(':visible')){
    	if($('#QuickViewDialog #thumbnails ul').hasClass('slick-slider')){
	    	$('#QuickViewDialog #thumbnails ul').slick('unslick');
	    }
        if($('#QuickViewDialog #thumbnails ul li').length < ($('.site-mk').length > 0 ? 2 : 3) && $(window).width() < 481){
        	$slick = false;
        }else if($('#QuickViewDialog #thumbnails ul li').length < ($('.site-mk').length > 0 ? 4 : 5) && $(window).width() > 480 && $(window).width() < 768){
        	$slick = false;
        }else if($('#QuickViewDialog #thumbnails ul li').length < ($('.site-mk').length > 0 ? 2 : 3) && $(window).width() > 767 && $(window).width() < 960){
        	$slick = false;
        }else if($('#QuickViewDialog #thumbnails ul li').length < 3 && $(window).width() > 959){
        	$slick = false;
        }else{
        	$slick = true;
        }
        
        if($slick){
        	$('#QuickViewDialog #thumbnails ul').slick({
            	infinite: false,
            	speed: 300,
            	slidesToShow: 3,
            	slidesToScroll: 1,
            	swipe: true,
            	responsive: [{
            		breakpoint: 960,
            		settings: {
            			slidesToShow: $('.site-mk').length > 0 ? 2 : 3
            		}
            	},{
            		breakpoint: 768,
            		settings: {
            			slidesToShow: $('.site-mk').length > 0 ? 4 : 5
            		}
            	},{
            		breakpoint: 481,
            		settings: {
            			slidesToShow: $('.site-mk').length > 0 ? 2 : 3
            		}
            	}
            	]
            });
        }
    }else{
    		
    	if($('#thumbnails ul').hasClass('slick-slider')){
            $('#thumbnails ul').slick('unslick');
        }
	    $('.product-primary-image ul').each(function(){
            if($(this).hasClass('slick-slider')){
                $(this).slick('unslick');
	        }
	    });
    	$('.product-primary-image ul').slick({
    	    //infinite: false,
    	    infinite: $('.site-mk').length > 0 ? false : false,
    	    speed: 400,
    	    //slidesToShow: 1,
    	    slidesToShow:$('.site-mk').length > 0 ? $('.product-primary-image ul li').length : 1,
    	    slidesToScroll: 1,
    	    //arrows: true,
    	    arrows: $('.site-mk').length > 0 ? false : true,
    	    asNavFor:'.pdp-main #thumbnails ul',
    	    vertical: $('.site-mk').length > 0 ? true : false,
    	    responsive: [{
    	    	breakpoint: 767,
        		settings: {
        			slidesToShow: 1,
            	    slidesToScroll: 1,
        			arrows: true,
        			vertical: false,
        			infinite: false,
        		}
    	    },{
        		breakpoint: 481,
        		settings: {
        			slidesToShow: 1,
            	    slidesToScroll: 1,
        			arrows: true,
        			vertical: false,
        			infinite: false,
        		}
        	}
    	    ]
    	});

        
        
    	var $currObj = $('.product-col-1 #thumbnails ul');
        var currObjlen = 3;
        if($('.site-sk').length > 0) {
        	currObjlen = 8;
        }
        if ($currObj.find('li').length <= currObjlen){
        	$currObj.closest('#thumbnails').addClass('height');
        } else {
        	$currObj.closest('#thumbnails').removeClass('height');
        }
        $currObj.slick({
        	//infinite: false,
        	speed: 300,
        	asNavFor:'.pdp-main .product-primary-image ul',
        	slidesToShow: $('.site-mk').length > 0 ? 3 : 8,
        	slidesToScroll: 1,
        	swipe: true,
        	arrows: true,
        	infinite: $('.site-mk').length > 0 ? false : false,
        	focusOnSelect: true,
        	//vertical: $('.site-mk').length > 0 ? false : true,
        	vertical: $('.site-mk').length > 0 ? true : true,
        	adaptiveHeight: true,
        	responsive: [{
        		breakpoint: 960,
        		settings: {
        			slidesToShow: $('.site-mk').length > 0 ? 3 : 6
        		}
        	},{
        		breakpoint: 767,
        		settings: {
        			slidesToShow: $('.site-mk').length > 0 ? 5 : 6,
					vertical: false,
					infinite: false,
        		}
        	},{
        		breakpoint: 481,
        		settings: {
        			slidesToShow: $('.site-mk').length > 0 ? 3 : 4,
					vertical: false,
					infinite: false,
        		}
        	}
        	]
        });
        $('#carousel-recommendation .tiles-container').slick({
        	infinite: false,
        	arrows: true,
        	slidesToShow: 4,
    	    slidesToScroll: 1,
    	    vertical: false
        });
    }
    if($('.site-mk').length > 0) {
    	$('.product-thumbnails .slick-prev').on('click',function(){
    		var selectedThumb = $('.thumb.slick-current').attr('data-slick-index');
    		var thumbLength = $(".thumb.slick-slide").length - 1
    		if(selectedThumb == 0)
    			$("#thumbnails .slick-slider").slick('slickGoTo',thumbLength,true);
    	});
    	$('.product-thumbnails .slick-next').on('click',function(){
    		var selectedThumb = $('.thumb.slick-current').attr('data-slick-index');
    		var thumbLength = $(".thumb.slick-slide").length - 1
    		if(selectedThumb == thumbLength)
    			$("#thumbnails .slick-slider").slick('slickGoTo',0,true);
    	});
    }
}

/**
 * @description Sets the main image attributes and the href for the surrounding <a> tag
 * @param {Object} atts Object with url, alt, title and hires properties
 */
function setMainImage (atts) {
    $('#pdpMain .primary-image').attr({
        src: atts.url,
        alt: atts.alt,
        title: atts.title
    });
    updatePinButton(atts.url);
    if (!dialog.isActive() && !util.isMobile()) {
    	if(atts.hires == ""){
    		$('#pdpMain .main-image').removeAttr('href');
    	}else{
    		$('#pdpMain .main-image').attr('href', atts.hires);
    	}
        
    }
    //loadZoom();
}

function updatePinButton (imageUrl) {
    var pinButton = document.querySelector('.share-icon[data-share=pinterest]');
    if (!pinButton) {
        return;
    }
    var newUrl = imageUrl;
    if (!imageUrl) {
        newUrl = document.querySelector('#pdpMain .primary-image1').getAttribute('src');
    }
    var href = url.parse(pinButton.href);
    var query = qs.parse(href.query);
    query.media = url.resolve(window.location.href, newUrl);
    query.url = window.location.href;
    var newHref = url.format(_.extend({}, href, {
        query: query, // query is only used if search is absent
        search: qs.stringify(query)
    }));
    pinButton.href = newHref;
}

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
function replaceImages () {
    var $newImages = $('#update-images'),
        $imageContainer = $('#pdpMain .product-image-container');
    if ($newImages.length === 0) { return; }

    $imageContainer.html($newImages.html());
    $newImages.remove();
    //loadZoom();
    if($('#pdpMain .product-primary-image ul').length > 0 || $('#thumbnails ul').length > 0) { 
    	if ($(window).width() < 767) {
    		thumbSlick();
    	}
    	if ($(window).width() > 767) {
    		$('.pdp-main').addClass('load-pdp');
	    	imagesLoaded('.product-primary-image, .product-thumbnails').on('done', function () {
	    		thumbSlick();
	    		$('.pdp-main').removeClass('load-pdp');
	        });
    	}
    }
}


function replaceVideo () {
	$('.thumb .thumbnail-link').on('click',function (e) {
		if($(this).hasClass('thumbnail-videolink')) {
		    $('.product-thumbnails .selected.slick-current').addClass('deselectVal');
		} else {
		    $('.deselectVal').removeClass('deselectVal');
		}
		var $currObj = $('#pdpMain');
		$currObj.removeClass('lg-md-fixed');
		$currObj.removeClass('lg-md-absolute');
		$currObj.find('.product-thumbnails').removeClass('lg-md-thumb-absolute');
    	if($(this).closest('.thumb').hasClass('thumbvideo')){
        // switch indicator
    		$("html,body").animate({ scrollTop :  0 }, 0);
    		e.preventDefault();
    		var videoUrl = $(this).attr('data-video-url'),
    		videoImgWidth = $('.product-primary-image').width();
	        $('.video-rendering-area').addClass('active');
	        var imageHeightLi = $('.primary-image-list li:first-child').height();
	        $('.primary-image-list').addClass('primaryset');
	        //adding the height and width of the primary image to the video iframe
	        $(this).closest('.product-col-1').find('.video-rendering-area iframe').width(videoImgWidth);
	        $(this).closest('.product-col-1').find('.video-rendering-area iframe').height(imageHeightLi);
	        $(this).closest('.product-col-1').find('.video-rendering-area iframe').attr('src',videoUrl);
	        $(".product-primary-image").addClass("videoPrimary");
	        $("#thumbnails").addClass("videoPrimarythumb"); 
    	}
    	else{
    		e.preventDefault();
    		$(this).closest('.product-col-1').find('.video-rendering-area iframe').attr('src','');
    		$('.primary-image-list').removeClass('primaryset');
            $('.video-rendering-area').removeClass('active');
            $(".product-primary-image").removeClass("videoPrimary");
            $("#thumbnails").removeClass("videoPrimarythumb"); 
            thumbSlick();
    	}
   });
}

$( window ).load(function() {
	var imageHeightLi = $('.primary-image-list li:first-child').height();
    var videoImgWidth = $('.product-primary-image').width();
    $('.video-rendering-area iframe').width(videoImgWidth);
    $('.video-rendering-area iframe').height(imageHeightLi);
    
    var $window = $(window);
    var windowWidth = $window.width();
	$( window ).resize(function(e) {
		if (($(window).width() > 767 && $(window).width() < 1025)) {
			 if ($window.width() != windowWidth) {
				 	//replaceImages ();
					var imageHeightLi = $('.primary-image-list li:first-child').height();
				    var videoImgWidth = $('.product-primary-image').width();
				    $('.video-rendering-area iframe').width(videoImgWidth);
				    $('.video-rendering-area iframe').height(imageHeightLi);
		            windowWidth = $window.width();
		            
	        }
		}   
	});
});

$( window ).resize(function(e) {
	if (!($(window).width() > 767 && $(window).width() < 1024)) {
		var imageHeightLi = $('.primary-image-list li:first-child').height();
	    var videoImgWidth = $('.product-primary-image').width();
	    $('.video-rendering-area iframe').width(videoImgWidth);
	    $('.video-rendering-area iframe').height(imageHeightLi);
	} 
});

if ($(window).width() < 767) {
    imagesLoaded('.primary-image-list').on('always', function () {
    	var mainHeight = $('.primary-image-list li:first-child').height()
    	$('.product-primary-image').height(mainHeight); 
    });
}

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
    if (dialog.isActive() || util.isMobile()) {
        $('#pdpMain .main-image').removeAttr('href');
    }
    updatePinButton();
    //loadZoom();
    if($('#thumbnails ul').length > 0){ thumbSlick(); }
    // handle product thumbnail click event
    $('#pdpMain').on('click', '.productthumbnail', function () {
        // switch indicator
        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
        $(this).closest('.thumb').addClass('selected');

        setMainImage($(this).data('lgimg'));
    });
    replaceVideo();
};
module.exports.loadZoom = loadZoom;
module.exports.setMainImage = setMainImage;
module.exports.replaceImages = replaceImages;
module.exports.replaceVideo = replaceVideo;
module.exports.thumbSlick = thumbSlick; 
