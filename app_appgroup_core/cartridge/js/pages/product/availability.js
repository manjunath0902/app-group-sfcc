'use strict';

var ajax = require('../../ajax'),
    util = require('../../util'),
    bisn = require('../../bisn'),
    fitpredictor = require('../../fitpredictor');

var updateContainer = function (data, $this) {
    var currentForm = $this.closest('form[id]');
    var $availabilityMsgContainer = currentForm.find('.availability-msg'),
    //JIRA PREV-542: Inventory checks are not happening in PDP. Added $availabilityDateMsg.
        $availabilityDateMsg,
        $availabilityErrorMsg,
        $availabilityMsg;
    if (!data) {
        $availabilityMsgContainer.html(Resources.ITEM_STATUS_NOTAVAILABLE);
        return;
    }
    if (data.isMaster) {
        return false;
    }
    $availabilityMsgContainer.empty();
    // Look through levels ... if msg is not empty, then create span el
    if (data.levels.IN_STOCK > 0) {
        $availabilityMsg = $availabilityMsgContainer.find('.in-stock-msg');
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            // Just in stock or perpetual
        	if( data.ats < 3 && data.ats > 0) {
        		if ($availabilityMsg.length === 0) {
                    $availabilityMsg = $('<p/>').addClass('in-stock-left').appendTo($availabilityMsgContainer);
                }
                $availabilityMsg.text(Resources.ONLY_QUANTITY + data.ats + ' '+Resources.LEFT_QUANTITY);
        	}else if(data.ats >= 3 || data.levels.IN_STOCK > 0){
        		if ($availabilityMsg.length === 0) {
                    $availabilityMsg = $('<p/>').addClass('in-stock-msg').appendTo($availabilityMsgContainer);
                }
                $availabilityMsg.text(Resources.IN_STOCK);
        	}
        } else {
            // In stock with conditions ...
            $availabilityMsg.text(data.inStockMsg);
            if(parseInt(data.statusQuantity) > parseInt(data.ats)){
            	$availabilityErrorMsg = $availabilityMsgContainer.find('.availabilityError');
            	if($availabilityErrorMsg.length === 0){
            		$availabilityErrorMsg = $('<p/>').addClass('availabilityError').appendTo($availabilityMsgContainer);
            		$availabilityErrorMsg.text(Resources.ADJUST_QUANTITY);
            	}
            	
            }
        }
        currentForm.find('.add-to-cart').attr('disabled', false);
    }
    if (data.levels.PREORDER > 0) {
        $availabilityMsg = $availabilityMsgContainer.find('.preorder-msg');
        if ($availabilityMsg.length === 0) {
            $availabilityMsg = $('<p/>').addClass('preorder-msg').appendTo($availabilityMsgContainer);
        }
        if (data.levels.IN_STOCK === 0 && data.levels.BACKORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            // Just in stock
            $availabilityMsg.text(Resources.PREORDER);
        } else {
            $availabilityMsg.text(data.preOrderMsg);
        }
    }
    if (data.levels.BACKORDER > 0) {
        $availabilityMsg = $availabilityMsgContainer.find('.backorder-msg');
        if ($availabilityMsg.length === 0) {
            $availabilityMsg = $('<p/>').addClass('backorder-msg').appendTo($availabilityMsgContainer);
        }
        if (data.levels.IN_STOCK === 0 && data.levels.PREORDER === 0 && data.levels.NOT_AVAILABLE === 0) {
            // Just in stock
            $availabilityMsg.text(Resources.BACKORDER);
        } else {
            $availabilityMsg.text(data.backOrderMsg);
        }
    }
    if (data.inStockDate !== '') {
        //JIRA PREV-542: Inventory checks are not happening in PDP. Replaced $availabilityMsg with $availabilityDateMsg.
        $availabilityDateMsg = $availabilityMsgContainer.find('.in-stock-date-msg');
        if ($availabilityDateMsg.length === 0) {
            $availabilityDateMsg = $('<p/>').addClass('in-stock-date-msg');
        }
        $availabilityDateMsg.text(String.format(Resources.IN_STOCK_DATE, data.inStockDate));
    }
    if (data.levels.NOT_AVAILABLE > 0) {
        $availabilityMsg = $availabilityMsgContainer.find('.not-available-msg');
        if ($availabilityMsg.length === 0) {
            $availabilityMsg = $('<p/>').addClass('not-available-msg').appendTo($availabilityMsgContainer);
        }
        if (data.levels.PREORDER === 0 && data.levels.BACKORDER === 0 && data.levels.IN_STOCK === 0) {
            $availabilityMsg.text(Resources.NOT_AVAILABLE);
        } else {
            $availabilityMsg.text(Resources.REMAIN_NOT_AVAILABLE);
        }
        currentForm.find('.add-to-cart').attr('disabled', true);
        $('#add-all-to-cart').attr('disabled', true);

    } else { 
        if ($('.add-to-cart[disabled="disabled"]').length === 0) { 
            $('#add-all-to-cart').attr('disabled', false);
        } else {
            $('#add-all-to-cart').attr('disabled', true);
        }
    }

    //JIRA PREV-690: Prevail : Inventory status displayed twice for a variant product available for store pick up. Commented $availabilityMsgContainer.append($availabilityMsg);
    //$availabilityMsgContainer.append($availabilityMsg);
    //JIRA PREV-542: Inventory checks are not happening in PDP. Added code block for BACKORDER and PREORDER in-stock date.
    if (data.levels.BACKORDER > 0 || data.levels.PREORDER > 0) {
        $availabilityMsgContainer.append($availabilityDateMsg);
    }
};

var getAvailability = function () {
	// Initialize Back in stock functionality when user selects any variants
	if (isBISNEnabled) {
		bisn.init();
	}
    var $this = $(this);
    ajax.getJson({
        url: util.appendParamsToUrl(Urls.getAvailability, {
            pid: $this.closest('form').find('#pid').val(), // JIRA PREV-55:Inventory message is not displaying for the individual product within the product set.
            Quantity: $(this).val()
        }),
        async: false, //JIRA PREV-45 : Able to proceed to checkout flow with more than instock qty by clicking on Go straight to checkout in the Mini cart.
        callback: function (data) {
            updateContainer(data, $this);
        }
    });
    
    //Fit Predictor Events
    var isFPEnabled = $('input[id="isFPEnabled"]').val();
    if(isFPEnabled && typeof ssp != 'undefined'){
        var trackColorAnsSizeSelection = fitpredictor.fpProductContextEvents();
        if(trackColorAnsSizeSelection.salePrice != undefined && trackColorAnsSizeSelection.size != undefined ){
            ssp('set', 'product', {
                productId : trackColorAnsSizeSelection.productId,
                price : trackColorAnsSizeSelection.standardPrice,
                salePrice : trackColorAnsSizeSelection.salePrice,
                color : trackColorAnsSizeSelection.color,
                sizes : trackColorAnsSizeSelection.sizesArray,
                availableSizes : trackColorAnsSizeSelection.availableSizesArray,
                size : trackColorAnsSizeSelection.size
            });
        }else if(trackColorAnsSizeSelection.salePrice != undefined){
        	ssp('set', 'product', {
                productId : trackColorAnsSizeSelection.productId,
                price : trackColorAnsSizeSelection.standardPrice,
                salePrice : trackColorAnsSizeSelection.salePrice,
                color : trackColorAnsSizeSelection.color,
                sizes : trackColorAnsSizeSelection.sizesArray,
                availableSizes : trackColorAnsSizeSelection.availableSizesArray
            });
        }else if (trackColorAnsSizeSelection.size != undefined){
        	ssp('set', 'product', {
                productId : trackColorAnsSizeSelection.productId,
                price : trackColorAnsSizeSelection.standardPrice,
                color : trackColorAnsSizeSelection.color,
                sizes : trackColorAnsSizeSelection.sizesArray,
                availableSizes : trackColorAnsSizeSelection.availableSizesArray,
                size : trackColorAnsSizeSelection.size
            });
        }else{
        	ssp('set', 'product', {
                productId : trackColorAnsSizeSelection.productId,
                price : trackColorAnsSizeSelection.standardPrice,
                color : trackColorAnsSizeSelection.color,
                sizes : trackColorAnsSizeSelection.sizesArray,
                availableSizes : trackColorAnsSizeSelection.availableSizesArray
            });
        }
        
        ssp('start', 'fitpredictor');
    }
};

module.exports = function() {
    $('#pdpMain').on('change', '.pdpForm input[name="Quantity"]', getAvailability);
};
