'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    recommendations = require('./recommendations'),
    variant = require('./variant'),
    pdpSticky = require('./pdpSticky');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    productNav();
    recommendations();
    tooltip.init();
    pdpSticky();
}

function recommendationSlick(){
	//slick slider
	if($('#pdpMain div.tiles-container').hasClass('slick-slider')){
		 $('#pdpMain div.tiles-container').slick('unslick');
	}
    $('#pdpMain div.tiles-container').slick({
    	infinite: false,
    	speed: 300,
    	slidesToShow: 4,
    	slidesToScroll: 1,
    	swipe: false,
    	responsive: [{
    		breakpoint: 1023,
    		settings: {
    			slidesToShow: 2,
    			slidesToScroll: 1,
    			swipe: true,
    		}
    	},
    	{
    		breakpoint: 767,
    		settings: {
    			slidesToShow: 1,
    			slidesToScroll: 1,
    			swipe: true,
    		}
    	}
    	]
    });
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    addToCart();
    availability();
    variant();
    image();
    productSet();
    recommendationSlick();
    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize());
        if (data.cartAction) {
            delete data.cartAction;
        }
        var url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
        if (isGTMEnabled) {
            require('../../gtm').addToWishlistPDP($(this));
        }
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
        var selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of thumbnail link and add this Button
    $pdpMain.on('click', '.thumbnail-link, .unselectable a', function (e) {
        e.preventDefault();
    });

    //JIRA PREV-386 : On click of size chart link, the page displayed instead of overlay. Replaced $('.size-chart-link a').on('click', with $pdpMain.on('click', '.size-chart-link a',
    $pdpMain.on('click', '.size-chart-link a', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
            	dialogClass: 'sizechart-table',
            	title: Resources.SIZECHARTLABEL
            }
        });
        if (isGTMEnabled) {
            require('../../gtm').sizeChart();
        }
    });
    
    $pdpMain.on('change', '.tab-head input[type="radio"]', function (e) {
    	if(util.isMobile() && $(window).width() < 768){ return; }
    	$(this).parent().siblings('.tab-container').find('.tab-content').removeClass('active');
    	$(this).parent().siblings('.tab-container').find('.'+$(this).attr('id')).addClass('active');
    	
    	if($(this).parent().siblings('.tab-container').find('.tab-content.active .recommendation').length > 0){
    		recommendationSlick();
    	}
    });
    
    $pdpMain.find('.tab-head input[type="radio"]:checked').trigger('change');
    
    $pdpMain.on('keyup','[name="Quantity"]', function(e){
    	var keyCode = e.keyCode || window.event.keyCode;
    	if(keyCode == 13){
    		e.preventDefault();
    		$pdpMain.find("add-to-cart").trigger('click');
    	}
    }).on('submit', '.pdpForm', function(e){
    	e.preventDefault();
		$pdpMain.find("add-to-cart").trigger('click');
    });
    
    //PDP zoom
    $('body').on('click', '.product-zoom', function(e) {
        e.preventDefault();
        var pid = $('#pdp-zoom-prod').val();
        var url = Urls.PDPZoom;
        url = util.appendParamToURL(url, 'pid', pid);
        dialog.open({
            url: url,
            options: {
                width: '100%',
                height: 'auto',
                dialogClass: 'PDP-product-zoom',
                open: function(){
                	//For selecting the same image which is being clicked in PDP
                	var str = $('.product-primary-image').find('li.slick-active').index();
                	var productThumbnails = $(".PDP-product-zoom .product-thumbnails");
                	productThumbnails.find('.thumb.selected').removeClass('selected');
                	productThumbnails.find('li').eq(str).addClass('selected');
                	var thumbnail = $(".PDP-product-zoom .product-thumbnails").find('li').eq(str).find('.productthumbnail');
                	 $('.PDP-product-zoom .primary-image').attr({
                        src: thumbnail.data('lgimg').url,
                        alt: thumbnail.data('lgimg').alt,
                        title: thumbnail.data('lgimg').title
                    });
                	
                	// same as .productthumbnail click event of PDP written in image.js
                	$('.PDP-product-zoom').on('click', '.productthumbnail', function () {
                        // switch indicator
                        $(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
                        $(this).closest('.thumb').addClass('selected');

                        //setMainImage($(this).data('lgimg'));
                        $('.PDP-product-zoom .primary-image').attr({
                            src: $(this).data('lgimg').url,
                            alt: $(this).data('lgimg').alt,
                            title: $(this).data('lgimg').title
                        });
                    });
                	if ($(window).width() < 767) {
                		$(".PDP-product-zoom .product-primary-image .main-images.hide-mobile").remove();
	                	$('.ui-dialog.PDP-product-zoom .product-primary-image ul').slick({
	                	    infinite: true,
	                	    speed: 400,
	                	    slidesToShow: 1,
	                	    slidesToScroll: 1,
	                	    arrows: true,
	                	    initialSlide: str,
	                	});
	                	 $('.pinch-zoom').each(function () {
                             new RTP.PinchZoom($(this), {});
                         });
                	}
                	if ($('.PDP-product-zoom').is(':visible')) {
                        $('body').addClass('noscroll');
                    } else {
                    	 $('body').removeClass('noscroll');
                    }
                    $('.ui-widget-overlay').on('click', function(){
                        $('.ui-dialog-titlebar-close').trigger('click');
                        
                    });
                    recommendationSlick();
                    if ($(window).width() < 1025) {
                    	//modalZoomInit();
                    }
                },
                close: function() {
                	if($('body').hasClass('noscroll')) {
                    	$('body').removeClass('noscroll');
                    }
                }
            }
        });
    });
    $('.delivery-returns .product-delivery-head').on('click', function() {
    	$(this).toggleClass('act');
    	$(this).closest('.delivery-returns').find('.deliveryreturnslot').toggleClass('active');
    });
    $(window).load(function(){
    	$("html,body").animate({ scrollTop :  0 }, 0);
    	pdpSticky();
    });
}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
    }
};

module.exports = product;
