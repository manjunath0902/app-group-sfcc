'use strict';

/**
 * @description Creates product sticky
 **/
module.exports = function() {
	var $this, sticktHeader, colOffsetTop, totalOffset, windowScroll, stickyDiv, $currObj, downPos, pdpPos, headerValue;
	stickyDiv = function() {
		if($('.header-banner').is(':visible')){
    		sticktHeader = $('.sticky-header1').outerHeight();
    	} else {
    		sticktHeader = $('.sticky-header1').outerHeight();
    	}
    }
	stickyDiv();
	$currObj = $('#pdpMain');
    $this = $currObj.find('.product-col-2-main');
    if ($('.fulldesc').length > 0) {
    	downPos = $('.fulldesc').offset().top;
    } 
    else if($('.secondary').length > 0) {
    	downPos = $('.secondary').offset().top;
    } else if ($('#recommendation-section').length > 0) {
    	downPos = $('#recommendation-section').offset().top;
    } else if($('.tab.reviews').length > 0) {
    	downPos = $('.tab.reviews').offset().top;
    } else {
    	downPos = $('#footer').offset().top;
    } 
    pdpPos = $('#pdpMain').offset().top;
	if($this.length > 0) {
		colOffsetTop = $this.offset().top;
	}
	
	if($('.site-mk').length > 0 && $(window).width() > 767) {
		$('.product-thumbnails .slick-prev').on('click',function(){
			var selectedThumb = $('.thumb.slick-current').attr('data-slick-index');
			var thumbLength = $(".thumb.slick-slide").length - 1
			if(selectedThumb == 0)
				$("#thumbnails .slick-slider").slick('slickGoTo',thumbLength,true);
		});
		$('.product-thumbnails .slick-next').on('click',function(){
			var selectedThumb = $('.thumb.slick-current').attr('data-slick-index');
			var thumbLength = $(".thumb.slick-slide").length - 1
			if(selectedThumb == thumbLength)
				$("#thumbnails .slick-slider").slick('slickGoTo',0,true);
		});
	}
	
	totalOffset = colOffsetTop - sticktHeader;
	var iScrollPos = 0;
	$(window).scroll(function(){
		if ($(window).width() > 767) {
			if(!$('.ui-dialog').hasClass('qv-dialog') && (!$('.video-rendering-area').hasClass('active')) && $('.site-mk').length > 0) {
				windowScroll = $(window).scrollTop();
		    	stickyDiv();
		    	headerValue = $('#header.header-fixed').find('.top-banner').outerHeight();
		    	if($('#header').hasClass('header-fixed')) {
		    		pdpPos = $('.pdp-main').offset().top - $('.header-banner').outerHeight();
		    	}
		    	if (windowScroll > pdpPos) {
		    		$currObj.addClass('lg-md-fixed');
		    		$currObj.find('.product-thumbnails').css('top', headerValue);
		    	} else if (windowScroll <= pdpPos) {
		    		$currObj.find('.product-thumbnails').removeAttr('style');
		    		$currObj.removeClass('lg-md-fixed');
		    	}
		    	$currObj.toggleClass('lg-md-absolute', 
		    			windowScroll > downPos - $this.height());
		    	$currObj.find('.product-thumbnails').toggleClass('lg-md-thumb-absolute', 
		    			windowScroll > downPos - $currObj.find('.product-thumbnails').height() - headerValue - 150);
			}
			if($('.site-mk').length > 0) {
			  var iCurScrollPos = $(this).scrollTop();
		        if (iCurScrollPos > iScrollPos) {
		                //console.log("down");
		                //var totalHeight = $('.brand-name').height() +$('.product-desc').height() + $('#product-content').height() + 15;
		    			//$('.pdp-main.lg-md-fixed .product-col-2-main').css('top', -totalHeight);
		                var topDescription = $('.product-information-section').position().top - $('.header-banner').outerHeight(); 
		                if(iCurScrollPos > topDescription) {
		                   $('.pdp-main.lg-md-fixed .product-col-2-main').css('top', -topDescription);
		                }
		                else {
		            	   $('.pdp-main.lg-md-fixed .product-col-2-main').css('top', -iCurScrollPos);
		            	}

		        } else {
		               //console.log("up");
		               $('.pdp-main.lg-md-fixed .product-col-2-main').css('top', headerValue);
		               //$('.pdp-main.lg-md-fixed .product-col-2-main').css('top', iCurScrollPos);
		        }
		        iScrollPos = iCurScrollPos;
			}
			var pdpMainTop = $('#pdpMain').offset().top;
	        var currentsel = $('.product-thumbnails .slick-current').attr('data-slick-index');
	        currentsel = parseInt(currentsel);
	        var extra = currentsel + 1;
	        var length = $('.primary-image-list li').length;
	        if(length > 1 && $('.site-mk').length > 0) {
	            if(extra >= length) {
	                            extra = length - 1;
	            }
	            var previousextra = currentsel - 1;
	            var previousli;
	            var pdpMainheight =$('#pdpMain').outerHeight();
	            var scrollposition = $(window).scrollTop();
	            if(scrollposition < pdpMainheight) {
	                var nextli = $('.product-primary-image ul li[data-slick-index=' + extra + ']').offset().top;
	                if(previousextra > -1) {
	                                previousli = $('.product-primary-image ul li[data-slick-index=' + previousextra + ']').offset().top;
	                }              
	                var exectwidth = pdpMainheight / length;
	                var incre = 1;
	                if(scrollposition > pdpMainTop) {
	                        while(incre <= length) {
	                                if(scrollposition >  nextli && scrollposition < pdpMainheight) {
	                                    //console.log("forward");
	                               
	                                    $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('slick-current');
	                                    $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('selected');
	                                    $('.product-thumbnails ul li[data-slick-index=' + extra + ']').addClass('slick-current');
	                                    $('.product-thumbnails ul li[data-slick-index=' + extra + ']').addClass('selected');                
	                                    var k = $('.product-thumbnails .selected.slick-current');
	                                    if(extra <= length - 3) {
	                                    	//$('.product-details .tab-label').html(k.position().top + " op");
	                                    	var transformValue = "translate3d(0px, -" + k.position().top + "px, 0px)";
	                                    	var  elem = document.getElementsByClassName('product-thumbnails')[0].getElementsByClassName('slick-track')[0];
	                                    	elem.style.transform = transformValue;
	                                    	elem.style.webkitTransform = transformValue;
	                                    	elem.style.msTransform = transformValue;
	                                        
	                                    	//$('.product-thumbnails .slick-track').prop('style',"transform: translate3d(0px, -" + k.position().top + "px, 0px); -webkit-transform: translate3d(0px, -" + k.position().top + "px, 0px);-moz-transform: translate3d(0px, -" + k.position().top + "px, 0px);-o-transform: translate3d(0px, -" + k.position().top + "px, 0px);-ms-transform: translate3d(0px, -" + k.position().top + "px, 0px)");
	                                    	/*$('.product-thumbnails .slick-track').css({'-webkit-transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'});
	                                    	$('.product-thumbnails .slick-track').css({'-moz-transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'});
	                                    	$('.product-thumbnails .slick-track').css({'-ms-transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'});
	                                    	$('.product-thumbnails .slick-track').css({'-o-transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'});*/
	                                    }
	                                    //localStorage['currentIndexThumb'] = extra;
	                                } else {
	                                    if(scrollposition < previousli && previousextra > -1) {
	                                        //console.log("reverse");
	                                        
	                                        $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('slick-current');
	                                        $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('selected');
	                                        $('.product-thumbnails ul li[data-slick-index=' + previousextra + ']').addClass('slick-current');
	                                        $('.product-thumbnails ul li[data-slick-index=' + previousextra + ']').addClass('selected');               
	                                        var k = $('.product-thumbnails .selected.slick-current');
	                                        if(extra <= length - 2) {
	                                        	//$('.product-details .tab-label').html(extra + " op1");
	                                            //$('.product-thumbnails .slick-track').css({'transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'});
	                                            var transformValue = "translate3d(0px, -" + k.position().top + "px, 0px)";
	                                        	var  elem = document.getElementsByClassName('product-thumbnails')[0].getElementsByClassName('slick-track')[0];
	                                        	elem.style.transform = transformValue;
	                                        	elem.style.webkitTransform = transformValue;
	                                        	elem.style.msTransform = transformValue;
	                                        }              
	                                        //localStorage['currentIndexThumb'] = extra;    
	                                    }                                                                              
	                                }
	                                incre++;
	                        }
					} else {
	                    if(previousextra > -1) {
	                    	previousli = $('.product-primary-image ul li[data-slick-index=' + previousextra + ']').offset().top;
	                    }
	                    if(scrollposition < previousli && previousextra > -1) {
	                        //console.log("reverse1");
	                        $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('slick-current');
	                        $('.product-thumbnails ul li[data-slick-index=' + currentsel + ']').removeClass('selected');
	                        $('.product-thumbnails ul li[data-slick-index=' + previousextra + ']').addClass('slick-current');
	                        $('.product-thumbnails ul li[data-slick-index=' + previousextra + ']').addClass('selected');                
	                        var k = $('.product-thumbnails .selected.slick-current');
	                        extra = extra - 1;
	                        if(extra <= length - 3) {
	                        	//$('.product-details .tab-label').html(k.position().top + " op2");
	                            //$('.product-thumbnails .slick-track').css({'-webkit-transform': 'translate3d(0px, -' + k.position().top + 'px, 0px'}); 
	                        	var transformValue = "translate3d(0px, -" + k.position().top + "px, 0px)";
	                        	var  elem = document.getElementsByClassName('product-thumbnails')[0].getElementsByClassName('slick-track')[0];
	                        	elem.style.transform = transformValue;
	                        	elem.style.webkitTransform = transformValue;
	                        	elem.style.msTransform = transformValue;
	                        }
	                        //localStorage['currentIndexThumb'] = extra;    
	                    }
	                }
	            }              
	        }

			}
		});
};
