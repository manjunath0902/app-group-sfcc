'use strict';

var ajax = require('../../ajax'),
    formPrepare = require('./formPrepare'),
    giftcard = require('../../giftcard'),
    progress = require('../../progress'),
    util = require('../../util');

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
    var $creditCard = $('[data-method="CREDIT_CARD"]');
    $creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
    $creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
    $creditCard.find('input[name*="_creditCard_number"]').val(data.maskedNumber).trigger('change');
    $creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
    $creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
    $creditCard.find('input[name$="_cvn"]').val('').trigger('change');
    var ccType=$creditCard.find('select[name$="_type"]').val();
    var result="";
    var $ccNum=$('#PaymentMethod_CREDIT_CARD').find('input[name*="_creditCard_number"]');
    if (ccType == "MasterCard") {
        result = "MasterCard";
        //var $creditCardType=$creditCardSelector.find('option:selected').attr('value')).show();
        //$creditCardSelector.next(".selectorOut").val(result);
        $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
        if($ccNum.hasClass('visa')){
        	$ccNum.removeClass('visa');
        }
        else if($ccNum.hasClass('mastercard')){
        	$ccNum.removeClass('mastercard');
        }
        else if($ccNum.hasClass('amex')){
        	$ccNum.removeClass('amex');
        }
        $('#PaymentMethod_CREDIT_CARD').find('input[name*="_creditCard_number"]').addClass('mastercard');

    }
    // then check for Visa
    else if (ccType == "Visa") {
        result = "Visa";
        //$creditCardSelector.next(".selectorOut").val(result);
        $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
        if($ccNum.hasClass('visa')){
        	$ccNum.removeClass('visa');
        }
        else if($ccNum.hasClass('mastercard')){
        	$ccNum.removeClass('mastercard');
        }
        else if($ccNum.hasClass('amex')){
        	$ccNum.removeClass('amex');
        }
        $('#PaymentMethod_CREDIT_CARD').find('input[name*="_creditCard_number"]').addClass('visa');
    }
    // then check for AmEx
    else if (ccType =="Amex") {
        result = "Amex";
        //$creditCardSelector.next(".selectorOut").val(result);
        $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
        if($ccNum.hasClass('visa')){
        	$ccNum.removeClass('visa');
        }
        else if($ccNum.hasClass('mastercard')){
        	$ccNum.removeClass('mastercard');
        }
        else if($ccNum.hasClass('amex')){
        	$ccNum.removeClass('amex');
        }
        $('#PaymentMethod_CREDIT_CARD').find('input[name*="_creditCard_number"]').addClass('amex');
    }
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
    // load card details
    var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
    ajax.getJson({
        url: url,
        callback: function(data) {
            if (!data) {
                window.alert(Resources.CC_LOAD_ERROR);
                return false;
            }
            setCCFields(data);
        }
    });
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
    var $paymentMethods = $('.payment-method');
    $paymentMethods.removeClass('payment-method-expanded');

    var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
    if ($selectedPaymentMethod.length === 0) {
        $selectedPaymentMethod = $('[data-method="Custom"]');
    }
    
    $selectedPaymentMethod.addClass('payment-method-expanded');
    
    // ensure checkbox of payment method is checked
    $('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
    $('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

    formPrepare.validateForm();
}

function updateOrderSummary() {
    var url = Urls.summaryRefreshURL;
    var summary = $('#secondary.summary');
    progress.show(summary);
    summary.load(url, function () {
        summary.find('.checkout-mini-cart .minishipment .header a').hide();
        summary.find('.order-totals-table .order-shipping .label a').hide();
        if ($cache.primary.find('.spc-summary').length > 0) {
        	summary.addClass('order-review');
        	$cache.primary.addClass('order-primary');
        } else {
        	summary.removeClass('order-review');
        	$cache.primary.removeClass('order-primary');
        }
        
        var $h = 0;
    	if($('.checkout-mini-cart .mini-cart-product').length > 4){
    		$.each($('.checkout-mini-cart .mini-cart-product').splice(0,4), function(){
    			$h += $(this).outerHeight();
    		});
    		$('.checkout-mini-cart').height($h);
    		util.customscrollbar();
    	}else{
    		$('.checkout-mini-cart').height('auto');
    	}
    	
    	if($(window).width() > 767 && util.isMobile()){
    		$('#primary, #secondary').removeAttr('style').syncHeight();
    	}else{
    		$('#primary, #secondary').height('auto');
    	}
    	progress.hide();
    	
    });
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function() {
    var $checkoutForm = $('.checkout-billing');
    var $addGiftCert = $('#add-giftcert');
    var $giftCertCode = $('input[name$="_giftCertCode"]');
    var $addCoupon = $('#add-coupon');
    var $couponCode = $('input[name$="_couponCode"]');
    var $selectPaymentMethod = $('.payment-method-options');
    var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();

    // APPG-458 : No error message when you skip the "email address" field in checkout
    var $firstName = $('input[name$="_firstName"]');
    var $lastName = $('input[name$="_lastName"]');
    var $address1 = $('input[name$="_address1"]');
    var $country = $('select[name$="_country"]');
    var $state = $('select[name$="_state"]');
    var $city = $('input[name$="_city"]');
    var $postal = $('input[name$="_postal"]');
    var $phone = $('input[name$="_phone"]');
    var $emailAddress = $('input[name$="_emailAddress"]');
    var $sameAsShipping = $('input[name="sameasshippingaddress"]');
    
    var $allFieldArray = [$firstName, $lastName, $address1, $country, $state, $city, $postal, $phone, $emailAddress];
    if($sameAsShipping.length > 0 && $sameAsShipping.val() != undefined && $sameAsShipping.val() == "true"){
	    for(var i=0;i<$allFieldArray.length;i++){
	    	if($allFieldArray[i].length > 0 && $allFieldArray[i].val() == ''){
	    		$allFieldArray[i].focusin();
	    		$allFieldArray[i].focusout();
	    	}
	    }
    }
    
	//    formPrepare.init({
	//        formSelector: 'form[id$="billing"]',
	//        continueSelector: '[name$="billing_save"]'
	//    });
    
    // Ending APPG-458
    
    /* JIRA PREV-135 : Disabled Credit Card in BM is displaying in the application.
       Changed default option from 'CREDIT_CARD' to first available payment method.*/
    updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : $('.payment-method-options input').first().val());
    $selectPaymentMethod.on('click', 'input[type="radio"]', function() {
        updatePaymentMethod($(this).val());
    });
    
	var $creditCardSelector=$('#PaymentMethod_CREDIT_CARD').find('select[name$="_type"]');
	var $creditCard =$('#PaymentMethod_CREDIT_CARD').find('input[name*="_creditCard_number"]');
	
	if ($creditCard.length > 0) {
	 $creditCard.keyup(function () {
		var cardregex =JSON.parse($('#ccRegex').val());
//	            {mastercard: /^5[1-5][0-9]{2,14}$|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))-?\d{4}-?\d{4}-?\d{4}$/,
//	            visa: /^4[0-9]{3,15}$/,
//	            amex: /^3[47]([0-9]{2,13})$/}
	     
		 var val = $.trim($(this).val());
		 var result="";
		 if (new RegExp(cardregex.mastercard).test(val)) {
	            result = "MasterCard";
	            //var $creditCardType=$creditCardSelector.find('option:selected').attr('value')).show();
	            //$creditCardSelector.next(".selectorOut").val(result);
	            $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
	            $(this).addClass('mastercard');

	        }
	        // then check for Visa
	        else if (new RegExp(cardregex.visa).test(val)) {
	            result = "Visa";
	            //$creditCardSelector.next(".selectorOut").val(result);
	            $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
	            $(this).addClass('visa');
	        }
	        // then check for AmEx
	        else if (new RegExp(cardregex.amex).test(val)) {
	            result = "Amex";
	            //$creditCardSelector.next(".selectorOut").val(result);
	            $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
	            $(this).addClass('amex');
	        }
	        // then check for Maestro
	        else if (new RegExp(cardregex.maestro).test(val)) {
	            result = "Maestro";
	            //$creditCardSelector.next(".selectorOut").val(result);
	            $('#dwfrm_billing_paymentMethods_creditCard_type').val(result).change();
	            $(this).addClass('mastercard');
	        }
	        else{
	        	if($(this).hasClass('mastercard')){
	        		$(this).removeClass('mastercard');
	        	}
	        	else if($(this).hasClass('visa')){
	        		$(this).removeClass('visa');
	        	}
	        	else if($(this).hasClass('amex')){
	        		$(this).removeClass('amex');
	        	}
	        }
      });
   			 
   	}
       
   

    // select credit card from list
    $('#creditCardList').on('change', function() {
        var cardUUID = $(this).val();
        if (!cardUUID) {
            return;
        }
        populateCreditCardForm(cardUUID);

        // remove server side error
        $('.required.error').removeClass('error');
        $('.error-message').remove();
    });

    $('#check-giftcert').on('click', function(e) {
        e.preventDefault();
        //PREVAIL-Added to handle client side error messages and server side error messages together.
        $checkoutForm.find('.giftcert-error').html('');
        //PREVAIL-Added to handle client side error messages and server side error messages together.
        var $balance = $('.balance');
        if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
            $balance.html('');
            var error = $balance.find('span.error');
            if (error.length === 0) {
                error = $('<span>').addClass('error').appendTo($balance);
            }
            error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        $balance.find('span.error').html(''); //PREVAIL-Added to handle client side error messages and server side error messages together.
        //PREVAIL-Added $("input[name$='_giftCertPin']").val() to handle custom GC.
        giftcard.checkBalance($giftCertCode.val(), $('input[name$="_giftCertPin"]').val(), function(data) {
            if (!data || !data.giftCertificate) {
                $balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
                return;
            }
            $balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
        });
    });

    $addGiftCert.on('click', function(e) {
        e.preventDefault();
        $('.balance').html('').find('span.error').html('');
        var code = $giftCertCode.val(),
            //PREVAIL-Added to handle custom GC
            pin = $('input[name$="_giftCertPin"]') ? $('input[name$="_giftCertPin"]').val() : null,
            $error = $checkoutForm.find('.giftcert-error');
        if (code.length === 0) {
            $error.html(Resources.GIFT_CERT_MISSING);
            return;
        }

        var url = util.appendParamsToUrl(Urls.redeemGiftCert, {
            giftCertCode: code,
            gcPin: pin,
            format: 'ajax'
        }); //PREVAIL-Added gcPin:pin to handle custom GC
        $.getJSON(url, function(data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }
            if (fail) {
                $error.html(msg);
                return;
            } else {
                if ($('.spc-billing').length === 0) { //PREVAIL-Added to handle custom GC.
                    window.location.assign(Urls.billing);
                } else {
                    $('.spc-billing .checkout-tab-head.open[data-refreshurl]').trigger('click');
                }
            }
        });
    });

    $addCoupon.on('click', function(e) {
        e.preventDefault();
        var $error = $checkoutForm.find('.coupon-error'),
        	$success = $checkoutForm.find('.coupon .success'),
            code = $couponCode.val();
        if (code.length === 0) {
            $error.html(Resources.COUPON_CODE_MISSING);
            return;
        }

        if (isGTMEnabled) {
            require('../../gtm').applyCoupon(code);
        }

        var url = util.appendParamsToUrl(Urls.addCoupon, {
            couponCode: code,
            format: 'ajax'
        });
        $.getJSON(url, function(data) {
            var fail = false;
            var msg = '';
            if (!data) {
                msg = Resources.BAD_RESPONSE;
                fail = true;
            } else if (!data.success) {
                msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
                fail = true;
            }

            if (fail) {
            	$success.empty();
                $error.html(msg);
                return;
            }

            //basket check for displaying the payment section, if the adjusted total of the basket is 0 after applying the coupon
            //this will force a page refresh to display the coupon message based on a parameter message
            if (data.success) {
            	$error.empty();
            	//$success.empty();
            	$('div.couponcode-main .redemption.coupon').append('<div class="success">'+data.message.split('<').join('&lt;').split('>').join('&gt;')+'</div>');
            	
                //PREVAIL-Added '$(".spc-billing").length === 0' to handle SPC.
                if ($('.spc-billing').length === 0) {
                    window.location.assign(Urls.billing);
                } else {
                    $('.spc-billing .checkout-tab-head.open[data-refreshurl]').trigger('click');
                }
                updateOrderSummary();
            }
        });
    });

    // trigger events on enter
    $couponCode.on('keydown', function(e) {
        if (e.which === 13) {
            e.preventDefault();
            $addCoupon.click();
        }
    });
    $giftCertCode.on('keydown', function(e) {
        if (e.which === 13) {
            e.preventDefault();
            $addGiftCert.click();
        }
    });
    //PREVAIL JIRA PERV-565: Custom GC is not applying when the user hits Enter key: Added event on enter for Gift Certificate Pin
    $('input[name$="_giftCertPin"]').on('keydown', function(e) {
        if (e.which === 13) {
            e.preventDefault();
            $addGiftCert.click();
        }
    });
    
    //JIRA PREV-38 : Billing page_Credit Card Section: CVV number should not pre-populate.
    if ($('.bypassDAV').length === 0) {
        $('.spc-billing .form-row.cvn input').val('');
    }
    
    if ($('.selected-year').length >= 1 && $('.selected-year').val() != "null") {

        $('.checkout-billing .year select').val($('.selected-year').val()).trigger('change');

    }
};
