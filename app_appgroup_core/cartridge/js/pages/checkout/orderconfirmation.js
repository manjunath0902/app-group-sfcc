'use strict';
var fitpredictor = require('../../fitpredictor');
/**
 * @description orderconfirmation page events
 **/
var orderconfirmation = {
	init: function (){
        if($('input[id$="isFPEnabled"]').val()){
            ssp('set', 'site', {
                currency: $('input[id="siteCurrency"]').val(),
                language: $('input[id="siteLanguage"]').val(),
                layout: fitpredictor.fpGetDeviceLayout(),
                market:  $('input[id="siteMarket"]').val()
            });
            
            if($('input[id="isloggedInCustomer"]').val() != undefined && $('input[id="isloggedInCustomer"]').val() == 'true'){
            	ssp('set', 'user', {
	                userId: ($('input[id="customerNumber"]').val() != null) ? $('input[id="customerNumber"]').val() : undefined,
	        		email: $('input[id="customerEmail"]').val()
	            });
            }else{
            	ssp('set', 'user', {
	        		email: $('input[id="customerEmail"]').val()
	            });
            }
            
            ssp('set', 'page', {
                type: 'ord'
            });
            
            $.each( $('.line-items .line-item'), function( index ){
            	ssp('track', 'orderVariant', {
                    orderId: $('.order-number-information .value').text(),
                    productId: $('.line-item #liMasterID').eq(index).val(),
                    variantId: $('.line-item #liVariantID').eq(index).val(),
                    purchasePrice: parseFloat($('.line-item #pliPrice').eq(index).val()),
                    quantity: parseInt($('.line-item-quantity .pdt-qty').eq(index).text().trim())
                });
            });
        }
	}
};
module.exports  = orderconfirmation;

