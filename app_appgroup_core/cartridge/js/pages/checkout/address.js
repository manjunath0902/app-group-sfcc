'use strict';

var util = require('../../util');
var shipping = require('./shipping');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function() {
    var $form = $('.address');
    // select address from list
    $('select[name$="_addressList"]', $form).on('change', function () {
    	$('input[id="clearBillingFields"]').val(true);
        var selected = $(this).children(':selected').first();
        var selectedAddress = $(selected).data('address');
        if (!selectedAddress) {
            return;
        }
        util.fillAddressFields(selectedAddress, $form);
        //APPG-503
        if ($('select[name$="_addressList"]').closest('form.checkout-shipping').valid() && $('select[name$="_addressList"]').closest('form.checkout-shipping').find('button.spc-shipping-btn') != undefined && $('select[name$="_addressList"]').closest('form.checkout-shipping').find('button.spc-shipping-btn').length > 0 && $('select[name$="_addressList"]').closest('form.checkout-shipping').find('button.spc-shipping-btn').is(":disabled")) {
        	$('select[name$="_addressList"]').closest('form.checkout-shipping').find('button.spc-shipping-btn').removeAttr('disabled');
        }
        shipping.updateShippingMethodList();
        // re-validate the form
        /* JIRA PREV-95 : Highlighting the Credit card  details field, when the user select any saved address from the 'Select An Address' drop down.
           (logged-in user) .Commented to prevent form from re-validation.
		*/
        //$form.validate().form();
    });
};
