'use strict';

var address = require('./address'),
    billing = require('./billing'),
    multiship = require('./multiship'),
    shipping = require('./shipping'),
    util = require('../../util');

/**
 * @function Initializes the page events depending on the checkout stage (shipping/billing)
 */
exports.init = function() {
    address.init();
    if ($('.checkout-shipping').length > 0) {
        shipping.init();
        if(window.location.href.indexOf("PayPal") > -1) {
        	
        	//Will remove paypal queryString, which is added in paypal flow, to avoid click of return shipping
        	var uri = window.location.toString();
        	if (uri.indexOf("?") > 0) {
        	    var clean_uri = uri.substring(0, uri.indexOf("?"));
        	    window.history.replaceState({}, document.title, clean_uri);
        	}
        	$('.paypal_shipping_error').removeClass('hide');
        	$('.checkout-shipping').valid();
        	//$('button.spc-shipping-btn').trigger('click');
 		}
    } else if ($('.checkout-multi-shipping').length > 0) {
        multiship.init();
    } else if ($('.checkout-billing').length > 0) { //PREVAIL-Added $('.checkout-billing').length > 0 to handle SPC
        billing.init();
    }
    
    var $h = 0;
	if($('.checkout-mini-cart .mini-cart-product').length > 4){
		$.each($('.checkout-mini-cart .mini-cart-product').splice(0,4), function(){
			$h += $(this).outerHeight();
		});
		$('.checkout-mini-cart').height($h);
		util.customscrollbar();
	}else{
		$('.checkout-mini-cart').height('auto');
	}

	if($(window).width() > 767 && util.isMobile()){
		$('#primary, #secondary').removeAttr('style').syncHeight();
	}else{
		$('#primary, #secondary').height('auto');
	}
	
    //if on the order review page and there are products that are not available disable the submit order button
    if ($('.order-summary-footer').length > 0) {

        //JIRA PREV-505:Order confirmation page not displayed when user clicks more than once on Place Order button: Added one click function
        $('.submit-order').closest('form').one('submit', function() {
            $('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
        });

        if ($('.notavailable').length > 0) {
            $('.order-summary-footer .submit-order .button-fancy-large').attr('disabled', 'disabled');
        }
    }
};
