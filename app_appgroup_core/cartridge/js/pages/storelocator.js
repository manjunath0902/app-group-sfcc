'use strict';
var dialog = require('../dialog'),
	util = require('../util');

exports.init = function() {
	$( document ).ready(function() {
		if($(".storelocator-position").length > 0) {
		    var storelocatorPosition = $(".storelocator-position").offset().top;
			$('html, body').animate({
		        scrollTop: storelocatorPosition
		    }, 0);
		}
	});
    //PREVAIL - Added below block for Google maps integration.
    if (isPREVAILStoreLocatorEnabled) {
        /* "storeMap" is a global function that will be passed as callback function for google maps API. See below
         * <script src="//maps.googleapis.com/maps/api/js?callback=storeMap" async defer></script> 
         */
    	window.initForMap = function(){
    		initAutoComplete();
    		storeMap();
    	}
    }
    
    $('.store-details-link').on('click', function(e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });
    
    // Autocompite functionality
    var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      
     function initAutoComplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        var autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('dwfrm_storelocator_rawString')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      
     $('#dwfrm_storelocator_rawString').on('keyup',function(e){
    	 e.preventDefault();
    	 if (navigator.geolocation) {
	          navigator.geolocation.getCurrentPosition(function(position) {
	            var geolocation = {
	              lat: position.coords.latitude,
	              lng: position.coords.longitude
	            };
	            var circle = new google.maps.Circle({
	              center: geolocation,
	              radius: position.coords.accuracy
	            });
	            autocomplete.setBounds(circle.getBounds());
	          });
	        }
     });
    
     
     function storeMap() {
         //Create map
         var mapCanvas = document.getElementById('map');
         if ($(window).width()<768) {
         	mapCanvas = document.getElementById('map-1');
         }
         var mapOptions = {
             center: new google.maps.LatLng(37.09024, -95.712891),
             zoom: 10,
             mapTypeId: google.maps.MapTypeId.ROADMAP
         };
         var map = new google.maps.Map(mapCanvas, mapOptions);
         var customIcon ;
         var stores = $('.storeJSON').data('storejson'),
             markers = [],
             label,
             bounds = new google.maps.LatLngBounds(),
             store, marker, storeCenter, markerContent, mapLink,
             storeName, storePhone, storAddress1, storAddress2, storeCity, storeStateCode, storePostalCode, storeEmail, storeCountryCode;
         
        if($('.searchAction').val() == 'true'){
        	var lat = $('input[name=latitude]').val();
        	var lng = $('input[name=longitude]').val();
        	stores[stores.length] = {'latitude':lat,'longitude':lng,'icon':'http://www.robotwoods.com/dev/misc/bluecircle.png','label':''}

        }
         //Create markers and info window and add them to map.
         if (stores.length > 0) {
             for (var index in stores) {
                 store = stores[index];
                 if (!store.latitude || !store.longitude) {
                     continue;
                 }
                 storeCenter = new google.maps.LatLng(store.latitude, store.longitude);
                 storeName = store.name ? store.name : '';
                 storePhone = store.phone ? store.phone : '';
                 storAddress1 = store.address1 ? store.address1 : '';
                 storAddress2 = store.address2 ? store.address2 : '';
                 storeCity = store.city ? store.city : '';
                 storeStateCode = store.stateCode ? store.stateCode : '';
                 storePostalCode = store.postalCode ? store.postalCode : '';
                 storeEmail = store.email ? store.email : '';
                 storeCountryCode = store.countryCode ? store.countryCode : '';
                 mapLink = 'http://maps.google.com/maps?hl=en&f=q&q=' +
                     storAddress1 +
                     ',' + storeCity +
                     ',' + storeStateCode +
                     ',' + storePostalCode +
                     ',' + storeCountryCode;
                 
                 if(storAddress2){
                	 markerContent = '<div class="store-infowindow">' +
                     '<h2>' + storeName + '</h2>' +
                     '<p>' + storAddress1 + '<br/>' + storAddress2 + '<br/>' + storeCity + ',' + storeStateCode + storePostalCode + '</p>' +
                     '<div class="storephone">' + storePhone + '<br/>' + storeEmail + '</div>' +
                     '<div class="storemap"><a class="googlemap" href="' + mapLink + '" target="_blank">get directions</a></div>' + '</div>';
                 }else{
                	 markerContent = '<div class="store-infowindow">' +
                     '<h2>' + storeName + '</h2>' +
                     '<p>' + storAddress1 + '<br/>' + storeCity + ',' + storeStateCode + storePostalCode + '</p>' +
                     '<div class="storephone">' + storePhone + '<br/>' + storeEmail + '</div>' +
                     '<div class="storemap"><a class="googlemap" href="' + mapLink + '" target="_blank">get directions</a></div>' + '</div>';
                 }                 
                 customIcon = (store.icon != undefined)  ? store.icon : $(".storelocator-custom-icon").find("img").attr('src');
                 marker = new google.maps.Marker({
                     position: storeCenter,
                     label:store.label.toString(),
                     title: store.name,
                     content: markerContent,
                     icon: customIcon
                 });

                 bounds.extend(storeCenter);
                 marker.setMap(map);
                 markers.push(marker);
             }

             var infowindow = new google.maps.InfoWindow({
                 content: ''
             });
             var that;
             for (var i = 0; i < markers.length; i++) {
                 marker = markers[i];
                 marker.index = i;
                 google.maps.event.addListener(marker, 'click', function() {
                	
                     if (that) {
                    	 that.setZIndex();
                     }
	                 that = this;
	                 this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
	                 if(that.title != null){
                     	infowindow.setContent(this.content);
                     	infowindow.open(map, this);
	                 }
                 });
             }
         }

         map.fitBounds(bounds);

         $('.store-click').each(function(i) {
             jQuery(this).bind('click', function(e) {
                 e.preventDefault();
                 google.maps.event.trigger(markers[i], 'click');
             });
         });
     }
 
    $('.drop-down-link').on('click',function(){
    	var currentDiv = $(this).closest('.drop-down-main-cont').find('.drop-down-cont');
    	if (currentDiv.hasClass('hide')) {
    		currentDiv.removeClass('hide');
    		$(this).find('.arrow-btn').addClass('extend');
    	}
    	else {
    		currentDiv.addClass('hide');
    		$(this).find('.arrow-btn').removeClass('extend');
    	}
    });
    
    $('.tab-link').on('click',function() {
    	var subDiv = $(this).data('tab');
    	$(this).addClass('active');
		$(this).siblings().removeClass('active');
    	if (subDiv == 'tab-online-content') {
    		$(this).closest('.storelocator_cont').addClass('online-tab');
    		$(this).closest('.storelocator_cont').find('.store-refinment-online-list').removeClass('hide');
    	} else {
    		$(this).closest('.storelocator_cont').removeClass('online-tab');
    		$(this).closest('.storelocator_cont').find('.store-refinment-online-list').addClass('hide');
    	}
    });
    
    $('.store-refinment').on('click',function() {
    	var subDiv = $(this).data('current'),
    		subContent = $(this).closest('.store-refinment-online-list').find('.clearfix');
    	$('.store-refinment').removeClass('active');
    	$(this).addClass('active');
    	subContent.each(function(){
    		if ($(this).hasClass(subDiv)) {
    			$(this).removeClass('hide');
    		} else {
    			$(this).addClass('hide');
    		}
    	});
    });
    
	
    $(window).load(function() {
    	if($('input[name=continentcode]').val() == 'Asia'){
    		$('.online-asia').trigger('click');
    	}else if($('input[name=continentcode]').val() == 'Canada'){
    		$('.online-canada').trigger('click');
    	}else if($('input[name=continentcode]').val() == 'Europe'){
    		$('.online-europe').trigger('click');
    	}else{
    		$('.online-us').trigger('click');
    	}
	});
};

