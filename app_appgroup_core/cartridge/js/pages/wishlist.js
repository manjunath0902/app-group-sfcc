'use strict';

var addProductToCart = require('./product/addToCart'),
	minicart = require('../minicart'),
    page = require('../page'),
    login = require('../login'),
    util = require('../util'),
    quickview = require('../quickview');//Prevail - JIRA PREV-661 : Application navigates to pd page on click of edit details link for a product in wish list page.

var removeProductFromWishlist= function() {
	
	$('.button-fancy-small.add-to-cart ').on('click',function(e) {
		e.preventDefault();
	    $(this).closest('.table-content').find('.item-availability').find('.button-text.delete-item').trigger('click');
	    minicart.show();
	});
}

exports.init = function () {
    addProductToCart();
    removeProductFromWishlist();
    $('#editAddress').on('change', function () {
        page.redirect(util.appendParamToURL(Urls.wishlistAddress, 'AddressID', $(this).val()));
    });

    //add js logic to remove the , from the qty feild to pass regex expression on client side
    $('.option-quantity-desired input').on('focusout', function () {
        $(this).val($(this).val().replace(',', ''));
    });

    login.init();
    //Prevail - JIRA PREV-661 : Application navigates to pd page on click of edit details link for a product in wish list page. Added click event.
    $('.item-list').on('click', '.item-details .edit-wishlist-item', function(e) {
        e.preventDefault();
        var url = util.appendParamToURL(e.target.href, 'cartAction', 'update');
        quickview.show({
            url: url,
            source: 'wishlist'
        });
    })

};
