'use strict';

var compareWidget = require('../compare-widget'),
    productTile = require('../product-tile'),
    progress = require('../progress'),
    util = require('../util'),
    imagesLoaded = require('imagesloaded');

function setVideo() {
	if($('.product-video').length > 0) {
        $('.product-video').each(function() {
        	var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
        	if(isiPad)
        	{
        		$(this).closest('.product-tile').find('.product-image').addClass('product-videoclick');
        	}
            var heightImage = $(this).closest('.product-tile').find('.product-image').outerHeight();
            var widthImage = $(this).closest('.product-tile').find('.product-image').width();
            $(this).find('iframe').css('height',heightImage);
            $(this).find('iframe').css('width',widthImage);
            $(this).css('font-size','0px');
        });
    }
}
function setVideoLazyLoad() {
	if($('.product-video').length > 0) {
    	var heightImage = $('.search-result-content .search-result-items').first().find('.product-tile').eq(0).find('.product-image').outerHeight();
    	var widthImage = $('.search-result-content .search-result-items').first().find('.product-tile').eq(0).find('.product-image').outerWidth();
		$('.product-video').each(function() {
        	var isiPad = /ipad/i.test(navigator.userAgent.toLowerCase());
        	if(isiPad)
        	{
        		$(this).closest('.product-tile').find('.product-image').addClass('product-videoclick');
        	}
            $(this).find('iframe').css('height',heightImage);
            $(this).find('iframe').css('width',widthImage);
            $(this).css('font-size','0px');
        });
    }
}

function infiniteScroll() {
    // getting the hidden div, which is the placeholder for the next page
    var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
    // get url hidden in DOM
    var gridUrl = loadingPlaceHolder.attr('data-grid-url');
    
    //updated if condition for APPG-537
    //if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
    
    if (loadingPlaceHolder.length === 1) {
        // switch state to 'loading'
        // - switches state, so the above selector is only matching once
        // - shows loading indicator
        loadingPlaceHolder.attr('data-loading-state', 'loading');
        loadingPlaceHolder.addClass('infinite-scroll-loading');


        // named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
        var fillEndlessScrollChunk = function (html) {
            loadingPlaceHolder.removeClass('infinite-scroll-loading');
            loadingPlaceHolder.attr('data-loading-state', 'loaded');
            $('div.search-result-content').append(html);
        };

        // old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
        // it was removed to temporarily address RAP-2649

        // For the ajax request, clear the contents of the asset-initialization array
        contentAssetHelper.clearInitFunctions();

        $.ajax({
            type: 'GET',
            dataType: 'html',
            url: gridUrl,
            success: function (response) {
                // put response into cache
                try {
                    sessionStorage['scroll-cache_' + gridUrl] = response;
                } catch (e) {
                    // nothing to catch in case of out of memory of session storage
                    // it will fall back to load via ajax
                }
                // update UI
                setVideoLazyLoad(); 
                fillEndlessScrollChunk(response);
                productTile.init();
                util.lazyLoad();
                //viewMore();
            },
	        complete: function() {
	        	setVideoLazyLoad();
	        }
        });
    }
}

function fillEndlessScrollChunk(html, $viewMorePlaceholder) {

    // Update the rendering of the loading-placeholder
    $viewMorePlaceholder.removeClass('viewmore-loading');
    $viewMorePlaceholder.attr('data-loading-state', 'loaded').hide();
    $viewMorePlaceholder.parents('.view-more').hide();

    // Append the PLP content
    $('div.search-result-content').append(html);

    // Initialize the grid-tiles
    productTile.init();
    setVideoLazyLoad();
}

function viewMoreProducts() {

    // Initialize local variables
    var $loadingPlaceHolder,
        gridUrl,
        sessionStorageCacheKey;

    // Let's retrieve however many instances of the view-more button exist
    $loadingPlaceHolder = $('.viewmore-grid[data-loading-state="unloaded"]').last();

    // Get url hidden in DOM element
    gridUrl = $loadingPlaceHolder.attr('href');

    // Is the gridUrl valid?
    if (gridUrl !== '' && gridUrl !== undefined) {

        // Build out the session-storage cache key
        sessionStorageCacheKey = 'scroll-cache_' + gridUrl;

        // Switch state to 'loading' so the above selector only matches once
        $loadingPlaceHolder.attr('data-loading-state', 'loading');
        $loadingPlaceHolder.addClass('viewmore-loading');

        // Do we currently have the next section cached in session-storage?
        if (sessionStorage.getItem(sessionStorageCacheKey) !== null) {

            // Append the cached search-result content to the current PLP display
            fillEndlessScrollChunk(sessionStorage.getItem(sessionStorageCacheKey), $loadingPlaceHolder);

        } else {

            // Otherwise, execute an ajax request to retrieve the next section's content
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: gridUrl,
                success: function (response) {

                    // put response into cache
                    try {
                        sessionStorage[sessionStorageCacheKey] = response;
                    } catch (e) {
                        // nothing to catch in case of out of memory of session storage
                        // it will fall back to load via ajax
                    }

                    // Update the UI display
                    fillEndlessScrollChunk(response, $loadingPlaceHolder);
                    setVideo(); 
                },
    	        complete: function() {
    	        	setVideoLazyLoad();
    	        }
            });
        }
    }
}

/**
 * @function loadProductsBackPlp
 * @description Loads products back to the PLP
 * @param {Boolean} initialInit Describes if this call of loadProducts is an initial init for the page
 */
function loadProductsBackPlp(initialInit) {

    // Default the initial-init flag (prevents duplicate initialization of gridTiles)
    if (initialInit === undefined) { initialInit = false; }

    // Helper function that abstracts the removal / clearing on PLP cookies
    var clearPLPCookies = function clearPLPCookies () {
        util.removeCookie('productId');
        util.removeCookie('offset-value');
    };

    // Only evaluate this if we have a cached view-link and referenced productId
    if (util.getCookie('productId') !== '' && util.getCookie('productId') !== undefined) {

        // Check if the product that was selected is visible in the product-listing
        // If it is, then scroll down to it and bring it into the window
        var checkAvailableId = function (id) {

            // Exit early if we don't have a valid product
            if (id === '' || id === undefined) { return; }

            // Create a reference to the product-tile referenced
            var $selectedProductTile = $('.product-tile[data-itemid="' + id + '"]');
            var scrollPosition = util.getCookie('offset-value');

            // Was a valid tile found?
            if ($selectedProductTile.length > 0) {

                // Don't re-initialize the tile on the initial it
                if (initialInit === false) { productTile.init(); }

                // Scroll down to the identified product tile
                setTimeout(function () {
                    util.scrollBrowser(scrollPosition);
                }, 100);

                // Clear / reset the PLP cookies
                clearPLPCookies();
            } else {
                loadMoreItems();
            }
        };

        // This function is used to load additional products into the PLP
        var loadMoreItems = function () {

            // Let's retrieve however many instances of the view-more button exist
            var $loadingPlaceHolder = $('.viewmore-grid[data-loading-state="unloaded"]');

            // Get the current / last view-more grid url
            var gridUrl = $('.viewmore-grid').last().attr('href');
            var scrollCacheKey = 'scroll-cache_' + gridUrl;
            var cookieProductId = util.getCookie('productId');

            // Was a valid grid-url found?
            if (gridUrl !== undefined) {

                // If so, then check session storage to determine if we can update
                // the display mark-up with the cached PLP content
                if (sessionStorage.getItem(scrollCacheKey) !== null) {

                    // Switch state to 'loading' so the above selector only matches once
                    $loadingPlaceHolder.attr('data-loading-state', 'loading');
                    $loadingPlaceHolder.addClass('viewmore-loading');

                    // Append the cached search-result content to the current PLP display
                    fillEndlessScrollChunk(sessionStorage.getItem(scrollCacheKey), $loadingPlaceHolder);
                }
                // Was a productID set?  If so, then evaluate if it can be selected
                checkAvailableId(cookieProductId);
            } else {
            	checkAvailableId(cookieProductId);
            }
        };
        loadMoreItems();
    } else {
        // Clear / reset the PLP cookies
        clearPLPCookies();
    }
}

/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url) {
    if (!url || url === window.location.href) {
        return;
    }
    progress.show($('.search-result-content'));
    $('#main').load(util.appendParamToURL(url, 'format', 'ajax'), function () {
        compareWidget.init();
        productTile.init();
        progress.hide();
        history.pushState(undefined, '', url);
        util.updateselect();
        util.lazyLoad();
        refinementScroll();
        imagesLoaded('.tiles-container').on('done', function () {
        	$('#primary, #secondary').removeAttr('style');
        });
        $('.refinement .selected').closest('.refinement').addClass('active');
        refinementScroll();
        //viewMore();
        setVideo();
        
    });
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
    var $main = $('#main');
    setVideo();
    var getid=$('.product-tile');

    var currentSiteID=$(getid).data('siteid');

    if(currentSiteID == "MK"||currentSiteID=="MK_CA"||currentSiteID=="MK_US"){
    	
   	$('.refinement.refinementColor').addClass('active');  var $x=$('.refinement.refinementColor').find('h3').addClass('expanded');var $v=$x.addClass('active');
    		$('.refinement.TemperatureEN').addClass('active'); var $x=$('.refinement.TemperatureEN').find('h3').addClass('expanded'); var $f=$x.addClass('active');
    	}
    else if(currentSiteID == "SK"||currentSiteID=="SK_CA"||currentSiteID=="SK_US"){
    	$('.refinement.refinementColor').addClass('active');  var $x=$('.refinement.refinementColor').find('h3').addClass('expanded');var $v=$x.addClass('active');
    	$('.refinement.OuterwearLengthEN').addClass('active');  var $x=$('.refinement.OuterwearLengthEN').find('h3').addClass('expanded');var $v=$x.addClass('active');
    	$('.refinement.TemperatureEN').addClass('active');  var $x=$('.refinement.TemperatureEN').find('h3').addClass('expanded');var $v=$x.addClass('active');
    	}
    
    $( window ).load(function() {
    	var categoryStructure = $('#categoryStructure').val();
    	var categoryStructureArray = categoryStructure.split(",");
    	var slotContentNotFound = true;
    	for(var i=0; i< categoryStructureArray.length; i++){
    		
    		if (slotContentNotFound) {
    			var slotcont = categoryStructureArray[i];	
		    	$.ajax({
		            dataType: 'html',
		            url: util.appendParamToURL(Urls.getslotconfiguration,'slotID',slotcont),        	
		            type: "POST",
		            success : function(data){
		            	if(data.length > 0 && $('.cat-slot-top-banner .bf-banner').length == 0){
		            		$('.cat_bannerSlot').append(data);
		            		slotContentNotFound = false;
		            	}
		            }
		    	});
    		}
	    	
    	}
    	$(document).trigger('click');
    });
    
    $( window ).resize(function() {
    	setVideo();
    });
                
    // compare checked
    $main.on('click', 'input[type="checkbox"].compare-check', function () {
        var cb = $(this);
        var tile = cb.closest('.product-tile');

        var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
        var itemImg = tile.find('.product-image a img').first();
        func({
            itemid: tile.data('itemid'),
            uuid: tile[0].id,
            img: itemImg,
            cb: cb
        });

    });

    // handle toggle refinement blocks
    $main.on('click', '.refinement h3', function () {
    	
    	$(this).closest('.refinement').find('.custom-scrollbar').toggleClass('active');
    	$(this).closest('.refinement').toggleClass('active');
        $(this).toggleClass('active');
            //.siblings().toggle();
        setVideo();
        refinementScroll();
    });
    $main.on('click', '.refinement-header', function(){
    	
    	$(this).toggleClass('active').next('.category-refinement').toggle();
    });
    $main.on('click', '.product-swatches-all', function (e) {
    	var container = $(".product-swatches-all");
    	$(container).not(this).removeClass('active').next('.swatch-toggle').hide();
    	$(this).toggleClass('active').next('.swatch-toggle').toggle();
    	setVideo();
    });
    $(document).on('click', function (e){ 
    	var container = $(".product-swatches-all, .swatch-toggle");
    	if (!container.is(e.target) && container.has(e.target).length === 0) {
        	$('.product-swatches-all').removeClass('active').next('.swatch-toggle').hide();
        	setVideoLazyLoad();
        }
    });
    
    // handle events for updating grid
    $main.on('click', '.refinements a, .pagination a, .breadcrumb-refinement-value a', function (e) {
        // don't intercept for category and folder refinements, as well as unselectable
        if ($(this).parents('.category-refinement').length > 0 || $(this).parents('.folder-refinement').length > 0 || $(this).parent().hasClass('unselectable')) {
            return;
        }
        e.preventDefault();
        setVideo();
        updateProductListing(this.href);

        if (isGTMEnabled) {
            require('../gtm').refinement($(this));
        }
    });
    
    $main.on("click",".pagination a",function(){
    	$("html,body").animate({ scrollTop: 0 }, "slow");
    });
    
    $main.on("click",".refinement a", function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    // handle events item click. append params.
    $main.on('click', '.product-tile a:not("#quickviewbutton")', function () {

        if (isGTMEnabled) {
            require('../gtm').productClick($(this));
        }

        var a = $(this);
        // get current page refinement values
        var wl = window.location;

        var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
        var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};

        // merge hash params with querystring params
        var params = $.extend(hashParams, qsParams);
        if (!params.start) {
            params.start = 0;
        }
        // get the index of the selected item and save as start parameter
        var tile = a.closest('.product-tile');
        var idx = tile.data('idx') ? +tile.data('idx') : 0;

        /*Start JIRA PREV-50 : Next and Previous links will not be displayed on PDP if user navigate from Quick View.Added cgid to hash*/
        if (!params.cgid && tile.data('cgid') !== null && tile.data('cgid') !== '') {
            params.cgid = tile.data('cgid');
        }
        /*End JIRA PREV-50*/

        // convert params.start to integer and add index
        params.start = (+params.start) + (idx + 1);
        // set the hash and allow normal action to continue
        a[0].hash = $.param(params);
    });

    // handle sorting change
    $main.on('change', '.sort-by select', function (e) {
        e.preventDefault();
        if (isGTMEnabled) {
            require('../gtm').sort($(this).find('option:selected').text());
            
        }
        
        updateProductListing($(this).find('option:selected').val());
        
    })
    .on('change', '.items-per-page select', function () {
        var refineUrl = $(this).find('option:selected').val();
        if (refineUrl === 'INFINITE_SCROLL') {
            $('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
        } else {
            $('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
            updateProductListing(refineUrl);
        }
    }).on('click', '.view-more', function(e) {
		e.preventDefault();
		viewMoreProducts();
		setVideoLazyLoad();
    });
}

function refinementScroll(){
	$('.refinement').each(function(){
		var $this = $(this);
		if(!$this.hasClass('category-refinement') && !$this.hasClass('refinementColor') && !$this.hasClass('size')){
			if($this.find('li:visible').length > 10){
				var $h = 0;
				$.each($this.find('li:visible').splice(0,10), function(i){
					$h += $(this).outerHeight();
				});
				$this.find('.custom-scrollbar').height($h);
				util.customscrollbar($this.find('.custom-scrollbar'));
			}else{
				$this.find('.custom-scrollbar').height("auto");
			}
		}	
	})
}

exports.init = function () {
    compareWidget.init();
    /*if (SitePreferences.LISTING_INFINITE_SCROLL) {
    	//Commented for APPG-537
        $(window).on('scroll', infiniteScroll);
    	viewMore();
    }*/
    productTile.init();
    initializeEvents();
    refinementScroll();
    loadProductsBackPlp(true);
    if(window.SitePreferences.SITE_CLASS == "site-mk"){
    	$(window).on('load resize', function(){
    		$('#primary, #secondary').removeAttr('style');
    		$('.refinement .selected').closest('.refinement').addClass('active');
            refinementScroll();
        	        	
        });
    }
};
