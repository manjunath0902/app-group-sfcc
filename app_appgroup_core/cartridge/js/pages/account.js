'use strict';

var giftcert = require('../giftcert'),
    tooltip = require('../tooltip'),
    util = require('../util'),
    dialog = require('../dialog'),
    page = require('../page'),
    login = require('../login'),
    validator = require('../validator'),
    orderreturn = require('../orderreturn'),
    ajax = require('../ajax');


var initCountryChange = function() {
    $('select.country').change(function() {
      if ($('form#edit-address-form').length > 0){
    	  if($(this).val() === "US")
              var url = window.Urls.addressesAddUS;
          else
              var url = window.Urls.addressesAdd;
          var addressID = $('#dwfrm_profile_address_addressid').val();
          var firstName = $('#dwfrm_profile_address_firstname').val();
          var lastName = $('#dwfrm_profile_address_lastname').val();
          var address1 = $('#dwfrm_profile_address_address1').val();
          var address2 = $('#dwfrm_profile_address_address2').val();
          var country = $(this).val();
          var state = $('#dwfrm_profile_address_states_state').val();
          var city = $('#dwfrm_profile_address_city').val();
          var postalCode = $('#dwfrm_profile_address_postal').val();
          var phone = $('#dwfrm_profile_address_phone').val();
          console.log(url);
          $.ajax({
              dataType: 'html',
              url: url
          })
          .done(function (data) {
              var form = $(data).find('form#edit-address-form');
              $('form#edit-address-form').html($(form).html());
              $('#dwfrm_profile_address_addressid').val(addressID);
              $('#dwfrm_profile_address_firstname').val(firstName);
              $('#dwfrm_profile_address_lastname').val(lastName);
              $('#dwfrm_profile_address_address1').val(address1);
              $('#dwfrm_profile_address_address2').val(address2);
              $('select.country option').removeAttr("selected");
              var countrySelector = 'select.country option[value="' + country + '"]';
              $(countrySelector).attr("selected", "selected");
              util.updateselect();
              $('#dwfrm_profile_address_states_state').val(state);
              $('#dwfrm_profile_address_city').val(city);
              $('#dwfrm_profile_address_postal').val(postalCode);
              $('#dwfrm_profile_address_phone').val(phone);
              initCountryChange();
          });
       }
    });
}

/**
 * @function
 * @description Initializes the events on the address form (apply, cancel, delete)
 * @param {Element} form The form which will be initialized
 */
function initializeAddressForm() {
    var $form = $('#edit-address-form');

    $form.find('input[name="format"]').remove();
    tooltip.init();
    //$("<input/>").attr({type:"hidden", name:"format", value:"ajax"}).appendTo(form);
    if (SitePreferences.ENABLE_UK_CHECKOUT === true) {
	    $form.on('change', 'div.custom-select .country', function (e) {
	    	e.preventDefault();
	        var url = Urls.addressUpdateStateDropDown;
	        url = util.appendParamToURL(url, 'selectedCountry', $(this).val());
	        ajax.load({
	            url: url,
	            type: 'GET',
	            callback: function (data) {
	                //$cache.primary.html(data);
	            	var result = $(data).find("div.custom-select");
	            	$('form#edit-address-form').find("select[name$='states_state']").closest('.field-wrapper').html(result);
	            	initializeEvents();
	                validator.init();
	            }
	        });
	    });
     }
    
    // Validate Address field to avoid sending '\n \r' APPG-1011
    $('form#edit-address-form').on('blur', 'input', function (e) {
    	var addressfieldval = $(this).val();
    	if (addressfieldval != '' && SitePreferences.ADDRESSFIELD_REGEX != ''){
    		var filteredInputValue = util.validateaddressformfield(addressfieldval, SitePreferences.ADDRESSFIELD_REGEX);
    		if (filteredInputValue != undefined) {
    			$(this).val(filteredInputValue);
    		}
    	}
    });

    $form.on('click', '.apply-button', function (e) {
        e.preventDefault();
        if (!$form.valid()) {
            return false;
        }
        var url = util.appendParamToURL($form.attr('action'), 'format', 'ajax');
        var applyName = $form.find('.apply-button').attr('name');
        var options = {
            url: url,
            data: $form.serialize() + '&' + applyName + '=x',
            type: 'POST'
        };
        $.ajax(options).done(function (data) {
            if (typeof(data) !== 'string') {
                if (data.success) {
                    dialog.close();
                    page.refresh();
                } else if (data.error) {
                    page.redirect(Urls.csrffailed);
                } else {
                    window.alert(data.message);
                    return false;
                }
            } else {
                $('#dialog-container').html(data);
                account.init();
                tooltip.init();
            }
        });
    })
    .on('click', '.cancel-button, .close-button', function (e) {
        e.preventDefault();
        dialog.close();
    })
    .on('click', '.delete-button', function (e) {
        e.preventDefault();
        if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
            var url = util.appendParamsToUrl(Urls.deleteAddress, {
                AddressID: $form.find('#addressid').val(),
                format: 'ajax'
            });
            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'json'
            }).done(function (data) {
                if (data.status.toLowerCase() === 'ok') {
                    dialog.close();
                    page.refresh();
                } else if (data.message.length > 0) {
                    window.alert(data.message);
                    return false;
                } else {
                    dialog.close();
                    page.refresh();
                }
            });
        }
    });

    initCountryChange();

    validator.init();
}
/**
 * @private
 * @function
 * @description Toggles the list of Orders
 */
function toggleFullOrder () {
    $('.order-items')
        .find('li.hidden:first')
        .prev('li')
        .append('<a class="toggle">View All</a>')
        .children('.toggle')
        .click(function () {
            $(this).parent().siblings('li.hidden').show();
            $(this).remove();
        });
}
/**
 * @private
 * @function
 * @description Binds the events on the address form (edit, create, delete)
 */
function initAddressEvents() {
    var addresses = $('#addresses');
    if (addresses.length === 0) { return; }

    addresses.on('click', '.address-edit, .address-create', function (e) {
        e.preventDefault();
        dialog.open({
            url: this.href,
            options: {
            	dialogClass: 'editaddress',
                open: initializeAddressForm
            }
        });
    }).on('click', '.delete', function (e) {
        e.preventDefault();
        if (window.confirm(String.format(Resources.CONFIRM_DELETE, Resources.TITLE_ADDRESS))) {
            $.ajax({
                url: util.appendParamToURL($(this).attr('href'), 'format', 'ajax'),
                dataType: 'json'
            }).done(function (data) {
                if (data.status.toLowerCase() === 'ok') {
                    page.redirect(Urls.addressesList);
                } else if (data.message.length > 0) {
                    window.alert(data.message);
                } else {
                    page.refresh();
                }
            });
        }
    });
}
/**
 * @private
 * @function
 * @description Binds the events of the payment methods list (delete card)
 */
function initPaymentEvents() {
    $('.add-card').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
            	dialogClass: 'add-credit-card',
                open: initializePaymentForm
            }
        });
    });

    var paymentList = $('.payment-list');
    if (paymentList.length === 0) { return; }

    util.setDeleteConfirmation(paymentList, String.format(Resources.CONFIRM_DELETE, Resources.TITLE_CREDITCARD));

    $('form[name="payment-remove"]').on('submit', function (e) {
        e.preventDefault();
        // override form submission in order to prevent refresh issues
        var button = $(this).find('.delete');
        $('<input/>').attr({
            type: 'hidden',
            name: button.attr('name'),
            value: button.attr('value') || 'delete card'
        }).appendTo($(this));
        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: data
        })
        .done(function () {
            page.redirect(Urls.paymentsList);
        });
    });
}

function initializePaymentForm() {
    $('#CreditCardForm').on('click', '.cancel-button', function (e) {
        e.preventDefault();
        dialog.close();
    });

}
/**
 * @private
 * @function
 * @description Binds the events of the order, address and payment pages
 */
function initializeEvents() {
    initializeAddressForm(); // JIRA PREV-63: 'Add Address' or 'Edit address' overlay changing to page after click on Apply button,when there is an error message on overlay.
    toggleFullOrder();
    initAddressEvents();
    initPaymentEvents();
    login.init();
    
    if ( $("input[name$='_profile_customer_addtoemaillist']") != undefined &&  $("input[name$='_profile_customer_addtoemaillist']").length > 0 &&  $("input[name$='_profile_customer_addtoemaillist']").is(':checked')) {
    	 $('.email-subscription-click').removeClass('hide');
    } else {
    	if (!( $('.email-subscription-click').hasClass('hide'))) {
    		$('.email-subscription-click').addClass('hide');
    	}
    }
    
    $('#RegistrationForm').on('click', "input[name$='_profile_customer_addtoemaillist']", function (e) {
    	
    	if ($("input[name$='_profile_customer_addtoemaillist']").is(':checked')) {
    		$('.email-subscription-click').removeClass('hide');
    	} else {
    		if (!( $('.email-subscription-click').hasClass('hide'))) {
        		$('.email-subscription-click').addClass('hide');
        	}
    	}

    	if ($('.js-customer-year').length >= 1 && $('.js-customer-year').val() != "null" && $('.js-customer-year').val().length > 0) {

            $('.birthaday_wrap .year select').val($('.js-customer-year').val()).trigger('change');

        }
        
        if ($('.js-customer-month').length >= 1 && $('.js-customer-month').val() != "null" && $('.js-customer-month').val().length > 0) {
        	
        	if ($('.js-customer-month').val() == 0) {
        		
        		$('.birthaday_wrap .month select').val("12").trigger('change');
        		
        	} else {
        		$('.birthaday_wrap .month select').val($('.js-customer-month').val()).trigger('change');
        	}

        }
        
        if ($('.js-customer-day').length >= 1 && $('.js-customer-day').val() != "null" && $('.js-customer-day').val().length > 0) {

            $('.birthaday_wrap .day select').val($('.js-customer-day').val()).trigger('change');

        }
    	
    });
    
}

var account = {
    init: function () {
        initializeEvents();
        giftcert.init();
        orderreturn.init();
    },
    initCartLogin: function () {
        login.init();
    }
};

module.exports = account;
