/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */
'use strict';

var countries = require('./countries'),
    dialog = require('./dialog'),
    minicart = require('./minicart'),
    page = require('./page'),
    searchplaceholder = require('./searchplaceholder'),
    searchsuggest = require('./searchsuggest'),
    tooltip = require('./tooltip'),
    util = require('./util'),
    validator = require('./validator'),
    global = require('./global'),
    tls = require('./tls');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
    var s = document.createElement('script');
    s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('./captcha')();

function initializeEvents() {
    var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

    //PREVAIL-Added to handle form dialog boxes server side issues.
    $('body').on('click', '.dialogify, [data-dlg-options], [data-dlg-action]', require('./dialogify').setDialogify)
        .on('keydown', 'textarea[data-character-limit]', function (e) {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length;

            if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
                e.preventDefault();
            }
        })
        .on('change keyup mouseup', 'textarea[data-character-limit]', function () {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length,
                charsRemain = charsLimit - charsUsed;

            if (charsRemain < 0) {
                $(this).val(text.slice(0, charsRemain));
                charsRemain = 0;
            }

            $(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

    /**
     * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
     * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
    **/
     var $searchContainer = $('#navigation .header-search');
     searchsuggest.init($searchContainer, "");

    // print handler
    $('.print-page').on('click', function () {
        window.print();
        return false;
    });

    var bLazy = new Blazy({
    	 
    	
    	});

    // add show/hide navigation elements
    $('.secondary-navigation .toggle').click(function () {
        $(this).toggleClass('expanded').next('ul').toggle();
    });

    // add generic toggle functionality
    $('.toggle').next('.toggle-content').hide();
    $('.toggle').click(function (e) {
        e.preventDefault(); //JIRA PREV-90 : When click on advance search from gift registry login page, focus is happening towards top of the page.
        $(this).toggleClass('expanded').next('.toggle-content').toggle();
    });

     // subscribe email box
    var $subscribeEmail = $('.subscribe-email');
    if ($subscribeEmail.length > 0) {
        $subscribeEmail.focus(function () {
            var val = $(this.val());
            if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
                return; // do not animate when contains non-default value
            }

            $(this).animate({
                color: '#999999'
            }, 500, 'linear', function () {
                $(this).val('').css('color', '#333333');
            });
        }).blur(function () {
            var val = $.trim($(this.val()));
            if (val.length > 0) {
                return; // do not animate when contains value
            }
            $(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
                .css('color', '#999999')
                .animate({
                    color: '#333333'
                }, 500, 'linear');
        });
    }

    $('.privacy-policy').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });

    // main menu toggle
    $('.menu-toggle').on('click', function () {
        $('#wrapper').toggleClass('menu-active');
    });
    $('.menu-category li .menu-item-toggle').on('click', function (e) {
        e.preventDefault();
        var $parentLi = $(e.target).closest('li');
        $parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('active');
        $parentLi.toggleClass('active');
        $(e.target).toggleClass('active');
    });
    
    if((!($('#wrapper').hasClass('pt_checkout') || $('#wrapper').hasClass('pt_order-confirmation'))) && $(window).width() > 767) {
    	var $topBanner = $('.top-banner'),
    		topBannerOffset = $topBanner.offset().top,
    		topBannerHeight = $topBanner.outerHeight(),
    		headerBanner = $('.header-banner').outerHeight();
    	$(window).scroll(function(){
    		var windowScroll = $(window).scrollTop();
			if (windowScroll > topBannerOffset && ($(document).height() - topBannerHeight) > $(window).height()) {
    			$('#header').addClass('header-fixed');
    			var totalHeight =  headerBanner + $('#header.header-fixed').find('.top-banner').outerHeight();
    			$('#wrapper').css('margin-top', totalHeight);
    		} else if (windowScroll <= topBannerOffset) {
    			$('#wrapper').removeAttr('style');
    			$('#header').removeClass('header-fixed');
    		}
    	});
    }
    
    /*stickyHeader();
    if (util.getCookie('cookie-header-banner') !== '' && 
    		util.getCookie('cookie-header-banner') !== undefined && 
    		util.getCookie('cookie-header-banner') == 'closing-banner') {
    	$('.header-banner').hide();
    }
    $('.header-banner').find('.close-btn').on('click', function() {
    	var bannerId = $(this).attr('id');
    	$(this).closest('.header-banner').hide();
    	stickyHeader();
    	util.createCookie('cookie-header-banner', bannerId);
    });
    
    $(window).resize(function() {
    	stickyHeader();
    });*/
    
    //Back-to-top functionality
    if($('#header').hasClass('french-site-btt')) {
    	$('#header').prepend('<div class="back-to-top-btn french-site-btt"></div>');
    } else {
    	$('#header').prepend('<div class="back-to-top-btn"></div>');
    }
    $('.back-to-top-btn').click(function() {
		$('html, body').animate({ scrollTop: 0 }, "slow");
	});
	
	$(window).scroll(function() {
		var windowTop = $(window).scrollTop(),
		footerTop = $('#footer').offset().top,
		footerHeight = $('#footer').height();
		if (windowTop > 500 && (!($('#wrapper').hasClass('pt_checkout') || $('#wrapper').hasClass('pt_cart')))) {
			$('.back-to-top-btn').addClass('fixed-button');
			$('.back-to-top-btn').show();
		} else {
			$('.back-to-top-btn').removeClass('fixed-button');
			$('.back-to-top-btn').hide();
		} 
		
	});
    
    $('.user-account').on('click', function (e) {
    	e.preventDefault();
    	if($(window).width() > 767) {
    		if($(this).closest(".user-logged-in").length > 0){
    			$(this).closest('.user-info').toggleClass('active');
    		}else if($(this).closest(".user-logged-in").length == 0 && window.pageContext.type == 'checkout'){
    			$(this).parent('.user-info').toggleClass('active');
    		}else{
    			$(this).parent('.user-info').toggleClass('active');
    		}
    	}
    });
    
    //bazaar voice changes
    $('.tab.reviews').on('click', '.tab-label', function() {
    	$(this).toggleClass('active');
    	$(this).closest('.tab.reviews').find('.tab-contentshow').toggleClass('active');
    });
    
    $('.footer-banner').on('click', "h2", function() {
		if($(window).width() <= 767)
		{
			if($(this).find('i').hasClass('footer-minus'))
			{
				$(this).closest('.footer-banner').find('i').removeClass('footer-minus');
				$(this).closest('.footer-banner').find('.menu-footer').slideUp();
			}
			else
			{
				$(this).closest('.footer-banner').find('i').removeClass('footer-minus');
				$(this).closest('.footer-banner').find('.menu-footer').slideUp();
				$(this).find('i').addClass('footer-minus');
				$(this).next('ul').slideDown();
			}
			var this_ul = $(this).next("ul.menu-footer");
			$("html,body").animate({
		        scrollTop: $(this_ul).offset().top-55
		    },'slow');
		}
	});
}
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
    // add class to html for css targeting
    $('html').addClass('js');
    
    if (navigator.userAgent.indexOf("MSIE") != -1){
    	$('body').addClass('IE');
    }
    
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('html').addClass('infinite-scroll');
    }
    // load js specific styles
    util.limitCharacters(); 
    util.selectbox();
    var bLazy = new Blazy({
   	 
    	/*success: function(){
    	    updateCounter();
    	  }*/
    	});
   	$('body').addClass(util.isMobile() ? 'app-device' : 'app-desktop');
}

//function stickyHeader() {
//	var sticyHeight = $('.sticky-header').outerHeight();
//    if($(window).width() > 767) {
//    	$('.sticky-header').addClass('fixed-header');
//    	$('#wrapper').css('padding-top', sticyHeight);
//    }
//}

var pages = {
    account: require('./pages/account'),
    cart: require('./pages/cart'),
    checkout: require('./pages/checkout'),
    compare: require('./pages/compare'),
    product: require('./pages/product'),
    registry: require('./pages/registry'),
    search: require('./pages/search'),
    storefront: require('./pages/storefront'),
    wishlist: require('./pages/wishlist'),
    storelocator: require('./pages/storelocator'),
    orderconfirmation: require('./pages/checkout/orderconfirmation')
};

var app = {
    init: function () {
        if (document.cookie.length === 0) {
            $('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
        }
        initializeDom();
        initializeEvents();

        // init specific global components
        countries.init();
        tooltip.init();
        minicart.init();
        validator.init();
        searchplaceholder.init();
        // execute page specific initializations
        $.extend(page, window.pageContext);
        var ns = page.ns;
        if (ns && pages[ns] && pages[ns].init) {
            pages[ns].init();
        }

        // Check TLS status if indicated by site preference
        if (SitePreferences.CHECK_TLS === true) {
            tls.getUserAgent();
        }

        //PREVAIL-Initializations
        if (isSPCEnabled) {
            require('./spc').init();
        }

        if (isGTMEnabled) {
            require('./gtm').init();
        }
        
        if (isBISNEnabled) {
            require('./bisn').init();
        }
        
        require('./sessionbased').init();
        
        global.init();
        
        if(isWelcomeMatEnabled || window.SitePreferences.WELCOMEMAT_NEWRULE){
        	require('./welcomemat').init();
        }
    }
};

// general extension functions
(function () {
    String.format = function () {
        var s = arguments[0];
        var i, len = arguments.length - 1;
        for (i = 0; i < len; i++) {
            var reg = new RegExp('\\{' + i + '\\}', 'gm');
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };
})();

// initialize app
$(document).ready(function () {
	app.init();
});
