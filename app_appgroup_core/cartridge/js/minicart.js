'use strict';

var util = require('./util'),
    bonusProductsView = require('./bonus-products-view');

var timer = {
    id: null,
    clear: function () {
        if (this.id) {
            window.clearTimeout(this.id);
            delete this.id;
        }
    },
    start: function (duration, callback) {
        this.id = setTimeout(callback, duration);
    }
};

var minicart = {
    init: function () {
        this.$el = $('#mini-cart');
        this.$content = this.$el.find('.mini-cart-content');

        // events
        this.$el.find('.mini-cart-total').on('mouseenter', function (e) {
            if (this.$content.not(':visible')) {
            	if(util.isMobile()){ $(e.target).addClass('clicked'); }
                this.slide();
            }
        }.bind(this));

        this.$content.on('mouseenter', function () {
            timer.clear();
        }).on('mouseleave', function () {
            timer.clear();
            timer.start(30, this.close.bind(this));
        }.bind(this));
        
        this.$el.find('.mini-cart-link:not(.mini-cart-empty)').on('click', function(e){
        	if(!$(e.target).hasClass('clicked') && util.isMobile()){
    			e.preventDefault();
    			$(e.target).addClass('clicked');
    		}
        }.bind(this));
    },
    /**
     * @function
     * @description Shows the given content in the mini cart
     * @param {String} A HTML string with the content which will be shown
     */
    show: function (html) {
        this.$el.html(html);
       	util.scrollBrowser(0);
       	setTimeout(function(){
       		bonusProductsView.loadBonusOption();	
       	}, 400);
        this.init();
        this.slide();
    },
    /**
     * @function
     * @description Slides down and show the contents of the mini cart
     */
    slide: function () {
        timer.clear();
        // show the item
        this.$content.slideDown('slow', function(){
        	$('#search-suggestions').empty().hide();
        	var $h = 0;
        	if($('.mini-cart-products .mini-cart-product').length > 4){
        		$.each($('.mini-cart-products .mini-cart-product').splice(0,4), function(){
        			$h += $(this).outerHeight();
        		});
        		$('.mini-cart-products').height($h);
        		util.customscrollbar($('#mini-cart .mini-cart-products'));
        	}else{
        		$('.mini-cart-products').height('auto');
        	}
        	
        });
        // after a time out automatically close it
        timer.start(6000, this.close.bind(this));
    },
    /**
     * @function
     * @description Closes the mini cart with given delay
     * @param {Number} delay The delay in milliseconds
     */
    close: function (delay) {
        timer.clear();
        this.$content.slideUp(delay);
    }
};

module.exports = minicart;
