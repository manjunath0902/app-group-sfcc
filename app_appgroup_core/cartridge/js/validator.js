'use strict';

var naPhone = /^\(?([2-9][0-8][0-9])\)?[\-\. ]?([2-9][0-9]{2})[\-\. ]?([0-9]{4})(\s*x[0-9]+)?$/;
var regex = {
    phone: {
        us: naPhone,
        ca: naPhone,
        generic: /^[\d| |(|)|+|-]+$/         
    },
    postal: {
        us: /^\d{5}(-\d{4})?$/,
        ca: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/,
        generic: /^(\w| |-){3,10}$/
    },
    notCC: /^(?!(([0-9 -]){13,19})).*$/
};
// global form validator settings
var settings = {
    errorClass: 'error',
    errorElement: 'span',
    onkeyup: false,
    onfocusout: function (element) {
    	if($('#footer-subscription-form').find('.subscription-success').text().length != 0){
    		$('#footer-subscription-form').find('.subscription-success').html('');
    	}
        if (!this.checkable(element)) {
            this.element(element);
        }
    },
    errorPlacement: function(error, element) {
		if($(element).hasClass('input-select')){
			$(element).closest('.custom-select').append(error);
		}else if($(element).hasClass('input-checkbox')){
			$(element).closest('.form-row') ? $(element).closest('.form-row').append(error) : $(error).insertAfter($(element).closest('.checkbox-wrapper'));
		}else{
			error.insertAfter(element);
		}
	}
};
/**
 * @function
 * @description Validates a given phone number against the countries phone regex
 * @param {String} value The phone number which will be validated
 * @param {String} el The input field
 */
var validatePhone = function (value, el) {
    var country = $(el).closest('form').find('.country');
    if (country.length === 0 || country.val().length === 0 ) {
        return true;
    }
    var rgx = regex.phone[country.val().toLowerCase()];
    if (!rgx) {
    	rgx = regex.phone["generic"];
    }

   
    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));

    return isOptional || isValid;
};

/**
 * @function
 * @description Validates that a credit card owner is not a Credit card number
 * @param {String} value The owner field which will be validated
 * @param {String} el The input field
 */
var validateOwner = function (value) {
    var isValid = regex.notCC.test($.trim(value));
    return isValid;
};

/**
 * Add phone validation method to jQuery validation plugin.
 * Text fields must have 'phone' css class to be validated as phone
 */
$.validator.addMethod('phone', validatePhone, Resources.INVALID_PHONE);

/**
 * Add CCOwner validation method to jQuery validation plugin.
 * Text fields must have 'owner' css class to be validated as not a credit card
 */
$.validator.addMethod('owner', validateOwner, Resources.INVALID_OWNER);

/**
 * Add gift cert amount validation method to jQuery validation plugin.
 * Text fields must have 'gift-cert-amont' css class to be validated
 */
$.validator.addMethod('gift-cert-amount', function (value, el) {
    var isOptional = this.optional(el);
    var isValid = (!isNaN(value)) && (parseFloat(value) >= 5) && (parseFloat(value) <= 5000);
    return isOptional || isValid;
}, Resources.GIFT_CERT_AMOUNT_INVALID);

/**
 * Add positive number validation method to jQuery validation plugin.
 * Text fields must have 'positivenumber' css class to be validated as positivenumber
 */
$.validator.addMethod('positivenumber', function (value) {
    if ($.trim(value).length === 0) { return true; }
    return (!isNaN(value) && Number(value) >= 0);
}, ''); // '' should be replaced with error message if needed

/*Start JIRA PREV-77 : Zip code validation is not happening with respect to the State/Country.*/
function validateZip(value, el) {
    var country = $(el).closest('form').find('.country');
    if (country.length === 0 || country.val().length === 0) {
        return true;
    }
    var countryCode = country.val();
    var isOrderTrackingForm = el.attributes.class.value.indexOf("order-tracking") != -1;
    if (isOrderTrackingForm) {
    	countryCode = "generic";
    }
    if (countryCode == 'CA') {
        $(el).val($(el).val().toUpperCase().replace(/\W/g,'').replace(/(...)/,'$1 '));
    }
    var rgx = regex.postal[country.val().toLowerCase()];
    if (!rgx) {
    	rgx = regex.postal["generic"];
    }

    var isOptional = this.optional(el);
    var isValid = rgx.test($.trim(value));
    return isOptional || isValid;
}
$.validator.addMethod('postal', validateZip, Resources.INVALID_ZIP);
/*End JIRA PREV-77*/

/*Start JIRA PREV-678 : Email and confirm email fields validation in gift certificate page not as expected.*/
var validateEmail = function (value, el) {
    value = $.trim(value);
    $(el).val(value);
    var isOptional = this.optional(el);
    var emailRegEx = /^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,}$/;
    var isValid = emailRegEx.test(value);
    return isOptional || isValid;
};
$.validator.addMethod('email', validateEmail, Resources.VALIDATE_EMAIL);
/*End JIRA PREV-678*/

$.extend($.validator.messages, {
    //required: Resources.VALIDATE_REQUIRED,
    remote: Resources.VALIDATE_REMOTE,
    email: Resources.VALIDATE_EMAIL,
    url: Resources.VALIDATE_URL,
    date: Resources.VALIDATE_DATE,
    dateISO: Resources.VALIDATE_DATEISO,
    number: Resources.VALIDATE_NUMBER,
    digits: Resources.VALIDATE_DIGITS,
    creditcard: Resources.VALIDATE_CREDITCARD,
    equalTo: Resources.VALIDATE_EQUALTO,
    maxlength: $.validator.format(Resources.VALIDATE_MAXLENGTH),
    minlength: $.validator.format(Resources.VALIDATE_MINLENGTH),
    rangelength: $.validator.format(Resources.VALIDATE_RANGELENGTH),
    range: $.validator.format(Resources.VALIDATE_RANGE),
    max: $.validator.format(Resources.VALIDATE_MAX),
    min: $.validator.format(Resources.VALIDATE_MIN)
});
$.validator.messages.required = function ($1, ele) {
	var requiredText = $(ele).closest('.form-row').attr('data-required-text');
	if (requiredText != 'null' && requiredText != 'undefined') {
		return requiredText || '';
	} else {
		return Resources.VALIDATE_REQUIRED;
	}
	
};

var validator = {
    regex: regex,
    settings: settings,
    init: function () {
        var self = this;
        $('form:not(.suppress)').each(function () {
            $(this).validate(self.settings);
        });
        $('.numberfield input').focusout(function (e) {
        	e.preventDefault();
	       	 if( isNaN( parseInt($(this).val())) && !$('.err-msg').length && $(this).val()) {	       		 
	       		   $('<span class="error err-msg">' +Resources.VALIDATE_NUMBER+ '</span>').appendTo('.numberfield .field-wrapper');	       		 
	       		   return false;
	       	 }    	
       });
        $('.numberfield input').focus(function () {
          	 $(this).closest('.numberfield').find('span.error').remove(); 	
          });
    },
    initForm: function (f) {
        $(f).validate(this.settings);
    }
};

module.exports = validator;
