'use strict';

exports.updatePaymentMethod = function(paymentMethodID, $selectedPaymentMethod) {
    var $paymentMethodeInput = $('#is-' + paymentMethodID);
    if ($paymentMethodeInput.data('is-hosted-tokenization') == true) {
        $('[name="dwfrm_billing_save"]').hide();
        $selectedPaymentMethod = $('.payment-method.hosted-tokenization-page');
        var url = Urls.hostedTokenizationPage;
        url += '?paymentMethode=' + ($paymentMethodeInput.val() == 'CREDIT_CARD' ? 'CreditCard' : $paymentMethodeInput.val());
        var saveAliasSelector = $('#dwfrm_billing_paymentMethods_saveAlias');
        var savedAliasSelector = $('#savedAliases');
        var paymentMethode;
        if ($('#savedAliases').length > 0) {
            paymentMethode = $('#savedAliases').children('[value="' + $('#savedAliases').val() + '"]').html().trim().replace(/\s/g, '_').toUpperCase();
        } else {
            paymentMethode = null;
        }
        if (saveAliasSelector.length > 0 && savedAliasSelector.length > 0 && savedAliasSelector.val().length > 0 && (paymentMethodID == paymentMethode)) {
            url += '&useAlias=' + savedAliasSelector.val();
        } else if (saveAliasSelector.filter(':checked').length > 0) {
            url += '&saveAlias=true';
        }
        if (paymentMethodID !== paymentMethode) {
            $('#savedAliases').val('');
        }
        $.get(url, function(data) {
            $selectedPaymentMethod.html(data);
        });
    } else {
        $('[name="dwfrm_billing_save"]').show();
    }
};

exports.init = function() {
    if ($('.checkout-billing').length == 0) {
        return;
    }

    $('.aliasGateway').children().click(function() {
        $('.payment-method-options.hide-payment-method').show();
        $('.payment-method.hide-credit-card div,span').show();
        $('.direct-debits.hide-direct-debits').show();
        $('.payment-method-display, div.cvn-tip .tooltip-content, div.cvn-tip .dw-object-rinclude').hide();
    });

    $('.input-radio').click(function(e) {
        var allowAlias = $(e.target).attr('data-alias-gateway-available');
        if (allowAlias == 'false') {
            $('.aliasGateway').hide();
        } else {
            $('.aliasGateway').show();

        }
    });

    $('#dwfrm_billing_paymentMethods_creditCard_type').change(function(e) {
        var creditCardType = $(e.target).find(':selected').val();
        var allowAliasCard = $('#' + creditCardType).attr('data-alias-card-available');
        if (allowAliasCard == 'false') {
            $('.aliasGateway').hide();
        } else {
            $('.aliasGateway').show();
        }
    });

    $('#creditCardList').change(function() {
        setTimeout('$("#dwfrm_billing_paymentMethods_creditCard_type").trigger("change");', 300);
    });

    $('#dwfrm_billing_paymentMethods_saveAlias').on('click', function() {
        $('.payment-method-options input:checked').first().click();
        $('.dw-object-rinclude').hide();
    });

    $('#savedAliases').on('change', function() {
        if ($(this).val().length > 0) {
            var paymentMethode = $(this).children('[value="' + $(this).val() + '"]').html().trim().replace(/\s/g, '_').toUpperCase();
            $('#is-' + paymentMethode).first().click();
            $('.dw-object-rinclude').hide();
        }
    });

    setInterval(function() {
        $.get(Urls.hostedTokenizationSucces, function(data) {
            if (data.success) {
                $('[name="dwfrm_billing_save"]').click();
            }
        })
    }, 5000)
}
