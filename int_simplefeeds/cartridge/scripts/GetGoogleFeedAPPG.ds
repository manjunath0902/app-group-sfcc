/**
* GoogleFeed.ds
* This script is used to create product feed in csv format*
* @input CurrentWorkflowComponentInstance : Object
* @input CurrentRequest : dw.system.Request
* @output File : dw.io.File;
*
*/

importPackage( dw.io );
importPackage( dw.net );
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.object );
importPackage( dw.catalog );
importPackage( dw.content );

function execute( args : PipelineDictionary ) : Number
{
 	
 	//Create Folder	  	
	(new dw.io.File(dw.io.File.IMPEX +'/src/GoogleFeed/')).mkdirs(); 
  	var file : File = new File(dw.io.File.IMPEX +'/src/GoogleFeed/' + args.CurrentWorkflowComponentInstance.getParameterValue('FileNamePrefix') +'_'+ dw.system.Site.getCurrent().ID +".csv");
	if( !file.exists() ){
		file.createNewFile();
	}

	var fileWriter : dw.io.FileWriter = new dw.io.FileWriter(file);
	var csvWriter : CSVStreamWriter =  new CSVStreamWriter(fileWriter);	
	
	var JSONString : String =[];
	  		JSONString[0]="id";
	  		JSONString[1]="title";
	  		JSONString[2]="description";
	  		JSONString[3]="link";
	  		JSONString[4]="image_link";
	  		JSONString[5]="availability";
	  		JSONString[6]="price";
	  		JSONString[7]="brand";
	  		JSONString[8]="gtin";
	  		JSONString[9]="condition";
	  		JSONString[10]="adult";
	  		JSONString[11]="age_group";
	  		JSONString[12]="color";
	  		JSONString[13]="gender";
	  		JSONString[14]="size";
	  		JSONString[15]="size_system";
	  		JSONString[16]="shipping";
	  		JSONString[17]="Item_group_id";
	  		JSONString[18]="Product_type";
	  		JSONString[19]="searchable";
	  		
	  	csvWriter.writeNext(JSONString);
	
	
	var it : SeekableIterator = ProductMgr.queryAllSiteProducts(); 
	var productList : ArrayList = it.asList();
	var productiterator : Iterator = productList.iterator();
	var siteLocales = dw.system.Site.getCurrent().allowedLocales;
	var friendlyURL = args.CurrentWorkflowComponentInstance.getParameterValue('FriendlyURL');
	try{
		while(productiterator.hasNext()){ 
			var product : Product = productiterator.next();
			var locale = '';

			if(product != null && !empty(product)){
				try 
				{	
					for(var i=0 ; i<siteLocales.size() ; i++){	
						locale = args.CurrentRequest.setLocale(siteLocales.get(i));
						//Product ID
						var id = product.getID();
						
						//Product Name
						if(product.getName() != null){
					  		var title = product.getName();
						}else{
							var title ="";
						}
						
						//Product short description
						if(product.shortDescription != null  && !empty(product.shortDescription)){
					  		var shortDescription = product.shortDescription;
						}else{
							var shortDescription ="";
						}
						var sitePrefs = dw.system.Site.getCurrent().getPreferences();
						//Product URL
						if(friendlyURL != null && friendlyURL != ''){
							var indexURL = 0;
							if(!empty(sitePrefs.getCustom()["siteName"]) && sitePrefs.getCustom()["siteName"].toString() == 'MK'){
								var indexURL = 0;
							} else if(!empty(sitePrefs.getCustom()["siteName"]) && sitePrefs.getCustom()["siteName"].toString() == 'SK'){
								var indexURL = 1;
							}
							
							if (dw.system.Site.getCurrent().ID.indexOf('_') == -1) {
								var productURL = dw.web.URLUtils.http('Product-Show', 'pid', product.getID()).host(friendlyURL.split(',')[indexURL]);
							} else {
								var urlLocale = ('/' + dw.system.Site.getCurrent().ID.split('_')[1].toLowerCase() + '/' + siteLocales.get(i).split('_')[0].toLowerCase()).replace('uk','gb');
								var productURL = dw.web.URLUtils.http('Product-Show', 'pid', product.getID()).host(friendlyURL.split(',')[indexURL]+urlLocale);
							}
						} else {
							var productURL = dw.web.URLUtils.http('Product-Show', 'pid', product.getID());
						}
					  	
						//Product's first feed image URL,if not present then checks for large image
						if(!empty(product.getImage("feed",0)) && product.getImage("feed",0) != null){
					  		var imageLink = product.getImage("feed",0).getURL().toString();
						}else if(!empty(product.getImage("large",0)) && product.getImage("large",0) != null){
					  		var imageLink = product.getImage("large",0).getURL().toString();
						}else{
							var imageLink="";
						}
						
						//Product availableStatus
						var availableStatus = "";
						if(!product.master){
							if (product.getAvailabilityModel().getAvailabilityLevels(1).inStock.value > 0) {
								availableStatus = "in-stock";
							}else if(product.getAvailabilityModel().getAvailabilityLevels(1).preorder.value > 0) {	
								availableStatus = "Preorder";
							}else if(product.getAvailabilityModel().getAvailabilityLevels(1).backorder.value > 0) {
								availableStatus = "Backorder";
							}else if(product.getAvailabilityModel().getAvailabilityLevels(1).notAvailable.value > 0) {
								availableStatus = "out of stock";
							}
						}
						
						//Product sale price
						var price = "";
						if(!product.master){
							if(product.getPriceModel().price != null){
								var price = product.getPriceModel().price;
						  	}else{
						  		var price = "";
						  	}
						}
					  	
						//Product level custom attribute BrandNameDesc
						if(product.custom.BrandNameDesc != null && !empty(product.custom.BrandNameDesc)){
					  		var brand = product.custom.BrandNameDesc;
						}else{
							var brand ="";
						}
						
						//Product UPC attribute
					  	if(product.getUPC() != null && !empty(product.getUPC())){
					  		var gtin=product.getUPC();
					  	}else{
					  		var gtin="";
					  	}
					  	
					  	// Static text for condition
					  	var condition = "new";
					  	
					  	// Static text for adult
					  	var adult = "no";
					  	
					  	// Static text for agegroup
					  	if(i == 0){
						  	var topCatID = getTopCategory(product);
						  	var ageGroup = '';
						  	if(topCatID == "Kids"){
						  		ageGroup = "Kids";
						  	}else{
						  		ageGroup = "Adult";
						  	}
					  	}
						// color
						var color = '';
						if(!product.master){
							if (product.isVariant()) {
								if(product.custom.color != null && (dw.system.Site.getCurrent().ID == 'MK' || dw.system.Site.getCurrent().ID == 'MK_CA')){
									color = product.custom.color;
								}else{
									color = product.custom.refinementColor;
								}
							}
						}
						
						// Static text for adult
					  	var gender = "";
					  	if(topCatID == "women" || topCatID == "Accessories" || topCatID == "sales"){
					  		gender = "Female";
					  	}else if(topCatID == "Kids"){
					  		gender = "Unisex";
					  	}else if(topCatID == "men"){
					  		gender = "Male";
					  	}
						
						// size
						var size = '';
						if(!product.master){
							if (product.isVariant()) {
								if(product.custom.size != null){
									size = product.custom.size;
								}
							}
						}
						
						// Static text for sizeSystem
						var sizeSystem = "US";
						
						// Static text for shipping
						var shipping = "Free Shipping";
						
						var itemGroupID = "";
						if(!product.master){
							itemGroupID = product.variationModel.master.ID;
						}
						
						var productType = getCategoryPath(product);
						
						var searchable = "No";
						if(product.searchable){
							searchable = "Yes";
						}else{
							searchable = "No";
						}
						
						var JSONString : String =[];
					  		JSONString[0]=id;
					  		JSONString[1]=title;
					  		JSONString[2]=shortDescription;
					  		JSONString[3]=productURL;
					  		JSONString[4]=imageLink;
					  		JSONString[5]=availableStatus;
					  		JSONString[6]=price;
					  		JSONString[7]=brand;
					  		JSONString[8]=gtin;
					  		JSONString[9]=condition;
					  		JSONString[10]=adult;
					  		JSONString[11]=ageGroup;
					  		JSONString[12]=color;
					  		JSONString[13]=gender;
					  		JSONString[14]=size;
					  		JSONString[15]=sizeSystem;
					  		JSONString[16]=shipping;
					  		JSONString[17]=itemGroupID;
					  		JSONString[18]=productType;
					  		JSONString[19]=searchable;
							
						csvWriter.writeNext(JSONString);
					}
				}
				catch (ex)
				{
					Logger.error( "Product Feed Error : " + product.getName() + ': ' + ex );		
				}
			}
		}
		fileWriter.flush();
		csvWriter.close();
		fileWriter.close();
		it.close();		
		args.File = file;
		
		return PIPELET_NEXT;
	}
	catch(e){
		Logger.error( "Product Feed Error : " + e );
		return PIPELET_ERROR;
	}	
}

function getTopCategory(product : Product) : String {
	var topProduct : Product = product;
	if( topProduct.isVariant() ) {
		topProduct = product.masterProduct;	
	}
	var theCategory : Category = topProduct.getPrimaryCategory();
	if( empty(theCategory) ) {
		var categories : Collection = topProduct.categories;
		if( !empty(	categories ) ) {
			theCategory = categories[0];	
		}
	}

	var cat : Category = theCategory;
	var path : dw.util.ArrayList = new dw.util.ArrayList();
	while( cat.parent != null)
	{
		if(cat.online){
			path.addAt( 0, cat );
		}
		cat = cat.parent; 
	}

	return path.length > 0 ? path.get(0).ID : '';
}

function getCategoryPath(product : Product) : String {
	var categoryPath : String ="";	
	var topProduct : Product = product;
		if( topProduct.isVariant() ) {
			topProduct = product.masterProduct;	
		}
		var theCategory : Category = topProduct.getPrimaryCategory();
		if( empty(theCategory) ) {
			var categories : Collection = topProduct.categories;
			if( !empty(	categories ) ) {
				theCategory = categories[0];	
			}
		}

	var cat : Category = theCategory;
	var path : dw.util.ArrayList = new dw.util.ArrayList();
			while( cat.parent != null)
			{
				if(cat.online){
					path.addAt( 0, cat );
				}	
				cat = cat.parent; 
			}

	var index : Number;	
	for(index=0; index<path.length ; index++){
		if(index==0) {
			categoryPath = categoryPath + path[index].getDisplayName();
		} else {
			categoryPath = categoryPath + ">" + path[index].getDisplayName();
		}
	}	
	if(categoryPath != ""){
		categoryPath = categoryPath + ">" + product.getName();
	}
	return categoryPath;
}