/**
* Script file for use in the Script pipelet node.
* To define input and output parameters, create entries of the form:
*
* @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
*
* where
*   <paramUsageType> can be either 'input' or 'output'
*   <paramName> can be any valid parameter name
*   <paramDataType> identifies the type of the parameter
*   <paramComment> is an optional comment
*
* For example:
*
*    @input orderUpdate  : Object
*    @output orderStatus : Object
*    @output order : dw.order.Order
*
*/

importPackage( dw.system );
importPackage( dw.net );
importPackage( dw.util );
importPackage( dw.value );
importPackage( dw.catalog );
importPackage( dw.order );

function execute( args : PipelineDictionary ) : Number
{

try {
	
	   var orderUpdate = args.orderUpdate;
	   var orderNumber : String = orderUpdate.webOrderNumber;
	   var order : Order = OrderMgr.getOrder(orderNumber);
	   var orderStatus : String = orderUpdate.orderStatus.value;
	   var productLineItem : ProductLineItem;
	   var lineItemUpdates : Array = orderUpdate.deliveryUpdates;
	   
	   for (var i=0;i<lineItemUpdates.length; i++) {
	   		var deliveryObj = {};
	   		var lineItemCD = lineItemUpdates[i];
	   		var lineItemDeliveryStatus = lineItemCD.deliveryStatus.value != null ? lineItemCD.deliveryStatus.value : '';
	   		var trackingNumber = lineItemCD.trackingNumber != null ? lineItemCD.trackingNumber : '';
	   		deliveryObj.lineItemDeliveryStatus = lineItemDeliveryStatus;
	   		deliveryObj.trackingNumber = trackingNumber;
	   		 
	   		var returnOptions : Array = lineItemCD.returnOptions != null ? lineItemCD.returnOptions : new Array();
	   		
	   		for (var j=0;j<returnOptions.length;j++) {
	   			var skuInfo : String = returnOptions[j];
	   			var skuID = skuInfo.split('=')[1].split(',')[0];
	   			
	   			if (order.getProductLineItems(skuID).length > 0) {
	   				productLineItem = order.getProductLineItems(skuID)[0];
	   				productLineItem.custom.chainDriveItemLevelDeliveryInfo = JSON.stringify(deliveryObj);
	   			}
	   		}
			   		
	   }
	  	
	   order.custom.cdOrderLevelStatus = orderStatus;	
	   args.order = order;
	   args.orderStatus = orderStatus;

}

catch (e) {
		var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Statck:" + e.stack;
		Logger.error(errorMsg);
		return PIPELET_ERROR;
}
  
  
   return PIPELET_NEXT;
}
