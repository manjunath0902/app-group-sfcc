/**
 *
 * A library file for Cybersource communication.
 * This file is included by several script nodes using:
 *
 * importScript( "chaindrive/libChainDrive.ds" );
 *
 * It cannot be used in a script node by itself.
 *
 */

importPackage( dw.system );
importPackage( dw.net );
importPackage( dw.util );
importPackage( dw.value );
importPackage( dw.catalog );
importPackage( dw.order );
importPackage( dw.rpc );
importPackage( dw.ws );
importPackage( dw.customer );
importPackage( dw.svc );
var ChainDriveConstants = require("~/cartridge/scripts/util/ChainDriveConstants");		

var ChainDriveHelper = {

	//cdReference : webreferences2.ChainDrive_Transaction,
	
	
	getMerchantID : function () {
		//return Site.getCurrent().getCustomPreferenceValue("CdMerchantId");
	},
	
	getSoapSecurityKey : function() {
		//return Site.getCurrent().getCustomPreferenceValue("CdSecurityKey");
	},
	
	getEndpoint : function getEndpoint() : String {
		//return Site.getCurrent().getCustomPreferenceValue("CdEndpoint") == null ? "" : Site.getCurrent().getCustomPreferenceValue("CdEndpoint").toString();
	},
	
	
	/*****************************************************************************
	 * Name: getAppVersion
	 * Description: get the AppVersion object.
	 ****************************************************************************/	
	getAppVersion : function(request : Object, order : dw.order.Order){
	
		var addAddress = {};
			
	},
	
	
	/*****************************************************************************
	 * Name: GetCustomers
	 * Description: Get the Customers object.
	 ****************************************************************************/	
	getCustomers : function(request : Object, order : dw.order.Order){
	
		var addAddress = {};
			
	},
	
	/*****************************************************************************
	 * Name: getOrderStatus
	 * Description: Get the OrderStatus object.
	 ****************************************************************************/	
	getOrderStatus : function(request : Object, order : dw.order.Order){
	
		var addAddress = {};
			
	},
	
	
	/*****************************************************************************
	 * Name: getDeliveriesUpdates
	 * Description: Get the DeliveriesUpdates of Order object.
	 ****************************************************************************/	
	getDeliveriesUpdates : function(request : Object, order : dw.order.Order){
	
		var addAddress = {};
			
	},
	
	/*****************************************************************************
	 * Name: getOrderObjForAddOrderDeliveryReq
	 * Description: set the Order Object for addOrderDelivery
	 ****************************************************************************/	
	getOrderObjForAddOrderDeliveryReq : function(cdReference : WebReference,  order : Order ){
		var requestObj = setOrderObj( cdReference, order );
		return requestObj; 
	},
	
}

// Helper method to export the helper
function getChainDriveHelper()
{
	return ChainDriveHelper;
}

/*****************************************************************************
	 * Name: getOrderWebAPILogin
	 * Description: Sets the OrderWebAPILogin to request.
	 ****************************************************************************/	

function setOrderWebAPILogin( requestObj : Object, svc : SOAPService ) : Object
{
	
	var serviceCred  = svc.configuration.getCredential();
	requestObj.OrderWebAPILogin.login=serviceCred.user;
	requestObj.OrderWebAPILogin.password=serviceCred.password;
	return requestObj;

}


/*****************************************************************************
	 * Name: setOrderObj
	 * Description: Sets the Order to request.
	 ****************************************************************************/	

function setOrderObj( cdReference : WebReference, order : Order ) : Object
{
	var cdOrderObj = new cdReference.NWebOrder();
	var orderNumber = order.orderNo;
	var currentCal : Calendar = dw.system.Site.getCurrent().getCalendar();
	currentCal.setTime(order.creationDate);
	
	var orderCreationDate : String = StringUtils.formatCalendar(currentCal,"YYYY-MM-DD HH:MM:SS");
	var customerId = order.customerNo != null ? order.customerNo : ' ' ;
	var cDCustomerId = '';//from SFCC it's empty
	
	var taxAmount = order.totalTax.value;
	var shippingAmount = order.adjustedShippingTotalPrice.value;
	var invoiceDiscountAmount = '';
	var referer = '';//FYI: who referred them to the website
	var internalOrderNotes = '';
	var externalOrderNotes = '';
	var iPAddress = '';
	var cDOrderType = ChainDriveConstants.cdOrderType; //13 Site pref
	
	var cCHTransactionid = '';
	var pickUpBranchId = '' ;
	var pickUpDateTime = null;
	
	var customerBillingAddress = new cdReference.CustomerAddress();
	var transporterObj = new cdReference.Transporter();
	var billingAddress = setCustomerAddress(customerBillingAddress, order.billingAddress, order);
	
	var shipment: Shipment = order.shipments[0];
	var shippingAddress : OrderAddress = shipment.shippingAddress;
	
	var customerShippingAddress = new cdReference.CustomerAddress();
	var shippingAddress = setCustomerAddress(customerShippingAddress, shippingAddress, order);
	var transporter = setOrderTransporter(transporterObj,order);
	
	var webDeliveryObj = new cdReference.NWebDelivery();
	webDeliveryObj.giftMessage = '';//TODO OOS
	var webDelivery = setWebDelivery(cdReference , webDeliveryObj, order);
	
	var requestOptions : Array = new Array();
	
	if (order.paymentInstruments.length > 0) {
		var paymentInstrument : OrderPaymentInstrument = order.paymentInstruments[0];
		var langCode = 1;
		if(order.custom.locale != undefined && order.custom.locale != null){
			var locale = order.custom.locale;
			if(locale.indexOf('en') != -1){
				langCode = 1;
			}else {
				langCode = 2;
			}
		}
		requestOptions.push('LANGUAGE='+langCode);
		/*
	 	if (paymentInstrument.paymentMethod ==  PaymentInstrument.METHOD_CREDIT_CARD) {
			if (paymentInstrument.custom.creditCardAuthResponse != undefined && !empty(paymentInstrument.custom.creditCardAuthResponse)) {
				var creditCardResponse = JSON.parse(paymentInstrument.custom.creditCardAuthResponse);
				//TODO: Comment this line to turn off profile ID to ChainDrive
				requestOptions.push('LANGUAGE='+langCode);
			}
		}else{
			requestOptions.push('LANGUAGE='+langCode);	
		}
	 	*/	
		
	}
	
	var date : Date = order.creationDate;
		
	cdOrderObj.orderNumber				= orderNumber;
	cdOrderObj.orderCreationDate		= date;
	cdOrderObj.customerId				= customerId;
	cdOrderObj.CDCustomerId				= cDCustomerId;
	cdOrderObj.customerAddress			= billingAddress;
	cdOrderObj.shipToAddress			= shippingAddress;
	cdOrderObj.orderTransporter			= transporter;
	cdOrderObj.taxAmount				= taxAmount;
	cdOrderObj.shippingAmount 			= shippingAmount;
	cdOrderObj.invoiceDiscountAmount 	= invoiceDiscountAmount;
	cdOrderObj.referer					= referer;
	cdOrderObj.internalOrderNotes		= internalOrderNotes;
	cdOrderObj.externalOrderNotes		= externalOrderNotes;
	cdOrderObj.IPAddress				= iPAddress;
	cdOrderObj.CDOrderType				= cDOrderType;
	cdOrderObj.CCHTransactionid			= cCHTransactionid;
	cdOrderObj.pickUpBranchId			= pickUpBranchId;
	cdOrderObj.pickUpDateTime			= date;
	cdOrderObj.deliveryList				= webDelivery;
	cdOrderObj.requestOptions			= requestOptions;
	
	return cdOrderObj;	
}


/*****************************************************************************
	 * Name: setCustomerAddress
	 * Description: Sets the CustomerAddress to request.
	 ****************************************************************************/	

function setCustomerAddress( requestObj : Object, orderAddress : OrderAddress, order : Order ) : Object
{
	
	var billingAddress : OrderAddress = order.billingAddress;
	var shipment: Shipment = order.shipments[0];
	var shippingAddress : OrderAddress = shipment.shippingAddress;
	var phone = null;
	
	if (orderAddress.phone != null && !empty(orderAddress.phone)) {
		phone = orderAddress.phone;
	} else if (billingAddress.phone != null && !empty(billingAddress.phone)) {
		phone = billingAddress.phone;
	} else if (shippingAddress.phone != null && !empty(shippingAddress.phone)) {
		phone = shippingAddress.phone;
	}
	
	var siteIDObj					= ('siteName' in dw.system.Site.getCurrent().preferences.getCustom() && !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('siteName'))) ? dw.system.Site.getCurrent().getCustomPreferenceValue('siteName') : dw.system.Site.getCurrent().ID;
	var siteID						= siteIDObj.toString();
	
	requestObj.title			= '';
	requestObj.firstName		= orderAddress.firstName !=null ? orderAddress.firstName : '';
	requestObj.lastName			= orderAddress.lastName != null ? orderAddress.lastName : '';
	requestObj.telephone		= phone;
	requestObj.fax				= '';
	requestObj.email			= order.customerEmail;
	requestObj.company			= '';
	requestObj.address1			= orderAddress.address1;
	requestObj.address2			= orderAddress.address2 != null ? orderAddress.address2 : '';
	requestObj.city				= orderAddress.city;
	if (empty(orderAddress.stateCode)) {
		requestObj.state		= orderAddress.countryCode.value;
	} else {
		requestObj.state		= orderAddress.stateCode;
	}
	requestObj.postalCode		= orderAddress.postalCode;
	requestObj.countryID		= orderAddress.countryCode.value;
	requestObj.locationType		= '';// from SFCC it's empty
	
	return requestObj;	
}

/*****************************************************************************
	 * Name: setShipToAddress
	 * Description: Sets the ShipToAddress to request.
	 ****************************************************************************/	
function setShipToAddress( requestObj : Object, order : Order ) : Object
{
	var shipment: Shipment = order.shipments[0];
	var shippingAddress : OrderAddress = shipment.shippingAddress;
	
	var siteIDObj					= ('siteName' in dw.system.Site.getCurrent().preferences.getCustom() && !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('siteName'))) ? dw.system.Site.getCurrent().getCustomPreferenceValue('siteName') : dw.system.Site.getCurrent().ID;
	var siteID						= siteIDObj.toString();
	
	requestObj.title			= '';
	requestObj.firstName		= shippingAddress.firstName;
	requestObj.lastName			= shippingAddress.lastName;
	requestObj.telephone		= shippingAddress.phone;
	requestObj.fax				= '';
	requestObj.email			= order.customerEmail;
	requestObj.company			= '';
	requestObj.address1			= shippingAddress.address1;
	requestObj.address2			= shippingAddress.address2 != null ? shippingAddress.address2 : '';
	requestObj.city				= shippingAddress.city;
	if (empty(shippingAddress.stateCode)) {
		requestObj.state		= shippingAddress.countryCode.value;
	} else {
		requestObj.state		= shippingAddress.stateCode;
	}
	requestObj.postalCode		= shippingAddress.postalCode;
	requestObj.countryID		= shippingAddress.countryCode.value;
	requestObj.locationType		= '';// from SFCC it's empty
	
	
	
	return requestObj;	
}

/*****************************************************************************
	 * Name: setOrderTransporter
	 * Description: Sets the OrderTransporter to request.
	 ****************************************************************************/	

function setOrderTransporter( requestObj : Object, order : Order ) : Object
{
	
	requestObj.transporterId 			= 'PP';//(PP - regular order, PK - ship to store)
	requestObj.transporterName_1 		= '';
	requestObj.transporterName_2 		= '';
	requestObj.trackingNumber 			= '';
	requestObj.shipDate 				= order.creationDate;//TODO
	
	return requestObj;	
}


/*****************************************************************************
	 * Name: setDeliveryList
	 * Description: Sets the DeliveryList to request.
	 ****************************************************************************/	

function setDeliveryList( requestObj : Object, order : Order ) : Object
{
	var shipment : Shipment = order.shipments[0];
	var deliveryNumber = '';
	var deliveryTransporter = 'PP';
	var weightInPounds = '';
	var shippingChargeId = ''; 
	var expediteChargeId = '';
	var expediteAmount = 0.00;
	var shippingAmount = order.adjustedShippingTotalPrice.value;//TODO: shipment.shippingTotalNetPrice; --> this is giving zero
	var userExpediteAmount = 0.00;
	var actualShippingCharges = '';//yet to set
	var shipAsGift = false;
	var giftMessage = '';//TODO
	
	return requestObj;	
}


/*****************************************************************************
	 * Name: setProductDetailList
	 * Description: Sets the ProductDetailList to request.
	 ****************************************************************************/	

function setProductDetailList( cdReference : WebReference, order : Order ) : Object
{
	var itemsList : Array = new Array();
	var paymentCount = order.paymentInstruments.length;
	var productlineItems : Iterator = order.productLineItems.iterator();
	
	var promotionCoupons : Array = new Array();
    try{
		for each (var couponLineItem : CouponLineItem in order.getCouponLineItems()) {
			promotionCoupons.push(couponLineItem.couponCode);
		}
    } catch(e){
    	//TODO: 
    }
	
	while (productlineItems.hasNext()) {
		var item : ProductLineItem = productlineItems.next();
		var itemQty = item.quantityValue;
		
		// Handle tax proration
		if (itemQty > 1) {
			var lineItemTax = item.tax;
			
			// Create an array of 0 value Money objects
			var zeroAmountArray : Array = new Array();
			for (var i=0; i<itemQty; i++) {
				zeroAmountArray.push(new Money(0, item.tax.currencyCode));
			}
			
			// Calculate prorated tax and create separate entries for each quantity of line item
			var proratedTaxArray = Money.prorate(lineItemTax, zeroAmountArray);
			for (var i=0; i<itemQty; i++) {
				var productDetailListObj = new cdReference.NOrderDetail();
				productDetailListObj.lineNote = ' ';//TODO
				var proratedTaxAmount = proratedTaxArray[i];
				var itemObj = setOrderDetail(productDetailListObj, item, paymentCount, promotionCoupons, proratedTaxAmount);
				itemsList.push(itemObj);
			}
		} else {
			var productDetailListObj = new cdReference.NOrderDetail();
			productDetailListObj.lineNote = ' ';//TODO
			var itemObj = setOrderDetail(productDetailListObj, item, paymentCount, promotionCoupons);
			itemsList.push(itemObj);
		}
	}
	
	return itemsList;	
}

/*****************************************************************************
	 * Name: setOrderDetail
	 * Description: Sets the OrderDetail to request.
	 ****************************************************************************/	

function setOrderDetail( requestObj : Object, item : ProductLineItem, paymentCount : Number, promotionCoupons : Array, proratedTaxAmount : Money ) : Object
{
		
        var requestOptions : Array = new Array();
        try{
	        if(!empty(promotionCoupons) && promotionCoupons.length > 0){
	        	for each (var promotionCoupon : String in promotionCoupons) {
	        		var threeCharCouponCode = !empty(promotionCoupon) ? (promotionCoupon.length > 2 ? promotionCoupon.substr(0, 3) : promotionCoupon) : "";//send the 1st 3 characters of the coupon code
	        		if(!empty(threeCharCouponCode)){
		        		requestOptions.push('DISCOUNTTYPE=' + threeCharCouponCode.toLocaleUpperCase());
		        		break;//just add the one coupon (and same coupon) to all lineitems
	        		}
	        	}
	        }
        } catch(e){
        	//TODO: 
        }
		requestObj.skuId			= item.productID; //item.product.UPC != null ? item.product.UPC : item.productID;
		requestObj.quantity			= 1;
		requestObj.sellprice		= (item.proratedPrice.value) / (item.quantityValue);
		requestObj.debitBranch		= '';
		requestObj.lineNote			= '';
		requestObj.retailPrice		= null;
		requestObj.PKPrice			= null; //(item.proratedPrice.value) / (item.quantityValue);
		requestObj.taxes			= proratedTaxAmount ? proratedTaxAmount.value : item.tax.value;
		requestObj.requestOptions	= requestOptions;
	
	return requestObj;	
}

/*****************************************************************************
	 * Name: setPaymentDetailList
	 * Description: Sets the PaymentDetailList to request.
	 ****************************************************************************/	

function setPaymentDetailList( requestObj : Object, order : Order ) : Object
{
	var paymentsList : Array = new Array();
	var paymentInstruments : Iterator = order.paymentInstruments.iterator();
	
	while (paymentInstruments.hasNext()) {
		var paymentInstrument : OrderPaymentInstrument = paymentInstruments.next();
		var paymentInstrumentObj = setOrderPayment(requestObj, paymentInstrument, order);
		paymentsList.push(paymentInstrumentObj);
	}
	
	return paymentsList;	
}

/*****************************************************************************
	 * Name: setOrderPayment
	 * Description: Sets the OrderPayment to request.
	 ****************************************************************************/	

function setOrderPayment( requestObj : Object, paymentInstrument : OrderPaymentInstrument, order : Order  ) : Object
{
	
		var requestOptions : Array = new Array();
		var paymentType : String;
		
		var paymentToken = '';
		
		if (paymentInstrument.paymentMethod ==  PaymentInstrument.METHOD_CREDIT_CARD || paymentInstrument.paymentMethod == PaymentInstrument.METHOD_DW_APPLE_PAY) {
			
			var orderPaymenType = (paymentInstrument.creditCardType.toLowerCase()).replace(/\s/g, "");
			//APPG-669
			if(paymentInstrument.paymentMethod == PaymentInstrument.METHOD_DW_APPLE_PAY){
				paymentType = ChainDriveConstants[PaymentInstrument.METHOD_DW_APPLE_PAY];
			}else{
				paymentType = ChainDriveConstants[orderPaymenType];
			}
			
			if (paymentInstrument.custom.creditCardAuthResponse != undefined && !empty(paymentInstrument.custom.creditCardAuthResponse)) {
				
				var creditCardResponse = JSON.parse(paymentInstrument.custom.creditCardAuthResponse);
				requestOptions.push('AUTHNUMBER=' + creditCardResponse.authCode);
				requestOptions.push('AUTH_ID=' + creditCardResponse.id);
				requestOptions.push('ORDERID=' + order.currentOrderNo);
				requestOptions.push('MERCH_REFNUM=' + order.currentOrderNo);
				if(paymentInstrument.paymentMethod ==  PaymentInstrument.METHOD_CREDIT_CARD){
					requestOptions.push('ACCOUNT=' + Site.getCurrent().getCustomPreferenceValue("PaysafeAccountId"));
				}else if(paymentInstrument.paymentMethod == PaymentInstrument.METHOD_DW_APPLE_PAY){
					requestOptions.push('ACCOUNT=' + Site.getCurrent().getCustomPreferenceValue("NetbanxApplePayAccountID"));
				}
				
				if (creditCardResponse.profile != null && creditCardResponse.profile.cards != null) {
					paymentToken = creditCardResponse.profile.cards[0].paymentToken;
				}
			
			}
			
		 } else if (paymentInstrument.paymentMethod ==  "PayPal") {
		 
		 	paymentType = ChainDriveConstants[paymentInstrument.paymentMethod.toLowerCase()];
		 	
		 	if (paymentInstrument.custom.paypalMappingAttriutesForChainDrive != undefined && !empty(paymentInstrument.custom.paypalMappingAttriutesForChainDrive)) {
		 		
			 	var payPalResponse = JSON.parse(paymentInstrument.custom.paypalMappingAttriutesForChainDrive);
				requestOptions.push('TRANSACTIONID=' + payPalResponse.transactionID);
				requestOptions.push('AUTHORIZATIONID=' + paymentInstrument.custom.paypalAuthID);
				requestOptions.push('MSGSUBID=' + payPalResponse.MSGSUBID);
				requestOptions.push('INVOICEID=' + order.currentOrderNo);
				requestOptions.push('PAYERID=' + payPalResponse.payerID);
				requestOptions.push('ACCOUNT=' + Site.getCurrent().getCustomPreferenceValue("PP_Merchant_ID"));
			 }
		 }
		
		
		requestObj.paymentTypeId 		= paymentType;
		requestObj.paymentDate 			= order.creationDate;//TODO 
		requestObj.amount 				= paymentInstrument.paymentTransaction.amount.value ;
		requestObj.currency 			= order.currencyCode;
		requestObj.exchangeRate 		= ChainDriveConstants.exchangeRate; //1.00 site pref
		requestObj.creditCardNumber 	= '';
		requestObj.creditCardExpiration = '';
		requestObj.creditCardHolder 	= '';
		requestObj.authorisationNumber	= '';
		requestObj.referenceNumber 		= '';
		requestObj.CCV2Code 			= '';
		requestObj.AVCCode 				= '';
		requestObj.custProfileId 		= '';
		requestObj.payProfileId 		= '';
		requestObj.isPreAuthorisation 	= ChainDriveConstants.isPreAuthorisation;// true site pref
		requestObj.paymentToken 		= paymentToken;
		requestObj.requestOptions 		= requestOptions;
	
	return requestObj;	
}

/*****************************************************************************
	 * Name: setWebDelivery
	 * Description: Sets the WebDelivery to request.
	 ****************************************************************************/	

function setWebDelivery( cdReference : WebReference,  requestObj : Object, order : Order  ) : Object
{
		var webDeliveryList : Array = new Array();
		var deliveryNumber : Number = 1000;
		
		/* Commenting as per APPG-583
		if (!empty(order.custom.deliveryNumber)) {
			deliveryNumber = new Number(order.custom.deliveryNumber).toFixed(0);
		} */
		
		 var shipments : Iterator = order.shipments.iterator();
		 while (shipments.hasNext()) {
			var customerAddress 				=   new cdReference.CustomerAddress();
			var transporterObj 					=   new cdReference.Transporter();
			
		 	var shipment : Shipment 			=	shipments.next();
			var shippingAddress : OrderAddress 	=	shipment.shippingAddress;
			var dummyRequestOptions : Array 	=	new Array();//TODO
		 	
			 requestObj.deliveryNumber			=	deliveryNumber;
		     requestObj.shipToAddress			=	setCustomerAddress(customerAddress, shippingAddress, order);
		     requestObj.deliveryTransporter		=	setOrderTransporter(transporterObj, order);
		     requestObj.weightInPounds			=	0.00;
		     requestObj.shippingChargeId		=	shipment.shippingMethodID;//TODO shipping method ID
		     requestObj.expediteChargeId		=	' ';//TODO
		     requestObj.expediteAmount			=	0.00;
		     requestObj.userExpediteAmount		=	0.00;
		     requestObj.actualShippingCharges	=	0.00;//TODO
		     requestObj.shipAsGift				=	false;
		     requestObj.shippingAmount			=	order.adjustedShippingTotalPrice.value;//TODO
		     requestObj.giftMessage				= 	'';
			 var productDetailList				=   setProductDetailList(cdReference, order);
			 var paymentDetailListObj 			=   new cdReference.NOrderPayment();
			 var paymentDetailList 				=   setPaymentDetailList(paymentDetailListObj, order);
		     requestObj.productDetailList		=	productDetailList;
		     requestObj.paymentDetailList		=	paymentDetailList;
		     requestObj.requestOptions			=	dummyRequestOptions;
		     
		     webDeliveryList.push(requestObj);
		     //++deliveryNumber;Commenting as per APPG-583
		     
		     order.custom.deliveryNumber = new Number(deliveryNumber);
		 }
		
	
	return webDeliveryList;	
}
