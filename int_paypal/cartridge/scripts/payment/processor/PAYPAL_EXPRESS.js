'use strict';

/* API Includes */
var Pipeline = require('dw/system/Pipeline');
var OrderMgr = require('dw/order/OrderMgr');
var URLUtils = require('dw/web/URLUtils');

function Handle(args) {
	var pdict = Pipeline.execute('PAYPAL_EXPRESS-Handle', {
		Basket: args.Basket,
		ContinueURL: URLUtils.https('Paypal-ContinueExpressCheckout')
	});
	if(pdict.isSuccess) {
		return {success: true};
	} else {
		return {error: true};
	}
}

function Authorize(args) {
	var pdict = Pipeline.execute('PAYPAL_EXPRESS-Authorize', {
		Order: OrderMgr.getOrder(args.OrderNo),
		PaymentInstrument: args.PaymentInstrument
	});
	if(pdict.isAuthorized) {
		return {authorized: true};
	} else {
		
		if (pdict.ResponseData != undefined && !empty(pdict.ResponseData)) {
			var data = pdict.ResponseData;
			var messages =  [];
			var payPalError = {};
			for (let i = 0, len = data.size(); i < len; i++) {
		
				let message = null;
		
				let errorcodeIndex = 'l_errorcode' + i;
				let shortMessageIndex  = 'l_shortmessage' + i;
				let longMessageIndex = 'l_longmessage' + i;
		
				if (data.containsKey(errorcodeIndex)) {
					payPalError.errorCode = data.get(errorcodeIndex);
					payPalError.message = data.get(shortMessageIndex);
					
				} else {
					continue;
				}
			}
			
		}
		session.privacy.payPalError = JSON.stringify(payPalError);
		return {error: true};
	}
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
