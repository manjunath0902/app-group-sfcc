/**
* Demandware Script File
*
* Captures Bronto cookies and stores them in the session if cookie transfer is enabled.
* This will also assign the TID cookie in the current order's bronto_tid attribute, if possible.
*
* @input CurrentRequest : dw.system.Request
* @input CurrentSession : dw.system.Session
* @input Order : dw.order.Order
*/
importPackage(dw.order);
importPackage(dw.system);
importPackage(dw.util);
importPackage(dw.web);

var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var TID_PREFIX : String = "tid_";
var COOKIES : Array = [TID_PREFIX, "__btr_em", "__btr_id", "bs_t_bc_", "bs_t_"];
var SESSION_PREFIX : String = "bronto_cookie_";
var SESSION_NAME_PREFIX : String = "bronto_cookiename_";
var SESSION_MAXAGE_PREFIX : String = "bronto_cookiemaxage_";
var TID_ATTR : String = "bronto_tid";

function execute(pdict : PipelineDictionary) : Number {
	var cookies : Map = captureCookies(pdict.CurrentRequest, pdict.CurrentSession);
	var tidCookie : Cookie = cookies.get(TID_PREFIX);
	if (!empty(pdict.Order) && !empty(tidCookie)) {
		// Store the TID in the Order so that when it is synced, the conversion is tracked
		pdict.Order.getCustom()[TID_ATTR] = tidCookie.getValue();
	}
	return PIPELET_NEXT;
}

function captureCookies(request : Request, session : Session) : Map {
	var settingsManager = new SettingsManager();
	var transferCookies = settingsManager.getSetting("advanced", ["extensions", "cookies", "transfer-cookies"]);
	var prefixToCookie : Map = new HashMap();
	for (var i = 0; i < COOKIES.length; i++) {
		var prefix : String = COOKIES[i];
		var cookie : Cookie = getCookieByPrefix(request, prefix);
		if (transferCookies) {
			if (!empty(cookie)) {
				// Store the cookie in the session so that it's available if a session transfer occurs
				session.getCustom()[SESSION_PREFIX + prefix] = cookie.getValue();
				session.getCustom()[SESSION_NAME_PREFIX + prefix] = cookie.getName();
				session.getCustom()[SESSION_MAXAGE_PREFIX + prefix] = cookie.getMaxAge();
			} else if (!empty(session.getCustom()[SESSION_PREFIX + prefix])) {
				// There's no cookie, but the cookie is in the session, write the cookie
				cookie = new Cookie(session.getCustom()[SESSION_NAME_PREFIX + prefix], session.getCustom()[SESSION_PREFIX + prefix]);
				cookie.setMaxAge(Number(session.getCustom()[SESSION_MAXAGE_PREFIX + prefix]));
				cookie.setPath("/");
				response.addHttpCookie(cookie);
			}
		}
		if (!empty(cookie)) {
			prefixToCookie.put(prefix, cookie);
		}
	}
	return prefixToCookie;
}

function getCookieByPrefix(request : Request, prefix : String) : Cookie {
	var retCookie : Cookie = null;
	var cookies : Cookies = request.getHttpCookies();
    for (var i = 0; i < cookies.getCookieCount(); i++) {
    	var cookie : Cookie = cookies[i];
    	if (cookie.getName().indexOf(prefix) === 0) {
    		retCookie = cookie;
    		break;
    	}
    }
    return retCookie;
}