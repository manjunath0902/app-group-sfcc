/**
* Demandware Script File
*
* Appends an element to the page if a coupon message is found in
* the session such that the message will be display.
*
* @input CurrentSession : dw.system.Session
* @output CouponMessage : String
* @output BackgroundColor : String
* @output ForegroundColor : String
*/
importPackage(dw.system);

var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");

var COUPON_MESSAGE : String = "brontoCouponMessage";
var COUPON_SUCCESS : String = "brontoCouponSuccess";

function execute(pdict : PipelineDictionary) : Number {
	var settingsManager = new SettingsManager();
	var settings : Object = settingsManager.getSetting("coupon-auto-apply", ["extensions", "message-settings"]);
	var message : String = pdict.CurrentSession.getCustom()[COUPON_MESSAGE];
	var success : Boolean = pdict.CurrentSession.getCustom()[COUPON_SUCCESS];
	if (empty(settings) || !settings["display-flash"] || empty(message)) {
		return PIPELET_ERROR;
	}
	pdict.CouponMessage = message;
	var type : String = success ? "success" : "failure";
	pdict.BackgroundColor = settings["flash-" + type + "-background"];
	pdict.ForegroundColor = settings["flash-" + type + "-foreground"];
	delete pdict.CurrentSession.getCustom()[COUPON_MESSAGE];
	delete pdict.CurrentSession.getCustom()[COUPON_SUCCESS];
	return PIPELET_NEXT;
}