/**
* Demandware Script File
*
* An engine for calculating product recommendations based on recommendation sources and a variety of
* configuration settings such as number of items, exclusions, etc.
*/
importPackage(dw.catalog);
importPackage(dw.util);

function RecommendationEngine(numberOfItems : Number, recommendationSources : Array, exclusionIds : Array) {
	this.numberOfItems = numberOfItems || 0;
	this.recommendationSources = recommendationSources || [];
	this.exclusionIds = exclusionIds || [];
}

/**
 * Determines if the specified product is one that should be excluded based on the exclusion IDs.
 */
RecommendationEngine.prototype.isExcluded = function(product : Product) : Boolean {
	var excluded : Boolean = false;
	var exclusionIds : Array = this.exclusionIds;
	if (!empty(exclusionIds)) {
		for (var i = 0; i < exclusionIds.length; i++) {
			if (exclusionIds[i] == product.getID()) {
				excluded = true;
				break;
			}
		}
	}
	return excluded;
};

/**
 * Gets a list of products that are recommended based on the optional collection of products passed in.
 * This method uses the recommendation sources defined by the constructor and may not return the total
 * number of requested recommendations if the recommendation sources run dry.
 */
RecommendationEngine.prototype.getRecommendations = function(products : Collection) : List {
	var recommendations : List = new ArrayList();
	var recommendationSources : Array = this.recommendationSources;
	if (!empty(recommendationSources)) {
		var productIndex : Number = 0;
		while (recommendations.size() < this.numberOfItems) {
			var sourceIndex : Number = 0;
			var recommendation : Product = null;
			var currProduct : Product = null;
			while (empty(recommendation)) {
				if (!empty(products)) {
					if (productIndex >= products.length) {
						productIndex = 0;
					}
					currProduct = products[productIndex];
				}
				var currRecommendation : Product = null;
				// Try to get a recommendation for the current source and product as long
				// as the current recommendation isn't supposed to be excluded or already 
				// in the list of recommendations
				do {
					currRecommendation = recommendationSources[sourceIndex].getNextRecommendation(currProduct);
				} while (!empty(currRecommendation) && (this.isExcluded(currRecommendation) || recommendations.contains(currRecommendation) || (products && products.contains(currRecommendation))));
				if (!empty(currRecommendation)) {
					recommendation = currRecommendation;
					break;
				} else {
					// No recommendation found, try the next product in the current source
					productIndex++;
				}
				if (empty(products) || productIndex >= products.length) {
					// Either no products were provided our we've tried all our products,
					// try the next source for the first product
					productIndex = 0;
					sourceIndex++;
				}
				if (sourceIndex >= recommendationSources.length) {
					// All of our sources have been exhausted, we can't find any additional product recommendations
					productIndex = 0;
					break;
				}
			}
			if (empty(recommendation)) {
				// No sources found a recommendation, so we've provided all that we can
				break;
			} else {
				// We found a recommendation, try finding a recommendation for the next product
				// on the next iteration
				productIndex++;
				recommendations.add(recommendation);
			}
		}
	}
	return recommendations;
};

module.exports = RecommendationEngine;