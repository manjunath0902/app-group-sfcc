/**
* Demandware Script File
*
* A recommendation source that provides the products that are of the specified recommendation type
* for the passed in Product. (e.g., Cross-sells, upsells, etc.)
*/
importPackage(dw.catalog);
importPackage(dw.util);

var RecommendationSource : Function = require("~/cartridge/scripts/recommendations/RecommendationSource");

function ProductRecommendationSource(type : Number) {
	RecommendationSource.call(this);
	this.type = type;
	this.recommendations = {};
}

ProductRecommendationSource.prototype = new RecommendationSource();
ProductRecommendationSource.constructor = ProductRecommendationSource;

ProductRecommendationSource.prototype.getRecommendations = function(product : Product) : Iterator {
	var recommendations : Iterator = null;
	if (!empty(product)) {
		if (!this.recommendations[product.getID()]) {
			this.recommendations[product.getID()] = product.getOrderableRecommendations(this.type).iterator();
		}
		recommendations = this.recommendations[product.getID()];
	}
	if (empty(recommendations)) {
		recommendations = List.EMPTY_LIST.iterator();
	}
	return recommendations;
};

ProductRecommendationSource.prototype.getNextRecommendation = function(product : Product) : Product {
	var recommendation : Product = null;
	var recommendations : Iterator = this.getRecommendations(product);
	if (recommendations.hasNext()) {
		var rec : Recommendation = recommendations.next();
		recommendation = rec.getRecommendedItem();
	}
	return recommendation;
};

module.exports = ProductRecommendationSource;