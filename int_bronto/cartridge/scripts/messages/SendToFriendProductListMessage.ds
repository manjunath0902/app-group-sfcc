/**
* Demandware Script File
*
* Provides a pipelet that can be used for sending a "send to friend" message for a product list in Bronto.
*
* @input ProductList : dw.customer.ProductList
* @input SendToFriendForm : dw.web.Form
* @input FromName : String
* @input FromEmail : String
* @input ReplyTo : String
* @input Objects : Array	Optional array of objects (JavaScript or ExtensibleObjects to include field tags)
* @input AdditionalRecipients : Array	Optional array of email addresses that will also receive this message
*/
importPackage(dw.customer);
importPackage(dw.util);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var TransactionalMessageHelper = require("~/cartridge/scripts/messages/TransactionalMessageHelper");
var TransactionalMessageScheduler = require("~/cartridge/scripts/messages/TransactionalMessageScheduler");
var CompositeFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeFieldDataProvider");
var GenericFormFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericFormFieldDataProvider");
var ProductListFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/ProductListFieldDataProvider");
var TransactionalMessageHelper : Function = require("~/cartridge/scripts/messages/TransactionalMessageHelper");
var GenericObjectFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericObjectFieldDataProvider");

function execute(pdict : PipelineDictionary) : Number {
	var logger = new BrontoLogger("SendToFriendProductListMessage");
	var messageIdSuffix : String = null;
	switch (pdict.ProductList.getType()) {
	case ProductList.TYPE_GIFT_REGISTRY:
		messageIdSuffix = "gift-registry";
		break;
	case ProductList.TYPE_WISH_LIST:
		messageIdSuffix = "wish-list";
		break;
	default:
		messageIdSuffix = "product-list";
		break;
	}
	var messageId : String = "send-to-friend-" + messageIdSuffix;
	var settingsManager = new SettingsManager();
	var otherField : String = settingsManager.getSetting("order-import", ["extensions", "settings", "other-field"]);
	try {
		var transactionalScheduler = new TransactionalMessageScheduler(messageId);
		var fieldProviders : Array = [
			new GenericFormFieldDataProvider(pdict.SendToFriendForm),
			new ProductListFieldDataProvider(pdict.ProductList, otherField, transactionalScheduler.getMessageOrDefaultSetting("view-type"), TransactionalMessageHelper.getDefaultValues())
		];
		GenericObjectFieldDataProvider.addObjectFieldDataProviders(pdict.Objects, fieldProviders);
		var compositeDataProvider = new CompositeFieldDataProvider(fieldProviders);
		var recipientEmail : String = pdict.SendToFriendForm.friendsemail.value;
		var products : List = TransactionalMessageHelper.getProductsFromListItems(pdict.ProductList.getProductItems());
		var recipients : Array = TransactionalMessageHelper.getCombinedRecipients(recipientEmail, pdict.AdditionalRecipients);
		transactionalScheduler.schedule(compositeDataProvider.getFields(), recipients, products, pdict.FromName, pdict.FromEmail, pdict.ReplyTo);
	} catch (e) {
		logger.getLogger().debug("Error scheduling send to friend product list message: " + e.message);
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}