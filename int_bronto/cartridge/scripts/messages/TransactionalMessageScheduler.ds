/**
* Demandware Script File
*
* Provides a means for scheduling a message to be sent to Bronto. These messages are queued
* and submitted to Bronto during the Process Deliveries job.
*/
importPackage(dw.campaign);
importPackage(dw.object);
importPackage(dw.system);
importPackage(dw.util);
importPackage(dw.web);
var Transaction = require('dw/system/Transaction');

var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var RecommendationHelper = require("~/cartridge/scripts/recommendations/RecommendationHelper");

var DELIVERIES_CUSTOM_TYPE = "BrontoMessageDelivery";

/**
 * Creates a transactional message sender for the specified extensionId. The extensionId is the ID
 * of the type of transactional message to be sent. If isCustom is true, the extensionId is looked
 * up in the custom messages settings. This constructor will throw an error if the message cannot
 * be sent to Bronto.
 */
function TransactionalMessageScheduler(extensionId : String, isCustom : Boolean) {
	this.settingsManager = new SettingsManager();
	this.extensionId = extensionId;
	this.isCustom = isCustom || false;
	this.verifyMessageCanBeSent();
}

/**
 * Gets a setting for a particular transactional message that was configured in Bronto Connector
 */
TransactionalMessageScheduler.prototype.getMessageSetting = function(settingId : String, extensionId : String) {
	var extension : String = extensionId || this.extensionId;
	return this.isCustom
		? this.settingsManager.getSetting("transactional-messages", ["objects", "custom-message", extension, settingId])
		: this.settingsManager.getSetting("transactional-messages", ["extensions", extension, settingId]);
};

/**
 * Appends common field tags for all messages. This includes information like the name and URL of the
 * site from which this message was sent.
 */
TransactionalMessageScheduler.prototype.appendCommonFieldData = function(fieldData : Object) {
	fieldData["siteName"] = Site.getCurrent().getName();
	fieldData["siteUrl"] = URLUtils.home().toString();
};

/**
 * Looks up the product recommendation settings object and builds API field loop tags for each recommendation, if possible.
 */
TransactionalMessageScheduler.prototype.getProductRecommendations = function(products : Collection, otherField : String) : Object {
	var fieldData : Object = {};
	var productRecommendationsId : String = this.getMessageSetting("product-recommendation");
	if (!empty(productRecommendationsId)) {
		var productRecommendationDefinition : Object = this.settingsManager.getSetting("product-recommendations", ["objects", "product-recommendation", productRecommendationsId]);
		if (!empty(productRecommendationDefinition)) {
			fieldData = RecommendationHelper.getProductRecommendations(productRecommendationDefinition, otherField, products);
		}
	}
	return fieldData;
};

/**
 * Appends a coupon code, if possible, if the message has a setting defined for a coupon to use
 */
TransactionalMessageScheduler.prototype.appendCouponCode = function(fieldData : Object) {
	var couponId : String = this.getMessageSetting("coupon-id");
	if (!empty(couponId)) {
		var coupon : Coupon = CouponMgr.getCoupon(couponId);
		if (!empty(coupon) && coupon.isEnabled()) {
			var code : String = coupon.getNextCouponCode();
			if (!empty(code)) {
				fieldData["couponCode"] = code;
			}
		}
	}
};

/**
 * Verifies this message can be sent based on the settings. A message can be sent
 * if it has an assigned message and a send type other than "demandware". This method 
 * will throw an error if there's no associated message or if the send type is 
 * specified to be "demandware".
 */
TransactionalMessageScheduler.prototype.verifyMessageCanBeSent = function() {
	var messageId : String = this.getMessageSetting("message-id");
	var sendType : String = this.getMessageSetting("send-type");
	if (empty(sendType) || sendType === "demandware") {
		throw new Error("Send type not specified, or set to using Demandware for the " + this.extensionId + " transactional message");
	}
	if (empty(messageId)) {
		throw new Error("No message has been set for the " + this.extensionId + " transactional message");
	}
};

/**
 * Schedules a transactional message for delivery. The fieldData is the API field tags to include, 
 * the recipients is an array of recipients to send to, the products is a Collection of Product 
 * objects from which recommendations can be based off of, fromName, fromEmail, and replyTo allow 
 * overriding the settings configured in Connector for this message.
 */
TransactionalMessageScheduler.prototype.schedule = function(fieldData : Object, recipients : Array, products : Collection, fromName : String, fromEmail : String, replyTo : String) {
	var otherField : String = this.settingsManager.getSetting("order-import", ["extensions", "settings", "other-field"]);
	var messageId : String = this.getMessageSetting("message-id");
	var sendType : String = this.getMessageSetting("send-type");
	this.appendCommonFieldData(fieldData);
	this.appendCouponCode(fieldData);
	var recommendations : Object = this.getProductRecommendations(products, otherField);
	for (var recommendation in recommendations) {
		fieldData[recommendation] = recommendations[recommendation];
	}
	var senderfromName : String = fromName || this.getMessageOrDefaultSetting("from-name");
	var senderfromEmail : String = fromEmail || this.getMessageOrDefaultSetting("from-email");
	var senderReplyTo : String = replyTo || this.getMessageOrDefaultSetting("reply-to");
	var messageDelivery : CustomObject;
	Transaction.wrap(function(){
		messageDelivery   = CustomObjectMgr.createCustomObject(DELIVERIES_CUSTOM_TYPE, UUIDUtils.createUUID());
	});
	var sendFlags : String = this.getMessageOrDefaultSetting("send-flags");
	var replyTracking : Boolean = !empty(sendFlags) && sendFlags.indexOf("reply-tracking") > -1;
	var authentication : Boolean = !empty(sendFlags) && sendFlags.indexOf("sender-auth") > -1;
	var fatigueOverride : Boolean = !empty(sendFlags) && sendFlags.indexOf("fatigue-override") > -1;
	var customData : Object = {
		id: UUIDUtils.createUUID(),
		messageId: messageId,
		type: sendType || "triggered",
		fields: JSON.stringify(fieldData),
		fromName: senderfromName,
		fromEmail: senderfromEmail,
		replyEmail: senderReplyTo || senderfromEmail,
		recipients: JSON.stringify(recipients),
		replyTracking: replyTracking,
		authentication: authentication,
		fatigueOverride: fatigueOverride
	};
	Transaction.wrap(function(){
		for (var name in customData) {
			messageDelivery.getCustom()[name] = customData[name];
		}
	});
};

/**
 * Gets the defined message setting for this transactional message, and if no setting is found, attempts to look up the
 * default value defined in the settings.
 */
TransactionalMessageScheduler.prototype.getMessageOrDefaultSetting = function(settingId : String) {
	var value : String = this.getMessageSetting(settingId);
	if (empty(value)) {
		value = this.getMessageSetting(settingId, "default-settings");
	}
	return value;
};

module.exports = TransactionalMessageScheduler;