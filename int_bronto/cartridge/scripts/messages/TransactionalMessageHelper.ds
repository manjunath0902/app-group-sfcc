/**
* Demandware Script File
*
* Helper class that provides methods that are useful for preparing a message to be scheduled for sending.
*/
importPackage(dw.customer);
importPackage(dw.order);
importPackage(dw.util);

var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");

var AVAILABLE_DEFAULTS = [
	"default-customer-name",
	"default-first-name",
	"view-type"
];

var TransactionalMessageHelper = {
	/**
	 * Converts a single recipient into an array, combining the optional additionalRecipients into that array.
	 */
	getCombinedRecipients: function(recipient : String, additionalRecipients : Array) {
		var recipients : Array = [recipient];
		if (additionalRecipients) {
			for (var i = 0; i < additionalRecipients.length; i++) {
				recipients.push(additionalRecipients[i]);
			}
		}
		return recipients;
	},
	
	/**
	 * From a list of ProductLineItems, this function will create a List of the Product objects.
	 */
	getProductsFromLineItems: function(lineItems : Collection) : List {
		var products : List = new ArrayList();
		var i : Iterator = lineItems.iterator();
		while (i.hasNext()) {
			var product : ProductLineItem = i.next();
			if (!empty(product.getProduct())) {
				products.push(product.getProduct());
			}
		}
		return products;
	},
	
	/**
	 * From a list of ProductListItems, this function wil lcreate a List of the Product objects.
	 */
	getProductsFromListItems: function(listItems : Collection) : List {
		var products : List = new ArrayList();
		var i : Iterator = listItems.iterator();
		while (i.hasNext()) {
			var productListItem : ProductListItem = i.next();
			if (productListItem.isPublic()) {
				products.add(productListItem.getProduct());
			}
		}
		return products;
	},
	
	/**
	 * Gets the default settings for a all transactional messages from the settings synced from Bronto Connector.
	 */
	getDefaultValues: function() : Object {
		var defaultValues = {};
		var settingsManager = new SettingsManager();
		var defaultSettings : Object = settingsManager.getSetting("transactional-messages", ["extensions", "default-settings"]);
		if (!empty(defaultSettings)) {
			for (var i = 0; i < AVAILABLE_DEFAULTS.length; i++) {
				var defaultName = AVAILABLE_DEFAULTS[i];
				defaultValues[defaultName] = defaultSettings[defaultName];
			}
		}
		return defaultValues;
	}
};

module.exports = TransactionalMessageHelper;