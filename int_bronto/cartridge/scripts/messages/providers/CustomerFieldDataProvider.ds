/**
* Demandware Script File
*
* Provides API field tag data for a transactional message that is sending customer information.
*/
importPackage(dw.customer);
importPackage(dw.web);

var FieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/FieldDataProvider");

function CustomerFieldDataProvider(customer : Customer, defaultValues : Object) {
	this.customer = customer;
	this.defaultValues = defaultValues;
}

CustomerFieldDataProvider.prototype = new FieldDataProvider();
CustomerFieldDataProvider.constructor = CustomerFieldDataProvider;

CustomerFieldDataProvider.prototype.provideFields = function() : Object {
	var fieldData : Object = {};
	if (!empty(this.customer) && !empty(this.customer.getProfile())) {
		var profile : Profile = this.customer.getProfile();
		var customerName = empty(profile.getFirstName()) || empty(profile.getLastName())
			? this.defaultValues["default-customer-name"]
			: profile.getFirstName() + " " + profile.getLastName();
		fieldData["custName"] = customerName;
		fieldData["custFirstName"] = empty(profile.getFirstName()) ? this.defaultValues["default-first-name"] : profile.getFirstName();
		fieldData["custLastName"] = profile.getLastName();
		fieldData["custSalutation"] = profile.getSalutation();
		fieldData["custSecondName"] = profile.getSecondName();
		fieldData["custSuffix"] = profile.getSuffix();
		fieldData["custTitle"] = profile.getTitle();
		fieldData["custCompany"] = profile.getCompanyName();
		fieldData["custEmail"] = profile.getEmail();
		fieldData["accountUrl"] = URLUtils.https("Account-Show").toString();
	}
	return fieldData;
};

module.exports = CustomerFieldDataProvider;