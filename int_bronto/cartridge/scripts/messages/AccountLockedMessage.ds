/**
* Demandware Script File
*
* Provides a pipelet that can be used for sending an account locked message in Bronto.
*
* input Customer : dw.customer.Customer
* input Objects : Array	Optional array of objects (JavaScript or ExtensibleObjects to include field tags)
* input AdditionalRecipients : Array	Optional array of email addresses that will also receive this message
*/
var customer = require("dw/customer");
var system = require("dw/system");

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var TransactionalMessageHelper = require("~/cartridge/scripts/messages/TransactionalMessageHelper");
var TransactionalMessageScheduler = require("~/cartridge/scripts/messages/TransactionalMessageScheduler");
var CustomerFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CustomerFieldDataProvider");
var GenericObjectFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericObjectFieldDataProvider");
var CompositeFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeFieldDataProvider");

function execute(pdict) : Number {
	var logger = new BrontoLogger("AccountLockedMessage");
	var sitePrefs = system.Site.getCurrent().getPreferences();
	var customMessageID = "";
	if(!empty(sitePrefs.getCustom()["customMessageID"]) && (JSON.parse(sitePrefs.getCustom()["customMessageID"])["account-locked"+'-'+request.locale.toString()] != null)){
		customMessageID = JSON.parse(sitePrefs.getCustom()["customMessageID"])["account-locked"+'-'+request.locale.toString()];
	}
	try {
		var transactionalScheduler = new TransactionalMessageScheduler(customMessageID, true);
		var fieldProviders : Array  = [];
		fieldProviders.push(new CustomerFieldDataProvider(pdict.Customer, TransactionalMessageHelper.getDefaultValues()));
		GenericObjectFieldDataProvider.addObjectFieldDataProviders(pdict.Objects, fieldProviders);
		var compositeFieldProvider = new CompositeFieldDataProvider(fieldProviders);
		var recipients : Array = TransactionalMessageHelper.getCombinedRecipients(pdict.Customer.getProfile().getEmail(), pdict.AdditionalRecipients);
		transactionalScheduler.schedule(compositeFieldProvider.getFields(), recipients);
	} catch (e) {
		logger.getLogger().debug("Error scheduling account locked message: " + e.message);
		return false;
	}
	return true;
}

exports.execute = execute;