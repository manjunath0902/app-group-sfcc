/**
* Demandware Script File
*
* Provides a pipelet that can be used for sending a contact us message in Bronto.
*
* input Form : dw.web.Form
* input RecipientEmail : String
* input Objects : Array	Optional array of objects (JavaScript or ExtensibleObjects to include field tags)
* input AdditionalRecipients : Array	Optional array of email addresses that will also receive this message
*/
var system = require("dw/system");

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var TransactionalMessageHelper = require("~/cartridge/scripts/messages/TransactionalMessageHelper");
var TransactionalMessageScheduler = require("~/cartridge/scripts/messages/TransactionalMessageScheduler");
var GenericFormFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericFormFieldDataProvider");
var GenericObjectFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/GenericObjectFieldDataProvider");
var CompositeFieldDataProvider : Function = require("~/cartridge/scripts/messages/providers/CompositeFieldDataProvider");

function execute(pdict) : Number {
	var logger = new BrontoLogger("ContactUsMessage");
	try {
		var transactionalScheduler = new TransactionalMessageScheduler("contact-us");
		var fieldProviders : Array  = [];
		fieldProviders.push(new GenericFormFieldDataProvider(pdict.contactUsForm));
		GenericObjectFieldDataProvider.addObjectFieldDataProviders(pdict.Objects, fieldProviders);
		var compositeFieldProvider = new CompositeFieldDataProvider(fieldProviders);
		var recipients : Array = TransactionalMessageHelper.getCombinedRecipients(pdict.RecipientEmail, pdict.AdditionalRecipients);
		transactionalScheduler.schedule(compositeFieldProvider.getFields(), recipients);
	} catch (e) {
		logger.getLogger().debug("Error scheduling generic form message: " + e.message);
		return false;
	}
	return true;
}

exports.execute = execute;