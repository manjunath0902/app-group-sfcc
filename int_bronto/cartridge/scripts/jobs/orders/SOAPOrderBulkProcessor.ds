/**
* Demandware Script File
*
* OrderBulkProcessor strategy for submitting batches of Orders to the Bronto SOAP API.
*/
importPackage(dw.system);
importPackage(dw.util);

importPackage( dw.svc );
importPackage( dw.ws );


var OrderBulkProcessor = require("~/cartridge/scripts/jobs/orders/OrderBulkProcessor");
var ApiConstants = require("~/cartridge/scripts/api/ApiConstants");

var UTC_TIME_ZONE : String = "UTC";

function SOAPOrderBulkProcessor(brontoApi, viewType : String, productDescriptionId : String, bulkSize : Number) {
	OrderBulkProcessor.call(this, viewType, productDescriptionId, null, null, bulkSize);
	this.brontoApi = brontoApi;
}

SOAPOrderBulkProcessor.prototype = new OrderBulkProcessor();
SOAPOrderBulkProcessor.constructor = SOAPOrderBulkProcessor;

SOAPOrderBulkProcessor.prototype.getBrontoApi = function() {
	return this.brontoApi;
};

SOAPOrderBulkProcessor.prototype.processOrders = function(orders : List) : List {
	// Build the Bronto API order objects
	var orderObjects : List = new ArrayList();
	var i : Iterator = orders.iterator();
	while (i.hasNext()) {
		var order : Object = i.next();
		var orderObj = this.getBrontoApi().createOrderObject();
		orderObj.setId(order.customerOrderId);
		orderObj.setEmail(order.emailAddress);
		var orderDate : Calendar = new Calendar();
		orderDate.setTimeZone(UTC_TIME_ZONE);
		orderDate.parseByFormat(order.orderDate, ApiConstants.DATE_FORMAT);
		orderObj.setOrderDate(orderDate);
		if (!empty(order.tid)) {
			orderObj.setTid(order.tid);
		}
		for (var li = 0; li < order.lineItems.length; li++) {
			var lineItem = order.lineItems[li];
			var productObject = this.getBrontoApi().createProductObject();
			productObject.setSku(lineItem.sku);
			productObject.setQuantity(lineItem.quantity);
			productObject.setPrice(lineItem.salePrice);
			productObject.setName(lineItem.name);
			if (lineItem.description) {
				productObject.setDescription(lineItem.description);
			}
			if (lineItem.category) {
				productObject.setCategory(lineItem.category);
			}
			if (lineItem.imageUrl) {
				productObject.setImage(lineItem.imageUrl);
			}
			if (lineItem.productUrl) {
				productObject.setUrl(lineItem.productUrl);
			}
			orderObj.getProducts().add(productObject);	
		}
		orderObjects.add(orderObj);
	}
	
	var response = this.getBrontoApi().addOrUpdateOrders(orderObjects);
	var responseIterator : Iterator = response.getResults().iterator();
	var results : List = new ArrayList();
	while (responseIterator.hasNext()) {
		var resp = responseIterator.next();
		results.add({
			id: resp.id,
			isNew: resp.isNew,
			isError: resp.isError,
			errorCode: resp.errorCode,
			errorString: resp.errorString
		});
	}
	return results;
};

SOAPOrderBulkProcessor.prototype.deleteOrders = function(orders : List) : List {
	
	var orderObjects : List = new ArrayList();
	var i : Iterator = orders.iterator();
	// Build the Bronto API order objects
	while (i.hasNext()) {
		var order : Object = i.next();
		var orderObj = this.getBrontoApi().createOrderObject();
		orderObj.setId(order.customerOrderId);
		orderObj.setEmail(order.emailAddress);
		var orderDate : Calendar = new Calendar();
		orderDate.setTimeZone(UTC_TIME_ZONE);
		orderDate.parseByFormat(order.orderDate, ApiConstants.DATE_FORMAT);
		orderObj.setOrderDate(orderDate);
		if (!empty(order.tid)) {
			orderObj.setTid(order.tid);
		}
		for (var li = 0; li < order.lineItems.length; li++) {
			var lineItem = order.lineItems[li];
			var productObject = this.getBrontoApi().createProductObject();
			productObject.setSku(lineItem.sku);
			productObject.setQuantity(lineItem.quantity);
			productObject.setPrice(lineItem.salePrice);
			productObject.setName(lineItem.name);
			if (lineItem.description) {
				productObject.setDescription(lineItem.description);
			}
			if (lineItem.category) {
				productObject.setCategory(lineItem.category);
			}
			if (lineItem.imageUrl) {
				productObject.setImage(lineItem.imageUrl);
			}
			if (lineItem.productUrl) {
				productObject.setUrl(lineItem.productUrl);
			}
			orderObj.getProducts().add(productObject);	
		}
		
		orderObjects.add(orderObj);
	}
	
	var response = this.getBrontoApi().deleteOrders(orderObjects);
	var responseIterator : Iterator = response.getResults().iterator();
	var results : List = new ArrayList();
	while (responseIterator.hasNext()) {
		var resp = responseIterator.next();
		results.add({
			id: resp.id,
			isNew: resp.isNew,
			isError: resp.isError,
			errorCode: resp.errorCode,
			errorString: resp.errorString
		});
	}
	return results;
};

module.exports = SOAPOrderBulkProcessor;