/**
* Demandware Script File
*
* Prepares the contact import job by verifying the Bronto API is configured and 
* defines a BulkProcessor that will process all contacts in batches for optimal
* import performance with Bronto.
*
* @output BulkProcessor : Object
*/
importPackage(dw.system);
importPackage(dw.util);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var BrontoApi = require("~/cartridge/scripts/api/BrontoApi");
var ContactBulkProcessor = require("~/cartridge/scripts/jobs/contacts/ContactBulkProcessor");
var ContactHelper = require("~/cartridge/scripts/contacts/ContactHelper");

function execute(pdict : PipelineDictionary) : Number {
	var logger = new BrontoLogger("ContactImportPreparation");
	var settingsManager = new SettingsManager();
	var apiToken : String = settingsManager.getSetting("main", "apiToken");
	if (empty(apiToken)) {
		logger.getLogger().error("No API token set for this site");
		return PIPELET_ERROR;
	}
	var extensions : Array = settingsManager.getSetting("contact-import", "extensions");
	var brontoApi = new BrontoApi(apiToken);
	pdict.BulkProcessor = new ContactBulkProcessor(brontoApi, new ContactHelper(brontoApi, extensions));
	return PIPELET_NEXT;
}