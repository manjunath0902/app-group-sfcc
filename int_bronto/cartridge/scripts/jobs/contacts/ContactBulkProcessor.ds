/**
* Demandware Script File
*
* A BulkProcessor that will take a list of Profile data and batch send them to the Bronto API.
*/
importPackage(dw.customer);
importPackage(dw.system);
importPackage(dw.util);
importPackage(dw.web);

var BulkProcessor : Function = require("~/cartridge/scripts/jobs/BulkProcessor");
var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var ApiConstants = require("~/cartridge/scripts/api/ApiConstants");
var BrontoConstants = require("~/cartridge/scripts/util/BrontoConstants");

var DEFAULT_BULK_SIZE : Number = 100;

function ContactBulkProcessor(brontoApi, contactHelper, bulkSize : Number) {
	BulkProcessor.call(this, bulkSize || DEFAULT_BULK_SIZE);
	this.logger = new BrontoLogger("ContactBulkProcessor");
	this.brontoApi = brontoApi;
	this.contactHelper = contactHelper;
}

ContactBulkProcessor.prototype = new BulkProcessor();
ContactBulkProcessor.constructor = ContactBulkProcessor;

ContactBulkProcessor.prototype.getBrontoApi = function() {
	return this.brontoApi;
};

ContactBulkProcessor.prototype.getContactHelper = function() {
	return this.contactHelper;
};

ContactBulkProcessor.prototype.process = function() {
	var contacts : List = new ArrayList();
	var itemsIterator : Iterator = this.getItems().iterator();
	var brontoApi = this.getBrontoApi();
	// Build a Bronto API SOAP ContactObject for each profile
	while (itemsIterator.hasNext()) {
		var profile : Profile = itemsIterator.next();
		var contactData = this.getContactHelper().getContactData(profile);
		var contact = brontoApi.createContactObject();
		contact.setEmail(contactData["email"]);
		contact.setStatus("transactional");
		var fields : Array = contactData["fields"];
		for (var i = 0; i < fields.length; i++) {
			var fieldData : Object = fields[i];
			var field = brontoApi.createContactField();
			field.setFieldId(fieldData["id"]);
			field.setContent(fieldData["content"]);
			contact.getFields().add(field);
		}
		contacts.add(contact);
	}
	if (!contacts.isEmpty()) {
		var response = brontoApi.addOrUpdateContacts(contacts);
		var importDate : Date = new Date();
		var results : List = response.getResults();
		for (var i = 0; i < results.size(); i++) {
			var profile : Profile = this.getItems()[i];
			var resp = results[i];
			// Check for errors and mark contacts as imported or requiring a retry if a recoverable error occurred
			if (resp.isError) {
				if (resp.getErrorCode() === ApiConstants.OFFLINE_ERROR_CODE) {
					// For offline error codes, don't count this as an import error
					continue;
				}
				var errorCount : Number = profile.getCustom()["brontoImportErrorCount"];
				if (empty(errorCount)) {
					errorCount = 0;
				}
				profile.getCustom()["brontoImported"] = false;
				profile.getCustom()["brontoImportErrorCount"] = errorCount + 1;
				var errorMessage : String = Resource.msgf("errors.response", "errors", null, resp.getErrorCode(), resp.getErrorString());
				profile.getCustom()["brontoLastImportError"] = errorMessage;
				this.logger.getLogger().error("Error importing customer \"{0}\". Message: {1}", profile.getCustomerNo(), errorMessage);
			} else {
				profile.getCustom()["brontoImported"] = true;
				profile.getCustom()["brontoLastImportDate"] = importDate;
			}
		}
	}
};

ContactBulkProcessor.prototype.suppressItem = function(item : Number) : String {
	var profile : Profile = this.getItems()[item];
	profile.getCustom()["brontoImported"] = false;
	profile.getCustom()["brontoImportErrorCount"] = BrontoConstants.MAX_ERROR_COUNT;
	return "Customer (" + profile.getCustomerNo() + ")";
};

module.exports = ContactBulkProcessor;