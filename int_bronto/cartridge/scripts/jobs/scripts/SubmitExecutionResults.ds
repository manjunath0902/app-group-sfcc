/**
* Demandware Script File
*
* An endpoint that schedules a triggered execution script to run if the request
* passed the auth token validation.
*
* @input Execution : dw.object.CustomObject
*/
importPackage(dw.net);
importPackage(dw.object);
importPackage(dw.system);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var RegistrationManager = require("~/cartridge/scripts/registration/RegistrationManager");
var BrontoConstants = require("~/cartridge/scripts/util/BrontoConstants");

var RESULTS_ENDPOINT : String = "/results";

function execute(pdict : PipelineDictionary) : Number {
	var registration : CustomObject = RegistrationManager.getRegistrationForCurrentSite();
	if (!empty(registration)) {
		var id : String = pdict.Execution.getCustom()["id"];
		var logger = new BrontoLogger("SubmitExecutionResults");
		
		var service = dw.svc.ServiceRegistry.get("bronto.rest" + '.' + Site.getCurrent().ID);
			service.URL += RESULTS_ENDPOINT;
		var callArgs = {
			callType : "POST",
			authToken : registration.getCustom()["authToken"]
		};
		var callParams : Object = {
			installKey: registration.getCustom()["installKey"],
			scopeId: "global",
			extensionId: "advanced",
			scriptId: id,
			result: JSON.parse(pdict.Execution.getCustom()["result"])
		};
		service.call(callArgs, callParams);
		var httpClient = service.client;
		if (httpClient.getStatusCode() !== 201) {
			logger.getLogger().error("Unable to submit results for execution \"{0}\". Code: {1}, Message: {2}", id, httpClient.getStatusCode(), httpClient.getText());
		}
	}
	return PIPELET_NEXT;
}