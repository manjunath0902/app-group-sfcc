/**
* Demandware Script File
*
* A triggered scheduled execution that will provide the data for a test import into Bronto for an order.
*
* @input Execution : dw.object.CustomObject
* @output Items : dw.util.Iterator
* @output BulkProcessor : Object
*/
importPackage(dw.order);
importPackage(dw.system);
importPackage(dw.util);

var BrontoLogger = require("~/cartridge/scripts/util/BrontoLogger");
var SettingsManager = require("~/cartridge/scripts/util/SettingsManager");
var OrderHelper = require("~/cartridge/scripts/orders/OrderHelper");
var BrontoApi = require("~/cartridge/scripts/api/BrontoApi");
var BrontoRestApi = require("~/cartridge/scripts/api/BrontoRestApi");
var SOAPOrderBulkProcessor = require("~/cartridge/scripts/jobs/orders/SOAPOrderBulkProcessor");
var RESTOrderBulkProcessor = require("~/cartridge/scripts/jobs/orders/RESTOrderBulkProcessor");

function execute(pdict : PipelineDictionary) : Number {
	var data : Object = JSON.parse(pdict.Execution.getCustom()["data"]);
	var orderNumber : String = data["order-number"];
	var performImport : Boolean = data["import"];
	var order : Order = null;
	if (empty(orderNumber)) {
		order = OrderMgr.searchOrders("", null).first();
	} else {
		order = OrderMgr.searchOrder("orderNo = {0}", orderNumber);
	}
	var orderData = null;
	if (!empty(order)) {
		var settingsManager = new SettingsManager();
		var viewType : String = settingsManager.getSetting("order-import", ["extensions", "settings", "view-type"]);
		var productDescriptionId : String = settingsManager.getSetting("order-import", ["extensions", "settings", "product-description"]);
		var orderStatus : String = settingsManager.getSetting("order-import", ["extensions", "settings", "order-status"]);
		var otherField : String = settingsManager.getSetting("order-import", ["extensions", "settings", "other-field"]);
		orderData = OrderHelper.getOrderData(order, viewType, productDescriptionId, orderStatus, otherField);
		if (performImport) {
			var logger = new BrontoLogger("TestOrderImport");
			var apiToken : String = settingsManager.getSetting("main", "apiToken");
			if (empty(apiToken)) {
				logger.getLogger().error("No API token set for this site");
				return PIPELET_ERROR;
			}
			var orderList : List = new ArrayList();
			orderList.add(order);
			pdict.Items = orderList.iterator();
			if (settingsManager.getSetting("main", ["featureMap", "enableOrderService"])) {
				var oauthToken : String = settingsManager.getSetting("main", "oauthToken");
				var refreshToken : String = settingsManager.getSetting("main", "refreshToken");
				pdict.BulkProcessor = new RESTOrderBulkProcessor(new BrontoRestApi(oauthToken, refreshToken), viewType, productDescriptionId, orderStatus, otherField);
			} else {
				pdict.BulkProcessor = new SOAPOrderBulkProcessor(new BrontoApi(apiToken), viewType, productDescriptionId);
			}
		}
	}
	var result : Object = {
		order: orderData
	};
	pdict.Execution.getCustom()["result"] = JSON.stringify(result);
	return PIPELET_NEXT;
}