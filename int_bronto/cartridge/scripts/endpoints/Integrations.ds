/**
* Demandware Script File
*
* JSON discovery endpoint that defines the settings and jobs available to be configured for the Integrations settings.
*
* @output json : String
*/
importPackage(dw.system);

function execute(pdict : PipelineDictionary) : Number {
	pdict.json = JSON.stringify({
		extensions: [
			{
				id: "cart-recovery",
				name: "Cart Recovery",
				fields: [
					{
						id: "enabled",
						name: "Enabled",
						type: "boolean",
						required: true,
						typeProperties: {
							"default": false,
							bronto: {
								type: "cartRecovery"
							}
						}
					},
					{
						id: "embed-code",
						name: "Embed Code",
						type: "textarea",
						typeProperties: {
							bronto: {
								type: "cartRecoveryEmbed"
							}
						},
						depends: [
							{ id: "enabled", values: [ true ] }
						]
					}
				]
			},
			{
				id: "popup-manager",
				name: "Pop-Up Manager",
				fields: [
					{
						id: "enabled",
						name: "Enabled",
						type: "boolean",
						required: true,
						typeProperties: {
							"default": false
						}
					},
					{
						id: "popups",
						name: "Domain",
						type: "select",
						depends: [
							{ id: "enabled", values: [ true ] }
						],
						typeProperties: {
							bronto: {
								type: "popupManager"
							},
							multiple: true
						}
					}
				]
			},
			{
				id: "coupon-manager",
				name: "Coupon Manager",
				fields: [
					{
						id: "enabled",
						name: "Enabled",
						type: "boolean",
						required: true,
						typeProperties: {
							"default": false
						}
					},
					{
						id: "redemption-type",
						name: "Redemption Tracking",
						type: "select",
						depends: [
							{ id: "enabled", values: [ true ] }
						],
						required: true,
						typeProperties: {
							options: [
								{
									id: "javascript",
									name: "Storefront JavaScript"
								},
								{
									id: "pipeline",
									name: "Server-Side Pipeline"
								}
							],
							"default": "javascript"
						}
					}
				]
			},
			{
			    id : "browseRecovery",
			    name : "Browse Recovery",
			    fields : [ 
			        {
			            id : "enabled",
			            name : "Enabled",
			            type : "boolean",
			            required : true,
			            typeProperties : {
			                "default" : false
			            }
			        }, 
			        {
			            id : "site",
			            name : "Site",
			            type : "select",
			            required : true,
			            depends : [ 
			                { id : "enabled", values : [ true ] }
			            ],
			            typeProperties : {
			                bronto : {
			                    type : "browseRecovery"
			                }
			            }
			        }
			    ]
			}
		]
	});
	return PIPELET_NEXT;
}