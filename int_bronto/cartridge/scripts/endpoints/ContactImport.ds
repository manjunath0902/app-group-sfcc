/**
* Demandware Script File
*
* JSON discovery endpoint that defines the settings and jobs available to be configured for the Contact Import settings.
*
* @output json : String
*/
importPackage(dw.customer);
importPackage(dw.object);
importPackage(dw.system);
importPackage(dw.web);
importPackage(dw.util);

function execute(pdict : PipelineDictionary) : Number {
	var typeDefinition : ObjectTypeDefinition = CustomerMgr.describeProfileType();
	var groups : Array = [];
	var attrGroups : Collection = typeDefinition.getAttributeGroups();
	var foundAttributes : Map = new HashMap();
	for (var i = 0; i < attrGroups.size(); i++) {
		var group : ObjectAttributeGroup = attrGroups[i];
		var attrDefs : Collection = group.getAttributeDefinitions();
		var attributes : Array = [];
		for (var x = 0; x < attrDefs.size(); x++) {
			var attribute : ObjectAttributeDefinition = attrDefs[x];
			if (attribute.getValueTypeCode() === ObjectAttributeDefinition.VALUE_TYPE_EMAIL || foundAttributes.containsKey(attribute.getID())) {
				continue;
			}
			attributes.push(getAttribute(attribute));
			foundAttributes.put(attribute.getID(), true);
		}
		if (group.getID() === "brontoImport") {
			continue;
		}
		groups.push({
			id: group.getID(),
			name: group.getDisplayName(),
			fields: attributes
		});
	}
	createCustomGroup("bronto-preferred-address", "Address Book", "CustomerAddress", groups);
	createCustomGroup("bronto-active-data", "Active Data", "CustomerActiveData", groups);
	var attrDefs : List = typeDefinition.getAttributeDefinitions();
	var attributes : Array = [];
	for (var i = 0; i < attrDefs.size(); i++) {
		var attribute : ObjectAttributeDefinition = attrDefs[i];
		if (attribute.getValueTypeCode() === ObjectAttributeDefinition.VALUE_TYPE_EMAIL || foundAttributes.containsKey(attribute.getID())) {
			continue;
		}
		attributes.push(getAttribute(attribute));
	}
	if (attributes.length > 0) {
		groups.push({
			id: "bronto-other",
			name: "Other",
			fields: attributes
		});
	}
	var jobs : Array = [
		{
			id: "ContactImport",
			name: "Import Schedule",
			fields: [
				{
					id: "items-to-sync",
					name: "Limit Per Run",
					type: "select",
					advanced: true,
					required: true,
					depends: [
						{ id: "enabled", values: [ true ] }
					],
					typeProperties: {
						options: [
							{
								id: "100",
								name: "100"
							},
							{
								id: "250",
								name: "250"
							},
							{
								id: "500",
								name: "500"
							},
							{
								id: "1000",
								name: "1000"
							}
						],
						"default": "1000"
					}
				}
			]
		}
	];
	pdict.json = JSON.stringify({
		extensions: groups,
		jobs: jobs
	});
	return PIPELET_NEXT;
}

function createCustomGroup(id : String, name : String, type : String, groups : Array) {
	var typeDef : ObjectTypeDefinition = SystemObjectMgr.describe(type);
	var attrGroups : Iterator = typeDef.getAttributeGroups().iterator();
	while (attrGroups.hasNext()) {
		var attrGroup : ObjectAttributeGroup = attrGroups.next();
		var attrDefs : Collection = attrGroup.getAttributeDefinitions();
		var attributes : Array = [];
		for (var i = 0; i < attrDefs.size(); i++) {
			var attribute : ObjectAttributeDefinition = attrDefs[i];
			if (!attribute.isMultiValueType()
					&& attribute.getID() !== "UUID"
					&& attribute.getID() !== "creationDate"
					&& attribute.getID() !== "lastModified") {
				attributes.push(getAttribute(attribute, id + "_"));
			}
		}
		if (attributes.length > 0) {
			var displayName : String = empty(attrGroup.getDisplayName()) ? attrGroup.getID() : attrGroup.getDisplayName();
			groups.push({
				id: id + "_" + attrGroup.getID(),
				name: name + " - " + displayName,
				fields: attributes
			});
		}
	}
}

function getAttribute(attribute : ObjectAttributeDefinition, idPrefix : String) {
	idPrefix = idPrefix || "";
	var displayName : String = empty(attribute.getDisplayName()) ? attribute.getID() : attribute.getDisplayName();
	return {
		id: idPrefix + attribute.getID(),
		name: displayName,
		type: "select",
		typeProperties: {
			bronto: {
				type: "contactField",
				displayType: getDisplayTypeForAttribute(attribute),
			}
		}
	};
}

function getDisplayTypeForAttribute(attribute : ObjectAttributeDefinition) {
	var displayType = null;
	switch (attribute.getValueTypeCode()) {
		case ObjectAttributeDefinition.VALUE_TYPE_PASSWORD:
			displayType = "password";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_TEXT:
		case ObjectAttributeDefinition.VALUE_TYPE_HTML:
			displayType = "textarea";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_INT:
			displayType = "integer";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_MONEY:
			displayType = "currency";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_NUMBER:
		case ObjectAttributeDefinition.VALUE_TYPE_QUANTITY:
			displayType = "float";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_DATE:
		case ObjectAttributeDefinition.VALUE_TYPE_DATETIME:
			displayType = "date";
			break;
		case ObjectAttributeDefinition.VALUE_TYPE_BOOLEAN:
			displayType = "checkbox";
			break;
		default:
			displayType = "text";
			break;
	}
	return displayType;
}