/**
 * @param {dw.util.ArrayList} stores store search result
 * @returns {string} Store data in JSON string format
 */
var getStoreJSONData = function(stores) {
    var store, storeObj = [],customStoreObj = [],newlist = [],count = 1,label,
        storeJSONObj = {};
    for (var i = 0; i < stores.length; i++) {
        store = stores[i];
        if(store.custom.storeType == 'Mackage'){
        	label = 'M';
        }else if(store.custom.storeType == 'SoiaKyo'){
        	label = 'S'
        }else{
        	label = count;
        	count = count+1;
        }
        storeJSONObj = {};
        storeJSONObj.name = store.name;
        storeJSONObj.city = store.city;
        storeJSONObj.latitude = store.latitude;
        storeJSONObj.longitude = store.longitude;
        storeJSONObj.address1 = store.address1;
        storeJSONObj.address2 = store.address2;
        storeJSONObj.stateCode = store.stateCode;
        storeJSONObj.postalCode = store.postalCode;
        storeJSONObj.phone = store.phone;
        storeJSONObj.email = store.email;
        storeJSONObj.countryCode = store.countryCode ? store.countryCode.toString() : '';
        storeJSONObj.label = label;
        if(store.custom.storeType == 'Mackage' || store.custom.storeType == 'SoiaKyo'){
        	customStoreObj.push(storeJSONObj);
        }else{
        	storeObj.push(storeJSONObj);
        }
        
        newlist = customStoreObj.length > 0 ? customStoreObj.concat( storeObj ) : storeObj;
    }
    return JSON.stringify(newlist);
};

exports.getStoreJSONData = getStoreJSONData;
