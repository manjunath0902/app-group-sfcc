'use strict';

/**
 *	Store utility functions
 */

var Logger = require('dw/system/Logger');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var StoreMgr = require('dw/catalog/StoreMgr');
var Site = require('dw/system/Site');
var Result = require('dw/svc/Result');
var ArrayList = require('dw/util/ArrayList');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var SortedSet = require('dw/util/SortedSet');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
	
var StoreUtils = {

    getStoreLookupDistanceUnit: function() {
        return Site.getCurrent().getCustomPreferenceValue('storeLookupUnit').getValue();
    },

    getStoreLookupMaxDistance: function() {
        return Site.getCurrent().getCustomPreferenceValue('storeLookupMaxDistance').getValue();
    },
    
    getGoogleMapsAPIKey: function() {
        return Site.getCurrent().getCustomPreferenceValue('googleMapsAPIKey');
    },

    getNearestStore: function() {
        var geoLocation = request.getGeolocation();
        var latitude = geoLocation.getLatitude();
        var longitude = geoLocation.getLongitude();

        if (latitude && longitude) {
            var nearestStores = StoreMgr.searchStoresByCoordinates(latitude, longitude, this.getStoreLookupDistanceUnit(), this.getStoreLookupMaxDistance());
            if (nearestStores.size() > 0){
                return nearestStores.entrySet()[0].getKey();
            } else {
                return null;
            }
        } else {
            return null;
        }
    },
    
    getNearestStores: function(latitude, longitude, resultType, addressShortName) {
        if (latitude && longitude && resultType != 'country') {
        	var maxdistance = 30;
        	var flag = true;
        	for (var i=1;flag; maxdistance) {
        		var stores = StoreMgr.searchStoresByCoordinates(latitude, longitude, this.getStoreLookupDistanceUnit(), maxdistance);
        		if (stores.size() != 0 || maxdistance > 50000 ) {
        			return stores;
        		}
        		maxdistance = maxdistance+100;
        	}
        } else {
            var storeList = new dw.util.ArrayList();
            var storesObj;
            if (!empty(addressShortName)) {
                storesObj = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'countryCode desc', addressShortName);
            } else {
                storesObj = SystemObjectMgr.querySystemObjects('Store', '', 'countryCode desc');
            }
            if (storesObj.getCount() > 0) {
                while (storesObj.hasNext()) {
                    var store = storesObj.next();
                    storeList.push(store);
                }
            } else {
            	var maxdistance = 30;
            	var flag = true;
            	for (var i=1;flag; maxdistance) {
            		var stores = StoreMgr.searchStoresByCoordinates(latitude, longitude, this.getStoreLookupDistanceUnit(), maxdistance);
            		if (stores.size() != 0 || maxdistance > 50000 ) {
            			return stores.keySet();
            		}
            		maxdistance = maxdistance+100;
            	}
            }
            return storeList;
        }
    },

    getFullAddressBasedOnRawString: function(rawString) {
        
        //Service definition.
        var geoCodeService = LocalServiceRegistry.createService('storelocator.http.geocoding.post', {
            createRequest: function(svc) {
                svc = svc.setRequestMethod('POST');
                svc = svc.addHeader('Content-type', 'application/x-www-form-urlencoded');
                svc = svc.addParam('address', rawString);
                var APIKey = StoreUtils.getGoogleMapsAPIKey();
                
                if (APIKey) {
                    svc = svc.addParam('key', APIKey);
                }                                
                return '';
            },
            parseResponse: function(svc, response) {
                return response;
            }
        });

        //Call the Service
        var result = geoCodeService.call();

        //Check for service availability.
        if (result.status == Result.SERVICE_UNAVAILABLE) {
            Logger.getLogger('GeoCoding', 'GeoCoding').fatal('Service down notification error in [libStoreUtils.js] while executing geocoding service for the string: ' + rawString);
            return null;
        }

        //Return JSON object on success.
        if (result.object && result.object.statusCode == '200') {
            return JSON.parse(result.object.text);
        } else {
            return null;
        }
    },
    getStoreType: function(params){
    	try {
    		var stores = params, storetype,
        		mackage = new ArrayList(),
        		retail = new ArrayList(),
        		soiaKyo = new ArrayList(),
        		storeContainer = new ArrayList();
	        	if(stores.length > 0){
		        	for each ( var store in stores ) {
		            	storetype = store.custom.storeType.value;
		            	if(storetype == 'Mackage'){
		            		mackage.push(store);
		            	}else if(storetype == 'SoiaKyo'){
		            		soiaKyo.push(store);
		            	}else{
		            		retail.push(store);
		            	}
		            } 
	        	}
	        	
	        	if(mackage.length > 0){
	        		storeContainer.push(mackage);
	        	}
	        	if(soiaKyo.length > 0){
	        		storeContainer.push(soiaKyo);
	        	}
	        	if(retail.length > 0){
	        		storeContainer.push(retail);
	        	}
	        	
	        	return storeContainer;
        } catch (e) {
            Logger.error( 'Error found in libStoreUtils.js getStoreType ()'+e);
        }
    },
    
    getStoreUrls : function(){
    	try{
    		var customObjects = CustomObjectMgr.getAllCustomObjects("AppStoreData"),
    			us = new ArrayList(),
    			canada = new ArrayList(),
    			europe = new ArrayList(),
    			asia = new ArrayList(),i;
    		
    		if(customObjects.count > 0){
    			for each(var customObject in customObjects ){
        			if(customObject.custom.storeCountry.toUpperCase() == 'UNITED STATES'){
        				us.push(customObject.custom.storeURL);
        			}else if(customObject.custom.storeCountry.toUpperCase() == 'CANADA'){
        				canada.push(customObject.custom.storeURL);
        			}else if(customObject.custom.storeCountry.toUpperCase() == 'EUROPE'){
        				europe.push(customObject.custom.storeURL);
        			}else{
        				asia.push(customObject.custom.storeURL);
        			}
        		}
    		}
    		
    		if(us.length > 0){
    			for (i = 0; i <= us.length-1; i++){
        			if(us[i].indexOf('www.mackage.com') != -1 || us[i].indexOf('www.soiakyo.com') != -1){
        				us.addAt(0,us[i]);
        				us.removeAt(i+1);
        			}
        		}
    		}
    		if(canada.length > 0){
    			for (i = 0; i <= canada.length-1; i++){
        			if(canada[i].indexOf('www.mackage.com') != -1 || canada[i].indexOf('www.soiakyo.com') != -1 ){
        				canada.addAt(0,us[i]);
        				canada.removeAt(i+1);
        			}
        		}
    		}
    		if(europe.length > 0){
    			for (i = 0; i <= europe.length-1; i++){
        			if(europe[i].indexOf('www.mackage.com') != -1 || europe[i].indexOf('www.soiakyo.com') != -1){
        				europe.addAt(0,us[i]);
        				europe.removeAt(i+1);
        			}
        		}
    		}
    		if(asia.length > 0){
    			for (i = 0; i <= europe.length-1; i++){
        			if(asia[i].indexOf('www.mackage.com') != -1 || asia[i].indexOf('www.soiakyo.com') != -1 ){
        				asia.addAt(0,us[i]);
        				asia.removeAt(i+1);
        			}
        		}
    		}
    		
			return {
				'us':us,
				'canada':canada,
				'europe':europe,
				'asia':asia
			};
    	}
    	catch(e){
    		Logger.error( 'Error found in libStoreUtils.js getStoreUrls ()'+e);
    	}
    	
    },
    getContinent : function(){
    	try{
    		var countryCode = request.geolocation.countryCode,
    		customObjects = CustomObjectMgr.getCustomObject("CustomDropDowns", "storeLocatorContinents"),
    		cList,continent;
    		if(!empty(customObjects.custom.storeLocatorContinents)){
    			cList = JSON.parse(customObjects.custom.storeLocatorContinents);
    			for (var i = 0; i <= cList.length-1; i++) {
    		    	if (cList[i].ID == countryCode) {
    		    		continent =  cList[i].Continent;
    		    		return continent;
    		    	}
    		  	}
    		}
    		if(empty(continent)){
    			continent="UNITED STATES";
    		}
    		return continent;
    	}catch(e){
    		Logger.error( 'Error found in libStoreUtils.js getContinent ()'+e);
    	}
    	
    	
    }
};

/*Module Exports*/
module.exports = StoreUtils;
