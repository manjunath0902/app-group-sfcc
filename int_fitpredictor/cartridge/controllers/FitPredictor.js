'use strict';

/**
* Description of the Controller and the logic it provides
* Fit Predictor Events Data
* @module  controllers/FitPredictor
*/


var app = require('app_appgroup_controllers/cartridge/scripts/app');
var guard = require('app_appgroup_controllers/cartridge/scripts/guard');
var ProductMgr = require('dw/catalog/ProductMgr');

/**
* Provides Fit Predictor Events Data
*
*/
var start = function(){
	var productID = request.httpParameterMap.pid.stringValue;
	var product = ProductMgr.getProduct(productID);
	app.getView({
		Product: product
	}).render('fitpredictor/fitpredictoreventsdata');
}

/**
 * Renders the fitpredictoreventsdata template.
 * @see module:controllers/FitPredictor~start
 */
exports.Start = guard.ensure([], start);

