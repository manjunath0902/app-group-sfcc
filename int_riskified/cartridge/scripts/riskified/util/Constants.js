'use strict';

var Site = require('dw/system/Site');

/**
 * Sitegenesis cartridge path 
 */
exports.APPGROUP_CONTROLLER_CARTRIDGE = "app_appgroup_controllers";

/**
 * This constant defines Riskified date format used in cartridge
 */
exports.RISKIFIED_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
                                 
/**
 * Order review pending status
 */
exports.ORDER_REVIEW_PENDING_STATUS = 1; 

/**
* Order review approved status
*/
exports.ORDER_REVIEW_APPROVED_STATUS = 2;

/**
* Order review declined status
*/
exports.ORDER_REVIEW_DECLINED_STATUS = 3;