'use strict';

/**
 * This includes common functions for Riskified.
 * 
 * @module riskified/export/api/common/CommonModel
 */

/* API Includes */
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var ServiceRegistry = require('dw/svc/ServiceRegistry');

/* Script Modules */
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var system = require('dw/system');
var object = require('dw/object');


/**
 * This method updates order confirmation status.
 * 
 * @param order The order to be updated
 * @param orderConfirmationStatus The order confirmation status
 * @param moduleName The name of module in current request
 * 
 * @returns {Boolean}
 */
function updateOrderConfirmationStatus(order, orderConfirmationStatus, moduleName) {
	
	var logLocation = moduleName + "CommonModel~updateOrderConfirmationStatus";
	RCLogger.logMessage("SetOrderConfirmationStatus: The order confirmation status is : " + orderConfirmationStatus, "debug", logLocation);
	
	try{
		Transaction.wrap(function(){
			order.setConfirmationStatus(orderConfirmationStatus);	
		});
	}catch(e){
		RCLogger.logMessage("Cannot update the order confirmation status\n Error is  "+ e, "error", logLocation);
		return false;
	}
	
	return true;
}

/**
 * This method save order analysis status in Order.
 * 
 * @param order The order to be updated
 * @param status The order analysis status
 * @param moduleName The name of module in current request
 * 
 * @returns {Boolean}
 */
function setOrderAnalysisStatus(order, status, moduleName) {
	
	var logLocation = moduleName + "CommonModel~setOrderAnalysisStatus";
	var sitePrefs = dw.system.Site.getCurrent().getPreferences();
	var recipientsArray : Array = [];
	var customMessageID = "";
	RCLogger.logMessage("The order analysis status is: " + status,"debug", logLocation);
	
	try {
		Transaction.wrap(function() {
			
			if(status == 3){
				//Changing order status in BM
				dw.order.OrderMgr.cancelOrder(order);
				
				//Sends email for order cancellation
				if (!empty(sitePrefs.getCustom()["emailMarketingService"]) && (sitePrefs.getCustom()["emailMarketingService"] == 'Bronto')
	        			&& !empty(sitePrefs.getCustom()["brontoControl"]) && (JSON.parse(sitePrefs.getCustom()["brontoControl"]).OrderCancellation)){
	            	recipientsArray.push(order.customerEmail);
	            	if(!empty(sitePrefs.getCustom()["customMessageID"]) && (JSON.parse(sitePrefs.getCustom()["customMessageID"])["order-cancellation"+'-'+order.customerLocaleID.toString()] != null)){
	            		customMessageID = JSON.parse(sitePrefs.getCustom()["customMessageID"])["order-cancellation"+'-'+order.customerLocaleID.toString()];
	            	}
	                sendOrderCancellationEmail(order,recipientsArray,customMessageID);
	            }
				
				//setting the cancel code & description
				var subject = "CANCELLED BY RISKIFIED";
				var text = request.httpParameterMap.requestBodyAsString;
				order.setCancelCode(subject);
				order.setCancelDescription(text);
				
				order.setConfirmationStatus(dw.order.Order.CONFIRMATION_STATUS_NOTCONFIRMED);
				order.setExportStatus(dw.order.Order.EXPORT_STATUS_NOTEXPORTED);
				
			}
						
			order.custom.riskifiedOrderAnalysis = status;
		});
	}catch(e) {
		RCLogger.logMessage(
				"Error occurred while setting order analysis status error is " + e,"error", logLocation);
		return false;
	}
	
	return true;
}


/**
 * This method save payment information in order's custom attributes 
 * 
 * @param order The order to be updated
 * @param moduleName The name of module in current request
 * 
 * @returns {Boolean}
 */
function savePaymentInformationInOrder(order, orderParams, moduleName) {
	
	var logLocation = moduleName+ "CommonModel~savePaymentInformationInOrder";
	
	var sessionId = orderParams.sessionId;
	var requestIp = orderParams.requestIp;
	var paymentParams = orderParams.paymentParams;
	var checkoutId = orderParams.checkoutId;
	
	try{
		Transaction.wrap(function(){
			
			order.custom.sessionID = sessionId;
			order.custom.requestIP = requestIp;
			order.custom.checkoutId = checkoutId;
			order.custom.paymentMethod = paymentParams.paymentMethod;
			
			if(paymentParams.paymentMethod == "CREDIT_CARD"){
				
				order.custom.avsResultCode = paymentParams.avsResultCode;
				order.custom.cvvResultCode = paymentParams.cvvResultCode;
				order.custom.cardIIN = paymentParams.cardIIN;
			}
			else if(paymentParams.paymentMethod == "PayPal"){
				
				order.custom.authorizationID = paymentParams.authorizationID;
				order.custom.payerEmail = paymentParams.payerEmail;
				order.custom.payerStatus = paymentParams.payerStatus;
				order.custom.payerAddressStatus = paymentParams.payerAddressStatus;
				order.custom.protectionEligibility = paymentParams.protectionEligibility;
				order.custom.paypalPaymentStatus = paymentParams.paymentStatus;
				order.custom.pendingReason = paymentParams.pendingReason;
			}
			
		});
	}catch(e){
		RCLogger.logMessage("Error occurred while saving data in custom attributes of order. Error is " + e, "error", logLocation);
		return false;
	}
	return true;
}

/**
 * This method post JSON payload to Riskified. This is used by both checkout denied and order export. 
 * 
 * @param moduleName The name of module in current request.
 * @param payload The request data in JSON format.
 * @param isCheckoutDenied The flag to indicate if current export is checkout denied or order related.
 * 
 * @returns {Boolean}
 */
function postToRiskified(moduleName, payload, isCheckoutDenied) {
	
	var logLocation = moduleName + "CommonModel~postToRiskified";
	var response;
	
	if(empty(payload)) {
		RCLogger.logMessage("Payload is missing, therefore cannot proceed further.", "error", logLocation);
		response = {
				error : true,
				recoveryNeeded: true,
				message : "Data Export Failed."
			};
		
		return response;
	}	
	
	var service;
	var result;
	var countryCode;
	
	var odrObj = JSON.parse(payload);
		if (moduleName == "CheckOutDenied"){
			 countryCode = odrObj.checkout.currency;
		}else{
			 countryCode = odrObj.order.currency;
		}
	var siteName = ('siteName' in system.Site.getCurrent().preferences.getCustom() && !empty(system.Site.getCurrent().getCustomPreferenceValue('siteName'))) ? system.Site.getCurrent().getCustomPreferenceValue('siteName') : system.Site.getCurrent().ID;
	
		if(isCheckoutDenied){
			service = ServiceRegistry.get("int_riskified.https.post.api.denied" + "." + siteName + "." + countryCode);
		} else {
			service = ServiceRegistry.get("int_riskified.https.post.api" +"."+ siteName +"."+ countryCode);
		}
		
	var serviceCredential = service.getConfiguration().getCredential();
	var merchantDomainAddressOnRiskified = serviceCredential.custom.merchantDomainAddressOnRiskified;
	var riskifiedAuthCode = serviceCredential.custom.riskifiedAuthCode;
	var hmac = RCUtilities.calculateRFC2104HMAC(payload, riskifiedAuthCode);
	
	if(empty(hmac)){
		RCLogger.logMessage("Some error occurred while calculating hash map, therefore cannot proceed further.", "error", logLocation);
		response = {
				error : true,
				recoveryNeeded: true,
				message : "Data Export Failed."
			};
		return response;
	}
	
	service.addHeader("X_RISKIFIED_HMAC_SHA256",hmac);
	service.addHeader("X_RISKIFIED_SHOP_DOMAIN", merchantDomainAddressOnRiskified);
	
	result = service.call(payload);
	
	if(result.ok) {
		
		response = parseRiskifiedResponse(result.object, moduleName, isCheckoutDenied);
		
	}else {
		
		RCLogger.logMessage("Data export to Riskified failed - HTTP Status Code is: " + result.error  
				+ ", Error Text is: "+ result.errorMessage, "error", logLocation);
		
		response = {
				error : true,
				recoveryNeeded: true,
				message : "Data Export Failed."
			};
	}
	
	return response;
}

/**
 * This method parse Riskified response and returns either data is successfully submited or not  
 * 
 * @param responseFromRiskified The HTTP response received from call to Riskified endpoint.
 * @param moduleName The name of module in current request
 * @param isCheckoutDenied The flag to indicate if current export is checkout denied or order related.
 * 
 * @returns {Object} The object that has status and message extracted from response.
 */
function parseRiskifiedResponse(responseFromRiskified,moduleName, isCheckoutDenied) {
	
	var logLocation = moduleName + "CommonModel~parseRiskifiedResponse";
	var obj = JSON.parse(responseFromRiskified);
	var error;
	var message;
	var recoveryNeeded = false;
	
	if (obj.error) {
		error = true;
		message = obj.error.message;
		recoveryNeeded = true;
	} else {
		error = false;
		
		if(isCheckoutDenied){
			RCLogger.logMessage( "Data transferred successfully to Riksifed", "debug", logLocation);
			message = "Checkout Deined data transferred successfully.";
			
		} else {
			RCLogger.logMessage( "Data transferred successfully. \n Riksifed order details are:"+
					" Order Id - " + obj.order.id + 
					" Order Status - " + obj.order.status + 
					" Order Description - " + obj.order.description, "debug", logLocation);
		
			message = obj.order.description;
		}
	}
	
	var response = {
			error : error,
			recoveryNeeded: recoveryNeeded,
			message : message
		};
	
	return response;
}

/*
 * Bronto order cancellation email
 */
function sendOrderCancellationEmail(order,RecipientsArray,messageID) {
	var CreateOrderCancellationCustomMessage = require('int_bronto/cartridge/scripts/messages/CustomMessage');
	CreateOrderCancellationCustomMessage.execute({
		Recipients: RecipientsArray,
		MessageID: messageID,
		Order: order
    });
}

/*
 * Module exports
 */
exports.updateOrderConfirmationStatus = updateOrderConfirmationStatus; 
exports.setOrderAnalysisStatus = setOrderAnalysisStatus;
exports.postToRiskified = postToRiskified;
exports.savePaymentInformationInOrder = savePaymentInformationInOrder;