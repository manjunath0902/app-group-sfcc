'use strict';
/**
 * This includes functionality to save data objects in case of export failure. It also includes
 * functionality to retry export.
 * 
 * @module riskified/export/api/common/RecoveryModel
 */

/* API Includes */
var Site = require('dw/system/Site');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Calendar = require('dw/util/Calendar');
var Transaction =require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');

/* Script Modules */
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var ExportOrderModel = require('~/cartridge/scripts/riskified/export/api/order/ExportOrderModel');
var CheckoutDeniedModel = require('~/cartridge/scripts/riskified/export/api/order/CheckoutDeniedModel');

/**
 * This method save order information in temporary data objects in case of failure during export to the Riskified
 * 
 * @param moduleName The name of module in current request
 * @param orderNo The order number that was failed to be transferred
 * @param checkoutDeniedParams The object that holds information related to checkout denied 
 * 
 * @returns {Boolean}
 */

function saveDataObject(moduleName, orderNo, checkoutDeniedParams){
	
	var logLocation  = moduleName + " : APIExportRecoveryModel~backupOrderInformation";
	
	try{
		var UUIDUtils = require('dw/util/UUIDUtils');
		var Transaction = require('dw/system/Transaction');

		Transaction.wrap(function () {
			
			var rcDataObject  = CustomObjectMgr.createCustomObject("RCDataObject", UUIDUtils.createUUID());
			rcDataObject.custom.orderNo = orderNo;
			rcDataObject.custom.retryIndex = 1;
			rcDataObject.custom.isCheckoutDenied = checkoutDeniedParams.isCheckoutDenied;
			
			if(checkoutDeniedParams.isCheckoutDenied){
				rcDataObject.custom.authErrorCreationTime = checkoutDeniedParams.createdAt;
				rcDataObject.custom.authErrorCode = checkoutDeniedParams.authErrorCode;
				rcDataObject.custom.authErrorMsg = checkoutDeniedParams.authErrorMsg;
			}  
		});
	}catch(e){
		RCLogger.logMessage("Exception occured while saving data in RCDataObject : " + e, "error", logLocation);
		return false;
	}
	
	return true;
}

/**
 * This method retries to export order or checkout denied data to Riskifeid in case of failure
 * 
 * @param moduleName The name of module in current request
 * 
 * @returns {Boolean}
 */

function retryExport(moduleName){
	
	var logLocation  = moduleName + " : RecoveryModel~retryExport";
	var dataObjsIterator  = getDataObjects(moduleName);
	
	if(empty(dataObjsIterator) || !dataObjsIterator.hasNext()){
		RCLogger.logMessage("No data objects found therefore exiting.", "debug",  logLocation);
		return true;
	}
	
	var retryLimit  = Site.getCurrent().getPreferences().custom.rcRetryLimit;
	var rcDataObject ;
	var dataObjectId;
	var order;
	var isCheckoutDenied;
	var orderParams;
	var checkoutDeniedParams;
	var response;
	
	try{
		while(dataObjsIterator.hasNext()){
			
			rcDataObject = dataObjsIterator.next();
			dataObjectId = rcDataObject.custom.id;
			
			try{
				
				order = OrderMgr.getOrder(rcDataObject.custom.orderNo);
				orderParams = RCUtilities.loadOrderParams(order, moduleName);
				
				if(empty(orderParams)){
					Transaction.wrap(function () {
						CustomObjectMgr.remove(rcDataObject);
					});
					
					RCLogger.logMessage("The order " + order.orderNo + " was missing important information in custom attributes, " +
							"therefore cannot execute recovey and removed data object with ID " + dataObjectId, "info",  logLocation);
					
					continue;
					
				}
				
				isCheckoutDenied = rcDataObject.custom.isCheckoutDenied;
				

				if(isCheckoutDenied){
					
					checkoutDeniedParams = {
							isCheckoutDenied: isCheckoutDenied,
							createdAt: rcDataObject.custom.authErrorCreationTime,
							authErrorCode:rcDataObject.custom.authErrorCode,
							authErrorMsg:rcDataObject.custom.authErrorMsg
						}
					response = CheckoutDeniedModel.exportCheckoutDeniedData(moduleName, order, orderParams, checkoutDeniedParams);
				}
				else{
					
					checkoutDeniedParams = {
							isCheckoutDenied: isCheckoutDenied
						}
					response = ExportOrderModel.exportOrderData(moduleName, order, orderParams, checkoutDeniedParams);
				}
				
				if (response.error){
					RCLogger.logMessage("Some error occured while retrying data export to Riskified. " +
							"\n Order Id: " + order.orderNo +
							"\n Checkout Denied: " + isCheckoutDenied +
							"\n Data Object ID: " + dataObjectId +
							"\n Error Message: " + response.message, "error", logLocation);
				} 
				
				if(response.recoveryNeeded){
					handleRetryError(rcDataObject, retryLimit, moduleName);
				} else {
					RCLogger.logMessage("Data transferred successfully to Riskified, therefore removing data object." +
							"\n Order Id: " + order.orderNo +
							"\n Checkout Denied: " + isCheckoutDenied, "debug", logLocation);
					
					Transaction.wrap(function () {
						CustomObjectMgr.remove(rcDataObject);
					});
				}
				
			}
			catch(e){
				RCLogger.logMessage("Some error occured while retrying data export to Riskified. " +
						"\n Order Id: " + order.orderNo +
						"\n Checkout Denied: " + isCheckoutDenied +
						"\n Data Object ID: " + dataObjectId +
						"\n Error Message: " + response.message +
						"\n Exception is: " + e
						, "error", logLocation);
				
				handleRetryError(rcDataObject, retryLimit, moduleName);
			}
			
		} //end while
	} catch(e){
		RCLogger.logMessage("Some error occurred while retrying export of order Information : Exception is: "+e, "error",  logLocation);
		return false;
	}
	
	return true;
}

/**
* This function loads data objects that hold backup information for order or checkout denied export.
* 
* @param moduleName The name of module in current request
*/
function getDataObjects(moduleName){
	var logLocation = moduleName + " : RecoveryModel~getDataObjects";

	var dataObjsIterator = null;
	
	try{
		var queryString  = "creationDate < {0}";
		var date  = new Calendar().getTime();
		
		dataObjsIterator = CustomObjectMgr.queryCustomObjects("RCDataObject", queryString, null, date);	
	
	}catch(e){
		RCLogger.logMessage("Some error occurred while retrieving RCDataObject : Exception is: "+e, "error",  logLocation);

	}
	
	return dataObjsIterator;
}

/**
* This function handles the retry attempt error. It updates or removes the custom object. 
* 
* @param rcDataObject The data object to be updated or removed
* @param retryLimit The number of retries after which object should be removed
* @param moduleName The name of module in current request
*/
function handleRetryError(rcDataObject, retryLimit, moduleName){
	var logLocation  = moduleName + " : RecoveryModel~handleRetryError";
		
	if(rcDataObject.custom.retryIndex == retryLimit ){
		var id = rcDataObject.custom.id;
		Transaction.wrap(function () {
			CustomObjectMgr.remove(rcDataObject);
		});
		
		RCLogger.logMessage("The retry of data object export with ID " + id + 
				" has reached to maximum limit of " + retryLimit + 
				", therefore removed data object", "debug",  logLocation);
		} else {
		Transaction.wrap(function () {
			rcDataObject.custom.retryIndex += 1;
		});
	}
}

/*
 * Module exports
 */
exports.saveDataObject = saveDataObject;
exports.retryExport = retryExport;