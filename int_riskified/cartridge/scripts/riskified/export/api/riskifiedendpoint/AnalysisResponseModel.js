'use strict';

/**
 * The module that handles analysis response from Riskified.
 * 
 * @module riskified/export/api/riskifiedendpoint/AnalysisResponseModel
 */

/* API Includes */
var OrderMgr = require('dw/order/OrderMgr');
var Site = require('dw/system/Site');

/* Script Modules */
var CommonModel = require('~/cartridge/scripts/riskified/export/api/common/CommonModel');
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');

var Logger = require('dw/system/Logger');

/**
 *This method handles order analysis response from Riskified and perform necessary tasks.
 *
 * @param moduleName The name of module which calls this method.
 * 
 * @returns {Boolean}
*/
function handle(moduleName)
{
	var logLocation = moduleName+ "AnalysisResponseModel~handle";
	var response;
	var riskifiedAuthCodeObj = JSON.parse(Site.getCurrent().preferences.custom.riskifiedAuthCode);

    var riskifiedAuthCode = riskifiedAuthCodeObj[request.locale.toString()];
	
	if(empty(riskifiedAuthCode)){
		
		RCLogger.logMessage("Riskified Authentication Code site preference is not set, therefore cannot proceed further.","error",logLocation);
		response = {
			error : true,
			message : "riskifiedAuthCode site preference not set in cartridge"
		};
		
		return response;
	}
	
	var inHMAC  = request.httpHeaders.get("x-riskified-hmac-sha256");
	
	if(empty(inHMAC)){
		response = {
			error : true,
			message : "Header HMAC missing"
		};
		
		return response;
	}
	
	var body= request.httpParameterMap.requestBodyAsString;
	var calculatedHMAC  = RCUtilities.calculateRFC2104HMAC(body, riskifiedAuthCode);
	
	if(!inHMAC.equals(calculatedHMAC)){
		response = {
			error : true,
			message : "Authentication error, calculated HashMAC not similar to request header"
		};
		
		return response;
	}
	
	var jsonObj = JSON.parse(body);
	var orderIdFromRiskified = jsonObj.order.id;
	var analysisStatus = jsonObj.order.status;
	
	Logger.info("Riskified order status : " + JSON.stringify(jsonObj));
	RCLogger.logMessage("Order Id from Riskified JSON ","info",orderIdFromRiskified);
	
	if(empty(orderIdFromRiskified)){
		response = {
			error : true,
			message : "Order ID is missing"
		};
		
		return response;
	}
	
	var order = OrderMgr.getOrder(orderIdFromRiskified);
	if(empty(order)){
		response = {
			error : true,
			message : "Order not found with order ID " + orderIdFromRiskified
		};
		
		return response;
	}
	
	var orderAnalysisResponse = parseOrderAnalysisResponse(analysisStatus, moduleName);
	var enablePaySafeSettlements = ('enablePaySafeSettlementsAfterRiskified' in dw.system.Site.getCurrent().preferences.getCustom() && !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('enablePaySafeSettlementsAfterRiskified'))) ? (dw.system.Site.getCurrent().getCustomPreferenceValue('enablePaySafeSettlementsAfterRiskified').toString() == 'Yes') : false;

	if(!orderAnalysisResponse.status){
		response = {
			error : true,
			message : "Unknown Order Analysis Status"
		};
	
		return response;
	} else if (enablePaySafeSettlements && "approved".equals(analysisStatus)) {
		var paymentInstrument : OrderPaymentInstrument = order.paymentInstruments[0];
		var cardAuthResponseId = JSON.parse(paymentInstrument.custom.creditCardAuthResponse).id;
		
		let Pipeline = require('dw/system/Pipeline');
		var pdict = Pipeline.execute('Netbanx-SettleOrderTransaction', {OrderId:order.orderNo,
			merchantRefNum:order.orderNo,
			Amount:order.totalGrossPrice.value,
			orderAuthId:cardAuthResponseId,
			OrderCurrency:order.currencyCode});
	}

		
	var orderAnalysisUpdateResult = CommonModel.setOrderAnalysisStatus(order, orderAnalysisResponse.orderAnalaysisStatus, moduleName);
	if(!orderAnalysisUpdateResult){
		response = {
			error : true,
			message : "Order review status couldn't be udpated."
		};
		
		return response;
	}
	
	if(Site.getCurrent().preferences.custom.holdOrderUntilRiskifiedDecision){
		var confirmationStatus = CommonModel.updateOrderConfirmationStatus(order, order.CONFIRMATION_STATUS_CONFIRMED, moduleName); 
		
		if(!confirmationStatus){
			response = {
				error : true,
				message : "Order review status couldn't be udpated."
			};
			
			return response;
		}
	}
	
	response = {
		error : false,
		message : "Order review status udpated successfully."
	};
	
	return response;
}
/**
 * This function parses analysis response.
 * 
 * @param orderAnalysisStatus The order analysis receive from Riskified
 * @param moduleName The name of module for current request.
 * 
 * @return {Boolean}
 */
function parseOrderAnalysisResponse(orderAnalysisStatus, moduleName) {
	
	var logLocation = moduleName + "AnalysisResponseModel~parseOrderAnalysisResponse";
	var orderAnalaysisStatus;
	var status = true;
	
	RCLogger.logMessage("ParseOrderAnalysisResponse: Riskified order analysis status: " + orderAnalysisStatus, "debug", logLocation);
		
	if("approved".equals(orderAnalysisStatus)){
		orderAnalaysisStatus = Constants.ORDER_REVIEW_APPROVED_STATUS;
	} else if("declined".equals(orderAnalysisStatus)){
		orderAnalaysisStatus = Constants.ORDER_REVIEW_DECLINED_STATUS;
	} else {
		status= false;
	}
	
	var orderAnalysisResponse = {
		status: status,
		orderAnalaysisStatus: orderAnalaysisStatus
	};
	
	return orderAnalysisResponse;
}

/*
 * Module exports
 */
exports.handle = handle;