'use strict';

/**
 * This model compose Order or Checkout Denied JSON
 * 
 * @module riskified/export/api/order/ExportDataComposer
 */

/* API Includes */
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');

var PaymentMgr = require('dw/order/PaymentMgr');

/* Script Modules */
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
/**
 * This function prepare request payload in JSON format for both order and checkout denied export to Riskified.
 * 
 * @param moduleName The name of module in current request.
 * @param order The failed or successful order to be exported 
 * @param orderParams The object that hold necessary information including payment data
 * @param checkoutDeniedParams The object that hold authorisation failure related information
 * 
 * @returns {String} The JSON payload
 */
function prepareJSON(moduleName, order, orderParams, checkoutDeniedParams) {
	
	var regex = "([\\\\&\"\'\\\/])";
	var regExp = new RegExp(regex, "gi");
	var sessionId = orderParams.sessionId;
	var requestIp = orderParams.requestIp;
	var paymentParams = orderParams.paymentParams;
	var checkoutId = orderParams.checkoutId;
	var id;
	var isCheckoutDenied = checkoutDeniedParams.isCheckoutDenied;
	var result = "";
	var temp = "";

	var logLocation = moduleName + ": ExportDataComposer~prepareOrderJSON";

	result += "{";

	if (isCheckoutDenied) {

		result += "\"checkout\":";
		result += "{";
		id = checkoutId;

	} else {
		
		result += "\"order\":";
		result += "{";
		result += "\"checkout_id\": \"" + checkoutId + "\",";
		id = order.orderNo;
	}

	temp += "\"cancel_reason\": null," 
			+ "\"cancelled_at\": null,"
			+ "\"cart_token\": \"{0}\"," 
			+ "\"closed_at\": null," 
			+ "\"created_at\": \"{1}\","
			+ "\"currency\": \"{2}\"," 
			+ "\"email\" : \"{3}\","
			+ "\"gateway\" : \"{4}\"," 
			+ "\"id\": \"{5}\","
			+ "\"total_discounts\": \"{6}\"," 
			+ "\"total_price\" : \"{7}\","
			+ "\"updated_at\": \"{8}\"," 
			+ "\"browser_ip\": \"{9}\","
			+ "\"source\": \"{10}\",";

	
	var paymentProcessor = null;
	var orderPaymInstrument = null;
	var ordPaymInstIt = order.getPaymentInstruments().iterator();

	while (ordPaymInstIt.hasNext()) {
		orderPaymInstrument = ordPaymInstIt.next();

		if (paymentParams.paymentMethod == "CREDIT_CARD") {
			paymentProcessor = PaymentMgr.getPaymentMethod(orderPaymInstrument.paymentMethod).paymentProcessor.ID;
			break;
		}
		
		if (paymentParams.paymentMethod == "PayPal") {
			paymentProcessor = PaymentMgr.getPaymentMethod(orderPaymInstrument.paymentMethod).paymentProcessor.ID;
			break;
		}
		
		if (paymentParams.paymentMethod == dw.order.PaymentInstrument.METHOD_DW_APPLE_PAY) {
			paymentProcessor = "Apple Pay";
			break;
		}
	}

	var merchTotalExclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(false);
	var merchTotalInclOrderDiscounts = order.getAdjustedMerchandizeTotalPrice(true);
	var orderDiscount = merchTotalExclOrderDiscounts.subtract(merchTotalInclOrderDiscounts);
	var orderTotal;

	if (order.totalGrossPrice.available) {
		orderTotal = order.totalGrossPrice;
	} else {
		orderTotal = merchTotalInclOrderDiscounts.add(order.giftCertificateTotalPrice);
	}

	var calendar = new Calendar(order.creationDate);
	var creationDate = StringUtils.formatCalendar(calendar, Constants.RISKIFIED_DATE_FORMAT);

	calendar = new Calendar(order.lastModified);
	var modifiedDate = StringUtils.formatCalendar(calendar, Constants.RISKIFIED_DATE_FORMAT);
	if (empty(requestIp)) {

		requestIp = "";
	}
	if (empty(sessionId)) {
		sessionId = "";
	}
	result += StringUtils.format(temp, sessionId, creationDate, order.currencyCode,
			order.customerEmail, paymentProcessor, id,
			orderDiscount.decimalValue, orderTotal.decimalValue, modifiedDate,
			requestIp);

	result += "\"discount_codes\": [ ";

	var first = true;
	var priceAdjustmentItr;
	
	if (!empty(order.getPriceAdjustments())) {

		priceAdjustmentItr = order.getPriceAdjustments().iterator();

		while (priceAdjustmentItr.hasNext()) {
			var priceAdjustment = priceAdjustmentItr.next();

			if (!first) {
				result += ",";
			}
			result += "{" + "\"amount\": \""
					+ -(priceAdjustment.price.decimalValue) + "\","
					+ "\"code\": \"" + priceAdjustment.promotionID + "\"" + "}";

			first = false;
		}
	}
	result += "],";
	result += "\"line_items\": [ ";
	
	var ProductMgr = require('dw/catalog/ProductMgr');
	
	var shipment;
	var prodLineItemIt;
	var masterID;
	var category;
	var brand;
	var productLineItem;
	var product;
	first = true;
	var orderShipmentIt = order.getShipments().iterator();
	
	while (orderShipmentIt.hasNext()) {
		shipment = orderShipmentIt.next();
		prodLineItemIt = shipment.getProductLineItems().iterator();
		masterID = null;

		while (prodLineItemIt.hasNext()) {

			productLineItem = prodLineItemIt.next();
			product = ProductMgr.getProduct(productLineItem.productID);
			brand = product.getBrand();
			category  = product.getPrimaryCategory();
			
			if (product.isVariant()
					&& !empty(product.getVariationModel().getMaster())) {
				
				var masterProduct = product.getVariationModel().getMaster();
				masterID = masterProduct.getID();
				
				if( empty(brand) ){
				
					brand = masterProduct.getBrand();
				}
				if( empty(category) ){
					
					category = masterProduct.getPrimaryCategory();
				}
				
			} else {

				masterID = product.getID();
				
			}

			if (!first) {
				result += ",";
			}
			
			var catName = '';
			
			if (category != null && !empty(category)) {
				catName =  category.getDisplayName();
			}
			
			result += "{" + 
					"\"title\": \"" + RCUtilities.escape(productLineItem.productName,regExp,"",true) + "\"," + 
					"\"price\": \"" + productLineItem.adjustedPrice.decimalValue + "\"," + 
					"\"product_id\": \"" + masterID + "\"," + 
					"\"quantity\": \"" + productLineItem.quantity.value + "\"," + 
					"\"sku\": \"" + productLineItem.productID + "\"," + 
					"\"brand\": \"" + RCUtilities.escape(brand,regExp,"",true)+ "\"," +
					"\"category\": \"" + catName + "\"" +
					"}";

			first = false;
		}
	}

	result += "],";

	var shipmentTitle;

	result += "\"shipping_lines\": [";
	orderShipmentIt = order.getShipments().iterator();
	first = true;

	while (orderShipmentIt.hasNext()) {
		shipment = orderShipmentIt.next();

		if (shipment.shippingMethod != null) {
			shipmentTitle = shipment.shippingMethod.displayName;
		} else {
			shipmentTitle = shipment.shippingMethodID;
		}

		if (!first) {
			result += ",";
		}

		result += "{" + 
				"\"code\": \"" + RCUtilities.escape(shipment.shippingMethodID,regExp,"",true) + "\"," + 
				"\"price\": \"" + shipment.adjustedShippingTotalPrice.decimalValue+ "\"," + 
				"\"title\": \"" + RCUtilities.escape(shipmentTitle,regExp,"",true) + "\"" + 
				"}";

		first = false;
	}

	result += "],";
	
	result +="\"client_details\": {" +
				"\"accept_language\": \""+ request.locale.toString() +"\"," +
				"\"user_agent\": \""+  request.httpUserAgent +"\"" + 
				"},";
	

	ordPaymInstIt = order.getPaymentInstruments().iterator();

	while (ordPaymInstIt.hasNext()) {

		orderPaymInstrument = ordPaymInstIt.next();

		if (paymentParams.paymentMethod == "CREDIT_CARD" || paymentParams.paymentMethod == dw.order.PaymentInstrument.METHOD_DW_APPLE_PAY) {
			var credit_card_bin = paymentParams.paymentMethod != dw.order.PaymentInstrument.METHOD_DW_APPLE_PAY?RCUtilities.escape(paymentParams.cardIIN,regExp,"",true):"";
			result += "\"payment_details\": {" + 
					"\"avs_result_code\": \"" + RCUtilities.escape(paymentParams.avsResultCode,regExp,"",true) + "\"," +
					"\"credit_card_bin\": \"" + credit_card_bin + "\"," + 
					"\"credit_card_company\": \"" + RCUtilities.escape(orderPaymInstrument.creditCardType,regExp,"",true) + "\"," + 
					"\"credit_card_number\": \"" + RCUtilities.escape(orderPaymInstrument.maskedCreditCardNumber,regExp,"",true) + "\"," +
					"\"cvv_result_code\": \"" + RCUtilities.escape(paymentParams.cvvResultCode,regExp,"",true) + "\"";

		if (isCheckoutDenied) {
				result += ",";
				result += "\"authorization_error\": {"
						+ "\"created_at\": \"" + checkoutDeniedParams.createdAt + "\"," 
						+ "\"error_code\": \"" + RCUtilities.escape(checkoutDeniedParams.authErrorCode,regExp,"",true) + "\"," 
						+ "\"message\": \"" + RCUtilities.escape(checkoutDeniedParams.authErrorMsg,regExp,"",true) + "\"" 
						+ "}";
			}
			result += "},";
			break;
		}
		if (paymentParams.paymentMethod == "PayPal") {

			result += "\"payment_details\": {" + 
					"\"authorization_id\": \"" + RCUtilities.escape(paymentParams.authorizationID,regExp,"",true) + "\"," +
					"\"payer_email\": \"" + RCUtilities.escape(paymentParams.payerEmail ,regExp,"",true)+ "\"," +
					"\"payer_status\": \"" + RCUtilities.escape(paymentParams.payerStatus,regExp,"",true) + "\"," + 
					"\"payer_address_status\": \"" + RCUtilities.escape(paymentParams.payerAddressStatus,regExp,"",true) + "\"," +
					"\"protection_eligibility\": \"" + RCUtilities.escape(paymentParams.protectionEligibility,regExp,"",true) + "\"," +
					"\"payment_status\": \"" + RCUtilities.escape(paymentParams.paymentStatus,regExp,"",true) + "\"," + 
					"\"pending_reason\": \"" + RCUtilities.escape(paymentParams.pendingReason ,regExp,"",true)+ "\"";
			
			
			if (isCheckoutDenied) {
				result += ",";
				result += "\"authorization_error\": {"
					+ "\"created_at\": \"" + checkoutDeniedParams.createdAt + "\"," 
					+ "\"error_code\": \"" + RCUtilities.escape(checkoutDeniedParams.authErrorCode,regExp,"",true) + "\"," 
					+ "\"message\": \"" + RCUtilities.escape(checkoutDeniedParams.authErrorMsg ,regExp,"",true)+ "\"" 
					+ "}";
			}
			result += "},";
			break;
		}
	}

	var billingAddres = order.getBillingAddress();
	var billingCompanyName;
	
	if (empty(billingAddres.companyName)) {
		billingCompanyName = "";
	}
	result += "\"billing_address\": {" + 
				"\"address1\": \"" + RCUtilities.escape(billingAddres.address1,regExp,"",true) + "\"," + 
				"\"address2\": \"" + RCUtilities.escape(billingAddres.address2,regExp,"",true) + "\"," + 
				"\"city\": \"" + RCUtilities.escape(billingAddres.city,regExp,"",true) + "\"," + 
				"\"company\": \"" + RCUtilities.escape(billingCompanyName,regExp,"",true) + "\"," + 
				"\"country\": \""+ billingAddres.countryCode.displayValue + "\"," +
				"\"country_code\": \"" + billingAddres.countryCode.value + "\"," +
				"\"first_name\": \"" + RCUtilities.escape(billingAddres.firstName,regExp,"",true) + "\"," +
				"\"last_name\": \"" + RCUtilities.escape(billingAddres.lastName,regExp,"",true) + "\"," +
				"\"name\": \"" + RCUtilities.escape(billingAddres.fullName,regExp,"",true) + "\"," + 
				"\"phone\": \"" + RCUtilities.escape(billingAddres.phone,regExp,"",true) + "\"," + 
				"\"province\": \"" + billingAddres.stateCode + "\"," + 
				"\"province_code\": \"" + billingAddres.stateCode + "\"," + 
				"\"zip\": \"" + billingAddres.postalCode + "\"" + 
				"},";

	orderShipmentIt = order.getShipments().iterator();
	var shippingCompanyName;
	var shippingAddress;
	
	while (orderShipmentIt.hasNext()) {

		shipment = orderShipmentIt.next();
		shippingAddress = shipment.shippingAddress;
		if (empty(shippingAddress.companyName)) {
			shippingCompanyName = "";
		}
		
		result += "\"shipping_address\": {" + 
					"\"address1\": \"" + RCUtilities.escape(shippingAddress.address1,regExp,"",true) + "\"," + 
					"\"address2\": \"" + RCUtilities.escape(shippingAddress.address2,regExp,"",true) + "\"," + 
					"\"city\": \"" + RCUtilities.escape(shippingAddress.city,regExp,"",true) + "\"," + 
					"\"company\": \"" + RCUtilities.escape(shippingCompanyName,regExp,"",true) + "\"," + 
					"\"country\": \"" + shippingAddress.countryCode.displayValue + "\"," +
					"\"country_code\": \"" + shippingAddress.countryCode.value + "\"," + 
					"\"first_name\": \"" + RCUtilities.escape(shippingAddress.firstName,regExp,"",true) +  "\"," + 
					"\"last_name\": \"" + RCUtilities.escape(shippingAddress.lastName,regExp,"",true) + "\"," + 
					"\"name\": \"" + RCUtilities.escape(shippingAddress.fullName,regExp,"",true) + "\"," +
					"\"phone\": \"" + RCUtilities.escape(shippingAddress.phone,regExp,"",true) + "\"," +
					"\"province\": \"" + shippingAddress.stateCode + "\"," +
					"\"province_code\": \"" + shippingAddress.stateCode + "\"," +
					"\"zip\": \"" + shippingAddress.postalCode + "\"" + 
					"},";
	}
	
	result += "\"source\": \""+ "web" +"\","; 

	
	var customer = order.customer;

	if (customer.isRegistered()) {
		RCLogger.logMessage("PrepareOrderJSON: The customer is registered",
				"debug", logLocation);

		Calendar = new Calendar(customer.profile.getCreationDate());
		var customerCreationDate = StringUtils.formatCalendar(Calendar, Constants.RISKIFIED_DATE_FORMAT);
		var customerNote;
		
		if (empty(customer.note)) {
			customerNote = "";
		}
		
		result += "\"customer\": {" + 
				"\"created_at\": \"" + customerCreationDate + "\"," + 
				"\"email\": \"" + RCUtilities.escape(customer.profile.email,regExp,"",true) + "\"," + 
				"\"first_name\": \"" + RCUtilities.escape(customer.profile.firstName,regExp,"",true) + "\"," + 
				"\"id\": \"" + RCUtilities.escape(customer.profile.customerNo,regExp,"",true) + "\"," + 
				"\"last_name\": \"" + RCUtilities.escape(customer.profile.lastName,regExp,"",true) + "\"," + 
				"\"note\": \"" + customerNote + "\"," + 
				"\"orders_count\": \"" + customer.orderHistory.orderCount + "\"," + 
				"\"verified_email\": \"false\"" + 
				"}";
	} else {

		RCLogger.logMessage("PrepareOrderJSON: The customer is a guest user.",
				"debug", logLocation);

		result += "\"customer\": {" + 
				"\"created_at\": \"\"," + "\"email\": \"" + RCUtilities.escape(order.customerEmail ,regExp,"",true)+ "\"," + 
				"\"first_name\": \"" + RCUtilities.escape(order.customerName ,regExp,"",true)+ "\"," + 
				"\"id\": \"" + customer.orderHistory.orderCount + "\"," + 
				"\"last_name\": \"\"," + 
				"\"note\": \"\"," + 
				"\"orders_count\": \"\"," + 
				"\"verified_email\": \"false\""
				+ "}";
	}
	
	result += "}";
	result += "}";
	
	return result;
}

/*
 * Module exports
 */
exports.prepareJSON = prepareJSON;