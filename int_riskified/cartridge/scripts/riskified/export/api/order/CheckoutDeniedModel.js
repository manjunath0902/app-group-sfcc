'use strict';

/**
 * This model handles checkout denied related functionality.
 * 
 * @module riskified/export/api/order/CheckoutDeniedModel
 */

/* Script Modules */
var CommonModel = require('~/cartridge/scripts/riskified/export/api/common/CommonModel');
var ExportDataComposer = require('~/cartridge/scripts/riskified/export/api/order/ExportDataComposer');

/**
 * This function exports checkout denied data to Riskified. 
 * 
 * @param moduleName The name of module in current request.
 * @param order The order to be exported.
 * @param orderParams The mandatory parameters required for order export.
 * @param checkoutDeniedParams The parameter required for checkout denied export
 * 
 * @returns {Object} The response information in JSON format 
 */
function exportCheckoutDeniedData(moduleName, order, orderParams, checkoutDeniedParams) {
	
	var checkoutDeniedJSON = ExportDataComposer.prepareJSON(moduleName, order, orderParams, checkoutDeniedParams);
		
	var response = CommonModel.postToRiskified(moduleName, checkoutDeniedJSON, checkoutDeniedParams.isCheckoutDenied);
	return response;
}

/*
 * Module exports
 */
exports.exportCheckoutDeniedData = exportCheckoutDeniedData;
