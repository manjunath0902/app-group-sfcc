'use strict';

/**
 * This model handles order export related functionality.
 * 
 * @module riskified/export/api/order/ExportOrderModel
 */

/* API Includes */
var Site = require('dw/system/Site');

/* Script Modules */
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
var CommonModel = require('~/cartridge/scripts/riskified/export/api/common/CommonModel');
var ExportDataComposer = require('~/cartridge/scripts/riskified/export/api/order/ExportDataComposer');

/**
 * This function exports order data to Riskified. 
 * 
 * @param moduleName The name of module in current request.
 * @param order The order to be exported.
 * @param orderParams The mandatory parameters required for order export.
 * @param checkoutDeniedParams The parameter required for checkout denied export.
 * 
 * @returns {Object} The response information in JSON format 
 */
function exportOrderData(moduleName, order, orderParams, checkoutDeniedParams) {
	
	var orderJSON = ExportDataComposer.prepareJSON(moduleName, order, orderParams, checkoutDeniedParams);
		
	var response = CommonModel.postToRiskified(moduleName, orderJSON, checkoutDeniedParams.isCheckoutDenied);
	
	if(!response.error) {
		var orderAnalysisResult = CommonModel.setOrderAnalysisStatus(order, Constants.ORDER_REVIEW_PENDING_STATUS, moduleName);
		if(!orderAnalysisResult){
			response = {
				error : true,
				recoveryNeeded: false,
				message : "Order review status couldn't be updated."
			};
			
			return response;
		}
		
		var holdOrderUntilRiskifiedDecision = Site.getCurrent().preferences.custom.holdOrderUntilRiskifiedDecision;
		
		if (holdOrderUntilRiskifiedDecision) {
			var orderConfirmationResult = CommonModel.updateOrderConfirmationStatus(order, order.CONFIRMATION_STATUS_NOTCONFIRMED, moduleName);
			
			if(!orderConfirmationResult){
				response = {
					error : true,
					recoveryNeeded: false,
					message : "Order confirmation status couldn't be udpated."
				};
				
				return response;
			}
		}
	}
	
	return response;
}

/*
 * Module exports
 */
exports.exportOrderData = exportOrderData;