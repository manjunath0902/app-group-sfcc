'use strict';
/**
 * Controller that contains all hooks for integration with Riskified cartridge.
 * 
 * @module controllers/Riskified
 */

/* API Includes */
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');

/* Script Modules */
var CheckoutDeniedModel = require('~/cartridge/scripts/riskified/export/api/order/CheckoutDeniedModel');
var CommonModel = require('~/cartridge/scripts/riskified/export/api/common/CommonModel');
var ExportOrderModel = require('~/cartridge/scripts/riskified/export/api/order/ExportOrderModel');
var PaymentInformationModel = require('~/cartridge/scripts/riskified/export/api/payment/PaymentInformationModel');
var RecoveryModel = require('~/cartridge/scripts/riskified/export/api/common/RecoveryModel');
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
var UUIDUtils = require('dw/util/UUIDUtils');

/**
 * This method saves payment related information during billing step in checkout. It also generates
 * unique checkout id, which is used if payment authorisation fails later.
 * 
 * @param paymentMethod The payment current used for billing.
 * 
 * @return {Boolean}
 */
function handlePayment(paymentMethod) {
	
	var moduleName = "HandlePayment";
	var logLocation = moduleName + " : Riskified~handlePayment";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		return false;
	}
	
	PaymentInformationModel.savePaymentDetails(paymentMethod);
	
	return true;
}

/**
 * This method is called during the checkout process after the payment authorisation. It stores
 * payment information in user session to be used afterwards in order export.
 * 
 * @param paymentParams The payment information that holds authorisation related information.
 * 
 * @return {Boolean}
 */
function storePaymentDetails(paymentParams) {
	
	var moduleName = "StorePaymentAuthorizationDetails";
	var logLocation = moduleName + " : Riskified~storePaymentDetails";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preferences is not enabled therefore cannot proceed further", "debug", logLocation);
		return false;
	}
	
	PaymentInformationModel.savePaymentAuthorizationDetails(paymentParams, moduleName);
	
	return true;
}

/**
 * This method export order information to Riskified.
 * 
 * @param order The order to be exported
 * 
 * @return {Boolean}
 */
function sendOrder(order) {
	var moduleName = "ExportOrder";
	var logLocation = moduleName + " : Riskified~sendOrder";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		return false;
	}
	
	if(empty(session.custom.paymentParams)){
		RCLogger.logMessage("Payment related information is lost, therefore cannot proceed further", "error", logLocation);
		return false;
	}
	
	/*if(empty(session.custom.checkoutUUID)){
		RCLogger.logMessage("checkoutUUID is lost, therefore cannot proceed further", "error", logLocation);
		return false;
	}*/

	var orderParams = {
		sessionId: session.getSessionID(),
		requestIp: request.getHttpRemoteAddress(),
		paymentParams: session.custom.paymentParams,
		checkoutId: !empty(session.custom.checkoutUUID)? session.custom.checkoutUUID : UUIDUtils.createUUID()
	}
	
	var checkoutDeniedParams = {
		isCheckoutDenied: false
	}
	
	CommonModel.savePaymentInformationInOrder(order, orderParams, moduleName);
	var response = ExportOrderModel.exportOrderData(moduleName, order, orderParams, checkoutDeniedParams);
	
	
	session.custom.checkoutUUID = null;
	session.custom.paymentParams = null;
	
	if(response.error){
		RCLogger.logMessage("Error occured while exporting order " + order.orderNo + " to Riskified. \n Error Message: " + response.message, "error", logLocation);
		
		if(response.recoveryNeeded){
			RecoveryModel.saveDataObject( moduleName, order.orderNo, checkoutDeniedParams);
		}
		
		return false;
	}
	
	return true;
}

/**
 * This function send failed order data along with authorisation error data, when payment instrument authorisation is failed during checkout.
 * 
 * @param order	The failed order to be exported
 * @param paymentParams This object holds payment instrument information e.g. AVS or CVV Result code for card payment and PayPal user information for PayPal payment
 * @param authErrorParams This object holds authorisation error code and message
 * 
 * @return {Boolean}
 */
function sendCheckoutDenied(order, paymentParams, authErrorParams){

	var moduleName = "CheckOutDenied";
	var logLocation = moduleName + " : Riskified~checkOutDenied";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		return false;
	}
	
	if(empty(paymentParams)){
		RCLogger.logMessage("Payment related information is missing, therefore cannot proceed further", "error", logLocation);
		return false;
	}
	
	/*if(empty(session.custom.checkoutUUID)){
		RCLogger.logMessage("checkoutUUID is lost, therefore cannot proceed further", "error", logLocation);
		return false;
	}*/

	if(paymentParams.paymentMethod== "CREDIT_CARD"){
		paymentParams.cardIIN = session.custom.cardIIN;
	}
	
	var orderParams = {
		sessionId: session.getSessionID(),
		requestIp: request.getHttpRemoteAddress(),
		paymentParams: paymentParams,
		checkoutId: !empty(session.custom.checkoutUUID)? session.custom.checkoutUUID : UUIDUtils.createUUID()
	}

	var currentDate = new Calendar();
	var creationTime = StringUtils.formatCalendar(currentDate, Constants.RISKIFIED_DATE_FORMAT);
	authErrorParams.createdAt = creationTime;
	
	var checkoutDeniedParams = {
			
		isCheckoutDenied: true,
		createdAt: authErrorParams.createdAt,
		authErrorCode: authErrorParams.authErrorCode,
		authErrorMsg: authErrorParams.authErrorMsg
		
	}
	
	CommonModel.savePaymentInformationInOrder(order, orderParams, moduleName);
	var response = CheckoutDeniedModel.exportCheckoutDeniedData(moduleName, order, orderParams, checkoutDeniedParams);
	
	if(response.error){
		RCLogger.logMessage("Error occured while sending checkout denied data with order number " + order.orderNo + " to Riskified. " +
				"\n Error Message: " + response.message, "error", logLocation);
		
		if(response.recoveryNeeded){
			RecoveryModel.saveDataObject( moduleName, order.orderNo, checkoutDeniedParams);
		}
		
		return false;
	}
	
	return true;
} 

/**
 * This function is called by the Recovery process job schedule. It retry sending order or checkout denied related data, 
 * which was failed in case of any failure.
 * 
 * @returns {Boolean}
 */
function executeRecoveryProcess(){
	
	var moduleName = "RetryDataExport";
	var logLocation = moduleName + " : Riskified~executeRecoveryProcess";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		return false;
	}
	
	return RecoveryModel.retryExport(moduleName);
}



/*
 * Module exports
 */
exports.handlePayment = handlePayment; 
exports.storePaymentDetails = storePaymentDetails;
exports.sendOrder = sendOrder;
exports.sendCheckoutDenied = sendCheckoutDenied;
exports.executeRecoveryProcess = executeRecoveryProcess;