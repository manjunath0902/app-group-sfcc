'use strict';

/**
 * @module controllers/RiskifiedEndPoint
 */


/* Script Modules */
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var AnalysisResponseModel = require('~/cartridge/scripts/riskified/export/api/riskifiedendpoint/AnalysisResponseModel');

/* API Includes */
var app = require(Constants.APPGROUP_CONTROLLER_CARTRIDGE+'/cartridge/scripts/app');
var guard = require(Constants.APPGROUP_CONTROLLER_CARTRIDGE+'/cartridge/scripts/guard');

/**
 * This function handles order analysis status request from Riskified. This perform authorisation
 * on incoming request to ensure that its a legitimate request. It also update analysis and order status 
 * accordingly.
 */
function handleAnalysisResponse(){
	
	var moduleName = "AnalysisNotificationEndpoint";
	var logLocation = moduleName + " : Riskified~handleAnalysisResponse";
	
	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		
		app.getView({
			CartridgeDisabled: true
		}).render('riskified/riskifiedorderanalysisresponse');	
	}
	
	var response = AnalysisResponseModel.handle(moduleName);
	
	if(response.error){
		RCLogger.logMessage("There were errors while updating order analysis. Error Message: " + response.message, "error", logLocation);
		
		app.getView({
			AnalysisUpdateError:true,
			AnalysisErrorMessage: response.message
			
		}).render('riskified/riskifiedorderanalysisresponse');
		
	} else {
		app.getView({
			ResponseStatus:'ok'
		}).render('riskified/riskifiedorderanalysisresponse');
	}
}

/*
 * Web exposed method 
 */
exports.AnalysisNotificationEndpoint = guard.ensure(['post', 'https'], handleAnalysisResponse);