'use strict';
/**
 * The controller that handles order submission from Business Manager custom interface. It also handles searching orders from
 * Business Manager custom interface.
 * 
 * @module controllers/RisikifiedBM
 */

/* Script Modules */
var Constants = require('~/cartridge/scripts/riskified/util/Constants');
var RCLogger = require('~/cartridge/scripts/riskified/util/RCLogger');
var RCUtilities = require('~/cartridge/scripts/riskified/util/RCUtilities');
var SearchOrderModel = require('~/cartridge/scripts/riskified/bm/SearchOrderModel');
var ExportOrderModel = require('~/cartridge/scripts/riskified/export/api/order/ExportOrderModel');

/* API Includes */
var app = require(Constants.APPGROUP_CONTROLLER_CARTRIDGE+ '/cartridge/scripts/app');
var guard = require(Constants.APPGROUP_CONTROLLER_CARTRIDGE + '/cartridge/scripts/guard');
var OrderMgr = require('dw/order/OrderMgr');

/**
 * 
 * This method submit order information through Riskified Business Manager  
 * @returns
 */
function submitOrder(){
	var moduleName = "ExportOrderFromBM";
	var logLocation = moduleName+ "RiskifiedBM~submitOrder";

	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preference is not enabled therefore cannot proceed further", "debug", logLocation);
		app.getView({
			CartridgeDisabled: true
		}).render('riskified/bm/ordersubmissionerror');
	}
	
	var orderNo = request.httpParameterMap.orderNo.stringValue;
	
	var order = OrderMgr.getOrder(orderNo);
	var orderParams = RCUtilities.loadOrderParams(order, moduleName);
	
	if(empty(orderParams)){
		app.getView({
			OrderInformationMissing: true
		}).render('riskified/bm/ordersubmissionerror');
		
		return;
	}
	
	var checkoutDeniedParams = {
		isCheckoutDenied: false
	}
	
	var response = ExportOrderModel.exportOrderData(moduleName, order, orderParams, checkoutDeniedParams);
	
	if (response.error){
		RCLogger.logMessage("Error occured while sumbitting Order to Riskified. \n Error Message: " + response.message, "error", logLocation);
		
		app.getView({
			MessageToBMUser: response.message
		}).render('riskified/bm/ordersubmissionerror');
		
	} else {
		app.getView().render('riskified/bm/ordersubmissionsuccess');
	}
}

/**
 * This method search orders based on query from the Riskified Business Manager
 */
function search(){
	
	var moduleName = "SearchOrders";
	var logLocation = moduleName+ "RiskifiedBM~search";
	var httpParameterMap = request.httpParameterMap;

	if(!RCUtilities.isCartridgeEnabled()){
		RCLogger.logMessage("riskifiedCartridgeEnabled site preferences is not enabled therefore cannot proceed further", "debug", logLocation);
		
		app.getView({
			CartridgeDisabled:true
		}).render('riskified/bm/ordersearch');	
	}
	
	var orderNo = httpParameterMap.orderNo.stringValue;
	var startDateString = httpParameterMap.startDate.stringValue;
	var endDateString = httpParameterMap.endDate.stringValue;
	var pageSizeIn = httpParameterMap.pageSize.stringValue;
	
	if(empty(orderNo) && empty(startDateString)&& empty(endDateString)){	
		
		app.getView().render('riskified/bm/ordersearch');	
		return;
	}
	
	var result = SearchOrderModel.searchOrders(orderNo, startDateString, endDateString, pageSizeIn, moduleName);

	if(empty(result)){
		result.ordersCount = null;
		result.orderPagingModel = null;
	}
	
	app.getView({
		OrdersCount : result.ordersCount,
		OrderPagingModel : result.orderPagingModel
	}).render('riskified/bm/ordersearch');	
}

/*
 * Public exports called by Business Manager
 */
exports.SubmitOrder = guard.ensure(['get', 'https'], submitOrder);
exports.Search = guard.ensure(['get', 'https'], search);

