module.exports = {
    options: {
        method: 'POST',
        headers: {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding': 'gzip,deflate',
            'Accept-Language': 'de-de,de;q=0.8,en-us;q=0.5,en;q=0.3',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0'
        }
    },
    createMetaDirectory: {
        options: {
            url: 'https://<%= environment.webdav.server %><%= dw_properties.webDav.meta_import_root %>',
            form: {
                method: 'MKCOL'
            },
            auth: {
                user: '<%= environment.webdav.username %>',
                pass: '<%= environment.webdav.password %>'
            },
            ignoreErrors: true
        }
    },
    unzipMeta: {
        options: {
            url: 'https://<%= environment.webdav.server %><%= dw_properties.webDav.meta_import_root %><%= settings["meta.archive.name"] %>.zip',
            form: {
                method: 'UNZIP'
            },
            auth: {
                user: '<%= environment.webdav.username %>',
                pass: '<%= environment.webdav.password %>'
            }
        }
    },
    login: {
        options: {
            url: 'https://<%= environment.webdav.server %>/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin',
            form: {
                LoginForm_Login: '<%= environment.webdav.username %>',
                LoginForm_Password: '<%= environment.webdav.password %>',
                LoginForm_RegistrationDomain: 'Sites'
            },
            jar: true,
            followRedirect: true,
            ignoreErrors: true
        }
    },
    deleteCodeVersion: {
        options: {
            url: 'https://<%= environment.webdav.server %><%= dw_properties.webDav.cartridge_root %><%= versionInfo.versionName %>',
            ignoreErrors: true,
            form: {
                method: 'DELETE'
            },
            auth: {
                user: '<%= environment.webdav.username %>',
                pass: '<%= environment.webdav.password %>'
            }
        }
    },
    createCodeVersion: {
        options: {
            url: 'https://<%= environment.webdav.server %><%= dw_properties.webDav.cartridge_root %><%= versionInfo.versionName %>',
            form: {
                method: 'MKCOL'
            },
            auth: {
                user: '<%= environment.webdav.username %>',
                pass: '<%= environment.webdav.password %>'
            }
        }
    },
    activateCodeVersion: {
        options: {
            url: 'https://<%= environment.webdav.server %>/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate',
            form: {
                CodeVersionID: '<%= versionInfo.versionName %>'
            },
            jar: true
        }
    },
    importContent: {
        // Import initial content package. Should be done after a dbinit.
        // Status of import can be checked with dw_bm_checkprogress:content.
        options: {
            url: 'https://<%= environment.webdav.server %>/on/demandware.store/Sites-Site/default/ViewSiteImpex-Dispatch',
            form: {
                ImportFileName: '<%= settings["content.archive.name"] %>.zip',
                import: 'OK',
                realmUse: 'true'
            },
            jar: true
        }
    },
    validateCustomMeta: {
        options: {
            url: 'https://<%= environment.webdav.server %>/on/demandware.store/Sites-Site/default/ViewCustomizationImport-Dispatch',
            form: {
                SelectedFile: 'custom-objecttype-definitions.xml',
                ProcessPipelineName: 'ProcessObjectTypeImport',
                ProcessPipelineStartNode: 'Validate',
                JobDescription: 'Validate+systemmeta+data+definitions',
                JobName: 'ProcessObjectTypeImpex',
                validate: ''
            },
            jar: true
        }
    },
    validateSystemMeta: {
        options: {
            url: 'https://<%= environment.webdav.server %>/on/demandware.store/Sites-Site/default/ViewCustomizationImport-Dispatch',
            form: {
                SelectedFile: 'system-objecttype-extensions.xml',
                ProcessPipelineName: 'ProcessObjectTypeImport',
                ProcessPipelineStartNode: 'Validate',
                JobDescription: 'Validate+custommeta+data+definitions',
                JobName: 'ProcessObjectTypeImpex',
                validate: ''
            },
            jar: true
        }
    }
};
