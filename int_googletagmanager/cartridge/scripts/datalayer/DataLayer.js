/* API Includes */
var MessageDigest = require('dw/crypto/MessageDigest');
var Encoding = require('dw/crypto/Encoding');
var BasketMgr = require('dw/order/BasketMgr');
var PagingModel = require('dw/web/PagingModel');
var ContentMgr = require('dw/content/ContentMgr');
var ProductMgr = require('dw/catalog/ProductMgr');

exports.getGlobalData = function(params) {

    var dataLayer = {};

    /******************************TYPE OVERRIDES******************************/
    if (params.cgid.stringValue && params.type.stringValue === 'search') {
        dataLayer.pageType = 'category';
    } else if (params.type.stringValue === 'storefront') {
        dataLayer.pageType = 'home';
    } else if (params.type.stringValue === 'quickview') {
        dataLayer.pageType = 'product';
    } else if (params.type.stringValue === 'orderconfirmation') {
        dataLayer.pageType = 'checkout';
    } else {
        dataLayer.pageType = params.type.stringValue;
    }

    /******************************TITLE OVERRIDES******************************/
    if (params.cgid.stringValue && params.type.stringValue === 'search') {
        dataLayer.pageTitle = params.cgid.stringValue;
    } else if (params.type.stringValue === 'quickview' || params.type.stringValue === 'product') {
        dataLayer.pageTitle = ProductMgr.getProduct(params.pid.stringValue).getName();
    } else {
        dataLayer.pageTitle = params.title.stringValue;
    }

    // Concatenate {{pageType}}: {{pageTitle}}
    dataLayer.pageName = dataLayer.pageType + ': ' + dataLayer.pageTitle;

    // Demandware assigned page category
    /******************************CATEGORY OVERRIDES******************************/
    if (params.cgid.stringValue && params.type.stringValue === 'search') {
        dataLayer.pageCategory = params.cgid.stringValue;
    } else if (params.cid.stringValue && params.type.stringValue === 'content') {
        var contentObj = ContentMgr.getContent(params.cid.stringValue);
        dataLayer.pageCategory = contentObj.getClassificationFolder() ? contentObj.getClassificationFolder().getID() : '';
    } else if (params.type.stringValue === 'storefront') {
        dataLayer.pageCategory = 'home';
    } else if (params.type.stringValue === 'OrderHistory'){
    	dataLayer.pageCategory = 'MyAccount';
    } else {
        dataLayer.pageCategory = params.type.stringValue;
    }

    //Geo location
    var geoLocation = request.getGeolocation();
    dataLayer.customerCountry = geoLocation.getCountryCode();
    dataLayer.customerState = geoLocation.getRegionCode();

    //Customer information
    if (customer.isAuthenticated()) {
        var messageDigest = new MessageDigest(MessageDigest.DIGEST_MD5);
        dataLayer.customerEmail = customer.getProfile().getEmail();

        //MD5 hashed email address
        dataLayer.userID = Encoding.toBase64(messageDigest.digestBytes(Encoding.fromBase64(customer.getProfile().getEmail())));

        //DW customer ID
        dataLayer.dwID = customer.profile.getCustomerNo();
        dataLayer.visitorLoginState = 'registered';
    } else {
        dataLayer.customerEmail = '';
        dataLayer.userID = '';
        dataLayer.dwID = '';
        dataLayer.visitorLoginState = 'guest';
    }

    //Session ID
    dataLayer.sessionId = session.getSessionID();

    //Property overrides to exclude from cache
    dataLayer.productsCartedNumber = getCartedNumber();

    return dataLayer;
}

exports.getPageData = function(args, pageContext, checkoutstep) {

    var pageData = {},
        currentDate, category, product, PLI, SKU, masterProduct, PVM, basket, productObj, order, productTotal, optionLineItems, x;

    //My Account Signup Start - Fire on page load of my account sign up page
    if (pageContext.type === 'MyAccount') {
        if (session.getClickStream().getLast().getPipelineName() == 'Account-StartRegister' && !customer.isAuthenticated()) {
            pageData = {
                'event': 'myAccountStart', // case sensitive - use exact value
                'eventCategory': pageContext.type,
                'eventAction': 'Sign Up',
                'eventLabel': 'Sign Up Start'
            };
        }
    } else if (pageContext.type === 'search') { //On page load - Fire on page load of category grid page first for page tracking

        var PSM = args.ProductSearchResult;
        var PM = new PagingModel(PSM.productSearchHits, PSM.count);

        if (request.httpParameterMap.start.submitted) {
            PM.setStart(request.httpParameterMap.start.intValue);
        }

        if (request.httpParameterMap.sz.submitted && request.httpParameterMap.sz.intValue <= 60) {
            PM.setPageSize(request.httpParameterMap.sz.intValue);
        } else {
            PM.setPageSize(12);
        }

        var productSearchHits = PM.getPageElements();
        var productSearchHit, impressions = [],
            i = 0;

        var list;
        if (PSM.isCategorySearch()) {
            category = PSM.getCategory();
            list = getList(category);
            //Reset category
            category = PSM.getCategory();
            pageData.productSKUFirst3 = [];
        } else {
            list = 'search result';
            pageData.searchTerm = PSM.getSearchPhrase();
            pageData.searchResults = PSM.getCount();
        }
        while (productSearchHits.hasNext()) {

            productSearchHit = productSearchHits.next();
            product = productSearchHit.getProduct();
            var productPrice = new Number();

            if (PSM.isCategorySearch() && pageData.productSKUFirst3.length < 3) {
                pageData.productSKUFirst3.push(productSearchHit.getProductID());
            }
            if(product.isProductSet()){
            	var pdtSet = product.productSetProducts;
            	var pdtSetPdt;
            	var pdtSetPdtPrice;
            	
            	for (var j = 0; j < pdtSet.length; j++) {
            		pdtSetPdt = pdtSet[j];
            		pdtSetPdtPrice = pdtSetPdt.priceModel.minPrice.decimalValue;
            		productPrice = productPrice + pdtSetPdtPrice;
            	}
            }else{
            	productPrice = productSearchHit.getMinPrice().getValueOrNull();
            }

            impressions.push({
                'name': product.getName(),
                'id': product.isVariant() ? product.getVariationModel().getMaster().getID() : productSearchHit.getProductID(),
                'price': productPrice,
                'category': product.getPrimaryCategory() ? product.getPrimaryCategory().getID() : '',
                'list': list, // product list value must persist to purchase
                'position': ++i
            });
        }

        pageData.productsCartedNumber = getCartedNumber();

        pageData.category = (category && category.getParent()) ? category.getParent().getID() : '';
        pageData.subcategory = category ? category.getID() : '';
        pageData.ecommerce = {};
        pageData.ecommerce.currencyCode = session.getCurrency().getCurrencyCode();
        pageData.ecommerce.impressions = impressions;

    } else if (pageContext.type === 'product' || pageContext.type === 'quickview') {

        product = args.Product;
        var primaryCategory = product.isVariant() ? product.getVariationModel().getMaster().getPrimaryCategory() : product.getPrimaryCategory();
        var mainCategory = primaryCategory ? primaryCategory.getParent() : '';

        pageData.productPageType = 'PDP';

        if (pageContext.type === 'quickview') {
            pageData.event = 'quickview';
            pageData.productPageType = 'quickview';
        }


        pageData.productCategory = mainCategory ? mainCategory.getID() : '';
        pageData.productSubcategory = primaryCategory ? primaryCategory.getID() : '';
        pageData.productSKU = product.getID();
        var productSU = getProductSkuId(product);
        pageData.ProductSkuId = productSU;
        pageData.productsCartedNumber = getCartedNumber();
        pageData.ecommerce = {};
        pageData.ecommerce.currencyCode = session.getCurrency().getCurrencyCode();
        pageData.ecommerce.detail = {};
        pageData.ecommerce.detail.actionField = {};
        pageData.ecommerce.detail.actionField.list = ''; // persisted value from productClick event
        
        // Handling product price
        var productPrice = new Number();
        if(product.isProductSet()){
        	var pdtSet = product.productSetProducts;
        	var pdtSetPdt;
        	var pdtSetPdtPrice;
        	
        	for (var j = 0; j < pdtSet.length; j++) {
        		pdtSetPdt = pdtSet[j];
        		pdtSetPdtPrice = pdtSetPdt.priceModel.minPrice.decimalValue;
        		productPrice = productPrice + pdtSetPdtPrice;
        	}
        }else{
        	productPrice = product.getPriceModel().getMinPrice().getValueOrNull();
        }

        pageData.ecommerce.detail.products = [{
            'name': product.getName(),
            'id': product.getID(),
            'price': productPrice,
            'category': primaryCategory ? primaryCategory.getID() : '',
            'dimension7': '',
            'dimension8': ''
        }];

        pageData.ecommerce.detail.impressions = [];

        // get all orderable cross sell recommendations (1 = cross sell)
        var recommendations = product.getOrderableRecommendations(1).iterator();

        // display 20 recommendations at maximum
        var maxRecs = 20,
            counter = 0;

        while (recommendations.hasNext()) {
            var recommendation = recommendations.next();
            var recommendedProduct = recommendation.getRecommendedItem();
            if (!empty(recommendedProduct) && recommendedProduct != null) {
            	pageData.ecommerce.detail.impressions.push({
                    'name': recommendedProduct.getName(),
                    'id': recommendedProduct.isVariant() ? ((!empty(recommendedProduct.getVariationModel().getMaster()) && recommendedProduct.getVariationModel().getMaster() != null) ? recommendedProduct.getVariationModel().getMaster().getID() : 'Master NA') : recommendedProduct.getID(),
                    'price': recommendedProduct.getPriceModel().getMinPrice().getValueOrNull(),
                    'category': product.isVariant() ? ((!empty(product.getVariationModel().getMaster().getPrimaryCategory()) && product.getVariationModel().getMaster().getPrimaryCategory() != null) ? product.getVariationModel().getMaster().getPrimaryCategory().getID() : 'Primary Category NA') : ((!empty(product.getPrimaryCategory()) && product.getPrimaryCategory() != null) ? product.getPrimaryCategory().getID() : ''),
                    'list': 'PDP you might also like',
                    'position': (counter + 1)
                });
            }

            if (++counter >= maxRecs) {
                break;
            }
        }

    } else if (pageContext.type === 'Cart' && checkoutstep === '') {
        pageData.event = 'viewCart';

        pageData.cartedProductID = [];
        pageData.cartedSKU = [];
        pageData.cartedQuantity = [];
        pageData.cartedProductName = [];
        pageData.cartedProductPrice = [];
        pageData.cartedProductCategory = [];
        pageData.cartedProductColor = [];
        pageData.cartedProductSize = [];
        pageData.cartedProductRating = [];
        pageData.cartedProductReviews = [];
        pageData.cartedVariationID = [];

        basket = BasketMgr.getCurrentBasket();
        if (basket) {
            PLIs = basket.getProductLineItems().iterator();
            while (PLIs.hasNext()) {
                PLI = PLIs.next();
                SKU = PLI.getProduct();
                if (SKU.isVariant()) {
                    masterProduct = SKU.getVariationModel().getMaster()
                    pageData.cartedProductID.push(masterProduct.getID());
                    pageData.cartedVariationID.push(SKU.getID());

                    PVM = masterProduct.variationModel;
                    colorAttr = PVM.getProductVariationAttribute('color');
                    sizeAttr = PVM.getProductVariationAttribute('size');

                    if (colorAttr) {
                        selectedColor = SKU.getVariationModel().getSelectedValue(colorAttr);
                        pageData.cartedProductColor.push(selectedColor.getDisplayValue());
                    } else {
                        pageData.cartedProductColor.push('');
                    }

                    if (sizeAttr) {
                        selectedSize = SKU.getVariationModel().getSelectedValue(sizeAttr);
                        pageData.cartedProductSize.push(selectedSize.getDisplayValue());
                    } else {
                        pageData.cartedProductSize.push('');
                    }

                } else {
                    pageData.cartedProductID.push(PLI.getProductID());
                    pageData.cartedVariationID.push('');
                    pageData.cartedProductColor.push('');
                    pageData.cartedProductSize.push('');
                }

                pageData.cartedSKU.push(SKU.getID());
                pageData.cartedQuantity.push(PLI.getQuantityValue());
                pageData.cartedProductName.push(SKU.getName());
                productTotal = PLI.getAdjustedPrice();
                optionLineItems = PLI.getOptionProductLineItems();
                for (x = 0; x < optionLineItems.getLength(); x++) {
                    productTotal = productTotal.add(optionLineItems[x].getAdjustedPrice());
                }
                pageData.cartedProductPrice.push(productTotal.getValue());

                if (masterProduct) {
                    pageData.cartedProductCategory.push(masterProduct.getPrimaryCategory().getID());
                } else if (SKU.getPrimaryCategory()) {
                    pageData.cartedProductCategory.push(SKU.getPrimaryCategory().getID());
                }
            }

            pageData.ecommerce = {};
            pageData.ecommerce.currencyCode = session.getCurrency().getCurrencyCode();
            pageData.ecommerce.impressions = impressions;
            pageData.basketUUID = basket.getUUID(); 
        }
    } else if (pageContext.type === 'checkout' || checkoutstep !== '') {
        checkoutstep = new Number(checkoutstep);
        pageData.ecommerce = {};
        pageData.ecommerce.currencyCode = session.getCurrency().getCurrencyCode();
        pageData.ecommerce.checkout = {};
        pageData.ecommerce.checkout.actionField = {};
        pageData.ecommerce.checkout.actionField.step = checkoutstep < 3 ? checkoutstep : (checkoutstep - 1);
        pageData.ecommerce.checkout.products = [];

        productObj = {};
        basket = BasketMgr.getCurrentBasket();
        if (basket) {
            var PLIs = basket.getProductLineItems().iterator();
            var colorAttr, sizeAttr, selectedColor, selectedSize;
            while (PLIs.hasNext()) {
                PLI = PLIs.next();
                SKU = PLI.getProduct();
                productObj = {};
                if (SKU.isVariant()) {
                    masterProduct = SKU.getVariationModel().getMaster()
                    productObj.id = masterProduct.getID();
                    productObj.dimension9 = SKU.getID();

                    PVM = masterProduct.variationModel;
                    colorAttr = PVM.getProductVariationAttribute('color');
                    sizeAttr = PVM.getProductVariationAttribute('size');

                    if (colorAttr) {
                        selectedColor = SKU.getVariationModel().getSelectedValue(colorAttr);
                        productObj.dimension5 = selectedColor.getDisplayValue();
                    } else {
                        productObj.dimension5 = '';
                    }

                    if (sizeAttr) {
                        selectedSize = SKU.getVariationModel().getSelectedValue(sizeAttr);
                        productObj.dimension6 = selectedSize.getDisplayValue();
                    } else {
                        productObj.dimension6 = '';
                    }

                } else {
                    productObj.id = SKU.getID();
                    productObj.dimension9 = '';
                    productObj.dimension5 = '';
                    productObj.dimension6 = '';
                }

                productObj.dimension7 = '';
                productObj.dimension8 = '';
                
                productObj.quantity = PLI.getQuantityValue();
                productObj.name = SKU.getName();
                productTotal = PLI.getAdjustedPrice();
                optionLineItems = PLI.getOptionProductLineItems();
                for (x = 0; x < optionLineItems.getLength(); x++) {
                    productTotal = productTotal.add(optionLineItems[x].getAdjustedPrice());
                }
                productObj.price = productTotal.getValue() / PLI.getQuantityValue();

                if (masterProduct && masterProduct.getPrimaryCategory()) {
                    productObj.category = masterProduct.getPrimaryCategory().getID();
                } else if (SKU.getPrimaryCategory()) {
                    productObj.category = SKU.getPrimaryCategory().getID();
                }

                pageData.ecommerce.checkout.products.push(productObj);
            }
        }

    } else if (pageContext.type === 'orderconfirmation') {
    	checkoutstep = new Number(3);
        order = args.Order;
        pageData.event = 'purchase';

        //Prepare shipment data
        pageData.shippingSpeed = '';
        pageData.shippingMethod = '';
        pageData.giftOrder = '';
        pageData.shippingZipCode = '';
        var shipment,shippingMethod,shippingPostal;
        var shipments = order.getShipments();
        for (x = 0; x < shipments.length; x++) {
            shipment = shipments[x];
            shippingMethod= shipment.getShippingMethod() ? shipment.getShippingMethod().getDisplayName() : '';
            shippingPostal= shipment.getShippingAddress().getPostalCode() ? shipment.getShippingAddress().getPostalCode() : '';
            pageData.shippingSpeed = pageData.shippingSpeed + '' + shippingMethod + '|';
            pageData.shippingMethod = pageData.shippingMethod + '' + (shipment.custom.fromStoreId ? 'ship to store' : 'buy online') + '|';
            pageData.giftOrder = pageData.giftOrder + '' + (shipment.isGift() ? 'yes' : 'no') + '|';
            pageData.shippingZipCode = pageData.shippingZipCode + '' + shippingPostal + '|';
        }
        pageData.shippingSpeed = pageData.shippingSpeed.substr(0, pageData.shippingSpeed.length - 1);
        pageData.shippingMethod = pageData.shippingMethod.substr(0, pageData.shippingMethod.length - 1);
        pageData.giftOrder = pageData.giftOrder.substr(0, pageData.giftOrder.length - 1);
        pageData.shippingZipCode = pageData.shippingZipCode.substr(0, pageData.shippingZipCode.length - 1);

        //Prepare payment data.
        pageData.paymentMethod = '';
        pageData.paymentType = '';
        var PI;
        var PIs = order.getPaymentInstruments();
        for (var j = 0; j < PIs.length; j++) {
            PI = PIs[j];
            pageData.paymentMethod = pageData.paymentMethod + '' + PI.getPaymentMethod() + '|';
            pageData.paymentType = pageData.paymentType + '' + PI.getCreditCardType() + '|';
        }
        pageData.paymentMethod = pageData.paymentMethod.substr(0, pageData.paymentMethod.length - 1);
        pageData.paymentType = pageData.paymentType.substr(0, pageData.paymentType.length - 1);

        pageData.billingZipCode = order.getBillingAddress().getPostalCode();
        pageData.ecommerce = {};
        pageData.ecommerce.purchase = {};
        pageData.ecommerce.purchase.actionField = {};
        pageData.ecommerce.purchase.actionField.id = order.getOrderNo();
        pageData.ecommerce.purchase.actionField.revenue = order.getAdjustedMerchandizeTotalPrice(true).add(order.giftCertificateTotalPrice).getValueOrNull();
        pageData.ecommerce.purchase.actionField.tax = order.getTotalTax().getValueOrNull();
        pageData.ecommerce.purchase.actionField.shipping = order.getAdjustedShippingTotalPrice().getValueOrNull();
        pageData.ecommerce.purchase.actionField.step = checkoutstep < 4 ? checkoutstep : (checkoutstep - 1);

        //Prepare coupon data
        pageData.ecommerce.purchase.actionField.coupon = '';
        var CL;
        var CLs = order.getCouponLineItems();
        for (var k = 0; k < CLs.length; k++) {
            CL = CLs[k];
            pageData.ecommerce.purchase.actionField.coupon = pageData.ecommerce.purchase.actionField.coupon + '' + CL.getCouponCode() + '|';
        }

        pageData.ecommerce.purchase.actionField.coupon = pageData.ecommerce.purchase.actionField.coupon.substr(0, pageData.ecommerce.purchase.actionField.coupon.length - 1);

        pageData.ecommerce.purchase.products = [];

        PLIs = order.getProductLineItems().iterator();
        while (PLIs.hasNext()) {
            PLI = PLIs.next();
            SKU = PLI.getProduct();
            productObj = {};
            if (SKU.isVariant()) {
                masterProduct = SKU.getVariationModel().getMaster()
                productObj.id = masterProduct.getID();
                productObj.dimension9 = SKU.getID();

                PVM = masterProduct.variationModel;
                colorAttr = PVM.getProductVariationAttribute('color');
                sizeAttr = PVM.getProductVariationAttribute('size');

                if (colorAttr) {
                    selectedColor = SKU.getVariationModel().getSelectedValue(colorAttr);
                    productObj.dimension5 = selectedColor.getDisplayValue();
                } else {
                    productObj.dimension5 = '';
                }

                if (sizeAttr) {
                    selectedSize = SKU.getVariationModel().getSelectedValue(sizeAttr);
                    productObj.dimension6 = selectedSize.getDisplayValue();
                } else {
                    productObj.dimension6 = '';
                }

            } else {
                productObj.id = SKU.getID();
                productObj.dimension9 = '';
                productObj.dimension5 = '';
                productObj.dimension6 = '';
            }

            productObj.quantity = PLI.getQuantityValue();
            productObj.name = SKU.getName();
            productTotal = PLI.getAdjustedPrice();
            optionLineItems = PLI.getOptionProductLineItems();
            for (x = 0; x < optionLineItems.getLength(); x++) {
                productTotal = productTotal.add(optionLineItems[x].getAdjustedPrice());
            }
            productObj.price = productTotal.getValue() /PLI.getQuantityValue();

            if (masterProduct && masterProduct.getPrimaryCategory()) {
                productObj.category = masterProduct.getPrimaryCategory().getID();
            } else if (SKU.getPrimaryCategory()) {
                productObj.category = SKU.getPrimaryCategory().getID();
            }

            pageData.ecommerce.purchase.products.push(productObj);
        }
    }

    //My Account Signup Complete - Fire on successful account creation
    if (customer.isAuthenticated()) {
        var customerCreationDate = customer.getProfile().getCreationDate();
        currentDate = new Date();

        // @FIXME : Replace with better solution.
        if ((currentDate.getTime() - customerCreationDate.getTime()) < 3000) {
            pageData.event = 'myAccountComplete'; // case sensitive - use exact value
            pageData.eventCategory = pageContext.type;
            pageData.eventAction = 'Sign Up';
            pageData.eventLabel = 'Sign Up Complete';
            return pageData;
        }
    }

    //My Account Sign In - Fire on successful signin to account
    if (customer.isAuthenticated()) {
        var lastLoginTime = customer.getProfile().getLastLoginTime();
        currentDate = new Date();

        // @FIXME : Replace with better solution.
        if ((currentDate.getTime() - lastLoginTime.getTime()) < 3000) {
            pageData.event = 'myAccountSignIn'; // case sensitive - use exact value
            pageData.eventCategory = pageContext.type;
            pageData.eventAction = 'Sign In';
            pageData.eventLabel = customer.getProfile().getCustomerNo(); // DW Customer ID
            return pageData;
        }
    }

    return pageData;
}

//This functions returns slot information required for analytics(ex:GTM) in JSON Object format.
exports.slotJSON = function(slotcontent) {
    var slotJSON = slotcontent.custom.slotJSON;
    var slotJSONArray = [];
    if (!slotJSON) {
        return JSON.stringify(slotJSONArray);
    }
    var slot = {};
    var slotArray;
    for (var slotIndex = 0; slotIndex < slotJSON.length; slotIndex++) {
        slot = {};
        slotArray = slotJSON[slotIndex].split('|');
        slot.name = slotArray[0].replace(/'/g, '');
        slot.creative = slotArray[1].replace(/'/g, '');
        slot.position = slotArray[2].replace(/'/g, '');
        slotJSONArray.push(slot);
    }
    return JSON.stringify(slotJSONArray);

}

//Get number of items in Cart
var getCartedNumber = function() {
    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        return 0;
    }
    return currentBasket.getProductLineItems().size();
}

//Get list
var getList = function(category) {
    var list;
    if (!category) {
        list = 'search result';
    } else {
        while (category && category.getID() !== 'root') {
            if (list) {
                list = category.getID() + ': ' + list;
            } else {
                list = category.getID();
            }
            category = category.getParent();
        }
    }

    return list;
}

var getProductSkuId = function(product) {
	var productSU, mastercounter = 1, vgcounter = 1;
	if (product.master) {
		var variantitem, firstvar;
		var variantIter = product.variationModel.variants.iterator();
		while (variantIter.hasNext()) {
			variantitem = variantIter.next();
			if (variantitem.availabilityModel.inStock) {
				productSU = variantitem.getID();
				break;
			} 
			mastercounter++;
		}
		if (product.variationModel.variants.length < mastercounter) {
			productSU=product.variationModel.variants[0].getID();
		}
		
	} else if (product.variationGroup) {
		var variantitem;
		var variantIter = product.variants.iterator();
		while (variantIter.hasNext()) {
			variantitem = variantIter.next();
			if (variantitem.availabilityModel.inStock) {
				productSU = variantitem.getID();
				break;
			}
			vgcounter++; 
		}
		if (product.variants.length < vgcounter) {
			productSU=product.variants[0].getID();
		}
		
	} else if (product.isVariant()) {
			productSU = product.getID();
	}
	return productSU;
}

