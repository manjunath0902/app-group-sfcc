/* Script Modules */
var app = require('app_appgroup_controllers/cartridge/scripts/app');
var guard = require('app_appgroup_controllers/cartridge/scripts/guard');

function start(){
	if (dw.system.Site.current.preferences.custom.enableMobify == true && dw.system.Site.current.preferences.custom.mobify_tag !== ''){
		response.setHttpHeader("Content-Type", "application/javascript");
	    response.writer.print(dw.system.Site.current.preferences.custom.mobifyServiceWorker);
	}
}

function getBasketUUID() {
    var cart = app.getModel('Cart').goc();
	var JSONResponse = {};
	JSONResponse.UUID = cart.getUUID();
	
    app.getView('Cart', {
        cart: cart,
        JSONResponse: JSONResponse 
    }).render('util/responsejson');
}

/*
 * Exposed methods.
 */
/** Gets cart UUID. **/
exports.GetBasketUUID = getBasketUUID;
exports.GetBasketUUID.public = true;

exports.Start = start;
exports.Start.public = true;