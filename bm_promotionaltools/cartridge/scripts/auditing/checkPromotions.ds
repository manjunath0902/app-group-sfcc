/**
* Digital Server Script File
* To define input and output parameters, create entries of the form:
*
 For example:
*   @input JobParameters : dw.util.HashMap
*-   @output activePromoIDs : String
*
*/
importPackage( dw.system );
importPackage( dw.campaign );
importPackage( dw.net );
importPackage( dw.util );

function execute( args : PipelineDictionary ) : Number
{

	try {
		//Pull active promotions (Returns all promotions scheduled for now) and upcoming promotions (Returns all promotions currently inactive, but scheduled for any time between now and now + hours)
		var daysUpcoming = (!empty(args.JobParameters.DaysUpcoming)) ? Number(args.JobParameters.DaysUpcoming) : '30';
		var getUpComingPromotions = PromotionMgr.getUpcomingPromotions(daysUpcoming*24).promotions;	 
		var getActivePromotions = PromotionMgr.getActivePromotions().promotions;
		//Convert the collections into iterators.
		var activePromotions = getActivePromotions.iterator();
		var upcomingPromotions = getUpComingPromotions.iterator();
		
		var recentlyAddedActivePromos : ArrayList = new ArrayList();
		var recentlyAddedUpcomingPromos : ArrayList = new ArrayList();
		
		var hoursSinceModified = (!empty(args.JobParameters.HoursSinceModified)) ? args.JobParameters.HoursSinceModified : '24';
		var hoursSinceModifiedMS = 3600000 * Number(args.JobParameters.HoursSinceModified);
		
		//Loop through the active promotions
		while (activePromotions.hasNext()) {
			
			var activePromo = activePromotions.next();
			var isEnabled = activePromo.isEnabled();
			var today : dw.util.Calendar = new dw.util.Calendar();
			
			//Get lastModified date because it will be the same as creation date OR newer if modified since creation.
			var lastModified : dw.util.Calendar = new dw.util.Calendar(activePromo.getLastModified());
			var timeDifferemceMS = Date.now()-Date.parse(lastModified.time);
			
			//Check if our millisecond time difference is under X hours in milliseconds - hours to check on pulled from job parameter
			if (isEnabled && timeDifferemceMS <= hoursSinceModifiedMS) {			

					recentlyAddedActivePromos.add(activePromo);
					
	//				Logger.info("\nStart Date: "+ startDate + "\nPromo ID: " + ID + "\nPromo Name: "+ name + "\nCallout Message: "+ calloutMsg + "\nDetails: " + details + "\nClass: "+ promoClass + "\nCampaign: "+campaign.ID + "\n");
			
			}
			
		}
		
		//Loop through the upcoming promotions just like we did for active promotions.
		while (upcomingPromotions.hasNext()) {
			
			var upcomingPromo = upcomingPromotions.next();
			var isEnabled = upcomingPromo.isEnabled();
			var today : dw.util.Calendar = new dw.util.Calendar();
			
			var lastModified : dw.util.Calendar = new dw.util.Calendar(upcomingPromo.getLastModified());
			var timeDifferemceMS = Date.now()-Date.parse(lastModified.time);
						
			if (isEnabled && timeDifferemceMS <= hoursSinceModifiedMS) {
				
				recentlyAddedUpcomingPromos.add(upcomingPromo);
					   
//				Logger.info("\nStart Date: "+ startDate + "\nPromo ID: " + ID + "\nPromo Name: "+ name + "\nCallout Message: "+ calloutMsg + "\nDetails: " + details + "\nClass: "+ promoClass + "\nCampaign: "+campaign.ID + "\n");
					
			}
			
		}
		
		var o: Map = new HashMap();
		o.put("activePromos",recentlyAddedActivePromos);
		o.put("upcomingPromos",recentlyAddedUpcomingPromos);
		o.put("hoursSinceModified", hoursSinceModified);
	
		sendMail(o,args.JobParameters);
	
	} catch(e) {
		var x = e;
		var errorMsg = x.message;	
		Logger.error("checkPromotions.ds - Promotion Audit Job Error: " + errorMsg);
		
		return PIPELET_ERROR;
		
	}	

    return PIPELET_NEXT;
}

function sendMail(o,params) {
	var template: Template = new Template("/audit/promoAuditEmail.isml");
	var emailTo = params.EmailTo;
	var emailFrom = params.EmailFrom;
	var emailSubject = params. EmailSubject;
	var content : dw.value.MimeEncodedText = template.render(o);
	
	var mail : Mail = new Mail();
	mail.addTo(emailTo);
	mail.setFrom(emailFrom);
	mail.setSubject(emailSubject);
	mail.setContent(content);
	
	mail.send();//returns either Status.ERROR or Status.OK, mail might not be sent yet, when this method returns
}