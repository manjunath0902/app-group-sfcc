/**
* 
* bvImportRatingFeed.ds
*
* process feed for the BazaarVoice. The feed will be FTPed to the BV FTP server
* 
* Product Rating data feed from BV 
* 
*<?xml version="1.0" encoding="UTF-8"?>
*	<Feed xmlns="http://www.bazaarvoice.com/xs/PRR/SyndicationFeed/5.6" name="ClientName" extractDate="2008-06-21T05:41:54.477-05:00">
*	<Product id="01">
*		<Source>ClientName</Source>
*		<ExternalId>01</ExternalId>
*		<Name>Product 1</Name>
*		<Description>Product description from product feed.</Description>
*		<Brand>Brand</Brand>
*		<NumQuestions>0</NumQuestions>
*		<NumAnswers>0</NumAnswers>
*		<CategoryItems>
*			<CategoryItem id="001">
*				<ExternalId>001</ExternalId>
*				<CategoryId>206470</CategoryId>
*				<CategoryName>Category 1</CategoryName>
*			</CategoryItem>
*		</CategoryItems>
*		<ProductPageUrl>http://www.client.com/dynamic/product.cfm?productID=2</ProductPageUrl>
*		<ProductReviewsUrl>http://reviews.client.com/0000/2/reviews.htm</ProductReviewsUrl>
*		<ImageUrl>http://images.client.com/images.jpg</ImageUrl>
*		<ReviewStatistics>
*			<AverageOverallRating>4.84</AverageOverallRating>
*			<OverallRatingRange>5</OverallRatingRange>
*			<TotalReviewCount>25</TotalReviewCount>
*			<RatingsOnlyReviewCount>11</RatingsOnlyReviewCount>
*			<RecommendedCount>25</RecommendedCount>
*			<AverageRatingValues>
*				<AverageRatingValue id="AverageFit">
*					<AverageRating>4.72</AverageRating>
*					<RatingDimension id="AverageFit" displayType="SLIDER">
*						<ExternalId>AverageFit</ExternalId>
*						<RatingRange>7</RatingRange>
*						<Label1>Small</Label1>
*						<Label1>Large</Label1>
*					</RatingDimension>
*				</AverageRatingValue>
*			</AverageRatingValues>
*			<RatingDistribution>
*				<RatingDistributionItem>
*					<RatingValue>4</RatingValue>
*					<Count>4</Count>
*				</RatingDistributionItem>
*				<RatingDistributionItem>
*					<RatingValue>5</RatingValue>
*					<Count>21</Count>
*				</RatingDistributionItem>
*			</RatingDistribution>
*		</ReviewStatistics>
*	</Product>
*	<Product id="02">				
*	.....
*	</product>
*	....
*</Feed>	
*  
* @input TempFile : dw.io.File
*
* @output Message : String
*/
 
var FileReader = require('dw/io/FileReader');
var ProductMgr = require('dw/catalog/ProductMgr');
var StringUtils = require('dw/util/StringUtils');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var Logger = require('dw/system/Logger').getLogger('Bazaarvoice', 'bvImportRatingFeed.ds');

var BV_Constants = require('int_bazaarvoice/cartridge/scripts/lib/libConstants').getConstants();
var BVHelper = require('int_bazaarvoice/cartridge/scripts/lib/libBazaarvoice').getBazaarVoiceHelper();

function execute(pdict) {
    var host = '';
    var user = '';
    var pwd = '';
    var fpath = '';
    var fname = '';
    
    try {
    	var tempFile = pdict.TempFile;
        
        var productIds = [];
        if(tempFile != null && tempFile.exists()){       
		    var xmlReader = new XMLStreamReader(new FileReader(tempFile, 'UTF-8'));
		    
		    //need to zero out any existing ratings
		    var products = ProductMgr.queryAllSiteProducts();
			while(products.hasNext()){
				var product = products.next();
				if(!empty(product.custom.bvAverageRating) || !empty(product.custom.bvReviewCount) || !empty(product.custom.bvRatingRange)){
					product.custom.bvAverageRating = '0.0';
					product.custom.bvReviewCount = '0';
					product.custom.bvRatingRange = '0';
				}
			}
			products.close();
		    
		    var productXML = null;
		    var iProcessed = 0;
		    var iFailed = 0;
		    var id = '';
		    var bvAverageRating = '';
		    var bvReviewCount = '';
		    var bvRatingRange = '';
		    
		    while(xmlReader.hasNext()) {
				xmlReader.next();
				if (xmlReader.getEventType() == XMLStreamConstants.START_ELEMENT && xmlReader.getLocalName() == 'Product') {
					productXML = xmlReader.readXMLObject();
					id = '';
					bvAverageRating = '';
					bvReviewCount = '';
					bvRatingRange = '';
					
					var ns = productXML.namespace();
					if(!empty(productXML.ns::ExternalId.toString())) {
						id = productXML.ns::ExternalId.toString();
						
						if (!empty(productXML.ns::ReviewStatistics.ns::AverageOverallRating.toString())) {
						    bvAverageRating = productXML.ns::ReviewStatistics.ns::AverageOverallRating.toString();
						}
						if (!empty(productXML.ns::ReviewStatistics.ns::TotalReviewCount.toString())) {
						    bvReviewCount = productXML.ns::ReviewStatistics.ns::TotalReviewCount.toString();
						}
						if (!empty(productXML.ns::ReviewStatistics.ns::OverallRatingRange.toString())) {
						    bvRatingRange = productXML.ns::ReviewStatistics.ns::OverallRatingRange.toString();
						}
						
						var product = ProductMgr.getProduct(BVHelper.decodeId(id));
						if (product != null) {
						    product.custom.bvAverageRating = bvAverageRating;
						    product.custom.bvReviewCount = bvReviewCount;
						    product.custom.bvRatingRange = bvRatingRange;
						    
						    productIds.push(product.ID);
						    iProcessed++;
						} else {
						    iFailed++;
						}
					}
				}
		    }
		
			xmlReader.close();
		
		    pdict.Message = StringUtils.format(BV_Constants.MESSAGE_TEMPLATE, 'SUCCESS', 'Import Processed - Connecton Details', host, user, pwd, fpath, fname);
		    pdict.Message += 'Total Record Updated: ' + + iProcessed.toString() + '\n';
		    pdict.Message += 'Total Record Failed: ' + iFailed.toString() + '\n';
        }else{
        	pdict.Message = StringUtils.format(BV_Constants.MESSAGE_TEMPLATE, 'INFO', 'No File to Import!', host, user, pwd, fpath, fname);
        }
    } catch (ex) {
		Logger.error('Exception caught: {0}', ex.message);
        pdict.Message = StringUtils.format(BV_Constants.MESSAGE_TEMPLATE, 'ERROR', 'Exception=' + ex.message, host, user, pwd, fpath, fname);
        return PIPELET_ERROR;
    }

    return PIPELET_NEXT;
}