/**
* Description of the Controller and the logic it provides
*
* @module  controllers/NETBANX_HOSTED_PAYMENT
*/

'use strict';

/* API Includes */
var Cart = require('~/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');
var Site = require('dw/system/Site');

/* Script Modules */
var app = require('~/cartridge/scripts/app');

/**
 * Integration of NETBANX Hosted Payment API
 */
function Handle(args) {
	    var cart = Cart.get(args.Basket);
	    var creditCardForm = app.getForm('billing.paymentMethods.creditCard');
	    var PaymentMgr = require('dw/order/PaymentMgr');

	    var cardNumber = creditCardForm.get('number').value();
	    var cardSecurityCode = creditCardForm.get('cvn').value();
	    var cardType = creditCardForm.get('type').value();
	    var expirationMonth = creditCardForm.get('expiration.month').value();
	    var expirationYear = creditCardForm.get('expiration.year').value();
	    var paymentCard = PaymentMgr.getPaymentCard(cardType);

	    var creditCardStatus = paymentCard.verify(expirationMonth, expirationYear, cardNumber, cardSecurityCode);

	    if (creditCardStatus.error) {

	        var invalidatePaymentCardFormElements = require('app_appgroup_core/cartridge/scripts/checkout/InvalidatePaymentCardFormElements');
	        invalidatePaymentCardFormElements.invalidatePaymentCardForm(creditCardStatus, session.forms.billing.paymentMethods.creditCard);

	        return {error: true};
	    }

	    Transaction.wrap(function () {
	    	
	    	cart.removeExistingPaymentInstruments(dw.order.PaymentInstrument.METHOD_CREDIT_CARD);
	        var paymentInstrument = cart.createPaymentInstrument(dw.order.PaymentInstrument.METHOD_CREDIT_CARD, cart.getNonGiftCertificateAmount());

	        paymentInstrument.creditCardHolder = creditCardForm.get('owner').value();
	        paymentInstrument.creditCardNumber = cardNumber;
	        paymentInstrument.creditCardType = cardType;
	        paymentInstrument.creditCardExpirationMonth = expirationMonth;
	        paymentInstrument.creditCardExpirationYear = expirationYear;
	    });

	    return {success: true};
}

/**
 * Authorizes a payment using a Netbanx service with redirection to Netbanx and back redirection to our site
 * 
 */
function Authorize(args) {
    var orderNo = args.OrderNo;
    var paymentInstrument = args.PaymentInstrument;
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
    var Order = OrderMgr.getOrder(orderNo);
    Transaction.wrap(function () {
        paymentInstrument.paymentTransaction.transactionID = orderNo;
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });
    	
 	let Pipeline = require('dw/system/Pipeline');
 	var action = 'Netbanx-CreateAuthOnlyTransaction';
 	var authwithtoken = ('NetbanxAuthType' in Site.getCurrent().preferences.getCustom() && !empty(Site.getCurrent().getCustomPreferenceValue('NetbanxAuthType'))) ? (Site.getCurrent().getCustomPreferenceValue('NetbanxAuthType').toString() == 'authwithtoken') : false;
 	if (authwithtoken && paymentInstrument.paymentMethod == paymentInstrument.METHOD_DW_APPLE_PAY) {
 		action = 'Netbanx-CreateAuthFromTokenTransaction';
 	} else if (authwithtoken) {
 		action = 'Netbanx-CreateAuthWithTokenTransaction';
 	} 
 	
    var pdict = Pipeline.execute(action, {Order : Order,orderNo : Order.getOrderNo()});
     if(pdict.EndNodeName == 'error') {
    	 
    	 var errorResult = {};
    	 
    	 errorResult.error = true;
    	 errorResult.response = pdict.NetbanxResponse;
    	 if (pdict.NetbanxResponse != undefined && !empty(pdict.NetbanxResponse)) {
    		 Transaction.wrap(function () {
        		 paymentInstrument.custom.creditCardAuthResponse = pdict.NetbanxResponse; 
        	 });
    	 }
    	 return errorResult;
     }else {
    	 
    	 if (pdict.NetbanxResponse != undefined && pdict.NetbanxResponse != null ){
    		
    		 Transaction.wrap(function () {
        		 if (authwithtoken && pdict.NetbanxResponse.response != undefined && pdict.NetbanxResponse.response != null && pdict.NetbanxResponse.response.text != undefined && pdict.NetbanxResponse.response.text != null) {
        			 var response = JSON.parse(pdict.NetbanxResponse.response.text);
            		 paymentInstrument.custom.paySafeAuthID = response.id;
            		 
            		 var response = JSON.parse(pdict.NetbanxResponse.response.text);
            		 response.profile = pdict.NetbanxResponse.profile;
            		 paymentInstrument.custom.creditCardAuthResponse = JSON.stringify(response);
        		 } else if (pdict.NetbanxResponse.text != undefined && pdict.NetbanxResponse.text != null){
        			 paymentInstrument.custom.creditCardAuthResponse = pdict.NetbanxResponse.text;
        			 paymentInstrument.custom.paySafeAuthID = JSON.parse(pdict.NetbanxResponse.text).id;
        		 }
        	 });
    		 
    	 }
    	 
    	 var success = {};
    	 success.authorized = true;
    	 if (authwithtoken) {
    		 success.response = pdict.NetbanxResponse.response.text;
    	 } else {
    		 success.response = pdict.NetbanxResponse.text;
    	 }
    	 return success;
     }
        
    
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;

