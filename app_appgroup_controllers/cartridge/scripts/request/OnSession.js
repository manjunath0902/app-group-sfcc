'use strict';

/**
 * The onSession hook is called for every new session in a site. This hook can be used for initializations,
 * like to prepare promotions or pricebooks based on source codes or affiliate information in
 * the initial URL. For performance reasons the hook function should be kept short.
 *
 * @module  request/OnSession
 */

var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Locale = require('dw/util/Locale');
var Currency = require('dw/util/Currency');
var Cookie = require('dw/web/Cookie');
var Url = require('app_appgroup_core/cartridge/scripts/util/Url');
var countries = require('app_appgroup_core/cartridge/countries');
if (Site.getCurrent().getCustomPreferenceValue('countrySelection') != undefined && Site.getCurrent().getCustomPreferenceValue('countrySelection') != null){
	countries = JSON.parse(Site.getCurrent().getCustomPreferenceValue('countrySelection'));
}

/* Script Modules */
var app = require('~/cartridge/scripts/app');

/**
 * Gets the device type of the current user.
 * @return {String} the device type (desktop, mobile or tablet)
 */
function getDeviceType() {
    var deviceType = 'desktop';
    var iPhoneDevice = 'iPhone';
    var iPadDevice = 'iPad';
    var androidDevice = 'Android'; //Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; ADR6300 Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1

    var httpUserAgent = request.httpUserAgent;

    if (!httpUserAgent) {
        return;
    }

    if (httpUserAgent.indexOf(iPhoneDevice) > -1) {
        deviceType = 'mobile';

    } else if (httpUserAgent.indexOf(androidDevice) > -1) {
        if (httpUserAgent.toLowerCase().indexOf('mobile') > -1) {
            deviceType = 'mobile';
        }
    } else if (httpUserAgent.indexOf(iPadDevice) > -1) {
        deviceType = 'tablet';
    }

    return deviceType;
}

function createSessionCookie(name, value, day){
	var cookie : Cookie = new Cookie(name, value);
	cookie.setMaxAge(day*24*60*60);
	cookie.setPath('/');
	response.addHttpCookie(cookie);
}

/**
 * The onSession hook function.
 */
exports.onSession = function () {
	session.custom.device = getDeviceType();
	
	// get session cookie
	var getSessionCookie,sessionCookieCount, newsessionCookieCount;
	var httpCookies = request.httpCookies;
	var siteID = Site.getCurrent().ID;
	var cookieAge = ((Site.getCurrent().getPreferences().getCustom()["appgrouppopupsetting"] != null) && (JSON.parse(Site.getCurrent().getPreferences().getCustom()["appgrouppopupsetting"]).DeferCookieAgeInDays != null))? JSON.parse(Site.getCurrent().getPreferences().getCustom()["appgrouppopupsetting"]).DeferCookieAgeInDays : 30;
	if(!empty(httpCookies) && !empty(httpCookies[siteID+'SessionCountCookie'])){
		getSessionCookie = httpCookies[siteID+'SessionCountCookie'];
		sessionCookieCount = getSessionCookie.getValue();
		sessionCookieCount = parseInt(sessionCookieCount);
		newsessionCookieCount = sessionCookieCount+1;
		// update the value of the cookie
		getSessionCookie.setMaxAge(0);
		response.addHttpCookie(getSessionCookie);
		createSessionCookie(siteID+'SessionCountCookie', newsessionCookieCount, parseInt(cookieAge));
	}else{
		// create session cookies and assign value
		createSessionCookie(siteID+'SessionCountCookie', 0, parseInt(cookieAge));
	}
	// Deleting the page count cookie if it is available
	if(!empty(httpCookies) && !empty(httpCookies[siteID+'newsletterpopupPageCount'])){
		createSessionCookie(siteID+'newsletterpopupPageCount', 0, parseInt(cookieAge));
	}

	//var test = dw.catalog.PriceBookMgr.getPriceBook("app-uk-sale-pricebook");
	var sitePrefs = dw.system.Site.getCurrent().getPreferences();
	var enableSiteLevelRestriction = ((sitePrefs.getCustom()["enablegeorestriction"] != null) && (JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).EnableSiteLevelRestriction != null))? JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).EnableSiteLevelRestriction : '';
    var custGroups = ((sitePrefs.getCustom()["enablegeorestriction"] != null) && (JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).RestrictedCustomerGroupIds != null))? JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).RestrictedCustomerGroupIds : '';
	var priceBookId = ((sitePrefs.getCustom()["enablegeorestriction"] != null) && (JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).PriceBook != null))? JSON.parse(sitePrefs.getCustom()["enablegeorestriction"]).PriceBook : '';
    var priceBook = dw.catalog.PriceBookMgr.getPriceBook(priceBookId);
	if(enableSiteLevelRestriction && customer.isMemberOfCustomerGroups(custGroups) && priceBook != ''){
    	dw.catalog.PriceBookMgr.setApplicablePriceBooks(priceBook);
    }
    return new Status(Status.OK);
};
