'use strict';

/**
 * The onRequest hook is called with every top-level request in a site. This happens both for requests to cached and non-cached pages.
 * For performance reasons the hook function should be kept short.
 *
 * @module  request/OnRequest
 */

var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var URLUtils = require('dw/web/URLUtils');
var Locale = require('dw/util/Locale');
var Currency = require('dw/util/Currency');
var ArrayList = require('dw/util/ArrayList');
var Url = require('app_appgroup_core/cartridge/scripts/util/Url');
var countries = require('app_appgroup_core/cartridge/countries');
var Resource = require('dw/web/Resource');
//getting the json from site pref
if (Site.getCurrent().getCustomPreferenceValue('countrySelection') != undefined && Site.getCurrent().getCustomPreferenceValue('countrySelection') != null){
	countries = JSON.parse(Site.getCurrent().getCustomPreferenceValue('countrySelection'));
}

var userRedirection = {};
userRedirection.isGeoLocationEnabled = false;
userRedirection.defaultLocale = Site.current.defaultLocale;
userRedirection.cookieMaxAge = 100000;
userRedirection.stateCode = "QC";

//browser preferred languages
var browserPreferedLanguages = JSON.parse(Resource.msg('revisioninfo.browserPreferedLanguages', 'revisioninfo', null));

var browserPreferedLocale = Site.getCurrent().getDefaultLocale();
if (!empty(request.httpHeaders['accept-language'])) {
	browserPreferedLocale = request.httpHeaders['accept-language'].split(',')[0];
}
var euroCountries = Resource.msg('revisioninfo.euroCountries', 'revisioninfo', null).split(',');
//getting the json from site pref
try{
	if (Site.getCurrent().getCustomPreferenceValue('userRedirection') != undefined && Site.getCurrent().getCustomPreferenceValue('userRedirection') != null){
		userRedirection = JSON.parse(Site.getCurrent().getCustomPreferenceValue('userRedirection'));
	}
} catch(e){
	//hardcode site pref values here
	userRedirection.isGeoLocationEnabled = true;
	userRedirection.defaultLocale = Site.current.defaultLocale;
	userRedirection.cookieMaxAge = 10000;
	userRedirection.stateCode = 'QC';
}


/* Script Modules */
var app = require('~/cartridge/scripts/app');

/**
 * The onRequest hook function.
 */
exports.onRequest = function () {
	
	//Ensuring the current request is not an AJAX & Riskified status updates call.
		var isNotAjaxRequest = true;
		if ((request.httpParameterMap.hasOwnProperty('format') && request.httpParameterMap.format != null && request.httpParameterMap.format.value == "ajax") || (request.httpParameterMap.hasOwnProperty('requestBodyAsString') && request.httpParameterMap.requestBodyAsString != null && !empty(request.httpParameterMap.requestBodyAsString.value)) ) {
			isNotAjaxRequest = false;
		}
		
		if (isNotAjaxRequest) {
			userRedirect();
		}
	
	
    return new Status(Status.OK);
};

//redirecting user based on cookie
function userRedirect(){
	
	var isGeoLocatorEnabled = false;
	
	isGeoLocatorEnabled = userRedirection.isGeoLocationEnabled;//to control the cookie logic
	
	if (isGeoLocatorEnabled) {
		
		var redirectObj = IsRedirect();
		
		if (redirectObj.isRedirect) {
			var locale = !empty(redirectObj.locale) ? redirectObj.locale : (userRedirection.defaultLocale  != null ? userRedirection.defaultLocale : Site.current.defaultLocale); //Read from site preference if exists, otherwise, set it to default locale of BM.
			var currentSiteID = Site.getCurrent().getID();
			
			setRedirectURL(redirectObj.locale, redirectObj.siteID);
			
		} else if (!(redirectObj.isRedirect) && redirectObj.isNewSite ) {
			setRedirectURL(request.locale, Site.getCurrent().getID().toString());
		}
	} else {
		
		deleteCookie('lastActionLocale');
		
	}	
}

//returns the JSON object to decide the redirection
function IsRedirect () {
	
	var lastActionLocale = request.locale;
	var currentSiteID = Site.getCurrent().getID().toString();
	
	var returnObj = {};
	
	var cookies = request.getHttpCookies();
	var	lastLocale = cookies['lastActionLocale'];
	
	var	isCtrySel = cookies['isCtrySel'];
	
	// Checks if user select the country/locale from country selection from Storefront 
	if (!empty(isCtrySel) && isCtrySel.value != null && isCtrySel.value) {
		
		createCookie(lastActionLocale,currentSiteID);
		deleteCookie('isCtrySel');
		returnObj.isRedirect = false;// As user selects the country from country selection dropdown
		
		return returnObj;
	}
	
	//check for service worker, exception to redirect rules
	var sitePrefs = dw.system.Site.getCurrent().getPreferences();
	var PreventGeoRedirect = sitePrefs.getCustom()["PreventGeoRedirect"];
	if(!empty(PreventGeoRedirect)) {
		for(var i =0;i<PreventGeoRedirect.length; i++) {
			if (request.getHttpURL().toString().indexOf(PreventGeoRedirect[i]) > -1) {
				returnObj.isRedirect = false;
				return returnObj;
			}
		}
	}
		
	if (!empty(lastLocale)) {
		
		var localeValueAndSite = lastLocale.value;
		var localeValue = localeValueAndSite.split('|')[0];
		var siteValue = localeValueAndSite.split('|')[1];
		//validates incoming request with current cookie value
		if ( localeValue == request.locale && siteValue == Site.getCurrent().getID() ) {
			
			returnObj.isRedirect = false;
			return returnObj;
			
		} else {
			
			try { 
				
				var localeValueAndSite = lastLocale.value;
				var locale = localeValueAndSite.split('|')[0];
				var cookieSiteID = localeValueAndSite.split('|')[1];
				var currentSiteID = Site.getCurrent().getID();
				var matchCookieID = cookieSiteID.split("_")[0];
				var matchSiteObj = currentSiteID.split("_");
				
				var matchSiteId;
				for (var i=0; i<matchSiteObj.length; i++) {
					matchSiteId = matchSiteObj[i];
					break;
				}
				
				returnObj.isRedirect = true; // Either locales or counties are not equal.
				//validates if both cookie and current sites ids are equal or not
				
				if (matchCookieID != matchSiteId) {
					returnObj.isRedirect = false;
					deleteCookie('lastActionLocale');
					returnObj.isNewSite = true;;
//					createCookie(lastActionLocale,currentSiteID);//creates the cookie since it's new site.
//			    	returnObj.siteID = currentSiteID;
//			    	returnObj.locale = request.locale;
					
				} else {
					
					
					var redirectLocale = locale;
					var redirectSiteID = cookieSiteID;
					
//					if (browserPreferedLanguages.hasOwnProperty(browserPreferedLocale) != undefined && browserPreferedLanguages[browserPreferedLocale] != null) {
//						redirectLocale = browserPreferedLanguages[browserPreferedLocale];
//						 if (redirectLocale.indexOf("CA") > -1 ) {
//							redirectSiteID  = redirectSiteID.split('_')[0];
//							redirectSiteID  = redirectSiteID+"_"+"CA";
//						}
//					}
					
					var siteAllowedLocals;
					for (var i = 0; i < countries.length; i++) {
						if (countries[i].site_id == Site.getCurrent().getID()) {
							siteAllowedLocals = countries[i].locales;
							break;
						}
					}
					redirectLocale = (siteAllowedLocals.indexOf(redirectLocale) >= 0) ? redirectLocale : siteAllowedLocals[0];
					
			    	returnObj.siteID = redirectSiteID;
			    	returnObj.locale = redirectLocale;
			    	createCookie(redirectLocale,redirectSiteID);
		
				}
		    	
				return returnObj;
				
			} catch (e) {
				//Ensure the request is not breaking
				returnObj.isRedirect = true;
				returnObj.siteID = Site.getCurrent().getID();
		    	returnObj.locale = request.locale;
		    	createCookie(request.locale,Site.getCurrent().getID());
				return returnObj;
				
			}
			 
		}
		
	} else {
		var requestCountryCode = request.geolocation.countryCode;
		var euroCoutriesList = new ArrayList(euroCountries);
		if (euroCoutriesList.contains(requestCountryCode)) {
			requestCountryCode = 'FR';
		}
		
		var defaultCountry;
		var siteAllowedLocals;
		for (var i = 0; i < countries.length; i++) {
			if (countries[i].site_id == Site.getCurrent().getID()) {
				defaultCountry =  countries[i].countryCode;
				siteAllowedLocals = countries[i].locales;
				break;
			}
		}
		if (Site.getCurrent().getDefaultLocale() != browserPreferedLocale && lastActionLocale != Site.getCurrent().getDefaultLocale() || defaultCountry != requestCountryCode) {
			var siteID = currentSiteID;
			
			for (var i = 0; i < countries.length; i++) {
				if (countries[i].countryCode == requestCountryCode) {
					siteID =  countries[i].site_id;
					siteAllowedLocals = countries[i].locales;
					break;
				}
			}
			
			var siteLocale = !empty(lastActionLocale && siteAllowedLocals.indexOf(lastActionLocale) >= 0) ? lastActionLocale : siteAllowedLocals[0];
			
			var isLocaleAllowed = false;
			for (var i =0; i < siteAllowedLocals.length; i++) {
				if (siteAllowedLocals[i] == browserPreferedLocale.replace('-','_')) {
					isLocaleAllowed = true;
				}
			}
			if (browserPreferedLanguages.hasOwnProperty(browserPreferedLocale) != undefined && browserPreferedLanguages[browserPreferedLocale] != null && isLocaleAllowed) {
				siteLocale = browserPreferedLanguages[browserPreferedLocale];
			}
			
			createCookie(siteLocale, siteID);
			returnObj.isRedirect = true;
			returnObj.locale = siteLocale;
			returnObj.siteID = siteID;
			
			/* Commenting this as per APPG-564
			if (request.geolocation.regionCode == userRedirection.stateCode) {
				createCookie("fr_CA", siteID);
				returnObj.isRedirect = true;
				returnObj.locale = "fr_CA";//TODO 
				returnObj.siteID = siteID;
			} else {
				createCookie("en_CA", siteID);
				returnObj.isRedirect = true;
				returnObj.locale = "en_CA";//TODO 
				returnObj.siteID = siteID;
			}*/
		} else {
			createCookie(lastActionLocale,currentSiteID);
			returnObj.isRedirect = false;
		}
		return returnObj;
		
	}
	
}

//creates the cookie
function createCookie (lastActionLocale, currentSiteID) {
	
	//var lastActionLocale = request.locale;
	//var currentSiteID = Site.getCurrent().getID().toString();
	//var countryCode = Locale.getLocale(lastActionLocale).getCountry();              //Get the country code from locale
	var lastActionSiteAndLocale = lastActionLocale + "|" + currentSiteID;
	
	var lastActionLocaleCookie = new dw.web.Cookie('lastActionLocale', lastActionSiteAndLocale);
	var url = request.httpURL.toString();
	// search for the hostname, and remove the left most part.
	// this will turn foo.bar.com into .bar.com and foo.bar.bash.com into .bar.bash.com.
	var hostname = url.replace(/^.*\/\/(.*?)\/.*$/, "$1"); // this will give us the hostname and domain
	var domain = hostname.replace(/^.*?(\..*)$/, "$1"); // this will remove the hostname and leave the domain
	
	lastActionLocaleCookie.setPath('/');
	lastActionLocaleCookie.setMaxAge(userRedirection.cookieMaxAge);
	lastActionLocaleCookie.setDomain(domain);
	
	response.addHttpCookie(lastActionLocaleCookie);
}

//redirect the user to respective site
function setRedirectURL(locale, currentSiteID) {
	
	var redirectUrl = Url.getRedirectURL(request, session, locale, currentSiteID);
	    
	 response.redirect(redirectUrl);
}

function deleteCookie(cookieName) {
	
	var cookie = new dw.web.Cookie(cookieName, null);
	var url = request.httpURL.toString();
	// search for the hostname, and remove the left most part.
	// this will turn foo.bar.com into .bar.com and foo.bar.bash.com into .bar.bash.com.
	var hostname = url.replace(/^.*\/\/(.*?)\/.*$/, "$1"); // this will give us the hostname and domain
	var domain = hostname.replace(/^.*?(\..*)$/, "$1"); // this will remove the hostname and leave the domain
	
	cookie.setPath('/');
	cookie.setMaxAge(0);
	cookie.setDomain(domain);
	
	response.addHttpCookie(cookie);
	
}
