'use strict';

/**
 * This controller updates the current session currency.
 *
 * @module controllers/Currency
 */

/* API Includes */
var Currency = require('dw/util/Currency');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var guard = require('~/cartridge/scripts/guard');
var Cart = require('~/cartridge/scripts/models/CartModel');

/**
 * This controller is used in an AJAX call to set the session variable 'currency'.
 */
function setSessionCurrency() {
   
	 var Response = require('~/cartridge/scripts/util/Response');
	/* Commenting since we are using different sites rather than locales 
	var currencyMnemonic = request.httpParameterMap.currencyMnemonic.value;
    var Response = require('~/cartridge/scripts/util/Response');
    var currency;
    
    if (currencyMnemonic) {
        currency = Currency.getCurrency(currencyMnemonic);
        if (currency) {
            session.setCurrency(currency);
            var test = request;
            Transaction.wrap(function () {
                var currentCart = Cart.get();
                if (currentCart) {
                	currentCart.updateCurrency();
                    currentCart.calculate();
                }
            });
        }
    }*/
	
	//creating cookie when user selects the local from Country selection to make this request shouldn't be redirected (OnRequest.js- line# starts from 80)
	var isCtrySel = new dw.web.Cookie('isCtrySel', true);
	var url = request.httpURL.toString();
	// search for the hostname, and remove the left most part.
	// this will turn foo.bar.com into .bar.com and foo.bar.bash.com into .bar.bash.com.
	var hostname = url.replace(/^.*\/\/(.*?)\/.*$/, "$1"); // this will give us the hostname and domain
	var domain = hostname.replace(/^.*?(\..*)$/, "$1"); // this will remove the hostname and leave the domain
	
	isCtrySel.setPath('/');
	isCtrySel.setMaxAge(4);
	isCtrySel.setDomain(domain);
	
	response.addHttpCookie(isCtrySel);

    Response.renderJSON({
        success: true
    });
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/Currency~setSessionCurrency */
exports.SetSessionCurrency = guard.ensure(['get'], setSessionCurrency);
