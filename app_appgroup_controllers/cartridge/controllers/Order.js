'use strict';

/**
 * Controller that manages the order history of a registered user.
 *
 * @module controllers/Order
 */

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PagingModel = require('dw/web/PagingModel');
var ArrayList = require('dw/util/ArrayList');
var Transaction =require('dw/system/Transaction');
var system = require('dw/system');
var svc = require('dw/svc');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var getRMAStatusMapping = {
		"rsUnknown" : "UNKNOWN",
		"rsUnprocessed" : "SUBMITTED",
		"rsApproved" : "APPROVED",
		"rsRefused" : "REFUSED"
		};


/**
 * Renders a page with the order history of the current logged in customer.
 *
 * Creates a PagingModel for the orders with information from the httpParameterMap.
 * Invalidates and clears the orders.orderlist form. Updates the page metadata. Sets the
 * ContinueURL property to Order-Orders and renders the order history page (account/orderhistory/orders template).
 */
function history() {

	//JIRA PREV-57 : In Order History 'FAILED' orders are displaying. Excluding FAILED and CREATED status orders.  
    var orders = OrderMgr.searchOrders('(customerNo={0} OR customerEmail={1}) AND status!={2} AND status!={3} AND status!={4}', 'creationDate desc',
            customer.profile.customerNo, customer.profile.email, dw.order.Order.ORDER_STATUS_REPLACED, dw.order.Order.ORDER_STATUS_FAILED, dw.order.Order.ORDER_STATUS_CREATED);

    var count = orders.count;
    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || 5;
    var start = parameterMap.start.intValue || 0;
    var orderPagingModel = new PagingModel(orders, count);//PREVAIL - getting count before and using it here for the sake of OMS integration.
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(ContentMgr.getContent('myaccount-orderhistory'));

    app.getView({
        OrderPagingModel: orderPagingModel,
        ContinueURL: dw.web.URLUtils.https('Order-Orders')
    }).render('account/orderhistory/orders');
}


/**
 * Gets an OrderView and renders the order detail page (account/orderhistory/orderdetails template). If there is an error,
 * redirects to the {@link module:controllers/Order~history|history} function.
 */
function orders() {
    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.handleAction({
        show: function (formGroup, action) {
            var Order = action.object;
            
            var siteIDObj					= ('siteName' in dw.system.Site.getCurrent().preferences.getCustom() && !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('siteName'))) ? dw.system.Site.getCurrent().getCustomPreferenceValue('siteName') : dw.system.Site.getCurrent().ID;
    		var siteID						= siteIDObj.toString();
    		var currentCurrencyCode			= system.Site.current.getDefaultCurrency();
			var serviceName					= "chaindrive.soap.GetRMA."+siteID+"."+currentCurrencyCode;
		    var service 					= svc.ServiceRegistry.get(serviceName);
		    var shipment = Order.shipments.toArray()[0]; // Since, we have only single shipping
			var prodLineItemIterator = shipment.productLineItems.iterator();
			while (prodLineItemIterator.hasNext()) {
		        var list = prodLineItemIterator.next();
		        if(list.custom.RMANumber != undefined && list.custom.RMANumber != null && list.custom.RMANumber != ''){
		        	if(list.custom.getRMAStatus != undefined){
		        		var getRMAStatusObj = list.custom.getRMAStatus;
		        		if(getRMAStatusObj.status != 'APPROVED'
		        			&& getRMAStatusObj.status != 'REFUSED'){
		        			var request = {RMANumber : list.custom.RMANumber};
				        	var getRMAUpdatesresponse = service.call(request);
				        	if(getRMAUpdatesresponse.status == 'OK'){
				        		var RMAStatus = getRMAUpdatesresponse.object.RMAUpdates[0].RMAStatus.value;
				        		var reason = getRMAUpdatesresponse.object.RMAUpdates[0].refusalReason;
				        		var getRMAContent = {"status":getRMAStatusMapping[RMAStatus],"reason":reason};
					        	Transaction.wrap(function () {
					        		if(list.custom.getRMAStatus != undefined && list.custom.getRMAStatus.status != undefined){
				    		        	list.custom.getRMAStatus = JSON.stringify(getRMAContent);
				    		        }
					    		});
				        	}
		        		}
		        	}
		        }
		    }

            app.getView({Order: Order}).render('account/orderhistory/orderdetails');
        },
        error: function () {
            response.redirect(dw.web.URLUtils.https('Order-History'));
        }
    });

}


/**
 * Renders a page with details of a single order. This function
 * renders the order details by the UUID of the order, therefore it can also be used
 * for unregistered customers to track the status of their orders. It
 * renders the order details page (account/orderhistory/orderdetails template), even
 * if the order cannot be found.
 */
function track () {
    var parameterMap = request.httpParameterMap;

    if (empty(parameterMap.orderID.stringValue)) {
        app.getView().render('account/orderhistory/orderdetails');
        return response;
    }

    var uuid = parameterMap.orderID.stringValue;
    var orders = OrderMgr.searchOrders('UUID={0} AND status!={1}', 'creationDate desc', uuid, dw.order.Order.ORDER_STATUS_REPLACED);

    if (empty(orders)) {
        app.getView().render('account/orderhistory/orderdetails');
    }

    var Order = orders.next();
    app.getView({Order: Order}).render('account/orderhistory/orderdetails');
}

function orderReturn() {
	var order = dw.order.OrderMgr.getOrder(request.httpParameterMap.orderNum.stringValue);
	var returnReasonList;
	var returnReason = dw.object.CustomObjectMgr.getCustomObject("CustomDropDowns", "returnReason");
	if (!empty(returnReason) && !empty(returnReason.getCustom()["DropDownContent"]) && returnReason.getCustom()["DropDownContent"] != null) {
		returnReasonList = new ArrayList(JSON.parse(returnReason.getCustom()["DropDownContent"]));
	}
	var firstReturnReason = returnReasonList[0].displayName;
	
	var returnproducts = app.getForm('returnproducts');
	returnproducts.clear();
    
    app.getForm('returnproducts.requestreason').setOptions(returnReasonList.iterator());
    
	app.getView({
		Order: order,
		FirstReturnReason: firstReturnReason,
		ContinueURL: dw.web.URLUtils.https('Order-OrderReturnInitiate')
	}).render('account/orderhistory/orderreturndetails');
}

function orderReturnInitiate() {
	var httpParameterMap = request.httpParameterMap;
	var OrdreturnDetail = httpParameterMap.requestBodyAsString;
	var CDOrderId = httpParameterMap.CDOrderId.value;
	var lastSixDigitOfCDOrderId = CDOrderId.toString().substring(CDOrderId.length-6);
	var date = new Date();
	
	
	var currentForm = session.forms;
	var ordModel = app.getModel('Order');
	var RMANumber = lastSixDigitOfCDOrderId+date.getFullYear().toString().substring(2)+date.getMonth()+date.getDate()+date.getHours()+date.getMinutes()+date.getSeconds();
	var CDResponse = ordModel.returnOrder(OrdreturnDetail, CDOrderId, RMANumber);
	var returnResponse = CDResponse.CDReturnResponse;
	
	if(returnResponse.RMASubmitted){
		var orderObj = dw.order.OrderMgr.getOrder(CDOrderId);
		var shipment = orderObj.shipments.toArray()[0]; // Since, we have only single shipping
		var prodLineItemIterator = shipment.productLineItems.iterator();
		var RMADetailObj = JSON.parse(CDResponse.orderDetails).RMADetail;
		var returnedProdIds = [];
		for(var i=0 ; i < RMADetailObj.length ; i++){
			returnedProdIds[i] = RMADetailObj[i].SkuId;
		}
		Transaction.wrap(function () {
			while (prodLineItemIterator.hasNext()) {
		        var list = prodLineItemIterator.next();
		        if(returnedProdIds.indexOf(list.productID) != -1){
		        	list.custom.RMANumber = RMANumber;
		        }
		    }
		});
	}
	
	var ordReturnList = JSON.parse(OrdreturnDetail).RMADetail;
	// [{"CDOrderId":"00001602","SkuId":"10003495","ReturnReason":"COLOR","WebDeliveryNumber":"0","CDDeliveryNumber":"521546","Qty":1},
	// {"CDOrderId":"00001602","SkuId":"10004680","ReturnReason":"DEFECTIVE","Qty":1}]
	if(CDResponse.EndNodeName == 'OK'){
		app.getView({
			OrdReturnList: ordReturnList,
			CDError: false
		}).render('account/orderhistory/orderreturnthankyou');
	}else{
		app.getView({
			OrdReturnList: ordReturnList,
			CDError: true
		}).render('account/orderhistory/orderreturnthankyou');
	}
	
}
/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Renders a page with the order history of the current logged in customer.
 * @see module:controllers/Order~history */
exports.History = guard.ensure(['get', 'https', 'loggedIn'], history);
/** Renders the order detail page.
 * @see module:controllers/Order~orders */
exports.Orders = guard.ensure(['post', 'https', 'loggedIn'], orders);
/** Renders a page with details of a single order.
 * @see module:controllers/Order~track */
exports.Track = guard.ensure(['get', 'https'], track);

exports.OrderReturn = guard.ensure(['get', 'https'], orderReturn);

exports.OrderReturnInitiate = guard.ensure(['post', 'https'], orderReturnInitiate);
