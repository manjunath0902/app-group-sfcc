/**
* Description of the Controller and the logic it provides
*
* @module  controllers/BISNSave
*/

'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

var params = request.httpParameterMap;
function popup() {
    let Product = app.getModel('Product');
    let prod = Product.get(params.pid.stringValue);
    var selectedColor = params.selectedcolor.stringValue;
    var variationModel = prod.getVariationModel();
	var variationAttributes = variationModel.getProductVariationAttributes();
	var attributeSize;
	var attributeColor;
	for each(var attr in variationAttributes){
	 if(attr.attributeID == 'size'){
		 attributeSize = attr;
	 } else if (attr.attributeID == 'color') {
		 attributeColor = attr;
	 }	
	}
	    
    app.getView({
    	product : prod,
    	variationModel : variationModel,
    	attributeSize : attributeSize,
    	attributeColor: attributeColor,
    	selectedColor : selectedColor
    }).render('product/bisn/bisn_popup');    
}

/* Exports of the controller */
exports.PopUp = guard.ensure(['post'], popup);
