'use strict';
/**
 * Controller that stores customer email preferences in custom object for email subscription.
 *
 * @module controllers/EmailSignUp
 */

/* API includes */
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var ArrayList = require('dw/util/ArrayList');
var Utils = require('app_appgroup_core/cartridge/scripts/account/Utils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

function footer() {	
	app.getForm('emailsubscribe').clear();
	app.getForm('emailsubscribe').object.email.value = request.httpParameterMap["subscription-email"].value;
	
	var yearList = new ArrayList(Utils.yearObj("DOBYear"));
	yearList.reverse();
	
	app.getForm('emailsubscribe').object.birthdate.year.setOptions(yearList.iterator());
	
    app.getView({
        ContinueURL: URLUtils.https('EmailSignUp-FooterSignUp')
    }).render('subscription/footeremailsubscription');
}

function footerSignUp() {
    app.getForm('emailsubscribe').handleAction({
        error: function() {
            app.getView({
                ContinueURL: URLUtils.https('EmailSignUp-FooterSignUp')
            }).render('subscription/footeremailsubscription');
        },

        confirm: function() {

            var footerSignUp = app.getForm('emailsubscribe').object;
            var args = {}, result;
            args.Email = footerSignUp.email.value;
            footerSignUp.language.value = request.locale.toString();
            Transaction.wrap(function() {
                result = dw.system.Pipeline.execute('BrontoOptIn-EmailSubscription', {
                	EmailSignUpForms: session.forms,
            		Email: args.Email,
            		Source: "Footer",
            		Subscribed: true
                });
            });

            if (result === 0) {
                app.getView({
                    ContinueURL: URLUtils.https('EmailSignUp-FooterSignUp')
                }).render('subscription/footeremailsubscription');
                return;
            }

            //Clear form.
            app.getForm('emailsubscribe').clear();

            app.getView().render('subscription/emailsingupsuccess');
        }
    });
}

function billing(options) {
    Transaction.wrap(function() {
        var billingSignUp = require('~/cartridge/scripts/util/EmailSignup.ds');
        billingSignUp.createObject(options);
    });
    return true;
}


function myAccount(options) {
    Transaction.wrap(function() {
        var accountSignUp = require('~/cartridge/scripts/util/EmailSignup.ds');
        accountSignUp.createObject(options);
    });
    return true;
}


function lightBoxSubmission() {
	var list = session.clickStream.clicks;
    var click = list[list.size()-1].referer;
	var footerSignUp = app.getForm('emailsubscribe').object;
    footerSignUp.email.value = request.httpParameterMap["subscription-email"].value;
    footerSignUp.language.value = request.locale.toString();
    Transaction.wrap(function() {
        dw.system.Pipeline.execute('BrontoOptIn-EmailSubscription', {
        	EmailSignUpForms: session.forms,
    		Email: footerSignUp.email.value,
    		Source: "LightBox PopUp",
    		Subscribed: true
        });
    });
    
    response.redirect(click);
}


/*Module Exports*/

/*
 * Local methods
 */
exports.billing = billing;
exports.myAccount = myAccount;
exports.LightBoxSubmission = guard.ensure(['https'], lightBoxSubmission);

/* Web exposed methods */
/** Renders the Footer SignUp Page.
 * @see {@link module:controllers/EmailSignUp~Footer} */
exports.Footer = guard.ensure(['https'], footer);

/** Renders the Footer SignUp Page.
 * @see {@link module:controllers/EmailSignUp~FooterSignUp} */
exports.FooterSignUp = guard.ensure(['post', 'https', 'csrf'], footerSignUp);