'use strict';

/**
 * Controller that renders the store finder and store detail pages.
 *
 * @module controllers/Stores
 */

/* API Includes */
var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var ArrayList = require('dw/util/ArrayList');
var SortedMap = require('dw/util/SortedMap');
var PropertyComparator = require('dw/util/PropertyComparator');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

var isSearched;
/**
 * Provides a form to locate stores by geographical information.
 *
 * Clears the storelocator form. Gets a ContentModel that wraps the store-locator content asset.
 * Updates the page metadata and renders the store locator page (storelocator/storelocator template).
 */
function find() {
		
	var storeLocatorForm = app.getForm('storelocator');
    storeLocatorForm.clear();
    
    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);
    if(request.httpParameterMap.load.value){
    	var storeList = new dw.util.ArrayList();
        var stores = SystemObjectMgr.querySystemObjects('Store', '', 'countryCode desc');

        if (stores.getCount() > 0) {
            while (stores.hasNext()) {
            	var store = stores.next();
            	if(store.custom.storeType.value == "Mackage" || store.custom.storeType.value == "SoiaKyo"){
            		storeList.push(store);    
            	}
            }
            stores.close();
        }
        app.getView({
            StoresCount: storeList.length,
            Stores: storeList,
            ViewAll: true,
            SearchString: '',
            ContinueURL: URLUtils.https('Stores-FindStores')
        }).render('storelocator/storelocatorresultswithmap');
    	
    }else{
    	app.getView({
            StoresCount: 0,
            Stores: null,
            SearchString: '',
            ContinueURL: URLUtils.https('Stores-FindStores')
        }).render('storelocator/storelocatorresultswithmap');
    }
  }

/**
 * The storelocator form handler. This form is submitted with GET.
 * Handles the following actions:
 * - findbycountry
 * - findbystate
 * - findbyzip
 * In all cases, gets the search criteria from the form (formgroup) passed in by
 * the handleAction method and queries the platform for stores matching that criteria. Returns null if no stores are found,
 * otherwise returns a JSON object store, search key, and search criteria information. If there are search results, renders
 * the store results page (storelocator/storelocatorresults template), otherwise renders the store locator page
 * (storelocator/storelocator template).
 */
function findStores() {
    isSearched = true;
    var Content = app.getModel('Content');
    var storeLocatorAsset = Content.get('store-locator');
    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(storeLocatorAsset);

    var storeLocatorForm = app.getForm('storelocator');
    
    var searchResult = storeLocatorForm.handleAction({
        findbycountry: function (formgroup) {
            var searchKey = formgroup.country.htmlValue;
            var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'ID asc', searchKey);
            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbycountry'};
            }
        },
        findbystate: function (formgroup) {
            var searchKey = formgroup.state.htmlValue;
            var stores = null;

            if (!empty(searchKey)) {
                stores = SystemObjectMgr.querySystemObjects('Store', 'stateCode = {0}', 'ID asc', searchKey);
            }

            if (empty(stores)) {
                return null;
            } else {
                return {'stores': stores, 'searchKey': searchKey, 'type': 'findbystate'};
            }
        },
        findbyzip: function (formgroup) {
            var searchKey = formgroup.postalCode.value;
            var storesMgrResult = StoreMgr.searchStoresByPostalCode(formgroup.countryCode.value, searchKey, formgroup.distanceUnit.value, formgroup.maxdistance.value);
            var stores = storesMgrResult.keySet();
            
            // Assigning all stores to ArrayList inorder to sort it in as per their ID in accending order
            var SortedStores : ArrayList = new ArrayList(stores);
            SortedStores.sort( new PropertyComparator("ID",true ) ); 
            if (empty(SortedStores)) {
                return null;
            } else {
                return {'stores': SortedStores, 'searchKey': searchKey, 'type': 'findbyzip'};
            }
        },
        findbyrawstring: function(formgroup) {
            var searchKey = formgroup.rawString.value;
            var StoreUtils = require('int_storelocator/cartridge/scripts/library/libStoreUtils.js');
            var stores;
            var result = StoreUtils.getFullAddressBasedOnRawString(searchKey);
            if (!empty(result) && result.status == 'OK' && !empty(result.results)) {
                var geoLocation = result.results[0].geometry.location;
                var lat = geoLocation.lat;
                var lang = geoLocation.lng;
                var resultType = result.results[0].types[0];
                if (resultType == 'country') {
                    var addressShortName = result.results[0].address_components[0].short_name;
                    var stores = StoreUtils.getNearestStores(lat, lang, resultType, addressShortName);
                } else {
	                var storesHashMap = StoreUtils.getNearestStores(lat, lang);
	                stores = storesHashMap.keySet();
                }
            }else{
            	var storeList = new dw.util.ArrayList();
                var storesObj = SystemObjectMgr.querySystemObjects('Store', '', 'countryCode desc');
                if (storesObj.getCount() > 0) {
                    while (storesObj.hasNext()) {
                    	var store = storesObj.next();
                    	if(store.custom.storeType.value == "Mackage" || store.custom.storeType.value == "SoiaKyo"){
                    		storeList.push(store);    
                    	}
                    }
                    storesObj.close();
                }
                stores = storeList;
            }
            if (empty(stores)) {
                return  null;
            } else {
                return {
                    'stores': stores,
                    'searchKey': searchKey,
                    'type': 'findbyrawstring',
                    'StoresHashMap': storesHashMap
                };
            }
        },
        usemylocation: function() {
            var StoreUtils = require('int_storelocator/cartridge/scripts/library/libStoreUtils.js');
            var lat = request.getGeolocation().getLatitude();
            var lang = request.getGeolocation().getLongitude();
            session.forms.storelocator.rawString.value = request.getGeolocation().city;
           
            var storesHashMap = StoreUtils.getNearestStores(lat, lang);
            var stores = storesHashMap.keySet();
            if (empty(stores)) {
                return null;
            } else {
                return {
                    'stores': stores,
                    'searchKey': 'My Location',
                    'type': 'findbyrawstring',
                    'StoresHashMap': storesHashMap,
                    'searchAction': 'uselocation'
                };
            }
        }
    });
    if (searchResult) {
        /*****************************GOOGLEMAPS - Store Locator Integration*****************************************/
        if (dw.system.Site.getCurrent().getCustomPreferenceValue('storeLocatorService').toString() === 'GOOGLEMAPS') {
            app.getView({
                StoresCount: (searchResult.stores instanceof dw.util.SeekableIterator) ? searchResult.stores.count:searchResult.stores.size(),
                Stores: new dw.util.ArrayList(searchResult.stores),
                SearchString: searchResult.searchKey,
                SearchType: searchResult.type,
                SearchAction : searchResult.searchAction
            }).render('storelocator/storelocatorresultswithmap');
        } else {
            app.getView('StoreLocator', searchResult).render('storelocator/storelocatorresults');
        }
    } else {
    	app.getView({
            StoresCount: 0,
            Stores: null,
            SearchString: '',
            NoStoresFound: true,
            ContinueURL: URLUtils.https('Stores-FindStores')
        }).render('storelocator/storelocatorresultswithmap');
    }

}

/**
 * Renders the details of a store.
 *
 * Gets the store ID from the httpParameterMap. Updates the page metadata.
 * Renders the store details page (storelocator/storedetails template).
 */
function details() {

    var storeID = request.httpParameterMap.StoreID.value;
    var store = dw.catalog.StoreMgr.getStore(storeID);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(store);

    app.getView({Store: store})
        .render('storelocator/storedetails');

}

/*
 * Exposed web methods
 */
/** Renders form to locate stores by geographical information.
 * @see module:controllers/Stores~find */
exports.Find = guard.ensure(['get'], find);
/** The storelocator form handler.
 * @see module:controllers/Stores~findStores */
exports.FindStores = guard.ensure(['post'], findStores);
/** Renders the details of a store.
 * @see module:controllers/Stores~details */
exports.Details = guard.ensure(['get'], details);
