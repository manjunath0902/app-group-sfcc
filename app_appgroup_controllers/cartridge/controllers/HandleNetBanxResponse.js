'use strict';

/**
 * Controller that is called whenever a technical error occurs while processing a
 * request. A standard error page is rendered.
 *
 * @module controllers/Error
 */

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');


/**
 * HandleSuccessResponse.
 */

function handleResponse(args) {
	let Pipeline = require('dw/system/Pipeline');
    var pdict = Pipeline.execute('Netbanx-HandleResponse');
    var orderSubmit = require("app_appgroup_controllers/cartridge/controllers/COPlaceOrder.js");
    
    if(pdict.EndNodeName == "OK") {
    	orderSubmit.BackFromNetBanx(pdict.Order);
    } else {
    	orderSubmit.Fail(pdict);
    }
    
}

/**
 * HandleErrorResponse.
 */
function handleErrorResponse() {
	let Pipeline = require('dw/system/Pipeline');
    var pdict = Pipeline.execute('Netbanx-HandleErrorResponse');
    
    if(pdict.EndNodeName == "END") {
    	app.getController('COSummary').Start();
    }
}

/**
 * ReturnToMerchant.
 */
function returnToMerchant() {
	let Pipeline = require('dw/system/Pipeline');
    var pdict = Pipeline.execute('Netbanx-ReturnToMerchant');
    
    if(pdict.EndNodeName == "END") {
    	app.getController('COSummary').Start();
    }
}


/*
 * Web exposed methods
 */
/** @see module:controllers/HandleNetBanxResponse~HandleResponse */
exports.HandleResponse = guard.ensure(['https'], handleResponse);
/** @see module:controllers/HandleNetBanxResponse~HandleErrorResponse */
exports.HandleErrorResponse = guard.ensure(['https'], handleErrorResponse);
/** @see module:controllers/HandleNetBanxResponse~ReturnToMerchant */
exports.ReturnToMerchant = guard.ensure(['https'], returnToMerchant);
