'use strict';

/**
 * This controller handles customer service related pages, such as the contact us form.
 *
 * @module controllers/CustomerService
 */

/* API Includes */
var Status = require('dw/system/Status');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var Locale = require('dw/util/Locale');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var ArrayList = require('dw/util/ArrayList');

/**
 * Renders the customer service overview page.
 */
function show() {
	
	var Content = app.getModel('Content');
	var customerService = Content.get('cs-landing');
    var pageMeta = require('~/cartridge/scripts/meta');
    
    pageMeta.update(customerService);
	
    app.getView('CustomerService').render('content/customerservice');
}

/**
 * Renders the left hand navigation.
 */
function leftNav() {
    app.getView('CustomerService').render('content/customerserviceleftnav');
}

/**
 * Provides a contact us form which sends an email to the configured customer service email address.
 */
function contactUs() {
	var stateList;
	var states : CustomObject = dw.object.CustomObjectMgr.getCustomObject("CustomDropDowns", "states");
	var Content = app.getModel('Content');
	var contactUs = Content.get('repairs');
    var pageMeta = require('~/cartridge/scripts/meta');
    var dropdownQuery = 'contactUsQueriesMapping' in dw.system.Site.current.preferences.custom ? dw.system.Site.getCurrent().getCustomPreferenceValue('contactUsQueriesMapping'):'';
    
    var countriesList =	new ArrayList();
    var stateList =	new ArrayList();
    var states : CustomObject = dw.object.CustomObjectMgr.getCustomObject("CustomDropDowns", "RepairsPageStates");
    var countries : CustomObject = dw.object.CustomObjectMgr.getCustomObject("CustomDropDowns", "RepairsPageCountries");
    if (!empty(countries) && !empty(countries.getCustom()["DropDownContent"])) {
        countriesList = new ArrayList(JSON.parse(countries.getCustom()["DropDownContent"]));
    }
    if (!empty(states) && !empty(states.getCustom()["DropDownContent"])) {
		stateList = new ArrayList(JSON.parse(states.getCustom()["DropDownContent"]));
	}

    if(dropdownQuery){
	    var queryOptions = JSON.parse(dropdownQuery); 
	    var querMap = queryOptions["ContactUsQueriesMapping"];
	    var currLocale = request.locale.toString();
	    var statusMapping = querMap[currLocale];
	    
	    pageMeta.update(contactUs);
		
	    app.getForm('contactus').clear();
	    var query = new ArrayList();
	    for(var i=0;i<statusMapping.length;i++){
	    	var option = {};
	    	option = statusMapping[i];
			query.add(option);
		}
	    app.getForm('contactus.repairtype').setOptions(query.iterator());
	    app.getForm('contactus.country').setOptions(countriesList.iterator());
	    app.getForm('contactus.state').setOptions(stateList.iterator());
    }
    
    if (!empty(states) && !empty(states.getCustom()["DropDownContent"]) && states.getCustom()["DropDownContent"] != null) 
    {
    	    	stateList = new ArrayList(JSON.parse(states.getCustom()["DropDownContent"]));
    }
    	   
    app.getForm('contactus').clear();
    app.getForm('contactus.state').setOptions(stateList.iterator());
    	    
    app.getView('CustomerService', {
        isContactUs: 'true'
    }).render('content/contactus');
    
    //app.getView('CustomerService').render('content/contactus');
}

/**
 * The form handler for the contactus form.
 */
function submit() {
    var contactUsForm = app.getForm('contactus');
    var sitePrefs : SitePreferences = dw.system.Site.getCurrent().getPreferences();
    var objectsArray : Array = [];

    var contactUsResult = contactUsForm.handleAction({
        send: function (formgroup) {
            // Change the MailTo in order to send to the store's customer service email address. It defaults to the
            // user's email.
        	if(!empty(sitePrefs.getCustom()["emailMarketingService"]) && (sitePrefs.getCustom()["emailMarketingService"] == 'Bronto')
        			&& !empty(sitePrefs.getCustom()["brontoControl"]) && (JSON.parse(sitePrefs.getCustom()["brontoControl"]).ContactUsEmail)){
        		var status = sendContactUsMessage(contactUsForm,formgroup.email.value);

            }else{
	            var Email = app.getModel('Email');
	            
	            var params : HttpParameterMap = request.httpParameterMap;

	            var filesFromDirectory : ArrayList = new ArrayList();
	            var fileNames = new ArrayList();
	            
	            // Process multi-part if applicable 
	            var files : LinkedHashMap = params.processMultipart( (function( field, ct, oname ){
	            	var imageFile : dw.io.File = new dw.io.File( dw.io.File.LIBRARIES + "/" + dw.content.ContentMgr.siteLibrary.ID + "/default/images/repair_" + session.sessionID + "_" + oname );
	            	filesFromDirectory.add(imageFile);
	            	fileNames.add(oname);
	                return imageFile;
	            }) );
	            
	            /*Commented for APPG-827 
	             * return Email.get('mail/contactus', dw.system.Site.getCurrent().getCustomPreferenceValue('customerServiceEmail'), filesFromDirectory)
	                .setFrom(formgroup.email.value)
	                .setSubject(formgroup.repairtype.value)
	                .send({});
	            */
	            
	            var zendeskServiceCalls = require('int_zendesk/cartridge/scripts/ZendeskServiceCalls');
	            var requestData = {};
	            var recipientEmail = 'zendeskRecipientEmail' in Site.current.preferences.custom ? Site.getCurrent().getCustomPreferenceValue('zendeskRecipientEmail'):'';
	            requestData.recipient = JSON.parse(recipientEmail)[Locale.getLocale(request.locale).ID];
	            requestData.subject = !empty(formgroup.repairtype.value) ? formgroup.repairtype.value : '';
	            requestData.comment = !empty(formgroup.comment.value) ? formgroup.comment.value : '';
	            requestData.label_code = !empty(formgroup.numberField.value) ? formgroup.numberField.value : '';
	            requestData.contact_firstname = !empty(formgroup.firstname.value) ? formgroup.firstname.value : '';
	            requestData.contact_lastname = !empty(formgroup.lastname.value) ? formgroup.lastname.value : '';
	            requestData.contact_email = !empty(formgroup.email.value) ? formgroup.email.value : '';
	            requestData.contact_phonenumber = !empty(formgroup.phone.value) ? formgroup.phone.value : '';
	            requestData.shipping_street = !empty(formgroup.address.value) ? formgroup.address.value : '';
	            requestData.shipping_city = !empty(formgroup.city.value) ? formgroup.city.value : '';
	            requestData.shipping_state = !empty(formgroup.state.value) ? formgroup.state.value : '';
	            requestData.shipping_country = !empty(formgroup.country.value) ? formgroup.country.value : '';
	            requestData.shipping_zipcode = !empty(formgroup.postal.value) ? formgroup.postal.value : '';
	            requestData.shipping_purchaselocation = !empty(formgroup.location.value) ? formgroup.location.value : '';
	            
	            var token = '';
	            var array = new Array();
	            try{
		           	for(var i=0; i < filesFromDirectory.length; i++){
		            	var reqData = {};
		            	if(!empty(token)){
		            		reqData.token = token;
		            	}
		            	reqData.fileName = fileNames.get(i);
		            	var imgFile = filesFromDirectory.get(i);
		            	var contentURL = URLUtils.absStatic(URLUtils.CONTEXT_LIBRARY, dw.content.ContentMgr.siteLibrary.ID, "/images/" + imgFile.name).toString()
		            	reqData.contentUrl = contentURL;
	            		if(contentURL.lastIndexOf('.') >= 0){
	            			reqData.contentType = contentURL.substring(contentURL.lastIndexOf('.')+1);
	            		}else{
	            			reqData.contentType = "image/jpg";
	            		}
		            	reqData.size = imgFile.length();
		            	
		            	//API call for adding attachments to the created ticket.
		            	var addAttachmentsResponse = zendeskServiceCalls.addAttachments(reqData);
		            	if(addAttachmentsResponse.success){
		            		var attachParsedResponse = JSON.parse(addAttachmentsResponse.response);
			            	token = attachParsedResponse.upload.token;
		            	}else{
		            		return {'errorInZendeskAPICall' : true};
		            	}
		            }
	            }catch(e){
	            	return {'errorInZendeskAPICall' : true};
	            }
	            array.push(token);
	            requestData.uploads = array;
	            //API call for creating a ticket with Zendesk
	            var createTicketResponse = zendeskServiceCalls.createTicket(requestData);
	            if(createTicketResponse.success){
	            	 return {'errorInZendeskAPICall' : false};
	            }else{
	            	return {'errorInZendeskAPICall' : true};
	            }
            }
        },
        error: function () {
            // No special error handling if the form is invalid.
            return null;
        }
    });
    
    if(contactUsResult && contactUsResult.errorInZendeskAPICall){
        app.getView('CustomerService', {
            ConfirmationMessage: 'errorInZendeskAPICall',
            isContactUs: 'true'
        }).render('content/contactus');
    } else {
        app.getView('CustomerService', {
            ConfirmationMessage: 'edit',
            isContactUs: 'true'
        }).render('content/contactus');
    }
}

/*
 * Bronto integration
 */
var sendContactUsMessage = function(contactUsForm,RecipientEmail) {
	var contactUsMessage = require('int_bronto/cartridge/scripts/messages/ContactUsMessage');
	var status = contactUsMessage.execute({
		contactUsForm: contactUsForm,
		RecipientEmail: RecipientEmail
    });
	return status;
}


/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** @see module:controllers/CustomerService~show */
exports.Show = guard.ensure(['get'], show);
/** @see module:controllers/CustomerService~leftNav */
exports.LeftNav = guard.ensure(['get'], leftNav);
/** @see module:controllers/CustomerService~contactUs */
exports.ContactUs = guard.ensure(['get', 'https'], contactUs);
/** @see module:controllers/CustomerService~submit */
exports.Submit = guard.ensure(['post', 'https'], submit);
