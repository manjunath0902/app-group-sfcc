<iscontent type="text/html" charset="UTF-8" compact="true"/>

<iscomment>

  Each page of the site including pipelines should have a canonical tag.
  The canonical URL should match the storefront URL (or pageURL override).

  Home page:

    The home page canonical should represent the domain (www.domain.com) vs a pipeline.
    If a multi-country/language site, the home page canonical should represent the storefront URL
    for the home page without any parameters www.domain.com/country or www.domain.com/country/language

  Catalog pages:

    The catalog page canonical tags should not change between http and https. If the site is https,
    then all category page canonicals must be https. If the site�s front end is not https,
    then the canonical tags for the front end pages should be http.
    Secure pages The secure page canonical tags should not change between http and https.
    All secure page canonicals should be https.

  Pipelines /Controllers:

    Customer facing pipelines (represented in navigation) should include canonical tags.
    If the pipeline has an alias setting in URL rules, the canonical should match that URL, not use the pipeline.
</iscomment>

<iscomment>Calculate protocol</iscomment>
<isif condition="${ pageContext.type == 'checkout' || 
					pageContext.type == 'Cart' || 
					pageContext.type == 'MyAccount' || 
					pageContext.type == 'orderconfirmation' ||
					pageContext.type == 'Wishlist' ||
					pageContext.type == 'OrderHistory' ||
					pageContext.type == 'GiftRegistry' || 
					(!empty(pdict.CurrentSession.clickStream.last) && pdict.CurrentSession.clickStream.last != null) ? pdict.CurrentSession.clickStream.last.pipelineName == 'CustomerService-ContactUs' : false
					}">
   <isset name="protocol" value="https" scope="page" />
<iselse/>
  <isset name="protocol" value="https" scope="page" />
</isif>

<iscomment>Calculate canonical URL</iscomment>
<isif condition="${pageContext.type == 'product'}">
   <isset name="cpid" value="${pdict.Product.variant ? pdict.Product.variationModel.master.ID : pdict.Product.ID}" scope="page" />
   <isset name="canonicalURL" value="${URLUtils.https('Product-Show','pid', cpid)}" scope="page" />
<iselseif condition="${!empty(pdict.CurrentSession.clickStream.last) && pdict.CurrentRequest.httpHeaders['x-is-path_info'].indexOf(pdict.CurrentSession.clickStream.last.pipelineName) != -1}"/>
   <isset name="canonicalURL" value="${protocol+'://'+pdict.CurrentRequest.httpHost+'/on/demandware.store'+pdict.CurrentRequest.httpHeaders['x-is-path_info']}" scope="page" />
<iselse/>
   <isset name="canonicalURL" value="${protocol+'://'+pdict.CurrentRequest.httpHost+pdict.CurrentRequest.httpHeaders['x-is-path_info']}" scope="page" />
</isif>

<iscomment>Canonical URL. Search results does not need a canonical URL.</iscomment>
<isif condition="${!(pdict.CurrentHttpParameterMap.q.submitted && pdict.CurrentHttpParameterMap.q.value != '')}">
	<link rel="canonical" href="${canonicalURL}" />
</isif>